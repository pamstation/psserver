

https://martin-grigorov.medium.com/building-linux-packages-for-different-cpu-architectures-with-docker-and-qemu-d29e4ebc9fa5
https://developer.toradex.com/torizon/application-development/working-with-containers/configure-build-environment-for-torizon-containers#linux

```shell
docker run --rm -it --privileged torizon/binfmt
docker run --rm -it --pull always --platform linux/arm64/v8 debian bash
```


# dockcross

https://github.com/dockcross/dockcross

```shell
docker run --rm linux-arm64-full > ./dockcross
chmod +x ./dockcross
mv ./dockcross ~/bin/


git clone https://github.com/dockcross/dockcross.git
cd dockcross
docker run --rm dockcross/linux-arm64 > ./dockcross-linux-arm64

```

# ARM8
 

```shell
docker pull dockcross/linux-arm64-full:20240304-9e57d2b
docker build -t apalis-imx8 .

docker run -it --rm --name imx8 apalis-imx8 /bin/bash
    
docker run --rm -it --privileged torizon/binfmt
docker run -it --rm --name imx8  \
    -v $(pwd):/work/ \
    -e ARCH="aarch64-unknown-linux" \
    apalis-imx8 /bin/bash 
    
```

# test camera 

```shell
vi pamstation_mode.json
{
  "isStub": true,
  "isStubBoard1": false,
  "isStubBoard2": false,
  "isStubBoard3": false,
  "kind": "Mode"
}
# see build.sh
# Building camera test
scp _build/aarch64-unknown-linux/psserver root@10.110.253.57:/var/lib/psserver/psserver

scp _build/aarch64-unknown-linux/pimcore/camera root@10.110.253.57:/var/lib/psserver/camera
scp _build/aarch64-unknown-linux/pimcore/pimcoretest root@10.110.253.57:/var/lib/psserver/pimcoretest

ssh root@10.110.253.57
cd /var/lib/psserver
chmod +x ./camera
chmod +x ./pimcoretest
./camera
./pimcoretest


```




## Deploy

Inside previous docker session

```bash
ssh root@10.110.253.49 mkdir -p /var/lib/psserver
scp ./psserver root@10.110.253.49:/var/lib/psserver/psserver
scp -r /home/root/psserver/web root@10.110.253.49:/var/lib/psserver/web 
  
```

# Old doc

## libtiff

```bash
cd pimcore/drivers/tiff
_build
rm -r build
mkdir build
cd build

rm -r _build
mkdir _build
cd _build
# see $CONFIGURE_FLAGS
# cat /usr/local/oecore-x86_64/environment-setup-armv7at2hf-neon-angstrom-linux-gnueabi
../configure $CONFIGURE_FLAGS --disable-jpeg --disable-zlib
make

```

## tinyxml2
```bash
cd ../../../third/tinyxml2

rm -r _builds
mkdir -p _builds/Debug

cmake -H. -B_builds/Debug -DBUILD_SHARED_LIBS:BOOL=OFF -DBUILD_STATIC_LIBS:BOOL=ON -DCMAKE_BUILD_TYPE=Debug -G "CodeBlocks - Unix Makefiles"
cd _builds/Debug
make

```
 
# psserver

```bash

rm -r _builds/arm


cmake -H. -B_builds/arm/Debug -DCMAKE_BUILD_TYPE=Debug -G "CodeBlocks - Unix Makefiles"
  
cd _builds/arm/Debug && make

readelf -d psserver

scp ./psserver root@10.110.253.47:~/psserver 
scp -r /home/root/psserver/web root@10.110.253.47:~/web 

ssh root@10.110.253.47
opkg update

opkg install boost


./psserver

http://10.110.253.47:8080


```

```xml
<protocol id="read"
          path="/tmp"
          barcode1="111111111111" barcode2="" barcode3=""
          wellBitSet="15">
    <steps>
        <step name="loadStep"/>
        <step name="readExStep">
            <filters>
                <filter id="1" exposureTime="200"/>
            </filters>
        </step>
        <step name="unLoadStep"/>
    </steps>
</protocol>
```

# Test


## build camera for testing

```bash
cd psserver/pimcore/test/camera

rm -r _builds/

cmake -H. -B_builds/Debug -DCMAKE_BUILD_TYPE=Debug -G "CodeBlocks - Unix Makefiles"
cd  _builds/Debug

make
 
# readelf -d camera
# 0x00000001 (NEEDED)                     Shared library: [libpylonbase-5.2.0.so]
# 0x00000001 (NEEDED)                     Shared library: [libGCBase_gcc_v3_1_Basler_pylon.so]
# 0x00000001 (NEEDED)                     Shared library: [libstdc++.so.6]
# 0x00000001 (NEEDED)                     Shared library: [libc.so.6]

 
scp ./camera root@10.110.253.49:/home/root/
 
export PYLON_CAMEMU=1 && ./_builds/Debug/camera
PYLON_CAMEMU=1 ./camera

```
   

## barcode

```bash
cd psserver/barcode/
cmake -H. -B_builds/arm/Debug -DCMAKE_BUILD_TYPE=Debug -G "CodeBlocks - Unix Makefiles"
cd _builds/arm/Debug
make

# exit docker
# exit 
scp ~/dev/bitbucket/pg/psserver/barcode/_builds/arm/Debug/barcode  root@10.110.253.47:~/barcode

```

## pimcore test

 ```bash
  
rm -r _builds
cmake -H. -B_builds/Debug -DCMAKE_BUILD_TYPE=Debug -G "CodeBlocks - Unix Makefiles"

cd _builds/Debug 
make

readelf -d pimcoretest

scp pimcoretest root@10.110.253.49:~/pimcoretest
scp -r /home/root/psserver/config/camera-settings.pfs root@10.110.253.49:/home/root/psserver/config/camera-settings.pfs

ssh root@10.110.253.49
./pimcoretest

PYLON_CAMEMU=1 ./pimcoretest

scp root@10.110.253.49:~/oimcoretest.tiff  ~/oimcoretest.tiff
```
