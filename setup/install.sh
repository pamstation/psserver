#!/usr/bin/env sh

# IP=10.110.253.57

# ssh root@${IP} mkfs.ext4 /dev/sda
# scp -r ./setup root@${IP}:~/
# scp ./setup/install.sh root@${IP}:~/setup/install.sh
# ssh root@${IP} /home/root/setup/install.sh
# ssh root@${IP} mkdir /var/lib/psserver/config


set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# install boost
# opkg update
# opkg install boost
DIR=/home/root/setup

echo $DIR
cd "$DIR"
echo $(pwd)



# install pylon

cd pylon
tar xvzf pylon-5.2.0.13457-arm64.tar.gz
cd pylon-5.2.0.13457-arm64
mkdir -p /opt
tar -C /opt -xzf pylonSDK*.tar.gz
chmod +x setup-usb.sh
yes | ./setup-usb.sh
cd ..
rm -r pylon-5.2.0.13457-arm64

# install pamstation_error

cd "$DIR"
echo $(pwd)

cp config/pamstation_error.json ~/pamstation_error.json

# udev

cp udev/99-rtc1.rules /etc/udev/rules.d/99-rtc1.rules


# datetime sync

/sbin/hwclock --systohc --rtc /dev/rtc1

cp systemd/date-hwtosys-sync.service /etc/systemd/system/date-hwtosys-sync.service
cp systemd/date-systohw-sync.service /etc/systemd/system/date-systohw-sync.service

systemctl daemon-reload
systemctl enable date-hwtosys-sync.service
systemctl start date-hwtosys-sync.service
systemctl disable date-systohw-sync.service
systemctl enable date-systohw-sync.service
systemctl start date-systohw-sync.service


# install mount point var-lib-psserver

cp systemd/var-lib-psserver.mount /etc/systemd/system/var-lib-psserver.mount

systemctl daemon-reload

systemctl enable var-lib-psserver.mount
systemctl start var-lib-psserver.mount

sleep 2

if grep -qs '/var/lib/psserver ' /proc/mounts; then
    echo "var-lib-psserver mounted"
else
    echo "var-lib-psserver not mounted"
   systemctl status var-lib-psserver.mount
   exit 1
fi

# install psserver

set +e
systemctl stop psserver
set -e

cp ./psserver/psserver /var/lib/psserver/psserver
cp -ar ./psserver/web /var/lib/psserver
# need this folder for camera config
mkdir -p /var/lib/psserver/config

chmod +x /var/lib/psserver/psserver

cp systemd/psserver.service /etc/systemd/system/psserver.service

systemctl daemon-reload

systemctl enable psserver
systemctl start psserver


sleep 2

systemctl status psserver



