#!/usr/bin/env bash

docker run --rm -it --privileged torizon/binfmt
docker run -t --rm --name imx8  \
    -v $(pwd):/work/ \
    -e ARCH="aarch64-unknown-linux" \
    apalis-imx8 /bin/bash -c "./build.sh"
