


```
cat /dev/input/by-path/platform-tegra-ehci.1-usb-0\:1.3\:1.0-event-kbd
cat /dev/input/by-id/usb-Dell_Dell_USB_Keyboard-event-kbd
cat /dev/input/event5


cat /proc/bus/input/devices


ssh root@10.110.253.47

xinput list
xinput test 11
```

# Keymap table

```bash
xmodmap -pke
```

# Build


```bash
 
cmake -H. -B_builds/Debug -DCMAKE_BUILD_TYPE=Debug -G "CodeBlocks - Unix Makefiles"
cmake --build _builds/Debug --target barcode -- -j 1

```