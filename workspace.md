# Install Pylon5

```bash
cd
# wget https://www.baslerweb.com/fp-1551786503/media/downloads/software/pylon_software/pylon_5.2.0.13457-deb0_amd64.deb
wget https://drive.google.com/file/d/1k6kbtrpk8_bn7H6aoOWFglCeMFIinTcX/view?usp=sharing -O pylon_5.2.0.13457-deb0_amd64.deb
wget https://drive.google.com/uc?export=download&id=1k6kbtrpk8_bn7H6aoOWFglCeMFIinTcX -O pylon_5.2.0.13457-deb0_amd64.deb
wget https://drive.google.com/open?id=1k6kbtrpk8_bn7H6aoOWFglCeMFIinTcX -O pylon_5.2.0.13457-deb0_amd64.deb

sudo dpkg -i pylon_5.2.0.13457-deb0_amd64.deb

# see /opt/pylon5/README to set proper permission on usb device
cd /opt/pylon5
./setup-usb.sh

```
## RPI

```bash
cd 
# if /opt/pylon5 does not exist
wget https://drive.google.com/open?id=1Vdl6cmrZ6qkx6D2MZ_vw3YlXzfLjYw8d -O pylon_5.2.0.13457-deb0_armhf.deb
# scp -P 5022 ~/Downloads/pylon_5.2.0.13457-deb0_armhf.deb pi@localhost:~/pylon_5.2.0.13457-deb0_armhf.deb
sudo dpkg -i pylon_5.2.0.13457-deb0_armhf.deb

# see /opt/pylon5/README to set proper permission on usb device
cd /opt/pylon5
./setup-usb.sh

```

# Test machine

```
ssh root@10.110.253.49
ssh camtest@10.110.253.97
testme

rm /tmp/ps.tiff
scp camtest@10.110.253.97:/tmp/ps.tiff /tmp/ps.tiff
scp camtest@10.110.253.97:/tmp/ImageResults/111111111111_W1_F1_T200_P0_I1_A31.tif /tmp/111111111111_W1_F1_T200_P0_I1_A31.tif

scp camtest@10.110.253.97:/tmp/ps2.tiff /tmp/ps2.tiff

scp camtest@10.110.253.97:~/psserver/pimcore/config/current-camera-settings.pfs ~/dev/bitbucket/pg/psserver/pimcore/config/current-camera-settings.pfs 

scp ~/dev/bitbucket/pg/psserver/pimcore/main.cpp camtest@10.110.253.97:~/psserver/pimcore/main.cpp
```

# Build libtiff, cdkl and tinyxml2

## libtiff
```
cd pimcore/drivers/tiff
mkdir build
cd build
../configure --disable-jpeg --disable-zlib
make

```

##  cdkl  


on RPI

```bash
sudo -apt-get install bc
sudo apt-get install raspberrypi-kernel-headers
sudo -s
cd /lib/modules
ln -s 4.14.98+ $(uname -r)
cd /lib/modules/$(uname -r)/build/arch && ln -s arm armv6l
```

```
cd ../../cdkl-2.15
sudo make cpcusb
sudo make install

# need the following
cd pimcore/drivers/cdkl-2.15 
sudo cp -p lib_pi2/libcpc.a /usr/local/lib/lib32/libcpc.a
sudo cp -p lib_pi2/libcpc.so /usr/local/lib/lib32/libcpc.so

# cp -p pimcore/drivers/cdkl-2.15/lib_pi2/libcpc.a third/tinyxml2/libcpc.a
```
## tinyxml2
```
cd ../../../third/tinyxml2
# build only static libs
rm CMakeCache.txt
cmake . -DBUILD_SHARED_LIBS:BOOL=OFF -DBUILD_STATIC_LIBS:BOOL=ON
cmake . -DBUILD_SHARED_LIBS:BOOL=OFF -DBUILD_STATIC_LIBS:BOOL=ON -DCMAKE_BUILD_TYPE=Debug

make
```



# Build psserver

## Install boost

```
sudo apt install -y libboost-all-dev
```

```
cd ../../../psserver
cmake -H. -B_builds/Debug -DCMAKE_BUILD_TYPE=Debug -G "CodeBlocks - Unix Makefiles"
cmake --build _builds/Debug --target psserver -- -j 1

cmake -H. -B_builds/Release -DCMAKE_BUILD_TYPE=Release -G "CodeBlocks - Unix Makefiles"
cmake --build _builds/Release --target psserver -- -j 1

cmake --build . --target psserver -- -j 4
cmake --build . --target psserver -- -j 1
make

# readelf -d _builds/Debug/psserver

########### PIMCORETEST
cd pimcore
cmake -H. -B_builds/Debug -DCMAKE_BUILD_TYPE=Debug -G "CodeBlocks - Unix Makefiles"
cmake --build _builds/Debug --target pimcoretest -- -j 1

# readelf -d _builds/Debug/pimcoretest
sudo cp -p drivers/cdkl-2.15/lib_pi2/libcpc.so /usr/local/lib/lib32/libcpc.so.2.43

./_builds/Debug/pimcoretest

######## CAMERA
cmake -H. -B_builds/Debug -DCMAKE_BUILD_TYPE=Debug -G "CodeBlocks - Unix Makefiles"
cmake --build _builds/Debug --target camera -- -j 1

# readelf -d _builds/Debug/camera
 
export PYLON_CAMEMU=1 && ./_builds/Debug/camera

######## simpletest
cmake --build _builds/Debug --target simpletest -- -j 1

# readelf -d _builds/Debug/simpletest
 
./_builds/Debug/simpletest


###################
####################
cmake -H. -B_builds/Debug -DCMAKE_BUILD_TYPE=Debug -G "CodeBlocks - Unix Makefiles"
cmake --build _builds/Debug --target simpletest -- -j 1
readelf -d _builds/Debug/simpletest
./_builds/Debug/simpletest
```

 

# running camera emulation

```bash

PYLON_CAMEMU=1 ./psserver

export PYLON_CAMEMU=1
valgrind -v ./psserver
```

# Evaluation board

```bash
ssh root@10.110.253.47
```

## Native compilation

https://developer.toradex.com/knowledge-base/native-compilation

```bash
opkg update
opkg install git
# git version 2.9.3
opkg install cmake
# Installing cmake (3.6.1) on root.

git clone https://amaurel@bitbucket.org/pamstation/psserver.git
```

### libtiff
```
cd pimcore/drivers/tiff
mkdir build
cd build
../configure --disable-jpeg --disable-zlib
make

```

###  cdkl  

```bash
opkg install arch

opkg install kmod-dev 
opkg install linux-headers 

```
 
```
cd ../../cdkl-2.15
make cpcusb
make install

# need the following
cd pimcore/drivers/cdkl-2.15 
sudo cp -p lib_pi2/libcpc.a /usr/local/lib/lib32/libcpc.a
sudo cp -p lib_pi2/libcpc.so /usr/local/lib/lib32/libcpc.so

# cp -p pimcore/drivers/cdkl-2.15/lib_pi2/libcpc.a third/tinyxml2/libcpc.a
```

# Cross compiling

https://developer.toradex.com/getting-started/module-2-my-first-hello-world-in-c/configure-toolchain-apalis-imx6?som=apalis-imx6&board=apalis-evaluation-board
