//
// Created by alex on 11/07/19.
//

#include "AMConfiguration.h"

#include <string>
#include <sstream>


#include "../pimcore/PamStationFactory.h"      //for obtaining & using PamStation
#include "../pimcore/PamStation.h"             //for obtaining & using PamStation
#include "../pimcore/DefaultGenerator.h"
#include "./DisableBoard.h"

#include <fstream>
#include <string>
#include <iostream>
#include <stdlib.h>
#include <exception>


AMConfiguration::AMConfiguration() {
    const char *homeDir = getenv("HOME");
    std::string folder = homeDir;
    settingsFile = folder + "/pamstation_settings.json";
    modeFile = folder + "/pamstation_mode.json";

    initialize();
}

std::shared_ptr<std::map<int, std::string>> AMConfiguration::getErrorMsgs() {
    const char *homeDir = getenv("HOME");
    std::string folder = homeDir;
    auto msgs = std::shared_ptr<std::map<int, std::string>>(new std::map<int, std::string>());
    auto errorFile = folder + "/pamstation_error.json";
    try {

        std::ifstream t(errorFile);
        if (t.good()) {
            std::string content((std::istreambuf_iterator<char>(t)),
                                std::istreambuf_iterator<char>());
            auto msgsJson = json::parse(content);

            // iterate the array
            for (json::iterator it = msgsJson.begin(); it != msgsJson.end(); ++it) {
//        std::cout << *it << '\n';
                json exception = *it;
                msgs->insert(std::make_pair(exception["id"], exception["message"]));
            }
        } else {
            std::cout << "Cannot find errors messages file -- " << errorFile << std::endl;
        }
    } catch (std::exception &e) {
        std::cout << "Failed to load errors messages file -- " << errorFile << " -- " << e.what() << std::endl;
        std::string errorMessage = "error.message.bad.format -- ";
        errorMessage = errorMessage + e.what();
        throw std::runtime_error(errorMessage);
    } catch (...) {
        std::cout << "Failed to load errors messages file -- " << errorFile << std::endl;
        throw std::runtime_error("error.message.bad.format");
    }


    return msgs;
}


void AMConfiguration::initialize() {
    loadConfigFromFile();

    if (modeFileExists()) {
        std::ifstream t(modeFile);
        std::string content((std::istreambuf_iterator<char>(t)),
                            std::istreambuf_iterator<char>());
        mode = json::parse(content);

    } else {
        mode = {{"kind",         "Mode"},
                {"isStub",       true},
                {"isStubBoard1", false},
                {"isStubBoard2", false},
                {"isStubBoard3", false}};
        saveModeToFile();
    }
}

Focus_LogicalWellPos_t AMConfiguration::getFocusLogicalWellPos() {

    Focus_LogicalWellPos_t tFocusLogWellPos;

    if (settingsFileExists()) {
        std::cout << "getFocusLogicalWellPos from settings" << std::endl;

        tFocusLogWellPos.rPosForFilter1 = settings["focus_logicalWellPos_t"]["rPosForFilter1"];
        tFocusLogWellPos.rPosForFilter2 = settings["focus_logicalWellPos_t"]["rPosForFilter2"];
        tFocusLogWellPos.rPosForFilter3 = settings["focus_logicalWellPos_t"]["rPosForFilter3"];
        tFocusLogWellPos.rOffsetForWell1 = settings["focus_logicalWellPos_t"]["rOffsetForWell1"];
        tFocusLogWellPos.rOffsetForWell2 = settings["focus_logicalWellPos_t"]["rOffsetForWell2"];
        tFocusLogWellPos.rOffsetForWell3 = settings["focus_logicalWellPos_t"]["rOffsetForWell3"];
        tFocusLogWellPos.rOffsetForWell4 = settings["focus_logicalWellPos_t"]["rOffsetForWell4"];
        tFocusLogWellPos.rOffsetForWell5 = settings["focus_logicalWellPos_t"]["rOffsetForWell5"];
        tFocusLogWellPos.rOffsetForWell6 = settings["focus_logicalWellPos_t"]["rOffsetForWell6"];
        tFocusLogWellPos.rOffsetForWell7 = settings["focus_logicalWellPos_t"]["rOffsetForWell7"];
        tFocusLogWellPos.rOffsetForWell8 = settings["focus_logicalWellPos_t"]["rOffsetForWell8"];
        tFocusLogWellPos.rOffsetForWell9 = settings["focus_logicalWellPos_t"]["rOffsetForWell9"];
        tFocusLogWellPos.rOffsetForWell10 = settings["focus_logicalWellPos_t"]["rOffsetForWell10"];
        tFocusLogWellPos.rOffsetForWell11 = settings["focus_logicalWellPos_t"]["rOffsetForWell11"];
        tFocusLogWellPos.rOffsetForWell12 = settings["focus_logicalWellPos_t"]["rOffsetForWell12"];
    } else {
        std::cout << "getFocusLogicalWellPos from instrument" << std::endl;
        auto pos = PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getFocusLogicalPositions();
        tFocusLogWellPos.rPosForFilter1 = pos->rPosForFilter1;
        tFocusLogWellPos.rPosForFilter2 = pos->rPosForFilter2;
        tFocusLogWellPos.rPosForFilter3 = pos->rPosForFilter3;

        tFocusLogWellPos.rOffsetForWell1 = pos->rOffsetForDisp1;
        tFocusLogWellPos.rOffsetForWell2 = pos->rOffsetForDisp1;
        tFocusLogWellPos.rOffsetForWell3 = pos->rOffsetForDisp1;
        tFocusLogWellPos.rOffsetForWell4 = pos->rOffsetForDisp1;
        tFocusLogWellPos.rOffsetForWell5 = pos->rOffsetForDisp2;
        tFocusLogWellPos.rOffsetForWell6 = pos->rOffsetForDisp2;
        tFocusLogWellPos.rOffsetForWell7 = pos->rOffsetForDisp2;
        tFocusLogWellPos.rOffsetForWell8 = pos->rOffsetForDisp2;
        tFocusLogWellPos.rOffsetForWell9 = pos->rOffsetForDisp3;
        tFocusLogWellPos.rOffsetForWell10 = pos->rOffsetForDisp3;
        tFocusLogWellPos.rOffsetForWell11 = pos->rOffsetForDisp3;
        tFocusLogWellPos.rOffsetForWell12 = pos->rOffsetForDisp3;
    }

    return tFocusLogWellPos;
}

std::string AMConfiguration::getUnitName() {
    if (settingsFileExists()) {
        return settings["unitName"];
    } else {
        return "psserver";
    }

}

bool AMConfiguration::isStub() {
    return mode["isStub"];
}

bool AMConfiguration::isStubBoard1() {
    return mode["isStubBoard1"];
}

bool AMConfiguration::isStubBoard2() {
    return mode["isStubBoard2"];
}

bool AMConfiguration::isStubBoard3() {
    return mode["isStubBoard3"];
}

void AMConfiguration::loadConfigFromFile() {
    if (settingsFileExists()) {
        std::ifstream t(settingsFile);
        std::string content((std::istreambuf_iterator<char>(t)),
                            std::istreambuf_iterator<char>());
        settings = json::parse(content);
        if (!settings.contains("unitName")){
            settings["unitName"] = "psserver";
        }
    }
}

void AMConfiguration::loadSettingsFromInstrument() {
    Wagon_LogicalXYPos_t *ww = PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getWagonLogicalPositions();
    Filter_LogicalPos_t *fl = PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getFilterLogicalPositions();
    Focus_LogicalPos_t *fol = PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getFocusLogicalPositions();
    Focus_LogicalWellPos_t *folw = PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getFocusLogicalWellPositions();

    LEDIntensity_t *li = PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getLEDIntensity();

    Load_Unload_Settings_t *lu = PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getLoad_Unload_Settings();
    PumpSettings_t *ps = PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getPumpingDefaults();
    AspirationSettings_t *as = PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getAspirationDefaults();
    DispenseSettings_t *ds = PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getDispenseDefaults();

    auto unitName = getUnitName();

    settings = {{"kind",                   "Settings"},
                {"unitName",                   unitName},
                {"wagon_logicalXYPos_t",   {
                                                   {"kind", "Wagon_LogicalXYPos_t"},
                                                   {"xyLoadPosition",       {
                                                                                    {"kind", "XYPos"},
                                                                                    {"x", ww->xyLoadPosition.X},
                                                                                    {"y", ww->xyLoadPosition.Y},
                                                                            }},
                                                   {"xyIncubationPosition",   {
                                                                                      {"kind", "XYPos"},
                                                                                      {"x", ww->xyIncubationPosition.X},
                                                                                      {"y", ww->xyIncubationPosition.Y},
                                                                              }},
                                                   {"xyFrontPanelPosition",    {
                                                                                       {"kind", "XYPos"},
                                                                                       {"x", ww->xyFrontPanelPosition.X},
                                                                                       {"y", ww->xyFrontPanelPosition.Y},
                                                                               }},
                                                   {"xyReadPosition",          {
                                                                                       {"kind", "XYPos"},
                                                                                       {"x", ww->xyReadPosition.X},
                                                                                       {"y", ww->xyReadPosition.Y},
                                                                               }},
                                                   {"xyAspiratePosition",  {
                                                                                   {"kind", "XYPos"},
                                                                                   {"x", ww->xyAspiratePosition.X},
                                                                                   {"y", ww->xyAspiratePosition.Y},
                                                                           }},
                                                   {"xyDispensePosition", {
                                                                                  {"kind", "XYPos"},
                                                                                  {"x", ww->xyDispensePosition.X},
                                                                                  {"y", ww->xyDispensePosition.Y},
                                                                          }},
                                                   {"xyOffset_Well",   {
                                                                               {"kind", "XYPos"},
                                                                               {"x", ww->xyOffset_Well.X},
                                                                               {"y", ww->xyOffset_Well.Y},
                                                                       }},
                                                   {"xyOffset_Disposable", {
                                                                                   {"kind", "XYPos"},
                                                                                   {"x", ww->xyOffset_Disposable.X},
                                                                                   {"y", ww->xyOffset_Disposable.Y},
                                                                           }},
                                                   {"xyOffset_FocusGlass", {
                                                                                   {"kind", "XYPos"},
                                                                                   {"x", ww->xyOffset_FocusGlass.X},
                                                                                   {"y", ww->xyOffset_FocusGlass.Y},
                                                                           }},
                                                   {"xyOffset_DispenseHead1", {
                                                                                      {"kind", "XYPos"},
                                                                                      {"x", ww->xyOffset_DispenseHead1.X},
                                                                                      {"y", ww->xyOffset_DispenseHead1.Y},
                                                                              }},
                                                   {"xyOffset_DispenseHead2", {
                                                                                      {"kind", "XYPos"},
                                                                                      {"x", ww->xyOffset_DispenseHead2.X},
                                                                                      {"y", ww->xyOffset_DispenseHead2.Y},
                                                                              }},
                                                   {"xyOffset_DispenseHead3", {
                                                                                      {"kind", "XYPos"},
                                                                                      {"x", ww->xyOffset_DispenseHead3.X},
                                                                                      {"y", ww->xyOffset_DispenseHead3.Y},
                                                                              }},
                                                   {"xyOffset_DispenseHead4", {
                                                                                      {"kind", "XYPos"},
                                                                                      {"x", ww->xyOffset_DispenseHead4.X},
                                                                                      {"y", ww->xyOffset_DispenseHead4.Y},
                                                                              }},
                                                   {"xyOffset_Prime1",  {
                                                                                {"kind", "XYPos"},
                                                                                {"x", ww->xyOffset_Prime1.X},
                                                                                {"y", ww->xyOffset_Prime1.Y},
                                                                        }},
                                                   {"xyOffset_Prime2",  {
                                                                                {"kind", "XYPos"},
                                                                                {"x", ww->xyOffset_Prime2.X},
                                                                                {"y", ww->xyOffset_Prime2.Y},
                                                                        }},
                                           }},
                {"filter_logicalPos_t",    {
                                                   {"kind", "Filter_LogicalPos_t"},
                                                   {"rPosFilter1",          fl->rPosFilter1},
                                                   {"rPosFilter2",            fl->rPosFilter2},
                                                   {"rPosFilter3",             fl->rPosFilter3},
                                           }},
                {"focus_logicalPos_t",     {
                                                   {"kind", "Focus_LogicalPos_t"},
                                                   {"rPosForFilter1",       fol->rPosForFilter1},
                                                   {"rPosForFilter2",         fol->rPosForFilter2},
                                                   {"rPosForFilter3",          fol->rPosForFilter3},
                                                   {"rOffsetForDisp1",         fol->rOffsetForDisp1},
                                                   {"rOffsetForDisp2",     fol->rOffsetForDisp2},
                                                   {"rOffsetForDisp3",    fol->rOffsetForDisp3},
                                           }},
                {"focus_logicalWellPos_t", {
                                                   {"kind", "Focus_LogicalWellPos_t"},
                                                   {"rPosForFilter1",       folw->rPosForFilter1},
                                                   {"rPosForFilter2",         folw->rPosForFilter2},
                                                   {"rPosForFilter3",          folw->rPosForFilter3},
                                                   {"rOffsetForWell1",         folw->rOffsetForWell1},
                                                   {"rOffsetForWell2",     folw->rOffsetForWell2},
                                                   {"rOffsetForWell3",    folw->rOffsetForWell3},
                                                   {"rOffsetForWell4", folw->rOffsetForWell4},
                                                   {"rOffsetForWell5",     folw->rOffsetForWell5},
                                                   {"rOffsetForWell6",     folw->rOffsetForWell6},
                                                   {"rOffsetForWell7",        folw->rOffsetForWell7},
                                                   {"rOffsetForWell8",        folw->rOffsetForWell8},
                                                   {"rOffsetForWell9",        folw->rOffsetForWell9},
                                                   {"rOffsetForWell10",       folw->rOffsetForWell10},
                                                   {"rOffsetForWell11", folw->rOffsetForWell11},
                                                   {"rOffsetForWell12", folw->rOffsetForWell12},
                                           }},
                {"ledIntensity_t",         {
                                                   {"kind", "LEDIntensity_t"},
                                                   {"uIntensity_LED1",      li->uIntensity_LED1},
                                                   {"uIntensity_LED2",        li->uIntensity_LED2},
                                                   {"uIntensity_LED3",         li->uIntensity_LED3},
                                           }},
                {"load_unload_settings_t", {
                                                   {"kind", "Load_Unload_Settings_t"},
                                                   {"rDefaultOverPressure", lu->rDefaultOverPressure},
                                                   {"rDefaultUnderPressure",  lu->rDefaultUnderPressure},
                                                   {"rInitWagonTemperature",   lu->rInitWagonTemperature},
                                                   {"rSafeUnloadTemperature",  lu->rSafeUnloadTemperature},
                                           }},
                {"pumpSettings_t",         {
                                                   {"kind", "PumpSettings_t"},
                                                   {"uValveOpenTime",       ps->uValveOpenTime},
                                                   {"uPressureSettlingDelay", ps->uPressureSettlingDelay},
                                                   {"uFluidFlowDelay",         ps->uFluidFlowDelay},
                                                   {"rBrokenMembranePressure", ps->rBrokenMembranePressure},
                                                   {"uMaximumWaitingTime", ps->uMaximumWaitingTime},
                                           }},
                {"aspirationSettings_t",   {
                                                   {"kind", "AspirationSettings_t"},
                                                   {"rPressureUnderlimit",  as->rPressureUnderlimit},
                                                   {"rPressureUpperLimit",    as->rPressureUpperLimit},
                                                   {"uAspirateTubeCleanDelay", as->uAspirateTubeCleanDelay},
                                                   {"uAspPressBuildupDelay",   as->uAspPressBuildupDelay},
                                           }},
                {"dispenseSettings_t",     {
                                                   {"kind", "DispenseSettings_t"},
                                                   {"uHeatableHeadBitSet",  ds->uHeatableHeadBitSet},
                                                   {"rPrimeAmount",           ds->rPrimeAmount},
                                                   {"rRetractAmount",          ds->rRetractAmount},
                                                   {"uRetractDelay",           ds->uRetractDelay},
                                           }},
    };

    // Sensor calibration
    const int N = 44;
    ECanObj listSensorECanObj[N] = {ECanObj::eTempSensDispense,
                                    ECanObj::eTempSensBottomLeft,
                                    ECanObj::eTempSensBottomMiddle,
                                    ECanObj::eTempSensBottomRight,
                                    ECanObj::eTempSensCoverLeft,
                                    ECanObj::eTempSensCoverMiddle,
                                    ECanObj::eTempSensCoverRight,
                                    ECanObj::eTempSensCalibration,
                                    ECanObj::ePressSensOverPress,
                                    ECanObj::ePressSensUnderPress,
                                    ECanObj::ePressSensAspirate,
                                    ECanObj::ePressSensWell1,
                                    ECanObj::ePressSensWell2,
                                    ECanObj::ePressSensWell3,
                                    ECanObj::ePressSensWell4,
                                    ECanObj::ePressSensWell5,
                                    ECanObj::ePressSensWell6,
                                    ECanObj::ePressSensWell7,
                                    ECanObj::ePressSensWell8,
                                    ECanObj::ePressSensWell9,
                                    ECanObj::ePressSensWell10,
                                    ECanObj::ePressSensWell11,
                                    ECanObj::ePressSensWell12,
                                    ECanObj::eEncDispenseHead1A,
                                    ECanObj::eEncDispenseHead2A,
                                    ECanObj::eEncDispenseHead3A,
                                    ECanObj::eEncDispenseHead4A,
                                    ECanObj::eEncFilterA,
                                    ECanObj::eEncFocusA,
                                    ECanObj::eEncTransXA,
                                    ECanObj::eEncTransYA,
                                    ECanObj::eSensAspirateHeadUp,
                                    ECanObj::eCurrSensAspirateMotor,
                                    ECanObj::eCurrSensBottomLeft,
                                    ECanObj::eCurrSensBottomMiddle,
                                    ECanObj::eCurrSensBottomRight,
                                    ECanObj::eCurrSensCoverLeft,
                                    ECanObj::eCurrSensCoverMiddle,
                                    ECanObj::eCurrSensCoverRight,
                                    ECanObj::eCurrSensDispenseHeater,
                                    ECanObj::eCurrSensFilter,
                                    ECanObj::eCurrSensFocus,
                                    ECanObj::eCurrSensTransX,
                                    ECanObj::eCurrSensTransY};

    json sensorsCalibrationData;

    for (auto &obj : listSensorECanObj) {

        Sensor_Calibration_Data_t *data = PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getSensorCalibrationData(
                obj);

        json json_data = {
                {"kind",          "Sensor_Calibration_Data_t"},
                {"eSensorObj",    (int) data->eSensorObj},
                {"rSysOffset",    data->rSysOffset},
                {"rSysGain",      data->rSysGain},
                {"rSysGain2",     data->rSysGain2},
                {"rSensOffset",   data->rSensOffset},
                {"rSensGain",     data->rSensGain},
                {"rSensGain2",    data->rSensGain2},
                {"rADConversion", data->rADConversion},
                {"rTsample",      data->rTsample},
        };

        sensorsCalibrationData.push_back(json_data);
    }

    settings["sensorCalibrationData_t"] = sensorsCalibrationData;

    const int NN = 18;
    ECanObj listCtrlECanObj[NN] = {ECanObj::eCtrlOverPressureSupplier,
                                   ECanObj::eCtrlUnderPressureSupplier,
                                   ECanObj::eCtrlTransporterX,
                                   ECanObj::eCtrlTransporterY,
                                   ECanObj::eCtrlFocusPositioner,
                                   ECanObj::eCtrlFilterPositioner,
                                   ECanObj::eCtrlHtrCoverLeft,
                                   ECanObj::eCtrlHtrBottomLeft,
                                   ECanObj::eCtrlHtrCoverMiddle,
                                   ECanObj::eCtrlHtrBottomMiddle,
                                   ECanObj::eCtrlHtrCoverRight,
                                   ECanObj::eCtrlHtrBottomRight,
                                   ECanObj::eCtrlAspirateHeadPositioner,
                                   ECanObj::eCtrlDispenseHeadHeater,
                                   ECanObj::eCtrlDispenseHeadPositioner1,
                                   ECanObj::eCtrlDispenseHeadPositioner2,
                                   ECanObj::eCtrlDispenseHeadPositioner3,
                                   ECanObj::eCtrlDispenseHeadPositioner4};

    json controllableSettings;

    for (auto &obj : listCtrlECanObj) {

        Controllable_Settings_t *data = PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getControllableSettings(
                obj);

        json json_data = {
                {"kind",                 "ControllableSettings"},
                {"eCtrlObj",             (int) data->eCtrlObj},
                {"eSensObj",             (int) data->eSensObj},
                {"eCurrSensObj",         (int) data->eCurrSensObj},

                {"uWaitReadyTimeout",    data->uWaitReadyTimeout},
                {"iDefaultPwm",          data->iDefaultPwm},
                {"rMaxProperty",         data->rMaxProperty},
                {"rMinProperty",         data->rMinProperty},
                {"rTrackingError",       data->rTrackingError},
                {"rTolerance",           data->rTolerance},

                {"rMaxCurrent",          data->rMaxCurrent},
                {"rMaxVelocity",         data->rMaxVelocity},
                {"rMinVelocity",         data->rMinVelocity},
                {"rMaxAcceleration",     data->rMaxAcceleration},
                {"rMinAcceleration",     data->rMinAcceleration},
                {"rDefaultVelocity",     data->rDefaultVelocity},
                {"rDefaultAcceleration", data->rDefaultAcceleration},
                {"uSettleTime",          data->uSettleTime},

                {"sd_KPp",               data->sd_KPp},
                {"sd_KIp",               data->sd_KIp},
                {"sd_KDp",               data->sd_KDp},
                {"sd_Integ_Lim_max",     data->sd_Integ_Lim_max},
                {"sd_Integ_Lim_min",     data->sd_Integ_Lim_min},
                {"KPi",                  data->KPi},
                {"KIi",                  data->KIi},
                {"DefaultVel",           data->DefaultVel},
                {"DefaultAcc",           data->DefaultAcc},

                {"sf_KPp",               data->sf_KPp},
                {"sf_KIp",               data->sf_KIp},
                {"sf_KDp",               data->sf_KDp},
                {"va_scale",             data->va_scale},
                {"T_Settletime",         data->T_Settletime},

                {"Allow_Homing",         data->Allow_Homing},
                {"Homing_tick_count",    data->Homing_tick_count},
                {"Homing_Speed_PWM",     data->Homing_Speed_PWM},
                {"Homing_Seeking_PWM",   data->Homing_Seeking_PWM},

                {"max_output",           data->max_output},
                {"min_output",           data->min_output},
                {"max_setpoint",         data->max_setpoint},
                {"min_setpoint",         data->min_setpoint},
                {"max_tracking_error",   data->max_tracking_error},
                {"max_tolerance_error",  data->max_tolerance_error},
                {"max_dc_i",             data->max_dc_i},
                {"max_dc_pwm",           data->max_dc_pwm},
                {"du_max_vel",           data->du_max_vel},
                {"du_min_vel",           data->du_min_vel},
                {"du_max_acc",           data->du_max_acc},
                {"du_min_acc",           data->du_min_acc},
        };

        controllableSettings.push_back(json_data);
    }

    settings["controllableSettings"] = controllableSettings;
}

void AMConfiguration::saveSettingsToFile() {
    std::ofstream out(settingsFile);
    out << settings.dump(2);
    out.close();
}

void AMConfiguration::saveModeToFile() {
    std::ofstream out(modeFile);
    out << mode.dump(2);
    out.close();
}

bool AMConfiguration::settingsFileExists() {
    std::ifstream t(settingsFile);
    return t.good();
}

bool AMConfiguration::modeFileExists() {
    std::ifstream t(modeFile);
    return t.good();
}

void AMConfiguration::flash() {
    DisableAllBoards disableBoards = DisableAllBoards();
    PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->flashAllBoards();
}

void AMConfiguration::applySettings() {
    Wagon_LogicalXYPos_t *ww = PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getWagonLogicalPositions();
    Filter_LogicalPos_t *fl = PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getFilterLogicalPositions();
    Focus_LogicalPos_t *fol = PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getFocusLogicalPositions();
    Focus_LogicalWellPos_t *folw = PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getFocusLogicalWellPositions();

    LEDIntensity_t *li = PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getLEDIntensity();

    Load_Unload_Settings_t *lu = PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getLoad_Unload_Settings();
    PumpSettings_t *ps = PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getPumpingDefaults();
    AspirationSettings_t *as = PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getAspirationDefaults();
    DispenseSettings_t *ds = PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getDispenseDefaults();

    ww->xyLoadPosition.X = settings["wagon_logicalXYPos_t"]["xyLoadPosition"]["x"];
    ww->xyLoadPosition.Y = settings["wagon_logicalXYPos_t"]["xyLoadPosition"]["y"];

    ww->xyIncubationPosition.X = settings["wagon_logicalXYPos_t"]["xyIncubationPosition"]["x"];
    ww->xyIncubationPosition.Y = settings["wagon_logicalXYPos_t"]["xyIncubationPosition"]["y"];

    ww->xyFrontPanelPosition.X = settings["wagon_logicalXYPos_t"]["xyFrontPanelPosition"]["x"];
    ww->xyFrontPanelPosition.Y = settings["wagon_logicalXYPos_t"]["xyFrontPanelPosition"]["y"];

    ww->xyReadPosition.X = settings["wagon_logicalXYPos_t"]["xyReadPosition"]["x"];
    ww->xyReadPosition.Y = settings["wagon_logicalXYPos_t"]["xyReadPosition"]["y"];

    ww->xyAspiratePosition.X = settings["wagon_logicalXYPos_t"]["xyAspiratePosition"]["x"];
    ww->xyAspiratePosition.Y = settings["wagon_logicalXYPos_t"]["xyAspiratePosition"]["y"];

    ww->xyDispensePosition.X = settings["wagon_logicalXYPos_t"]["xyDispensePosition"]["x"];
    ww->xyDispensePosition.Y = settings["wagon_logicalXYPos_t"]["xyDispensePosition"]["y"];

    ww->xyOffset_Well.X = settings["wagon_logicalXYPos_t"]["xyOffset_Well"]["x"];
    ww->xyOffset_Well.Y = settings["wagon_logicalXYPos_t"]["xyOffset_Well"]["y"];

    ww->xyOffset_Disposable.X = settings["wagon_logicalXYPos_t"]["xyOffset_Disposable"]["x"];
    ww->xyOffset_Disposable.Y = settings["wagon_logicalXYPos_t"]["xyOffset_Disposable"]["y"];

    ww->xyOffset_FocusGlass.X = settings["wagon_logicalXYPos_t"]["xyOffset_FocusGlass"]["x"];
    ww->xyOffset_FocusGlass.Y = settings["wagon_logicalXYPos_t"]["xyOffset_FocusGlass"]["y"];

    ww->xyOffset_DispenseHead1.X = settings["wagon_logicalXYPos_t"]["xyOffset_DispenseHead1"]["x"];
    ww->xyOffset_DispenseHead1.Y = settings["wagon_logicalXYPos_t"]["xyOffset_DispenseHead1"]["y"];

    ww->xyOffset_DispenseHead2.X = settings["wagon_logicalXYPos_t"]["xyOffset_DispenseHead2"]["x"];
    ww->xyOffset_DispenseHead2.Y = settings["wagon_logicalXYPos_t"]["xyOffset_DispenseHead2"]["y"];

    ww->xyOffset_DispenseHead3.X = settings["wagon_logicalXYPos_t"]["xyOffset_DispenseHead3"]["x"];
    ww->xyOffset_DispenseHead3.Y = settings["wagon_logicalXYPos_t"]["xyOffset_DispenseHead3"]["y"];

    ww->xyOffset_DispenseHead4.X = settings["wagon_logicalXYPos_t"]["xyOffset_DispenseHead4"]["x"];
    ww->xyOffset_DispenseHead4.Y = settings["wagon_logicalXYPos_t"]["xyOffset_DispenseHead4"]["y"];

    ww->xyOffset_Prime1.X = settings["wagon_logicalXYPos_t"]["xyOffset_Prime1"]["x"];
    ww->xyOffset_Prime1.Y = settings["wagon_logicalXYPos_t"]["xyOffset_Prime1"]["y"];

    ww->xyOffset_Prime2.X = settings["wagon_logicalXYPos_t"]["xyOffset_Prime2"]["x"];
    ww->xyOffset_Prime2.Y = settings["wagon_logicalXYPos_t"]["xyOffset_Prime2"]["y"];


    fl->rPosFilter1 = settings["filter_logicalPos_t"]["rPosFilter1"];
    fl->rPosFilter2 = settings["filter_logicalPos_t"]["rPosFilter2"];
    fl->rPosFilter3 = settings["filter_logicalPos_t"]["rPosFilter3"];


    fol->rPosForFilter1 = settings["focus_logicalPos_t"]["rPosForFilter1"];
    fol->rPosForFilter2 = settings["focus_logicalPos_t"]["rPosForFilter2"];
    fol->rPosForFilter3 = settings["focus_logicalPos_t"]["rPosForFilter3"];
    fol->rOffsetForDisp1 = settings["focus_logicalPos_t"]["rOffsetForDisp1"];
    fol->rOffsetForDisp2 = settings["focus_logicalPos_t"]["rOffsetForDisp2"];
    fol->rOffsetForDisp3 = settings["focus_logicalPos_t"]["rOffsetForDisp3"];



    //////////////////////////////////////////

    folw->rPosForFilter1 = settings["focus_logicalWellPos_t"]["rPosForFilter1"];
    folw->rPosForFilter2 = settings["focus_logicalWellPos_t"]["rPosForFilter2"];
    folw->rPosForFilter3 = settings["focus_logicalWellPos_t"]["rPosForFilter3"];
    folw->rOffsetForWell1 = settings["focus_logicalWellPos_t"]["rOffsetForWell1"];
    folw->rOffsetForWell2 = settings["focus_logicalWellPos_t"]["rOffsetForWell2"];
    folw->rOffsetForWell3 = settings["focus_logicalWellPos_t"]["rOffsetForWell3"];
    folw->rOffsetForWell4 = settings["focus_logicalWellPos_t"]["rOffsetForWell4"];
    folw->rOffsetForWell5 = settings["focus_logicalWellPos_t"]["rOffsetForWell5"];
    folw->rOffsetForWell6 = settings["focus_logicalWellPos_t"]["rOffsetForWell6"];
    folw->rOffsetForWell7 = settings["focus_logicalWellPos_t"]["rOffsetForWell7"];
    folw->rOffsetForWell8 = settings["focus_logicalWellPos_t"]["rOffsetForWell8"];
    folw->rOffsetForWell9 = settings["focus_logicalWellPos_t"]["rOffsetForWell9"];
    folw->rOffsetForWell10 = settings["focus_logicalWellPos_t"]["rOffsetForWell10"];
    folw->rOffsetForWell11 = settings["focus_logicalWellPos_t"]["rOffsetForWell11"];
    folw->rOffsetForWell12 = settings["focus_logicalWellPos_t"]["rOffsetForWell12"];



    ///////////////////////////////////////////

    li->uIntensity_LED1 = settings["ledIntensity_t"]["uIntensity_LED1"];
    li->uIntensity_LED2 = settings["ledIntensity_t"]["uIntensity_LED2"];
    li->uIntensity_LED3 = settings["ledIntensity_t"]["uIntensity_LED3"];


    lu->rDefaultOverPressure = settings["load_unload_settings_t"]["rDefaultOverPressure"];
    lu->rDefaultUnderPressure = settings["load_unload_settings_t"]["rDefaultUnderPressure"];
    lu->rInitWagonTemperature = settings["load_unload_settings_t"]["rInitWagonTemperature"];
    lu->rSafeUnloadTemperature = settings["load_unload_settings_t"]["rSafeUnloadTemperature"];


    ps->uValveOpenTime = settings["pumpSettings_t"]["uValveOpenTime"];
    ps->uPressureSettlingDelay = settings["pumpSettings_t"]["uPressureSettlingDelay"];
    ps->uFluidFlowDelay = settings["pumpSettings_t"]["uFluidFlowDelay"];
    ps->rBrokenMembranePressure = settings["pumpSettings_t"]["rBrokenMembranePressure"];
    ps->uMaximumWaitingTime = settings["pumpSettings_t"]["uMaximumWaitingTime"];


    as->rPressureUnderlimit = settings["aspirationSettings_t"]["rPressureUnderlimit"];
    as->rPressureUpperLimit = settings["aspirationSettings_t"]["rPressureUpperLimit"];
    as->uAspirateTubeCleanDelay = settings["aspirationSettings_t"]["uAspirateTubeCleanDelay"];
    as->uAspPressBuildupDelay = settings["aspirationSettings_t"]["uAspPressBuildupDelay"];


    ds->uHeatableHeadBitSet = settings["dispenseSettings_t"]["uHeatableHeadBitSet"];
    ds->rPrimeAmount = settings["dispenseSettings_t"]["rPrimeAmount"];
    ds->rRetractAmount = settings["dispenseSettings_t"]["rRetractAmount"];
    ds->uRetractDelay = settings["dispenseSettings_t"]["uRetractDelay"];


    DisableAllBoards disableBoards = DisableAllBoards();

    std::cout << "setWagonLogicalPositions" << std::endl;
    PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->setWagonLogicalPositions(*ww);
    std::cout << "setFilterLogicalPositions" << std::endl;
    PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->setFilterLogicalPositions(*fl);
    std::cout << "setFocusLogicalPositions" << std::endl;
    PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->setFocusLogicalPositions(*fol);
    std::cout << "setFocusLogicalWellPositions" << std::endl;
    PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->setFocusLogicalWellPositions(*folw);
    std::cout << "setLEDIntensity" << std::endl;
    PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->setLEDIntensity(*li);
    std::cout << "setLoad_Unload_Defaults" << std::endl;
    PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->setLoad_Unload_Defaults(*lu);
    std::cout << "setPumpingDefaults" << std::endl;
    PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->setPumpingDefaults(*ps);
    std::cout << "setAspirationDefaults" << std::endl;
    PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->setAspirationDefaults(*as);
    std::cout << "setDispenseDefaults" << std::endl;
    PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->setDispenseDefaults(*ds);

    json sensorsCalibrationData = settings["sensorCalibrationData_t"];

    for (json::iterator it = sensorsCalibrationData.begin(); it != sensorsCalibrationData.end(); ++it) {
//        std::cout << *it << '\n';
        json json_data = *it;
        Sensor_Calibration_Data_t cData;
        cData.eSensorObj = (ECanObj) json_data["eSensorObj"];
        cData.rSysOffset = json_data["rSysOffset"];
        cData.rSysGain = json_data["rSysGain"];
        cData.rSysGain2 = json_data["rSysGain2"];
        cData.rSensOffset = json_data["rSensOffset"];
        cData.rSensGain = json_data["rSensGain"];
        cData.rSensGain2 = json_data["rSensGain2"];
        cData.rADConversion = json_data["rADConversion"];
        cData.rTsample = json_data["rTsample"];

        PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->setSensorCalibrationData(cData);
    }

    json controllableSettings = settings["controllableSettings"];

    // iterate the array
    for (json::iterator it = controllableSettings.begin(); it != controllableSettings.end(); ++it) {
//        std::cout << *it << '\n';
        json json_ctrl_settings = *it;
        Controllable_Settings_t ctrl_settings;

        ctrl_settings.eCtrlObj = (ECanObj) json_ctrl_settings["eCtrlObj"];
        ctrl_settings.eSensObj = (ECanObj) json_ctrl_settings["eSensObj"];
        ctrl_settings.eCurrSensObj = (ECanObj) json_ctrl_settings["eCurrSensObj"];
        ctrl_settings.uWaitReadyTimeout = json_ctrl_settings["uWaitReadyTimeout"];
        ctrl_settings.iDefaultPwm = json_ctrl_settings["iDefaultPwm"];
        ctrl_settings.rMaxProperty = json_ctrl_settings["rMaxProperty"];
        ctrl_settings.rMinProperty = json_ctrl_settings["rMinProperty"];
        ctrl_settings.rTrackingError = json_ctrl_settings["rTrackingError"];
        ctrl_settings.rTolerance = json_ctrl_settings["rTolerance"];
        ctrl_settings.rMaxCurrent = json_ctrl_settings["rMaxCurrent"];
        ctrl_settings.rMaxVelocity = json_ctrl_settings["rMaxVelocity"];
        ctrl_settings.rMinVelocity = json_ctrl_settings["rMinVelocity"];
        ctrl_settings.rMaxAcceleration = json_ctrl_settings["rMaxAcceleration"];
        ctrl_settings.rMinAcceleration = json_ctrl_settings["rMinAcceleration"];
        ctrl_settings.rDefaultVelocity = json_ctrl_settings["rDefaultVelocity"];
        ctrl_settings.rDefaultAcceleration = json_ctrl_settings["rDefaultAcceleration"];
        ctrl_settings.uSettleTime = json_ctrl_settings["uSettleTime"];
        ctrl_settings.sd_KPp = json_ctrl_settings["sd_KPp"];
        ctrl_settings.sd_KIp = json_ctrl_settings["sd_KIp"];
        ctrl_settings.sd_KDp = json_ctrl_settings["sd_KDp"];
        ctrl_settings.sd_Integ_Lim_max = json_ctrl_settings["sd_Integ_Lim_max"];
        ctrl_settings.sd_Integ_Lim_min = json_ctrl_settings["sd_Integ_Lim_min"];
        ctrl_settings.KPi = json_ctrl_settings["KPi"];
        ctrl_settings.KIi = json_ctrl_settings["KIi"];
        ctrl_settings.DefaultVel = json_ctrl_settings["DefaultVel"];
        ctrl_settings.DefaultAcc = json_ctrl_settings["DefaultAcc"];
        ctrl_settings.sf_KPp = json_ctrl_settings["sf_KPp"];
        ctrl_settings.sf_KIp = json_ctrl_settings["sf_KIp"];
        ctrl_settings.sf_KDp = json_ctrl_settings["sf_KDp"];
        ctrl_settings.va_scale = json_ctrl_settings["va_scale"];
        ctrl_settings.T_Settletime = json_ctrl_settings["T_Settletime"];
        ctrl_settings.Allow_Homing = json_ctrl_settings["Allow_Homing"];
        ctrl_settings.Homing_tick_count = json_ctrl_settings["Homing_tick_count"];
        ctrl_settings.Homing_Speed_PWM = json_ctrl_settings["Homing_Speed_PWM"];
        ctrl_settings.Homing_Seeking_PWM = json_ctrl_settings["Homing_Seeking_PWM"];
        ctrl_settings.max_output = json_ctrl_settings["max_output"];
        ctrl_settings.min_output = json_ctrl_settings["min_output"];
        ctrl_settings.max_setpoint = json_ctrl_settings["max_setpoint"];
        ctrl_settings.min_setpoint = json_ctrl_settings["min_setpoint"];
        ctrl_settings.max_tracking_error = json_ctrl_settings["max_tracking_error"];
        ctrl_settings.max_tolerance_error = json_ctrl_settings["max_tolerance_error"];
        ctrl_settings.max_dc_i = json_ctrl_settings["max_dc_i"];
        ctrl_settings.max_dc_pwm = json_ctrl_settings["max_dc_pwm"];
        ctrl_settings.du_max_vel = json_ctrl_settings["du_max_vel"];
        ctrl_settings.du_min_vel = json_ctrl_settings["du_min_vel"];
        ctrl_settings.du_max_acc = json_ctrl_settings["du_max_acc"];
        ctrl_settings.du_min_acc = json_ctrl_settings["du_min_acc"];

        PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->setControllableSettings(ctrl_settings);
    }
}


// TODO
void AMConfiguration::validateSettings(json &aConfig) {
    if (!aConfig.contains("unitName")){
        aConfig["unitName"] = "psserver";
    }
}

// PUBLIC
void AMConfiguration::setSettings(json aSettings) {
    validateSettings(aSettings);
    settings = aSettings;
    applySettings();
    saveSettingsToFile();
}

// we save the mode to the file ... a turnOff/turnOn is necessary
void AMConfiguration::setMode(json aMode) {
    std::ofstream out(modeFile);
    out << aMode.dump(2);
    out.close();
}

json AMConfiguration::getSettings() {
    loadSettingsFromInstrument();
    saveSettingsToFile();
    return json::parse(settings.dump());
}

json AMConfiguration::getMode() {
    return json::parse(mode.dump());
}