//
// Created by alex on 08/03/19.
//

#ifndef PIMCORE_EVENT_H
#define PIMCORE_EVENT_H

#include <mutex>
#include <condition_variable>
#include <chrono>
#include <future>

//class Event {
//private:
//    mutable std::mutex m_mutex;
//    mutable std::promise<void> promise;
////    mutable std::shared_future<void> future;
//
//public:
//    Event()  {
////        future = promise.get_future();
//    }
//
//    template<typename R, typename P>
//    bool Wait(const std::chrono::duration<R, P> &crRelTime) const {
//        std::unique_lock<std::mutex> lock(m_mutex);
//        std::shared_future<void> future = promise.get_future();
//        lock.unlock();
//
//        return std::future_status::ready == future.wait_for(crRelTime);
//    }
//
//    bool WaitFor(unsigned delay) {
//        return Wait(std::chrono::milliseconds(delay));
//    }
//
//    void Signal() {
//        std::unique_lock<std::mutex> lock(m_mutex);
//        promise.get_future().valid();
//        promise.set_value();
//    }
//
//    bool ResetEvent() {
//        std::unique_lock<std::mutex> lock(m_mutex);
//        promise = std::promise<void>();
//    }
//
//
//    //unblock
//    void SetEvent() { return Signal(); }
//
//};

class Event {
private:
    bool m_isSignaled;
    mutable std::mutex m_mutex;
    mutable std::condition_variable m_condition;

public:
    Event() : m_isSignaled(false) {}

    template<typename R, typename P>
    bool Wait(const std::chrono::duration<R, P> &crRelTime) const {
        std::unique_lock<std::mutex> lock(m_mutex);
        if (m_isSignaled) return true;
        // handle Spurious wakeup
        return m_condition.wait_for(lock, crRelTime, [&]() -> bool { return m_isSignaled; });
    }

    bool WaitFor(unsigned delay) {
        return Wait(std::chrono::milliseconds(delay));
    }

    bool Signal() {
        bool bWasSignalled;
        m_mutex.lock();
        bWasSignalled = m_isSignaled;
        m_isSignaled = true;
        m_mutex.unlock();
        m_condition.notify_all();
        return !bWasSignalled;
    }

    bool ResetEvent() {
        bool bWasSignalled;
        m_mutex.lock();
        bWasSignalled = m_isSignaled;
        m_isSignaled = false;
        m_mutex.unlock();
        return bWasSignalled;
    }


    //unblock
    bool SetEvent() { return Signal(); }

};

//class Event {
//private:
//    bool m_bFlag;
//    mutable std::mutex m_mutex;
//    mutable std::condition_variable m_condition;
//
//public:
//    Event() : m_bFlag(false) {}
//
//    template<typename R, typename P>
//    bool Wait(const std::chrono::duration<R, P> &crRelTime) const {
//        std::unique_lock<std::mutex> lock(m_mutex);
//        if (m_bFlag) return true;
//        return m_condition.wait_for(lock, crRelTime, [&]() -> bool { return m_bFlag; });
//    }
//
//    bool WaitFor(unsigned delay) {
//        return Wait(std::chrono::milliseconds(delay));
//    }
//
//    bool Signal() {
//        bool bWasSignalled;
//        m_mutex.lock();
//        bWasSignalled = m_bFlag;
//        m_bFlag = true;
//        m_mutex.unlock();
//        m_condition.notify_all();
//        return !bWasSignalled;
//    }
//
//    bool ResetEvent() {
//        bool bWasSignalled;
//        m_mutex.lock();
//        bWasSignalled = m_bFlag;
//        m_bFlag = false;
//        m_mutex.unlock();
//        return bWasSignalled;
//    }
//
//
//    //unblock
//    bool SetEvent() { return Signal(); }
//
//};

//class SetEvent {
//    Event &m_;
//
//public:
//    explicit SetEvent(Event &m)
//            : m_(m) {}
//
//    ~SetEvent() {
//        m_.SetEvent();
//    }
//};

#endif //PIMCORE_EVENT_H
