//
// Created by alex on 08/03/19.
//

#ifndef PIMCORE_UTIL_VECTOR_H
#define PIMCORE_UTIL_VECTOR_H

#include <vector>
#include <algorithm>
#include <math.h>
#include <thread>
#include <chrono>

class Util {
public:

    template<typename T>
    static
    void erase(std::vector<T> &vector, T const &value) {
        vector.erase(std::remove(begin(vector), end(vector), value), end(vector));
    }


    static bool SameValue(float a, float b, float tolerance) {
        return fabs(a - b) < tolerance;
    }

    static void Sleep(int milliseconds) {
        std::this_thread::sleep_for(std::chrono::milliseconds(milliseconds));
    }

    static int Sign(float value) {
        if (value < 0) return -1;
        return 1;
    }
};


#endif //PIMCORE_UTIL_VECTOR_H
