//
// Created by alex on 11/07/19.
//

#ifndef PSSERVER_AMCONFIGURATION_H
#define PSSERVER_AMCONFIGURATION_H

#include <string>
#include <sstream>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include "../../../server/json.hpp"

//#include "modules/pimcore/PamDefs_Defaults.h"
#include "../pimcore/PamDefs_Can.h"
#include "../pimcore/PamDefs_Defaults.h"


using json = nlohmann::json;

// Short alias for this namespace
namespace pt = boost::property_tree;

class AMConfiguration {
public:
    AMConfiguration();
    void initialize();

    json getSettings();
    json getMode();

    void setSettings(json settings);
    void setMode(json mode);
    void flash();

    std::string getUnitName();

    bool isStub();
    bool isStubBoard1();
    bool isStubBoard2();
    bool isStubBoard3();

    bool settingsFileExists();

    Focus_LogicalWellPos_t getFocusLogicalWellPos();

    std::shared_ptr<std::map<int, std::string>> getErrorMsgs();

private:
    std::string settingsFile;
    std::string modeFile;
    json settings;
    json mode;


    bool modeFileExists();
    void loadConfigFromFile();
    void validateSettings(json & config);
    void loadSettingsFromInstrument();
    void saveSettingsToFile();
    void saveModeToFile();
    void applySettings();
    void configFromString(std::string &content);
};


#endif //PSSERVER_AMCONFIGURATION_H
