//
// Created by alex on 25/11/2019.
//

#ifndef PSSERVER_ENABLEBOARD_H
#define PSSERVER_ENABLEBOARD_H

#endif //PSSERVER_ENABLEBOARD_H
#include <iostream>
#include "../pimcore/PamStationFactory.h"
#include "../pimcore/DefaultGenerator.h"
#include "../pimcore/PamStation.h"

class DisableAllBoards
{
public:
    DisableAllBoards() {
        std::cout << "DisableAllBoards" << std::endl;
        PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->enableAllBoards(false);
    }

    ~DisableAllBoards() {
        std::cout << "EnableAllBoards" << std::endl;
        PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->enableAllBoards(true);
    }
};