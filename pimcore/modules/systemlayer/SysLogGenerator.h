//---------------------------------------------------------------------------
#ifndef SysLogGeneratorH
#define SysLogGeneratorH
//---------------------------------------------------------------------------
#include "../pimcore/LogGenerator.h"         //for inheritance (interface)
#include "SysEventSource.h"   //for inheritance (base-implementation)



class SysLogGenerator: virtual public LogGenerator, virtual public SysEventSource
{
  public:
   SysLogGenerator();
  ~SysLogGenerator();

  virtual void    addLogHandler(LogHandler * h);
  virtual void removeLogHandler(LogHandler * h);

  void OnLog(const std::string& strWhen, const std::string& strWho, const std::string& strWhat);
  virtual void logInfo(std::string aMsg);
  virtual void logError(std::string aMsg);

  private:
      LogHandler * handler;
      static int iInstanceCounter;   //for singleton behaviour test

};



#endif //SysLogGeneratorH
