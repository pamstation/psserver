//---------------------------------------------------------------------------


#pragma hdrstop

#include <cstring>
#include "CCMImage.h"   //for header-file
//#include "drivers/tiff/ImageIO.h"
#include "tiffiop.h"
#include <math.h>
#include <iostream>
#include <iomanip>
//---------------------------------------------------------------------------

#pragma package(smart_init)


CCMImage::CCMImage() {
    initPropertiesCCMImage();  //reset properties
}

CCMImage::~CCMImage() {
    //first delete then reset properties because of  value of m_pArrayOfPixels
    deleteArrayOfPixels(); //free memory,
    initPropertiesCCMImage(); //reset properties
}

void CCMImage::initPropertiesCCMImage() {
    m_pArrayOfPixels = nullptr;
//  m_pStreamOfPixels= nullptr             ;
    m_iSizeX = 0;
    m_iSizeY = 0;
    m_iBitsPerPixel = 0;
//m_rGain           = 0.0             ;
    m_dateDate = std::time(nullptr);
    m_uExposureTime = 0;
    m_eFilter = eFilterNoUnknown;
//m_rFocusPosition  = 0.0             ;
    m_eWell = eWellNoUnknown;
    m_uMinValue = 0;
    m_uMaxValue = 0;
    m_uAverageValue = 0.0;
    m_uSaturationLimit = 0;
    m_uNumSaturated = 0;
//m_rFocusValue     = 0.0             ;

    m_fImageStatisticsCalulated = false;
}


//---------------------------------------------------------------------

//unsigned  CCMImage::getPixel(int x, int y) { return 0               ; };

unsigned short *CCMImage::getArrayOfPixels() { return m_pArrayOfPixels; };

int CCMImage::getSizeX() { return m_iSizeX; };

int CCMImage::getSizeY() { return m_iSizeY; };

unsigned CCMImage::getSaturationLimit() { return m_uSaturationLimit; };  //4095
int CCMImage::getBitsPerPixel() { return m_iBitsPerPixel; };  //12
//float         CCMImage::getGain() 			       { return m_rGain            ; };
std::time_t CCMImage::getDate() { return m_dateDate; };

unsigned CCMImage::getExposureTime() { return m_uExposureTime; };

EFilterNo CCMImage::getFilter() { return m_eFilter; };

//float         CCMImage::getFocusPosition() 	   { return m_rFocusPosition   ; };
EWellNo CCMImage::getWell() { return m_eWell; };


unsigned CCMImage::getMinValue() {
    if (!m_fImageStatisticsCalulated) calculateImageStatistics();
    return m_uMinValue;
}

unsigned CCMImage::getMaxValue() {
    if (!m_fImageStatisticsCalulated) calculateImageStatistics();
    return m_uMaxValue;
}

unsigned CCMImage::getAverageValue() {
    if (!m_fImageStatisticsCalulated) calculateImageStatistics();
    return m_uAverageValue;
}

unsigned CCMImage::getNumSaturated() {
    if (!m_fImageStatisticsCalulated) calculateImageStatistics();
    return m_uNumSaturated;
}

double CCMImage::getRMSContrast() {
    if (!m_fImageStatisticsCalulated) calculateImageStatistics();
    return m_rmsContrast;
}

double  CCMImage::getSumDiff(){
    if (m_fImageStatisticsCalulated) return m_sumDiff;

    unsigned short *pPixelPtr = m_pArrayOfPixels;

    double sumDiff = 0.0;

    for (int iY = 0; iY < m_iSizeY; iY++)
    {
        pPixelPtr = m_pArrayOfPixels + (iY * m_iSizeX);

        for (int iX = 0; iX < m_iSizeX-1; iX++)
        {
            sumDiff += abs(pPixelPtr[iX]- pPixelPtr[iX+1]);
        }
    }

    return sumDiff;
}
/*
float           CCMImage::getFocusValue()
{
  if (!m_fImageStatisticsCalulated) calculateImageStatistics();
  return m_rFocusValue;
}
*/
//-------------------------------------------------------------------------
unsigned short *CCMImage::createArrayOfPixels(unsigned iSizeX, unsigned iSizeY) {
    // allocated memory on heap to contain an array of pixels.
    // each pixel is 2 bytes (hence the short*)
    // returns the pointer m_pArrayOfPixels to this memory block (= first pixel)

    m_pArrayOfPixels = nullptr;
    m_pArrayOfPixels = (unsigned short *) malloc(iSizeX * iSizeY * sizeof(unsigned short));
    assert (m_pArrayOfPixels); //not nullptr

    //test-option: preset contents to a black image (black = 0, white = 65535)
    //memset(m_pArrayOfPixels, 0, iSizeX *iSizeY*sizeof(unsigned short));

    //test-option: preset contents to a pattern
//    unsigned iPeriodX = iSizeX/8;
//    unsigned iPeriodY = iSizeY/8;
//    for (unsigned iY=0; iY < iSizeY; iY++)
//    {
//        unsigned short* pWRow = m_pArrayOfPixels + iY*iSizeX;
//        double          dSinY = sin(M_PI*iY/iPeriodY);
//
//        for (unsigned iX=0; iX < iSizeX; iX++)
//        {
//            double         dSinX = sin(M_PI*iX/iPeriodX);
//            unsigned short  uVal = 4095*(dSinX*dSinY+1)/2;
//            pWRow[iX] = uVal;
//        }
//    }

    return m_pArrayOfPixels;
}

void CCMImage::deleteArrayOfPixels(void) {
    if (m_pArrayOfPixels) {
        free(m_pArrayOfPixels);
        m_pArrayOfPixels = nullptr;
    }
}

Image *CCMImage::duplicate() {
    return new CCMImage(*this);   //destruction must be done by the caller of duplicate
//NB because duplicate returns an Image* and the "normal" cleaning of memeory is done in he CCMImage-destructor,
// an virtual ~Image() {}; is added to the interface to solve this.

}

CCMImage::CCMImage(const CCMImage &src)     // copy constructor
{
    //copy the properties
//m_pArrayOfPixels  =  src.m_pArrayOfPixels      ;
    m_iSizeX = src.m_iSizeX;
    m_iSizeY = src.m_iSizeY;
    m_iBitsPerPixel = src.m_iBitsPerPixel;
//m_rGain           =  src.m_rGain               ;
    m_dateDate = src.m_dateDate;
    m_uExposureTime = src.m_uExposureTime;
    m_eFilter = src.m_eFilter;
//m_rFocusPosition  =  src.m_rFocusPosition      ;
    m_eWell = src.m_eWell;
    m_uMinValue = src.m_uMinValue;
    m_uMaxValue = src.m_uMaxValue;
    m_uAverageValue = src.m_uAverageValue;
    m_uSaturationLimit = src.m_uSaturationLimit;
    m_uNumSaturated = src.m_uNumSaturated;
//m_rFocusValue     =  src.m_rFocusValue         ;

    //copy flag indicated ImagesStatistics are already calculated
    m_fImageStatisticsCalulated = src.m_fImageStatisticsCalulated;

    //alloc memory for duplicate, m_pArrayOfPixels is pointer to newly created memory
    createArrayOfPixels(src.m_iSizeX, src.m_iSizeY);

    assert (m_pArrayOfPixels);
    assert (m_pArrayOfPixels != src.m_pArrayOfPixels); //make sure memory is created and on a new adress

    //copy source pixel array to duplicate pixel array
    memcpy(m_pArrayOfPixels, src.m_pArrayOfPixels, m_iSizeX * m_iSizeY * sizeof(unsigned short));

}

void CCMImage::save(std::string path, std::map<int, std::string> *props) {
    //open a tiff-file
    TIFF *tif = 0;
    tif = TIFFOpen(path.c_str(), "w");  //"w" = (over)write mode
    assert(tif); //not 0;

    TIFFMergeFieldInfo(tif, xTiffFieldInfo, 11);

    std::ostringstream datebuffer;
    datebuffer << m_dateDate;
    std::string aDateTime = datebuffer.str();

    //fill description txt
    std::ostringstream buffer;
    buffer << "Well:" << m_eWell << ", Filter:" << m_eFilter << ", Time:" << aDateTime;
    std::string strDescription = buffer.str();

    //fill the requested TIFF fields

    TIFFSetField(tif, TIFFTAG_SUBFILETYPE, 0x00);  // Tag 254 - NewSubFileType
    TIFFSetField(tif, TIFFTAG_IMAGEWIDTH, m_iSizeX);  // Tag 256 - ImageWitdth
    TIFFSetField(tif, TIFFTAG_IMAGELENGTH, m_iSizeY);  // Tag 257 - ImageLength
    TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE, 16 /*m_iBitsPerPixel*/  );  // Tag 258 - BitsPerSample
    TIFFSetField(tif, TIFFTAG_COMPRESSION, COMPRESSION_NONE);  // Tag 259 - Compression
    TIFFSetField(tif, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);  // Tag 262 - PhotometricInterpertation
    TIFFSetField(tif, TIFFTAG_IMAGEDESCRIPTION, (char *) strDescription.c_str());  // Tag 270 - ImageDescitption (328 byte long)
    TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, 1);  // Tag 277 - SamplePerPixel
    TIFFSetField(tif, TIFFTAG_ROWSPERSTRIP, 4);  // Tag 278 - RowsPerStrip
    TIFFSetField(tif, TIFFTAG_XRESOLUTION, 0.0f);  // Tag 282 - XResolution
    TIFFSetField(tif, TIFFTAG_YRESOLUTION, 0.0f);  // Tag 283 - YResolution
    TIFFSetField(tif, TIFFTAG_RESOLUTIONUNIT, RESUNIT_CENTIMETER);  // Tag 296 - ResolutionUnit
    TIFFSetField(tif, TIFFTAG_DATETIME, (char *) aDateTime.c_str());  // Tag 306 - DateTime
    TIFFSetField(tif, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
    /**/

    if (props) {
        TIFFSetField(tif, PG_TAG_BARCODE, ((*props)[PG_TAG_BARCODE]).c_str());
        TIFFSetField(tif, PG_TAG_COL, ((*props)[PG_TAG_COL]).c_str());
        TIFFSetField(tif, PG_TAG_CYCLE, ((*props)[PG_TAG_CYCLE]).c_str());
        TIFFSetField(tif, PG_TAG_EXPOSURETIME, ((*props)[PG_TAG_EXPOSURETIME]).c_str());
        TIFFSetField(tif, PG_TAG_FILTER, ((*props)[PG_TAG_FILTER]).c_str());
        TIFFSetField(tif, PG_TAG_INSTRUMENTTYPE, ((*props)[PG_TAG_INSTRUMENTTYPE]).c_str());
        TIFFSetField(tif, PG_TAG_ROW, ((*props)[PG_TAG_ROW]).c_str());
        TIFFSetField(tif, PG_TAG_TEMPERATURE, ((*props)[PG_TAG_TEMPERATURE]).c_str());
        TIFFSetField(tif, PG_TAG_TIMESTAMP, ((*props)[PG_TAG_TIMESTAMP]).c_str());
        TIFFSetField(tif, PG_TAG_UNIT, ((*props)[PG_TAG_UNIT]).c_str());
        TIFFSetField(tif, PG_TAG_PROTOCOLID, ((*props)[PG_TAG_PROTOCOLID]).c_str());
    }



    //add the image to row by row to tif

    //declare a temporary buffer to contain the number of bytes of 1 row
    auto *buf = (unsigned short *) malloc(
            m_iSizeX * sizeof(unsigned short)); // short because of 2 bytes per pixel

    for (int row = 0; row < m_iSizeY; ++row) {
        auto *pSrc = (unsigned short *) m_pArrayOfPixels; //let pSrc point to start of ArrayofPixels
        auto *pDst = (unsigned short *) buf; //let pDst point to start of temporary buf

        pSrc += row * m_iSizeX;                   //let pSrc point to the start of the wanted row

        for (int col = 0; col < m_iSizeX; ++col) {
            *pDst++ = *pSrc++;                  //copy the pixels in this row, pixel for pixel ( = 2 bytes for 2 bytes)
        }

        TIFFWriteScanline(tif, buf, row, 0);  //write this row to tiff
    }

    //flush & close file
    TIFFFlushData(tif);
    TIFFClose(tif);

    //free buf-memory
    if (buf) {
        free(buf);
        buf = nullptr;
    }


}

void CCMImage::save(std::string path) {
    save(path, nullptr);
}

//bool CCMImage::writeTIFF(char *chFileName) {
//    //open a tiff-file
//    TIFF *tif = 0;
//    tif = TIFFOpen(chFileName, "w");  //"w" = (over)write mode
//    assert(tif); //not 0;
//
//    //convert TDateTime Now() to char*
//    std::ostringstream buffer;
//
//    buffer << m_dateDate;
//
//    std::string stDateTime = buffer.str();
//
//
//    char *chDateTime = (char *) stDateTime.c_str();
//
//    //fill description txt
//
//    buffer.clear();
//
//    std::string strDescription = std::string("Well:") + m_eWell +
//                                 ", Filter:" + m_eFilter +
//                                 ", Time:" + m_dateDate.TimeString();
//    char *chDescription = (char *) strDescription.c_str();
//
//    //fill the requested TIFF fields
//    TIFFSetField(tif, TIFFTAG_SUBFILETYPE, 0x00);  // Tag 254 - NewSubFileType
//    TIFFSetField(tif, TIFFTAG_IMAGEWIDTH, m_iSizeX);  // Tag 256 - ImageWitdth
//    TIFFSetField(tif, TIFFTAG_IMAGELENGTH, m_iSizeY);  // Tag 257 - ImageLength
//    TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE, 16 /*m_iBitsPerPixel*/  );  // Tag 258 - BitsPerSample
//    TIFFSetField(tif, TIFFTAG_COMPRESSION, COMPRESSION_NONE);  // Tag 259 - Compression
//    TIFFSetField(tif, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);  // Tag 262 - PhotometricInterpertation
//    TIFFSetField(tif, TIFFTAG_IMAGEDESCRIPTION, chDescription);  // Tag 270 - ImageDescitption (328 byte long)
//    TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, 1);  // Tag 277 - SamplePerPixel
//    TIFFSetField(tif, TIFFTAG_ROWSPERSTRIP, 4);  // Tag 278 - RowsPerStrip
//    TIFFSetField(tif, TIFFTAG_XRESOLUTION, 0);  // Tag 282 - XResolution
//    TIFFSetField(tif, TIFFTAG_YRESOLUTION, 0);  // Tag 283 - YResolution
//    TIFFSetField(tif, TIFFTAG_RESOLUTIONUNIT, RESUNIT_CENTIMETER);  // Tag 296 - ResolutionUnit
//    TIFFSetField(tif, TIFFTAG_DATETIME, chDateTime);  // Tag 306 - DateTime
//    TIFFSetField(tif, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
//    /**/
//
//    //add the image to row by row to tif
//
//    //declare a temporary buffer to contain the number of bytes of 1 row
//    auto *buf = (unsigned short *) malloc(
//            m_iSizeX * sizeof(unsigned short)); // short because of 2 bytes per pixel
//
//    for (int row = 0; row < m_iSizeY; ++row) {
//        auto *pSrc = (unsigned short *) m_pArrayOfPixels; //let pSrc point to start of ArrayofPixels
//        auto *pDst = (unsigned short *) buf; //let pDst point to start of temporary buf
//
//        pSrc += row * m_iSizeX;                   //let pSrc point to the start of the wanted row
//
//        for (int col = 0; col < m_iSizeX; ++col) {
//            *pDst++ = *pSrc++;                  //copy the pixels in this row, pixel for pixel ( = 2 bytes for 2 bytes)
//        }
//
//        TIFFWriteScanline(tif, buf, row, 0);  //write this row to tiff
//    }
//
//    //flush & close file
//    TIFFFlushData(tif);
//    TIFFClose(tif);
//
//    //free buf-memory
//    if (buf) {
//        free(buf);
//        buf = nullptr;
//    }
//
//    return true;
//}

void CCMImage::calculateImageStatistics() {
    //in an image the pixels have a value between 0 (black) and (2^m_iBitsPerPixel -1 =) 4095 (white)

    // Calculates the following properties over the entire image area:
    m_uMinValue = 4095;  //init with max-value  (2^12-1)
    m_uMaxValue = 0;  //init with min-value
    m_uAverageValue = 0;  //init with min-value
    m_uNumSaturated = 0;  //init with min-value
    m_rmsContrast = 0.0;


    assert (m_iBitsPerPixel); //typicaly 12
    assert (m_pArrayOfPixels); //not nullptr


    //declarations
    unsigned short *pPixelPtr = m_pArrayOfPixels; // init at start of image
    unsigned uSumPixel = 0;                //init with mini-value
    unsigned iVal = 0;                //pixel value between 0 (black) and 4095 (white)

    for (int iY = 0; iY < m_iSizeY; iY++)    //for each line
    {
        pPixelPtr = m_pArrayOfPixels + (iY * m_iSizeX); //let pPixelPtr point to start of new line

        for (int iX = 0; iX < m_iSizeX; iX++)  //for each pixel in this line
        {
            iVal = pPixelPtr[iX];         //for readability, iVal contains pixel-value of iXst pixel in this line
            if (iVal < m_uMinValue) m_uMinValue = iVal;
            if (iVal > m_uMaxValue) m_uMaxValue = iVal;
            if (iVal >= m_uSaturationLimit) m_uNumSaturated++;  // either nul or NumStaturated is Num of Max_value

            uSumPixel += iVal;    //uSumPixel should be < 2^32 = 4,3 miljard
        } //end line
    }

    m_uAverageValue = uSumPixel / (m_iSizeX * m_iSizeY);

    m_rmsContrast = 0.0;

    for (int iY = 0; iY < m_iSizeY; iY++)    //for each line
    {
        pPixelPtr = m_pArrayOfPixels + (iY * m_iSizeX); //let pPixelPtr point to start of new line

        for (int iX = 0; iX < m_iSizeX; iX++)  //for each pixel in this line
        {
            iVal = pPixelPtr[iX];
            m_rmsContrast += (iVal - m_uAverageValue)*(iVal - m_uAverageValue);
        } //end line
    }

    m_rmsContrast = sqrt(m_rmsContrast/ (m_iSizeY*m_iSizeX));

    uint64_t sumDiff = 0;

    for (int iY = 0; iY < m_iSizeY; iY++)    //for each line
    {
        pPixelPtr = m_pArrayOfPixels + (iY * m_iSizeX); //let pPixelPtr point to start of new line

        for (int iX = 0; iX < m_iSizeX-1; iX++)  //for each pixel in this line
        {
            m_sumDiff += abs(pPixelPtr[iX]- pPixelPtr[iX+1]);
         } //end line
    }

    m_fImageStatisticsCalulated = true;
}

//TMemoryStream *CCMImage::createStreamOfPixels() {
//    //allocate memory, clean-up has to be done by user!!!
//    m_pStreamOfPixels = new TMemoryStream();
//
//    //TMemoryStream->Write(const void *Buffer, int Count), writes Count bytes from Buffer
//    //Buffer is (start of) ArrayOfPixels
//    // Count is size of Image = X*Y*sizePixel, each pixel is 2 bytes (hence the short*)
//    m_pStreamOfPixels->Write(m_pArrayOfPixels, m_iSizeX * m_iSizeY * sizeof(unsigned short));
//
//
//    return m_pStreamOfPixels;
//}
