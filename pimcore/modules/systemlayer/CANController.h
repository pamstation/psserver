//---------------------------------------------------------------------------

#ifndef CANControllerH
#define CANControllerH

#include "SysEventSource.h"  //inheritance (implementation base-class)
//#include "cpc.h"                  //for CANmsg declaration


//---------------------------------------------------------------------------
/*
* This class contains the commands that can be send to the hardware behind the CANBus:
* CONTROLLERS
* - Heatables
*   - WagonHeater 6x
*   - DispenseHeadHeater
* - Pressurables
*   - OverPressureSupplier
*   - UnderPressureSupplier
* - Positionables
*   - AspirateHead
*   - DispenseHeadPositioner 4x
* - HomablePositionables
*   - FilterPositioner
*   - FocusPositioner
*   - WagonPositionerX
*   - WagonPositionerY
* OTHER ITEMS
* - LED Unit 3*
* - PressureValves 12* + 2*  + sensors
* - AspiratePump             + sensor
*
*/
class CanInterface; //either choose to use "real hardware" or ...
class CANStub;      //simulated CANStub ,  defines: _STUB
class CANController : virtual public SysEventSource//SysChangeSource
{
public:
    CANController();

    ~CANController();

    //IO & CONTROLLER COMMANDS
    void
    setTarget(ECanObj, int);  ///<setter, use for switching an IO or passing a new setpoint (in [LU]) to a controller
    int getActual(ECanObj);  ///<getter, returns the actual value ([LU]) of a IO or controller

    //CONTROLLER COMMANDS
    void setEnabled(ECanObj, bool);  ///<setter, use for enabling / disabling a controller
    void goHome(ECanObj);  ///<setter, use for homing a homable controller
    void setSpeed(ECanObj, float);  ///<setter, use for passing a new speed/ramp ([LU/sample]) to a controller
    int getStatus(ECanObj);  ///<getter, returns the status of a controller (bitwise: enabled, ready, homed, error)
    int getTarget(ECanObj);  ///<getter, returns the setpoint ([LU]) of the controller
    int getError(ECanObj);  ///<getter, returns the error number of the controller

    //BOARD COMMANDS (ECanObj = eBoard1Ctrl, eBoard2Ctrl or eBoard3Ctrl )
    void setBoardStatus(ECanObj, bool); ///<setter, use for enabling / disabling a board
    bool getBoardStatus(ECanObj); ///<getter, use for checking board status (enabled/ disabled)
    void flashBoard(ECanObj); ///<orders board to flash RAM to EEPROM, use only if board is in state disabled

    //DATA STORE COMMANDS
    void setSensorCalibrationData(
            Sensor_Calibration_Data_t);   ///<for analog sensors, current sensors, encoders, (and aspirateHeadPositioner)
    Sensor_Calibration_Data_t getSensorCalibrationData(ECanObj);

    void setControllerParameters(Controllable_Settings_t);

    Controllable_Settings_t getControllerParameters(ECanObj);

    void setWagonLogicalPostions(Wagon_LogicalXYPos_t);

    Wagon_LogicalXYPos_t getWagonLogicalPositions(void);

    void setFilterLogicalPostions(Filter_LogicalPos_t);

    Filter_LogicalPos_t getFilterLogicalPositions(void);

    void setFocusLogicalPostions(Focus_LogicalPos_t);

    Focus_LogicalPos_t getFocusLogicalPositions(void);

    void setLEDIntensity(LEDIntensity_t);

    LEDIntensity_t getLEDIntensity(void);

    void setBoardData(int iPS12_nr);

    PS12_Version_t getBoardData(void);


private:
    static int iInstanceCounter;   //for singleton behaviour test

    int send_rcv_CanMsg(ECanObj, ECanCmnd, int iData);

    std::recursive_mutex m_pcsCANGuard;
    CanInterface *m_pCanInterface;    //for "real"
    CANStub *m_pCANStub;         //for simulations

    bool isStub();
    bool isStubBoard1();
    bool isStubBoard2();
    bool isStubBoard3();
};


#endif  //CANControllerH
