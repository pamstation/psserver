//---------------------------------------------------------------------------

#ifndef SysExceptionGeneratorH
#define SysExceptionGeneratorH
//---------------------------------------------------------------------------
#include "../pimcore/ExceptionGenerator.h"   //for inheritance (interface)
#include "SysEventSource.h"   //for inheritance (base-implementation)
#include <map>
#include <string>
#include <memory>

class  SysExceptionGenerator: virtual public ExceptionGenerator, virtual public SysEventSource
{
  public:
     SysExceptionGenerator();
    ~SysExceptionGenerator();       

    //INTERFACE (obsolete, ExceptionHandlers are not used anymore)
    virtual void    addExceptionHandler(ExceptionHandler * h);
    virtual void removeExceptionHandler(ExceptionHandler * h);

    //for throwing exceptions
    void onException(void* source, int iErrorNo, EErrorType eErrType, const std::string& strExtraInfo);

//    void initializeErrorMsg();
    void initializeErrorMsg(std::shared_ptr<std::map<int, std::string>> exMsg);

  private:
    static int iInstanceCounter;  //for singleton behaviour test
    std::map<int, std::string> exceptionMsg;


};



#endif  // SysExceptionGeneratorH

