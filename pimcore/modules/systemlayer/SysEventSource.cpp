//---------------------------------------------------------------------------



#pragma hdrstop

#include <algorithm>

#include "../utils/util.h"
#include "SysEventSource.h"         //for header-file
//---------------------------------------------------------------------------

#pragma package(smart_init)

SysEventSource::SysEventSource() {
    //---INITIALISE AUXILIARIES FOR LISTENERS-----------
//  m_pcsListenerGuard            = new TCriticalSection; //needed for listener critical section
//  m_pListOfListeners            = new TList;            //needed for listener critical section
//
//  assert( m_pcsListenerGuard    );
//  assert( m_pListOfListeners    );

}

SysEventSource::~SysEventSource() {
//  FREEPOINTER( m_pcsListenerGuard   );
//  FREEPOINTER( m_pListOfListeners   );
}

// SysEventSource contains for each hardware device one (and only one)  ListOfListeners
// An addListener in a base-class results in adding the pointer to a callback-function on a list of void*
// Because a list contians in fact void*, some casting has to be done:
// Each listener is added as an EventHandler*, the base-class of all listeners
//
//using this pointer in an "onChanged-method" requires:
//  1 casting the void* back to a an EventHandler*
//  2 casting the EventHandler* back to the classlistener*
//  3 using this class*
//because of the use of EventHandler, this class may not be emty and needs a dummy
//
//use the lowest class to add a listener to the list of listeners
//for test reasons it is possible to add a listener from a higher level
//f.e. addWagonHeaterListener adss a listener, but its possible for addHeatableListener and addControllableListener too

void SysEventSource::addListenerToList(EventHandler *h) {
    std::lock_guard<std::recursive_mutex> guard(m_pcsListenerGuard);
    m_pListOfListeners.push_back(h);
}

void SysEventSource::removeListenerFromList(EventHandler *eh) {
    std::lock_guard<std::recursive_mutex> guard(m_pcsListenerGuard);
    Util::erase(m_pListOfListeners, eh);
}

