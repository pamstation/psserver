//---------------------------------------------------------------------------
#ifndef SysEventSourceH
#define SysEventSourceH

//---------------------------------------------------------------------------
#include "../pimcore/EventSource.h"   //for inheritance
#include <mutex>
#include <vector>

class EventHandler;

class SysEventSource: virtual public EventSource
{
 public:
    SysEventSource();
   ~SysEventSource();

  protected:

    void  addListenerToList    (EventHandler* h);
    void removeListenerFromList(EventHandler* h);

    std::recursive_mutex     m_pcsListenerGuard   ; //handle to listener critical section
    std::vector<EventHandler*> m_pListOfListeners   ; //handle to listenerslist

};


#endif //SysEventSourceH
