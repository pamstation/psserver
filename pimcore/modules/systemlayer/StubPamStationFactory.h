//---------------------------------------------------------------------------

#ifndef StubPamStationFactoryH
#define StubPamStationFactoryH
//---------------------------------------------------------------------------
#include "../pimcore/PamStationFactory.h"   //for inheritance
//---------------------------------------------------------------------------
class StubPamStation;
class StubPamStationFactory : virtual public PamStationFactory
{
  public:
    static StubPamStation* getCurrentPamStation();
    static void            shutdown();
    
  private:
    static StubPamStation* pCurrent;
};

#endif  //StubPamStationFactoryH
