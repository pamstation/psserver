//---------------------------------------------------------------------------

#ifndef SysChangeSourceH
#define SysChangeSourceH


//---------------------------------------------------------------------------
#include "../pimcore/ChangeSource.h"         //for inheritance
#include "SysEventSource.h"   //for inheritance

class SysExceptionGenerator;
class DefaultGenerator;
class CANController;

class SysChangeSource: virtual public ChangeSource, virtual public SysEventSource
{
 public:

      SysChangeSource();
      ~SysChangeSource();

  protected:
    DefaultGenerator     * m_pDefaultGenerator  ;
    SysExceptionGenerator* m_pExceptionGenerator;
    CANController        * m_pCAN               ;

    ECanObj                 m_eObj              ;  //identifier, initialised in all hardware classes

  private:
    void initialise(void);
    void terminate (void);


};

#endif   //SysChangeSourceH
