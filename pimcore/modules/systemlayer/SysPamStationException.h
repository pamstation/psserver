
#ifndef SysPamStationExceptionH
#define SysPamStationExceptionH
//---------------------------------------------------------------------------
#include <string>
#include "../pimcore/PamStationException.h"   //for inheritance

class SysPamStationException : virtual public PamStationException {
public:
    SysPamStationException(int id, std::string description, void *source);

    ~SysPamStationException();

    virtual int getId();

    // Returns the unique identifier of the error.
    virtual void setId(int id);
    // Sets the unique error identifier

    virtual std::string
    getDescription();

    // Returns the description of the failure. Should be Unicode compatible.
    virtual void setDescription(std::string description);
    //Sets the description for the error

    virtual void *getSourceObject();

    // Returns the stored object.
    virtual void setSourceObject(void *sourceObject);
    // Stores the object, that caused the error

    virtual SysPamStationException *duplicate();
    // Returns a copy of this (should be deleted by the caller)


protected:
    int m_iID;
    std::string m_strDescription;
    void *m_pSourceObject;

};


#endif  // SysPamStationExceptionH
