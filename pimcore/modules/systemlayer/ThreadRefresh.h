//---------------------------------------------------------------------------

#ifndef ThreadRefreshH
#define ThreadRefreshH

//---------------------------------------------------------------------------
//#include <Classes.hpp>
//---------------------------------------------------------------------------
class StubPamStation;

class ThreadRefresh {

public:
    ThreadRefresh();

    void start();

    void stop();

    StubPamStation *m_pParent;
    unsigned m_uThreadDelay;
    unsigned numError;
    bool running;
    std::thread thread;
};
//---------------------------------------------------------------------------
#endif
