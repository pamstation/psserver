//---------------------------------------------------------------------------
#pragma hdrstop

#include "SysPamStationException.h"  //for header-file

//---------------------------------------------------------------------------

#pragma package(smart_init)


SysPamStationException::SysPamStationException(int id, std::string description, void *source) {
    this->m_iID = id;
    this->m_strDescription = description;
    this->m_pSourceObject = source;
}

SysPamStationException::~SysPamStationException() {

}


int SysPamStationException::getId() {
    return this->m_iID;
}

void SysPamStationException::setId(int id) {
    this->m_iID = id;
}


std::string SysPamStationException::getDescription() {
    return this->m_strDescription;
}

void SysPamStationException::setDescription(std::string description) {
    this->m_strDescription = description;


}

void *SysPamStationException::getSourceObject() {
    return this->m_pSourceObject;
}

void SysPamStationException::setSourceObject(void *sourceObject) {
    this->m_pSourceObject = sourceObject;
}

SysPamStationException *SysPamStationException::duplicate() {
    SysPamStationException *aCopy = new SysPamStationException(m_iID, m_strDescription, m_pSourceObject);
    return aCopy;
}

