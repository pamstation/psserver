//---------------------------------------------------------------------------




#pragma hdrstop

#include "SysChangeSource.h"        //for header-file

#include "../pimcore/PamStationFactory.h"          //for obtaining PamStation object
#include "../pimcore/PamStation.h"                 //for obtaining PamStation object


#include "../pimcore/DefaultGenerator.h"            //for obtaining object

#include "SysExceptionGenerator.h"  //for obtaining object

#include "StubPamStation.h"         //for obtaining StubPamStation object
#include "CANController.h"          //for obtaining CANController
//---------------------------------------------------------------------------

#pragma package(smart_init)

SysChangeSource::SysChangeSource()
{
  initialise();
}
SysChangeSource::~SysChangeSource()
{
  terminate();
}
//---------
void SysChangeSource::initialise(void)
{
  //Get Pointers from PamStation to Generators like LogGenerator, ExceptionGenerator, ImageGenerator(?), DataStore
  PamStation           * pPam                  = PamStationFactory::getCurrentPamStation();

  //DataStore:
  m_pDefaultGenerator =  pPam->getDefaultGenerator();
  assert(m_pDefaultGenerator);


  //ExceptionGenerator:
  ExceptionGenerator   * pExc                  = pPam->getExceptionGenerator();
  m_pExceptionGenerator = dynamic_cast<SysExceptionGenerator*>(pExc);
  assert (m_pExceptionGenerator);

  //ImageGenerator: for now done in PropCCD-class

  //LogGenerator: to be implemented, if required

  //CANController, (get from StubPamStation)
  StubPamStation* pStubPamStation = dynamic_cast<StubPamStation*>(pPam);
  assert(pStubPamStation);

  m_pCAN = pStubPamStation->getCANController();
  assert(m_pCAN);
}
void SysChangeSource::terminate (void)
{
  m_pExceptionGenerator = nullptr; //actual delete is done in class StubPamStation
  m_pCAN                = nullptr; //actual delete is done in class StubPamStation
  m_pDefaultGenerator   = nullptr; //actual delete is done in class StubPamStation
}
