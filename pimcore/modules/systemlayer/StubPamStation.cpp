//---------------------------------------------------------------------------


#pragma hdrstop

#include "../utils/util.h"
#include "StubPamStation.h"   //for header-file
//Interface Generators
#include "SysDefaultGenerator.h"
#include "SysExceptionGenerator.h"
#include "SysLogGenerator.h"
//Interface Hardware
#include "../propertylayer/PropAspirateHeadPositioner.h"
#include "../propertylayer/PropAspiratePump.h"
#include "../propertylayer/PropCCD.h"
#include "../propertylayer/PropDispenseHeadHeater.h"
#include "../propertylayer/PropDispenseHeadPositioner.h"
#include "../propertylayer/PropDisposable.h"
#include "../propertylayer/PropFilterPositioner.h"
#include "../propertylayer/PropFocusPositioner.h"
#include "../propertylayer/PropLEDUnit.h"
#include "../propertylayer/PropOverPressureSupplier.h"
#include "../propertylayer/PropUnderPressureSupplier.h"
#include "../propertylayer/PropUnloadButton.h"
#include "../propertylayer/PropIncubationChamberHeater.h"
#include "../propertylayer/PropWagonHeater.h"
#include "../propertylayer/PropWagonPositioner.h"
#include "../propertylayer/PropWagonXYPositioner.h"
#include "../propertylayer/PropLeds.h"

//Non Interface Items
#include "CANController.h"
#include "CanInterface.h"
#include "ThreadRefresh.h"

#include "../pimcore/PamDefs_InitialValues.h"     //for initial values of initalise_protocolSettings
#include "../utils/AMConfiguration.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)


int StubPamStation::iInstanceCounter = 0;

StubPamStation::StubPamStation() {
    //NB during construction of a PamStation, don't call classes which need a pointer to a PamStation:
    // it will be constructed, during which this class is called again which will construct a PamStation etc
    //especially the LogGenerator

    m_configuration = new AMConfiguration();

    //Non Interface Items
    m_pCANController = nullptr;
    m_pCanInterface = nullptr;
    //Interface Generators
    m_pSysDefaultGenerator = nullptr;
    m_pSysExceptionGenerator = nullptr;
//    m_pSysImageGenerator = nullptr;
    m_pSysLogGenerator = nullptr;
    //Interface Hardware
    m_pPropAspirateHeadPositioner = nullptr;
    m_pPropAspiratePump = nullptr;
    m_pPropCCD = nullptr;
    m_pPropDispenseHeadHeater = nullptr;
    m_pPropLeds = nullptr;
    for (int i = eHeadMin; i < eHeadMax; i++)      //array[0, 1,2,3,4]
    { m_pPropDispenseHeadPositioner[i] = nullptr; }
    m_pPropDisposable = nullptr;
    m_pPropFilterPositioner = nullptr;
    m_pPropFocusPositioner = nullptr;
    for (int i = eLEDNoMin; i < eLEDNoMax; i++)    //array[0, 1,2,3]
    { m_pPropLEDUnit[i] = nullptr; }
    m_pPropOverPressureSupplier = nullptr;
    m_pPropUnderPressureSupplier = nullptr;
    m_pPropUnloadButton = nullptr;
    for (int i = eIncChmbrHtrMin; i < eIncChmbrHtrMax; i++)    //array[0,1,2,3,4,5,6]
    { m_pPropIncubationChamberHeater[i] = nullptr; }
    m_pPropWagonHeater = nullptr;
    for (int i = ePositionerMin; i < ePositionerMax; i++)    //array[0,1,2]
    { m_pPropWagonPositioner[i] = nullptr; }
    m_pPropWagonXYPositioner = nullptr;

    ++iInstanceCounter;
    assert (iInstanceCounter == 1);  //only one instance of this class allowed (singleton)

    m_uRefreshDelay = REFRESH_DELAY;              //[ms],  0 means loop with clock speed (for simulation reasons)
    m_uWaitTime = WAIT_DELAY;

    //START REFRESH PROPERTIES LOOP
    m_pThreadRefresh = new ThreadRefresh;
    m_pThreadRefresh->m_pParent = this;
    if (m_configuration->isStub()) {
        m_pThreadRefresh->m_uThreadDelay = 10; //[ms], 0 means loop with clock speed
    } else {
        m_pThreadRefresh->m_uThreadDelay = m_uRefreshDelay; //[ms], 0 means loop with clock speed
    }
}

void StubPamStation::initializeCanInterface() {
    m_pThreadRefresh->start();
}


StubPamStation::~StubPamStation() {
    this->onAbort();

    //STOP REFRESH PROPERTIES LOOP
    m_pThreadRefresh->stop();
    FREEPOINTER(m_pThreadRefresh);

    FREEPOINTER(m_pPropAspirateHeadPositioner);
    FREEPOINTER(m_pPropAspiratePump);
    FREEPOINTER(m_pPropCCD);
    FREEPOINTER(m_pPropLeds);
    FREEPOINTER(m_pPropDispenseHeadHeater);
    for (int i = eHeadMin; i < eHeadMax; i++)      //array[0, 1,2,3,4]
    { FREEPOINTER(m_pPropDispenseHeadPositioner[i]); }
    FREEPOINTER(m_pPropDisposable);
    FREEPOINTER(m_pPropFilterPositioner);
    FREEPOINTER(m_pPropFocusPositioner);
    for (int i = eLEDNoMin; i < eLEDNoMax; i++)     //array[0, 1,2,3]
    { FREEPOINTER(m_pPropLEDUnit[i]); }
    FREEPOINTER(m_pPropOverPressureSupplier);
    FREEPOINTER(m_pPropUnderPressureSupplier);
    FREEPOINTER(m_pPropUnloadButton);
    for (int i = eIncChmbrHtrMin; i < eIncChmbrHtrMax; i++)    //array[0,1,2,3,4,5,6]
    { FREEPOINTER(m_pPropIncubationChamberHeater[i]); }
    FREEPOINTER(m_pPropWagonHeater);
    for (int i = ePositionerMin; i < ePositionerMax; i++)    //array[0,1,2]
    { FREEPOINTER(m_pPropWagonPositioner[i]); }
    FREEPOINTER(m_pPropWagonXYPositioner);

    //NonInteface Items
    FREEPOINTER(m_pCANController);
    FREEPOINTER(m_pCanInterface);

    //NB LogGenerator: clean up last, because of possible logging in other clean ups
    FREEPOINTER(m_pSysDefaultGenerator);
    FREEPOINTER(m_pSysExceptionGenerator);
//    FREEPOINTER(m_pSysImageGenerator);
    FREEPOINTER(m_pSysLogGenerator);

//    FREEPOINTER(m_pGuard);
    FREEPOINTER(m_configuration);

    assert (--iInstanceCounter == 0);  //only one instance of this class allowed (singleton)
}

//-------------------------------------------------------------------------
AMConfiguration *StubPamStation::getConfig() {
    if (!m_configuration) {
        std::lock_guard<std::recursive_mutex> guard(m_pGuard);
        m_configuration = new AMConfiguration();
    }
    assert(m_configuration);
    return m_configuration;
}

//Generators
DefaultGenerator *StubPamStation::getDefaultGenerator() {
    if (!m_pSysDefaultGenerator) {
        std::lock_guard<std::recursive_mutex> guard(m_pGuard);

        m_pSysDefaultGenerator = new SysDefaultGenerator();

    }
    assert(m_pSysDefaultGenerator);
    return m_pSysDefaultGenerator;
}

//----------------------------------------------------
ExceptionGenerator *StubPamStation::getExceptionGenerator() {
    if (!m_pSysExceptionGenerator) {

        std::lock_guard<std::recursive_mutex> guard(m_pGuard);

        m_pSysExceptionGenerator = new SysExceptionGenerator();
        m_pSysExceptionGenerator->initializeErrorMsg(this->m_configuration->getErrorMsgs());

    }
    assert(m_pSysExceptionGenerator);
    return m_pSysExceptionGenerator;
}

//-----------------------------------------------------
//ImageGenerator *StubPamStation::getImageGenerator() {
//    if (!m_pSysImageGenerator) {
//        std::lock_guard<std::recursive_mutex> guard(m_pGuard);
//        m_pSysImageGenerator = new SysImageGenerator();
//    }
//    assert(m_pSysImageGenerator);
//    return m_pSysImageGenerator;
//}

//-------------------------------------------------
LogGenerator *StubPamStation::getLogGenerator() {
    if (!m_pSysLogGenerator) {
        std::lock_guard<std::recursive_mutex> guard(m_pGuard);
        m_pSysLogGenerator = new SysLogGenerator();
    }
    assert(m_pSysLogGenerator);
    return m_pSysLogGenerator;
}
//--------------------------------------------------

//Hardware
AspirateHeadPositioner *StubPamStation::getAspirateHeadPositioner() {
    if (!m_pPropAspirateHeadPositioner) {
        std::lock_guard<std::recursive_mutex> guard(m_pGuard);
        m_pPropAspirateHeadPositioner = new PropAspirateHeadPositioner();
    }
    assert(m_pPropAspirateHeadPositioner);
    return m_pPropAspirateHeadPositioner;
}

AspiratePump *StubPamStation::getAspiratePump() {
    if (!m_pPropAspiratePump) {
        std::lock_guard<std::recursive_mutex> guard(m_pGuard);
        m_pPropAspiratePump = new PropAspiratePump();
    }
    assert(m_pPropAspiratePump);
    return m_pPropAspiratePump;
}

CCD *StubPamStation::getCCD() {
    if (!m_pPropCCD) {
        std::lock_guard<std::recursive_mutex> guard(m_pGuard);
        m_pPropCCD = new PropCCD();
    }
    assert(m_pPropCCD);
    return m_pPropCCD;
}

DispenseHeadHeater *StubPamStation::getDispenseHeadHeater() {
    if (!m_pPropDispenseHeadHeater) {
        std::lock_guard<std::recursive_mutex> guard(m_pGuard);
        m_pPropDispenseHeadHeater = new PropDispenseHeadHeater();
    }
    assert(m_pPropDispenseHeadHeater);
    return m_pPropDispenseHeadHeater;
}

DispenseHeadPositioner *StubPamStation::getDispenseHeadPositioner(EDispenseHeadNo eHeadNo) {
    assert (eHeadNo > eHeadMin && eHeadNo < eHeadMax);
    //eHead1-4 are put in array[0, 1,2,3,4] , array[0] not used
    if (!m_pPropDispenseHeadPositioner[eHeadNo]) {
        std::lock_guard<std::recursive_mutex> guard(m_pGuard);
        m_pPropDispenseHeadPositioner[eHeadNo] = new PropDispenseHeadPositioner(eHeadNo);
    }
    assert(m_pPropDispenseHeadPositioner[eHeadNo]);
    return m_pPropDispenseHeadPositioner[eHeadNo];
}

Disposable *StubPamStation::getDisposable() {
    if (!m_pPropDisposable) {
        std::lock_guard<std::recursive_mutex> guard(m_pGuard);
        m_pPropDisposable = new PropDisposable();
    }
    assert(m_pPropDisposable);
    return m_pPropDisposable;
}

PropDisposable *StubPamStation::getPropDisposable() {
    if (!m_pPropDisposable) {
        std::lock_guard<std::recursive_mutex> guard(m_pGuard);
        m_pPropDisposable = new PropDisposable();
    }
    assert(m_pPropDisposable);
    return m_pPropDisposable;
}

PropLeds *StubPamStation::getLeds() {
    if (!m_pPropLeds) {
        std::lock_guard<std::recursive_mutex> guard(m_pGuard);
        m_pPropLeds = new PropLeds();
    }
    assert(m_pPropLeds);
    return m_pPropLeds;
}

FilterPositioner *StubPamStation::getFilterPositioner() {
    if (!m_pPropFilterPositioner) {
        std::lock_guard<std::recursive_mutex> guard(m_pGuard);
        m_pPropFilterPositioner = new PropFilterPositioner();
    }
    assert(m_pPropFilterPositioner);
    return m_pPropFilterPositioner;
}

FocusPositioner *StubPamStation::getFocusPositioner() {
    if (!m_pPropFocusPositioner) {
        std::lock_guard<std::recursive_mutex> guard(m_pGuard);
        m_pPropFocusPositioner = new PropFocusPositioner();
    }
    assert(m_pPropFocusPositioner);
    return m_pPropFocusPositioner;
}

LEDUnit *StubPamStation::getLEDUnit(ELEDNo eLedNo) {
    assert (eLedNo > eLEDNoMin && eLedNo < eLEDNoMax);

    //eLedNo1-3 are put in array[0, 1,2,3] , array[0] not used
    if (!m_pPropLEDUnit[eLedNo]) {
        std::lock_guard<std::recursive_mutex> guard(m_pGuard);
        m_pPropLEDUnit[eLedNo] = new PropLEDUnit(eLedNo);
    }
    assert(m_pPropLEDUnit[eLedNo]);
    return m_pPropLEDUnit[eLedNo];
}

OverPressureSupplier *StubPamStation::getOverPressureSupplier() {
    if (!m_pPropOverPressureSupplier) {
        std::lock_guard<std::recursive_mutex> guard(m_pGuard);
        m_pPropOverPressureSupplier = new PropOverPressureSupplier();

    }
    assert(m_pPropOverPressureSupplier);
    return m_pPropOverPressureSupplier;
}

UnderPressureSupplier *StubPamStation::getUnderPressureSupplier() {
    if (!m_pPropUnderPressureSupplier) {
        std::lock_guard<std::recursive_mutex> guard(m_pGuard);
        m_pPropUnderPressureSupplier = new PropUnderPressureSupplier();

    }
    assert(m_pPropUnderPressureSupplier);
    return m_pPropUnderPressureSupplier;
}

UnloadButton *StubPamStation::getUnloadButton() {
    if (!m_pPropUnloadButton) {
        std::lock_guard<std::recursive_mutex> guard(m_pGuard);
        m_pPropUnloadButton = new PropUnloadButton();
    }
    assert(m_pPropUnloadButton);
    return m_pPropUnloadButton;
}

//--
IncubationChamberHeater *StubPamStation::getIncubationChamberHeater(EIncubationChamberHeater eIncChmbrHtr) {
    std::lock_guard<std::recursive_mutex> guard(m_pGuard);
    assert (eIncChmbrHtr > eIncChmbrHtrMin && eIncChmbrHtr < eIncChmbrHtrMax);
    //ePositionerX&Y are put in array[0,1,2,3,4,5,6] , array[0] not used
    if (!m_pPropIncubationChamberHeater[eIncChmbrHtr]) {
        m_pPropIncubationChamberHeater[eIncChmbrHtr] = new PropIncubationChamberHeater(eIncChmbrHtr);
    }
    assert(m_pPropIncubationChamberHeater[eIncChmbrHtr]);
    return m_pPropIncubationChamberHeater[eIncChmbrHtr];
}
//--------


WagonHeater *StubPamStation::getWagonHeater() {
    if (!m_pPropWagonHeater) {
        std::lock_guard<std::recursive_mutex> guard(m_pGuard);
        m_pPropWagonHeater = new PropWagonHeater();
    }
    assert(m_pPropWagonHeater);
    return m_pPropWagonHeater;
}

WagonPositioner *StubPamStation::getWagonPositioner(EWagonPositioner ePositioner) {
    assert (ePositioner > ePositionerMin && ePositioner < ePositionerMax);
    //ePositionerX&Y are put in array[0,1,2] , array[0] not used
    if (!m_pPropWagonPositioner[ePositioner]) {
        std::lock_guard<std::recursive_mutex> guard(m_pGuard);
        m_pPropWagonPositioner[ePositioner] = new PropWagonPositioner(ePositioner);

    }
    assert(m_pPropWagonPositioner[ePositioner]);
    return m_pPropWagonPositioner[ePositioner];
}

//--
WagonXYPositioner *StubPamStation::getWagonXYPositioner() {
    if (!m_pPropWagonXYPositioner) {
        std::lock_guard<std::recursive_mutex> guard(m_pGuard);
        m_pPropWagonXYPositioner = new PropWagonXYPositioner();

    }
    assert(m_pPropWagonXYPositioner);
    return m_pPropWagonXYPositioner;
}
//--------------------------------

CANController *StubPamStation::getCANController() {
    if (!m_pCANController) {
        std::lock_guard<std::recursive_mutex> guard(m_pGuard);
        m_pCANController = new CANController();

    }
    assert(m_pCANController);
    return m_pCANController;
}

CanInterface *StubPamStation::getCanInterface() {
    if (!m_pCanInterface) {
        std::lock_guard<std::recursive_mutex> guard(m_pGuard);
        m_pCanInterface = new CanInterface();

    }
    assert(m_pCanInterface);
    return m_pCanInterface;
}


/**/
void StubPamStation::updateProperties(void) {

    std::lock_guard<std::recursive_mutex> guard(m_pGuard);
    if (m_pPropAspirateHeadPositioner) m_pPropAspirateHeadPositioner->updateProperties();
    Util::Sleep(m_uWaitTime);
    if (m_pPropAspiratePump) m_pPropAspiratePump->updateProperties();
    Util::Sleep(m_uWaitTime);
    //if  (m_pPropCCD)  m_pPropCCD->updateProperties();
    Util::Sleep(m_uWaitTime);
    if (m_pPropDispenseHeadHeater) m_pPropDispenseHeadHeater->updateProperties();
    Util::Sleep(m_uWaitTime);
    //if  (m_pPropDisposable)  m_pPropDisposable->updateProperties();
    Util::Sleep(m_uWaitTime);
    if (m_pPropFilterPositioner) m_pPropFilterPositioner->updateProperties();
    Util::Sleep(m_uWaitTime);
    if (m_pPropFocusPositioner) m_pPropFocusPositioner->updateProperties();
    Util::Sleep(m_uWaitTime);
    if (m_pPropOverPressureSupplier) m_pPropOverPressureSupplier->updateProperties();
    Util::Sleep(m_uWaitTime);
    if (m_pPropUnderPressureSupplier) m_pPropUnderPressureSupplier->updateProperties();
    Util::Sleep(m_uWaitTime);
    //if  (m_pPropUnloadButton)  m_pPropUnloadButton->updateProperties();
    Util::Sleep(m_uWaitTime);
    if (m_pPropWagonHeater) m_pPropWagonHeater->updateProperties();
    Util::Sleep(m_uWaitTime);
    if (m_pPropWagonXYPositioner) m_pPropWagonXYPositioner->updateProperties();


    for (int i = eHeadMin; i < eHeadMax; i++) {
        if (m_pPropDispenseHeadPositioner[i]) {
            m_pPropDispenseHeadPositioner[i]->updateProperties();
        }
    }
    Util::Sleep(m_uWaitTime);

    for (int i = eLEDNoMin; i < eLEDNoMax; i++) {
        if (m_pPropLEDUnit[i]) {
            m_pPropLEDUnit[i]->updateProperties();
        }
    }
    Util::Sleep(m_uWaitTime);

    for (int i = eIncChmbrHtrMin; i < eIncChmbrHtrMax; i++)    //array[0,1,2,3,4,5,6]
    {
        if (m_pPropIncubationChamberHeater[i]) {
            m_pPropIncubationChamberHeater[i]->updateProperties();
        }
    }
    Util::Sleep(m_uWaitTime);


    for (int i = ePositionerMin; i < ePositionerMax; i++)    //array[0,1,2]
    {
        if (m_pPropWagonPositioner[i]) {
            m_pPropWagonPositioner[i]->updateProperties();
        }
    }
    Util::Sleep(m_uWaitTime);


}

void StubPamStation::abort(void) {
    //use to stop protocolsteps in a controlled way:


    // - stop all hardware
    //onAbort();


    std::lock_guard<std::recursive_mutex> guard(m_pGuard);

//added Alex
//wakeup thread that are waiting
    for (int i = eIncChmbrHtrMin; i < eIncChmbrHtrMax; i++)    //array[0,1,2,3,4,5,6]
    {
        if (m_pPropIncubationChamberHeater[i] != nullptr) {
            m_pPropIncubationChamberHeater[i]->stopWaitReady();
        }
    }
    if (m_pPropWagonHeater) m_pPropWagonHeater->stopWaitReady();
    if (m_pPropDispenseHeadHeater) m_pPropDispenseHeadHeater->stopWaitReady();



    // - throw an exception to stop the protocolsteps - if any- in a controlled way
    /*  ExceptionGenerator* pExc    = getExceptionGenerator();
   SysExceptionGenerator* pSysExc = dynamic_cast<SysExceptionGenerator*>(pExc);
   pSysExc->onException(this, PAMSTATION_ABORT_CALLED, eErrorUnknown, "PAMSTATION_ABORT_CALLED");
   */
}

void StubPamStation::onAbort(void) {
    std::lock_guard<std::recursive_mutex> guard(m_pGuard);

//added Alex
//wakeup thread that are waiting
    for (int i = eIncChmbrHtrMin; i < eIncChmbrHtrMax; i++)    //array[0,1,2,3,4,5,6]
    {
        if (m_pPropIncubationChamberHeater[i] != nullptr) {
            m_pPropIncubationChamberHeater[i]->stopWaitReady();
        }
    }
    if (m_pPropWagonHeater) m_pPropWagonHeater->stopWaitReady();
    if (m_pPropDispenseHeadHeater) m_pPropDispenseHeadHeater->stopWaitReady();

//added Alex


    if (m_pPropAspirateHeadPositioner) m_pPropAspirateHeadPositioner->setEnabled(false);
    if (m_pPropAspiratePump) m_pPropAspiratePump->setSwitchStatus(false);
    if (m_pPropCCD) m_pPropCCD->setEnabled(false);
    if (m_pPropDispenseHeadHeater) m_pPropDispenseHeadHeater->setEnabled(false);
    //if  (m_pPropDisposable)              m_pPropDisposable
    if (m_pPropFilterPositioner) m_pPropFilterPositioner->setEnabled(false);
    if (m_pPropFocusPositioner) m_pPropFocusPositioner->setEnabled(false);
    if (m_pPropOverPressureSupplier) m_pPropOverPressureSupplier->setEnabled(false);
    if (m_pPropUnderPressureSupplier) m_pPropUnderPressureSupplier->setEnabled(false);
    //if  (m_pPropUnloadButton)            m_pPropUnloadButton          ->setEnabled(false);
    if (m_pPropWagonHeater) m_pPropWagonHeater->setEnabled(false);
    if (m_pPropWagonXYPositioner) m_pPropWagonXYPositioner->setEnabled(false);


    for (int i = eHeadMin; i < eHeadMax; i++) {
        if (m_pPropDispenseHeadPositioner[i]) {
            m_pPropDispenseHeadPositioner[i]->setEnabled(false);
        }
    }


    for (int i = eLEDNoMin; i < eLEDNoMax; i++) {
        if (m_pPropLEDUnit[i]) {
            m_pPropLEDUnit[i]->setSwitchStatus(false);
        }
    }


    for (int i = eIncChmbrHtrMin; i < eIncChmbrHtrMax; i++)    //array[0,1,2,3,4,5,6]
    {
        if (m_pPropIncubationChamberHeater[i]) {
            m_pPropIncubationChamberHeater[i]->setEnabled(false);
        }
    }


    for (int i = ePositionerMin; i < ePositionerMax; i++)    //array[0,1,2]
    {
        if (m_pPropWagonPositioner[i]) {
            m_pPropWagonPositioner[i]->setEnabled(false);
        }
    }


}

