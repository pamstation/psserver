//---------------------------------------------------------------------------
//  Interface for EMS-Thomas Wuensche Can cart
//  Send_ReceiveCANMessage expects two struct of type CPC_CAN_MSG
//-------------------------------------------------------------------------

#ifndef CanInterfaceH
#define CanInterfaceH

#include "cpc.h"
//#include "cpclib.h"

#include "SysEventSource.h"  //inheritance (implementation base-class)

//---------------------------------------------------------------------------
class SysExceptionGenerator;

class CanInterface : public SysEventSource {
public:
    CanInterface();

    ~CanInterface();

    void connect();

    void Send_ReceiveCanMessage(CPC_CAN_MSG_T *, CPC_CAN_MSG_T *);


private:
    void Initialise();

    void Terminate();

    unsigned int CountMessageInCanBuffer();

    void SendCanMessage(CPC_CAN_MSG_T *);

    void WaitForCanReply();

    void ReceiveCanMessage(CPC_CAN_MSG_T *);

    unsigned m_uCanTimeOut;     //timeout in ms
    int m_iCanHandle;
    bool m_fInitialised;

    CPC_INIT_PARAMS_T *m_pCPC_InitParams;

    SysExceptionGenerator *m_pExceptionGenerator;
    //TLog  m_tLog;

}; // end class

#endif

