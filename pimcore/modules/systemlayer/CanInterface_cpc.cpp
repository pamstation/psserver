#pragma hdrstop
//---------------------------------------------------------------------------

#include "CanInterface.h"
//#include <DateUtils.hpp> // for SecondsBetween() and MilliSecondsBetween()
#include <thread>
#include <chrono>

#include "../pimcore/PamStationFactory.h"
#include "../pimcore/PamStation.h"
#include "StubPamStation.h"         //for obtaining StubPamStation object
#include "SysExceptionGenerator.h" //for passing Exception to ExceptionGenerator
#include "../pimcore/PamStationException.h" //for using object

//---------------------------------------------------------------------------

#pragma package(smart_init)

//Constructor
CanInterface::CanInterface() {
    m_fInitialised = false;
    m_uCanTimeOut = 10000; //timeout on 10 sec = 10000 msec
    /*
    m_tLog.SetFile("CanLogging.txt");
    m_tLog.SetPrefix(true, false);
    */

}

//-------------------------------------------------------------------------
//Deconstructor
CanInterface::~CanInterface() {
    //call hardware terminate only if initialised
    if (m_fInitialised) {
        //call hardware terminate
        this->Terminate();
    }
    // note: if error during initialise, fInitialised stays false, so can-handle is not closed properly
    //m_tLog.Close();

}

void CanInterface::connect() {
    Initialise();
}

//-------------------------------------------------------------------------
void CanInterface::Initialise() {
    //Get Pointers to ExceptionGenerator
    PamStation *pPam = PamStationFactory::getCurrentPamStation();
    ExceptionGenerator *pExc = pPam->getExceptionGenerator();
    m_pExceptionGenerator = dynamic_cast<SysExceptionGenerator *>(pExc);
    assert (m_pExceptionGenerator);

    this->m_pCPC_InitParams = nullptr;
    this->m_iCanHandle = -1;

    int result = -1;

    //Initialisation of the CAN-Cart (EMS Thomas Wuensche CPC-PCI )
    // some parameters are stored in the cpcconf.ini:
    // some parameters are set in the cpcconf.ini, for values use "pcidetect.exe"
    // example:
    // PARAMETERS FOR PASSIVE PCI INTERFACES
    // [CHAN00]                	string passed to CPC_OpenChannel
    // InterfaceType=CPC-PCI  	type of the interface
    // BusNr=2                	PCI bus number
    // SlotNr=12              	PCI slot number
    // ChannelNr=0            	channel number on the interface (0 or 1)

    //Step1: open handle to CAN, handle should be >= 0 (expected ChannelNr=0)

    //const_cast channel name for c++11 to be happy
    this->m_iCanHandle = CPC_OpenChannel((char *) "CHAN00");
    if (this->m_iCanHandle < 0) {
        std::string strError = "Unable to open CAN-channel, error: " + std::to_string(this->m_iCanHandle);
        m_pExceptionGenerator->onException(this, CANINTERFACE_OPEN_CHANNEL, eErrorUnknown, strError);
    }

    //Step2: get a handle to the struct with initial parameters of this channel
    this->m_pCPC_InitParams = CPC_GetInitParamsPtr(this->m_iCanHandle);
    if (this->m_pCPC_InitParams == nullptr) {
        std::string strError = "Unable to get pointer to initial CAN-parameters";
        m_pExceptionGenerator->onException(this, CANINTERFACE_UNABLE_GET_POINTER, eErrorUnknown, strError);
    }

    //Step3: set the parameters
    //Step3a: the controller type should be SJA1000 (=2)
    // definitions other for controller types
    // PCA82C200  1
    // SJA1000    2
    // AN82527    3
    // M16C       4

    //NB cc_type is unsigned char, SJA1000 is integer

    if (this->m_pCPC_InitParams->canparams.cc_type != SJA1000) {
        std::string strError =
                "CAN controller is not SJA1000, type-nr: " + std::to_string(this->m_pCPC_InitParams->canparams.cc_type);
        m_pExceptionGenerator->onException(this, CANINTERFACE_NOT_SJA1000, eErrorUnknown, strError);
    }

    //step 3b: set baudrate to 1MBaud
    this->m_pCPC_InitParams->canparams.cc_params.sja1000.btr0 = 0x00;
    this->m_pCPC_InitParams->canparams.cc_params.sja1000.btr1 = 0x14;

    // possible values for the btr parameters:
    // btr0: 0x00, btr1: 0x14, 1   MBaud
    // btr0: 0x00, btr1: 0x16, 800 kBaud
    // btr0: 0x00, btr1: 0x1C, 500 kBaud
    // btr0: 0x01, btr1: 0x1C, 250 kBaud
    // btr0: 0x03, btr1: 0x1C, 125 kBaud
    // btr0: 0x04, btr1: 0x1C, 100 kBaud
    // btr0: 0x09, btr1: 0x1C, 50  kBaud
    // btr0: 0x18, btr1: 0x1C, 20  kBaud
    // btr0: 0x31, btr1: 0x1C, 10  kBaud

    //step 3c: enable + set the output stage to 0xDA
    this->m_pCPC_InitParams->canparams.cc_params.sja1000.outp_contr = 0xDA;

    //step 3d: initialise the CAN Controller with these values
    result = CPC_CANInit(this->m_iCanHandle, 0);
    if (result) {
        std::string strError = "Unable to initialise CAN-channel, error: " + std::to_string(result);
        m_pExceptionGenerator->onException(this, CANINTERFACE_UNABLE_TO_INITIALISE, eErrorUnknown, strError);
    }

    //this enables the continuous transmission of CAN messages from the interface to the PC
    CPC_Control(this->m_iCanHandle, (CONTR_CAN_Message | CONTR_CONT_ON));

    //ready initialise
    this->m_fInitialised = true;
}

//-------------------------------------------------------------------------
void CanInterface::Terminate() {
    int result = -1;

    //Terminate the CAN-driver
    result = CPC_CloseChannel(m_iCanHandle);
    if (result) {
        //don't throw because of disrupting shutdown
        /**/

        try {
            std::string strError = "Unable to close CAN-channel, error: " + std::to_string(result);
            m_pExceptionGenerator->onException(this, CANINTERFACE_UNABLE_TO_CLOSE, eErrorUnknown, strError);
        } catch (PamStationException &e) {

        }

    }

    this->m_pCPC_InitParams = nullptr;
    this->m_iCanHandle = -1;

    this->m_fInitialised = false;
    //ready terminate
}


//------------------------------------------------------------------------
//return value: number of messages in buffer
//NB User Manual syntax "CPC_BufferCnt" is wrong

unsigned int CanInterface::CountMessageInCanBuffer() {
    int result = -1;
    result = CPC_GetBufferCnt(this->m_iCanHandle);
    if (result < 0) {
        std::string strError = "Unable to count msg in CAN-buffer, error: " + std::to_string(result);
        m_pExceptionGenerator->onException(this, CANINTERFACE_UNABLE_TO_COUNT_MSG, eErrorUnknown, strError);
    }
    //so return-value = BufferCnt (>=0)
    return (unsigned int) result;
}

//-------------------------------------------------------------
void CanInterface::SendCanMessage(CPC_CAN_MSG_T *pSendCanMsg) {
    int result = CPC_SendMsg(this->m_iCanHandle, 0, pSendCanMsg);
    if (result) {
        //don't throw error when error = -23 (timeout) ,
        //in case of an error will be followed by -22 anyhow
        if (result != -23) {
            std::string strError = "Unable to send CAN-msg, error: " + std::to_string(result);
            m_pExceptionGenerator->onException(this, CANINTERFACE_UNABLE_TO_SEND, eErrorUnknown, strError);
        }
    }
    // on this point:  result=OK
}

//-------------------------------------------------------------
void CanInterface::WaitForCanReply() {
    unsigned int iReceived;
    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
    do {
        std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();

        if (std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() >= m_uCanTimeOut) {
            //timeout occured
            std::string strError = "CAN timeout, when waiting for reply message";
            m_pExceptionGenerator->onException(this, CANINTERFACE_WAIT_TIMEOUT, eErrorTimeOut, strError);
        }
        iReceived = this->CountMessageInCanBuffer(); // 0 or more messages
        std::this_thread::yield();
    } while (!iReceived);

    //on this point: one (or more) messages received
}

//-------------------------------------------------------------
// contract: there must be messages in the buffer, so buffer-cnt > 0;
void CanInterface::ReceiveCanMessage(CPC_CAN_MSG_T *pReceivedCanMsg) {
    CPC_MSG_T *pCpcMsg;   //struct which holds all kinds of CAN Controller info
    //p.e. the received message

    //get handle to received message struct; if messages received, this pointer has a value
    pCpcMsg = CPC_Handle(this->m_iCanHandle);

    if (pCpcMsg == nullptr) {
        std::string strError = "CAN handle to receive buffer is nullptr, no messages received";
        m_pExceptionGenerator->onException(this, CANINTERFACE_HANDLE_IS_NULL, eErrorUnknown, strError);
    }

    //check if message type is correct
    //CPC_MSG_T_RESYNC	   0  Normally to be ignored
    //CPC_MSG_T_CAN		     1  CAN data frame
    //CPC_MSG_T_BUSLOAD	   2  Busload message
    //CPC_MSG_T_STRING	   3  Normally to be ignored
    //CPC_MSG_T_CONTI		   4  Normally to be ignored
    //CPC_MSG_T_MEM		     7  Normally not to be handled
    //CPC_MSG_T_RTR		     8  CAN remote frame
    //CPC_MSG_T_TXACK		   9  Send acknowledge
    //CPC_MSG_T_POWERUP	  10  Power-up message
    //CPC_MSG_T_CMD_NO	  11  Normally to be ignored
    //CPC_MSG_T_CAN_PRMS  12  Actual CAN parameters
    //CPC_MSG_T_ABORTED	  13  Command aborted message
    //CPC_MSG_T_CANSTATE  14  CAN state message
    //CPC_MSG_T_RESET     15  used to reset CAN-Controller
    //CPC_MSG_T_XCAN		  16  XCAN data frame
    //CPC_MSG_T_XRTR      17  XCAN remote frame
    //CPC_MSG_T_INFO      18  information strings
    //CPC_MSG_T_CONTROL   19  used for control ofinterface/driver behaviour
    //CPC_MSG_T_CONFIRM   20  response type for confirmed requests
    //CPC_MSG_T_OVERRUN   21  response type for overrun conditions
    //CPC_MSG_T_KEEPALIVE 22  response type for keep alive conditions

    if (pCpcMsg->type != CPC_MSG_T_CAN) {
        std::string strError = "CAN reply message type incorrect, type-nr: " + std::to_string(pCpcMsg->type);
        m_pExceptionGenerator->onException(this, CANINTERFACE_REPLY_INCORRECT, eErrorUnknown, strError);
    }

    //fill the receiveCanMsg struct
    pReceivedCanMsg->id = pCpcMsg->msg.canmsg.id;
    pReceivedCanMsg->length = pCpcMsg->msg.canmsg.length;
    for (int i = 0; i < 8; i++) {
        pReceivedCanMsg->msg[i] = pCpcMsg->msg.canmsg.msg[i];
    }

}
//-----------------------------------------------------
//Send_ReceiveCanMessage() sends 1 CAN-msg struct and waits for 1 reply-msg
//Max waiting time: parameter canTimeout [s], else exception
//Only the first reply is evaluated
//This reply-msg should be a CAN-msg type, else exception
//All other received reply-msg (CAN-msg type or other) are ignored/cleared

void CanInterface::Send_ReceiveCanMessage(CPC_CAN_MSG_T *pSendCanMsg, CPC_CAN_MSG_T *pReceivedCanMsg) {

    if (!m_fInitialised) {
        //call hardware initialise
        this->Initialise();
    }

    // empty the receive buffer before sending new msg by reading
    while (this->CountMessageInCanBuffer()) {
        CPC_CAN_MSG_T structSendCanMsgExtraMsg;
        this->ReceiveCanMessage(&structSendCanMsgExtraMsg);
    }

//  try
//  {
    /*
    std::string sHlp;
    if (pSendCanMsg->msg[0] == 0x21 ||
        pSendCanMsg->msg[0] == 0x22 ||
        pSendCanMsg->msg[0] == 0x23  )
    {
    pSendCanMsg->id;
    sHlp.sprintf("S: %X %X %X %X %X %X %X %X", pSendCanMsg->id     ,
                                               pSendCanMsg->length ,
                                               pSendCanMsg->msg[0] ,    //obj
                                               pSendCanMsg->msg[1] ,    //cmd
                                               pSendCanMsg->msg[2] ,    //data0
                                               pSendCanMsg->msg[3] ,    //data1
                                               pSendCanMsg->msg[4] ,    //data2
                                               pSendCanMsg->msg[5] );   //data3
    m_tLog.Write(sHlp);
    m_tLog.Flush();
    }
    */
    //send msg
    this->SendCanMessage(pSendCanMsg);

    //wait for one (or more) received messages in buffer
    this->WaitForCanReply();

    //get (first) received msg, this msg is returned to calling function
    this->ReceiveCanMessage(pReceivedCanMsg);
    /*
    if (pSendCanMsg->msg[0] == 0x21 ||
        pSendCanMsg->msg[0] == 0x22 ||
        pSendCanMsg->msg[0] == 0x23  )
    {
    sHlp.sprintf("R: %X %X %X %X %X %X %X %X", pReceivedCanMsg->id     ,
                                               pReceivedCanMsg->length ,
                                               pReceivedCanMsg->msg[0] ,    //obj
                                               pReceivedCanMsg->msg[1] ,    //cmd
                                               pReceivedCanMsg->msg[2] ,    //data0
                                               pReceivedCanMsg->msg[3] ,    //data1
                                               pReceivedCanMsg->msg[4] ,    //data2
                                               pReceivedCanMsg->msg[5] );   //data3

    m_tLog.Write(sHlp);
    m_tLog.Flush();
    }
     */
//  }//end try


//  __finally
//  {
//    try
//    {
//      //empty "too many msg" by reading, without raising exceptions
//      while( this->CountMessageInCanBuffer() )
//      {
//        CPC_CAN_MSG_T  structSendCanMsgExtraMsg;
//        this->ReceiveCanMessage(&structSendCanMsgExtraMsg);
//      }
//    }
//    catch (Exception &e)
//    {
//    }
//  } // end finally

}

//---------------------------------------------------------------------------


//-----------------------------------------------------------------

