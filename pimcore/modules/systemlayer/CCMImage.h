//---------------------------------------------------------------------------

#ifndef CCMImageH
#define CCMImageH

#include "../pimcore/Image.h" //for inheritance (interface)
#include <map>
#include <string>
#include <ctime>
#include "tiffio.h"
#include "tiffiop.h"


#define	N(a)	(sizeof (a) / sizeof (a[0]))
#define PG_TAG_BARCODE		65050
#define PG_TAG_COL		65051
#define PG_TAG_CYCLE		65052
#define PG_TAG_EXPOSURETIME		65053
#define PG_TAG_FILTER		65054
#define PG_TAG_INSTRUMENTTYPE		65055
#define PG_TAG_ROW		65058
#define PG_TAG_TEMPERATURE		65059
#define PG_TAG_TIMESTAMP		65060
#define PG_TAG_UNIT		65061
#define PG_TAG_PROTOCOLID		65062

//---------------------------------------------------------------------------

static const TIFFFieldInfo xTiffFieldInfo[] = {
	  { PG_TAG_BARCODE,	-1,-1, TIFF_ASCII,	FIELD_CUSTOM,
      TRUE,	FALSE,	(char*)"BARCODE" },
	  { PG_TAG_COL,	-1,-1, TIFF_ASCII,	FIELD_CUSTOM,
      TRUE,	FALSE,	(char*)"COL" },
	  { PG_TAG_CYCLE,	-1,-1, TIFF_ASCII,	FIELD_CUSTOM,
      TRUE,	FALSE,	(char*)"CYCLE" },
	  { PG_TAG_EXPOSURETIME,	-1,-1, TIFF_ASCII,	FIELD_CUSTOM,
      TRUE,	FALSE,	(char*)"EXPOSURETIME" },
	  { PG_TAG_FILTER,	-1,-1, TIFF_ASCII,	FIELD_CUSTOM,
      TRUE,	FALSE,	(char*)"FILTER" },
	  { PG_TAG_INSTRUMENTTYPE,	-1,-1, TIFF_ASCII,	FIELD_CUSTOM,
      TRUE,	FALSE,	(char*)"INSTRUMENTTYPE" },
	  { PG_TAG_ROW,	-1,-1, TIFF_ASCII,	FIELD_CUSTOM,
      TRUE,	FALSE,	(char*)"ROW" },
	  { PG_TAG_TEMPERATURE,	-1,-1, TIFF_ASCII,	FIELD_CUSTOM,
      TRUE,	FALSE,	(char*)"TEMPERATURE" },
	  { PG_TAG_TIMESTAMP,	-1,-1, TIFF_ASCII,	FIELD_CUSTOM,
      TRUE,	FALSE,	(char*)"TIMESTAMP" },
	  { PG_TAG_UNIT,	-1,-1, TIFF_ASCII,	FIELD_CUSTOM,
      TRUE,	FALSE,	(char*)"UNIT" },
	  { PG_TAG_PROTOCOLID,	-1,-1, TIFF_ASCII,	FIELD_CUSTOM,
      TRUE,	FALSE,	(char*)"PROTOCOLID" },
        };

class CCMImage: virtual public Image
{
  friend class PropCCD;
  public:
     CCMImage();
    ~CCMImage();

    CCMImage(const CCMImage& src);   //copy constructor

    //POINTER TO ARRAY OF PIXELS
    virtual unsigned short* getArrayOfPixels(); /** Returns a pointer to the image data*/

    //INFO ON ARRAY OF PIXELS
    virtual int       getSizeX() 			        ;	/** Returns the actual x-size of the image*/
    virtual int       getSizeY() 			        ;	/** Returns the actual y-size of the image*/
    virtual int       getBitsPerPixel() 	    ;	/** Returns the bits per pixel*/
    virtual unsigned  getSaturationLimit()    ;	/** Returns the highest possible pixel value, = 2^getBitsPerPixel() -1*/
  //virtual float     getGain() 			        ;	/** Returns the photon gain of the CCD while acquiring this image.*/
  //virtual unsigned  getPixel(int x, int y)  ; /** Returns the pixelvalue at position (x,y), @throws RangeException*/

    //MOMENT OF IMAGE ACQUISITION
	virtual std::time_t     getDate() 			        ;	/** Returns the date at which the image was taken*/

    //HARDWARE STATUS ON MOMENT OF IMAGE ACQUISITION (WELL&ILLUMINATION)
    virtual unsigned  getExposureTime() 	    ; /** Returns the exposure time (in ms) used to acquire this image.*/
    virtual EFilterNo getFilter() 		        ;	/** Returns the filter, with which the image was made*/
  //virtual float     getFocusPosition() 	    ;	/** Returns the position (in [mm]) of the FocusMotor, when the image was made*/
    virtual EWellNo   getWell() 		          ;	/** Returns the well-coordinate from which the image was made*/

    //CALCULATE_STATISTICS_FROM_PIXEL_INFO
    virtual unsigned  getMinValue() 		      ;	/** Returns the minimum value of the pixel values*/
    virtual unsigned  getMaxValue() 		      ;	/** Returns the maximum value of the pixel values */
    virtual unsigned  getAverageValue() 	    ;	/** Returns the average light value of the pixels  */
    virtual unsigned  getNumSaturated() 	    ;	/** Returns the number of saturated pixels*/
  //virtual float     getFocusValue() 		    ;	/** Returns the sharpness of the pixel values     */
  virtual double getRMSContrast();
    virtual double getSumDiff();

    //IMAGE HANDLING METHODS
    virtual Image*    duplicate()             ;
    virtual void      save(std::string path) 	    ;
    virtual void      save(std::string path , std::map<int,std::string> * props) 	    ;
//    virtual TMemoryStream* createStreamOfPixels();

  private:
    //static  const TIFFFieldInfo xTiffFieldInfo[];

   
    void initPropertiesCCMImage();

    unsigned short*    m_pArrayOfPixels;
//    TMemoryStream*     m_pStreamOfPixels;
    int       m_iSizeX;
    int       m_iSizeY;
    unsigned  m_uSaturationLimit;
    int       m_iBitsPerPixel;
  //float     m_rGain;      //TBD
    std::time_t     m_dateDate;
    unsigned  m_uExposureTime;
    EFilterNo m_eFilter;
  //float     m_rFocusPosition; //TBD
    EWellNo   m_eWell;
    unsigned  m_uMinValue;
    unsigned  m_uMaxValue;
    unsigned  m_uAverageValue;
    unsigned  m_uNumSaturated;
	double m_rmsContrast;
    uint64_t m_sumDiff;
    //float     m_rFocusValue;

    unsigned short * createArrayOfPixels(unsigned iSizeX, unsigned iSizeY); //returns pointer to created memory
             void    deleteArrayOfPixels(void);                             //free memory


//    bool writeTIFF(char* chFileName);

   void      calculateImageStatistics(); //fills CALCULATE_STATISTICS_FROM_PIXEL_INFO
   bool      m_fImageStatisticsCalulated;

};

#endif

