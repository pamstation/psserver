//---------------------------------------------------------------------------

#pragma hdrstop

#include <iostream>
#include <cassert>
#include "SysExceptionGenerator.h"    //for header-file
#include "../pimcore/ExceptionHandler.h"             //for listener

#include "SysPamStationException.h"   //for SysPamStationException, PamStationRangeException, PamStationTimeoutException
//---------------------------------------------------------------------------

#pragma package(smart_init)

int SysExceptionGenerator::iInstanceCounter = 0;

SysExceptionGenerator::SysExceptionGenerator() {
    ++iInstanceCounter;
    assert (iInstanceCounter == 1);  //only one instance of this class allowed (singleton)
}


void SysExceptionGenerator::initializeErrorMsg(std::shared_ptr<std::map<int, std::string>> exMsg) {
    if (exMsg) {
        for (auto const &entry : *exMsg) {
            std::cout << "error message -- " << entry.first << " -- " << entry.second << std::endl;
            exceptionMsg[entry.first] = entry.second;
        }
    }
}


//void SysExceptionGenerator::initializeErrorMsg(std::string filename) {
//    tinyxml2::XMLDocument doc;
//
//    if (doc.LoadFile(filename.c_str()) == 0) {
//        tinyxml2::XMLElement *exceptionsElement = doc.FirstChildElement("exceptions");
//        tinyxml2::XMLElement *exceptionElement = exceptionsElement->FirstChildElement("exception");
//        while (exceptionElement) {
//            int id;
//            exceptionElement->QueryIntAttribute("id", &id);
//            std::string description = exceptionElement->GetText();
//            exceptionMsg[id] = description;
//            exceptionElement = exceptionElement->NextSiblingElement("exception");
//        }
//    }
//}

SysExceptionGenerator::~SysExceptionGenerator() {
    --iInstanceCounter;
    assert (iInstanceCounter == 0); //only one instance of this class allowed (singleton)
}

//--------------------------
void SysExceptionGenerator::addExceptionHandler(ExceptionHandler *h) {
    addListenerToList(h);
}

void SysExceptionGenerator::removeExceptionHandler(ExceptionHandler *h) {
    removeListenerFromList(h);
}

//---------------------------------------------------------------------------------------------
void
SysExceptionGenerator::onException(void *source, int iErrorNo, EErrorType eErrType, const std::string &strExtraInfo) {

    auto it = exceptionMsg.find(iErrorNo);
    std::string *msgDesc = nullptr;
    if (it != exceptionMsg.end()) {
        msgDesc = new std::string(it->second.c_str());
    } else {
        msgDesc = new std::string(strExtraInfo);
    }
    std::string desc = msgDesc->c_str();
    delete msgDesc;
    SysPamStationException pSysPamStationException = SysPamStationException(iErrorNo, desc, source);

    std::lock_guard<std::recursive_mutex> guard(m_pcsListenerGuard);

    for (int index = 0; index < m_pListOfListeners.size(); index++) {
        auto *pBase = (EventHandler *) m_pListOfListeners[index];
        auto *pExc = dynamic_cast<ExceptionHandler *>(pBase);
        pExc->onException(this, &pSysPamStationException);
    }

//    if (m_pListOfListeners.empty()) {
//        std::cout << "SysExceptionGenerator::onException : " << desc << std::endl;
//    }

    std::cout << "SysExceptionGenerator::onException : iErrorNo -- "<< iErrorNo << "-- description --" << desc << " -- original description --" << strExtraInfo  << std::endl;

    throw pSysPamStationException;

}
