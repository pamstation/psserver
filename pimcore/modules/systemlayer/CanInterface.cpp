#pragma hdrstop
//---------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <iostream>

#include <net/if.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <poll.h>

#include <linux/can.h>
#include <linux/can/raw.h>

#include "CanInterface.h"

#include <thread>
#include <chrono>

#include "../pimcore/PamStationFactory.h"
#include "../pimcore/PamStation.h"
#include "StubPamStation.h"         //for obtaining StubPamStation object
#include "SysExceptionGenerator.h" //for passing Exception to ExceptionGenerator
#include "../pimcore/PamStationException.h" //for using object
#include "../../modules/utils/util.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)

//Constructor
CanInterface::CanInterface() {
    m_fInitialised = false;
    m_uCanTimeOut = 10000; //timeout on 10 sec = 10000 msec
    /*
    m_tLog.SetFile("CanLogging.txt");
    m_tLog.SetPrefix(true, false);
    */
}

//-------------------------------------------------------------------------
//Deconstructor
CanInterface::~CanInterface() {
    //call hardware terminate only if initialised
    if (m_fInitialised) {
        //call hardware terminate
        this->Terminate();
    }
    // note: if error during initialise, fInitialised stays false, so can-handle is not closed properly
    //m_tLog.Close();

}

void CanInterface::connect() {
    Initialise();
}

//-------------------------------------------------------------------------
void CanInterface::Initialise() {
    //Get Pointers to ExceptionGenerator
    PamStation *pPam = PamStationFactory::getCurrentPamStation();
    ExceptionGenerator *pExc = pPam->getExceptionGenerator();
    m_pExceptionGenerator = dynamic_cast<SysExceptionGenerator *>(pExc);
    assert (m_pExceptionGenerator);


    int status = system("/sbin/ip link set can1 down");
    if (status) {
        try {
            std::string strError = "/sbin/ip link set can1 down : failed " + std::to_string(status);
            m_pExceptionGenerator->onException(this, CANINTERFACE_OPEN_CHANNEL, eErrorUnknown, strError);
        } catch (PamStationException &e) {

        }
    }

    status = system("/sbin/ip link set can1 type can bitrate 1000000");
    if (status) {
        std::string strError = "/sbin/ip link set can1 type can bitrate 1000000 : failed " + std::to_string(status);
        m_pExceptionGenerator->onException(this, CANINTERFACE_OPEN_CHANNEL, eErrorUnknown, strError);
    }

    status = system("/sbin/ip link set can1 up");
    if (status) {
        std::string strError = "/sbin/ip link set can1 up : failed " + std::to_string(status);
        m_pExceptionGenerator->onException(this, CANINTERFACE_OPEN_CHANNEL, eErrorUnknown, strError);
    }


    this->m_pCPC_InitParams = nullptr;
    this->m_iCanHandle = -1;


    int fd;
    struct sockaddr_can addr;
    struct ifreq ifr;

    const char *ifname = "can1";

    if ((fd = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
        std::string strError = "Error while opening socket: " + std::to_string(this->m_iCanHandle);
        m_pExceptionGenerator->onException(this, CANINTERFACE_OPEN_CHANNEL, eErrorUnknown, strError);
    }

    strcpy(ifr.ifr_name, ifname);
    ioctl(fd, SIOCGIFINDEX, &ifr);

    addr.can_family = AF_CAN;
    addr.can_ifindex = ifr.ifr_ifindex;

    printf("%s at index %d\n", ifname, ifr.ifr_ifindex);

    if (bind(fd, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
        std::string strError = "Unable to bind, error: " + std::to_string(this->m_iCanHandle);
        m_pExceptionGenerator->onException(this, CANINTERFACE_OPEN_CHANNEL, eErrorUnknown, strError);
    }

    m_iCanHandle = fd;

    //ready initialise
    this->m_fInitialised = true;
}

//-------------------------------------------------------------------------
void CanInterface::Terminate() {

    this->m_fInitialised = false;

    int result = close(this->m_iCanHandle);

    if (result) {
        //don't throw because of disrupting shutdown
        /**/

        try {
            std::string strError = "Unable to close CAN-channel, error: " + std::to_string(result);
            m_pExceptionGenerator->onException(this, CANINTERFACE_UNABLE_TO_CLOSE, eErrorUnknown, strError);
        } catch (PamStationException &e) {

        }

    }

    this->m_pCPC_InitParams = nullptr;
    this->m_iCanHandle = -1;


    //ready terminate
}


//------------------------------------------------------------------------
//return value: number of messages in buffer
//NB User Manual syntax "CPC_BufferCnt" is wrong

unsigned int CanInterface::CountMessageInCanBuffer() {
    int result = 0;
//    result = CPC_GetBufferCnt(this->m_iCanHandle);
//    if (result < 0) {
//        std::string strError = "Unable to count msg in CAN-buffer, error: " + std::to_string(result);
//        m_pExceptionGenerator->onException(this, CANINTERFACE_UNABLE_TO_COUNT_MSG, eErrorUnknown, strError);
//    }
//    //so return-value = BufferCnt (>=0)
    return (unsigned int) result;
}

//-------------------------------------------------------------
void CanInterface::SendCanMessage(CPC_CAN_MSG_T *pSendCanMsg) {

    if (!m_fInitialised) Initialise();

    struct can_frame frame;

    frame.can_id = pSendCanMsg->id;
    frame.can_dlc = pSendCanMsg->length;
    for (int i = 0; i < 8; i++) frame.data[i] = pSendCanMsg->msg[i];

    struct pollfd p[1];

    p[0].fd = this->m_iCanHandle;
    p[0].events = POLLOUT;

    bool isEAGAIN = true;

    while (isEAGAIN) {
        int result = poll(p, 1, -1);
        if (result == -1 && errno == EAGAIN) {
            std::cerr << "CanInterface::SendCanMessage -- poll -- EAGAIN."
                      << std::endl;
            Util::Sleep(100);
        } else {
            isEAGAIN = false;
        }
    }


    isEAGAIN = true;

    while (isEAGAIN) {
        ssize_t nbytes = write(this->m_iCanHandle, &frame, sizeof(struct can_frame));
        if (nbytes == -1 && errno == EAGAIN) {
            std::cerr << "CanInterface::SendCanMessage -- write -- EAGAIN."
                      << std::endl;
            Util::Sleep(100);
        } else {
            isEAGAIN = false;
            if (nbytes != 16) {
                std::string strError = "Unable to send CAN-msg, error, nbytes: " + std::to_string(nbytes);
                m_pExceptionGenerator->onException(this, CANINTERFACE_UNABLE_TO_SEND, eErrorUnknown, strError);
            }
        }
    }
}

//-------------------------------------------------------------
void CanInterface::WaitForCanReply() {
    if (!m_fInitialised) Initialise();

    struct pollfd p[1];

    p[0].fd = this->m_iCanHandle;
    p[0].events = POLLIN;

    int t = poll(p, 1, m_uCanTimeOut);

    if (t == 0) {
        std::string strError = "CAN timeout, when waiting for reply message";
        m_pExceptionGenerator->onException(this, CANINTERFACE_WAIT_TIMEOUT, eErrorTimeOut, strError);
    } else if (t < 0) {
        std::string strError = "CAN Wait failed";
        m_pExceptionGenerator->onException(this, CANINTERFACE_WAIT_TIMEOUT, eErrorTimeOut, strError);
    }

    //on this point: one (or more) messages received
}

//-------------------------------------------------------------
// contract: there must be messages in the buffer, so buffer-cnt > 0;
void CanInterface::ReceiveCanMessage(CPC_CAN_MSG_T *pReceivedCanMsg) {
    if (!m_fInitialised) Initialise();

    struct can_frame frame;
    struct pollfd p[1];

    p[0].fd = this->m_iCanHandle;
    p[0].events = POLLIN;

    int t = poll(p, 1, -1);

    // Try to read available data
    ssize_t nbytes = read(this->m_iCanHandle, &frame, sizeof(frame));

    if (nbytes != 16) {
        std::string strError = "Unable to read CAN-msg, error, nbytes: " + std::to_string(nbytes);
        m_pExceptionGenerator->onException(this, CANINTERFACE_REPLY_INCORRECT, eErrorUnknown, strError);
    }

    //fill the receiveCanMsg struct
    pReceivedCanMsg->id = frame.can_id;
    pReceivedCanMsg->length = frame.can_dlc;
    for (int i = 0; i < 8; i++) {
        pReceivedCanMsg->msg[i] = frame.data[i];
    }

//    CPC_MSG_T *pCpcMsg;   //struct which holds all kinds of CAN Controller info
//    //p.e. the received message
//
//    //get handle to received message struct; if messages received, this pointer has a value
//    pCpcMsg = CPC_Handle(this->m_iCanHandle);
//
//    if (pCpcMsg == nullptr) {
//        std::string strError = "CAN handle to receive buffer is nullptr, no messages received";
//        m_pExceptionGenerator->onException(this, CANINTERFACE_HANDLE_IS_NULL, eErrorUnknown, strError);
//    }
//
//    //check if message type is correct
//    //CPC_MSG_T_RESYNC	   0  Normally to be ignored
//    //CPC_MSG_T_CAN		     1  CAN data frame
//    //CPC_MSG_T_BUSLOAD	   2  Busload message
//    //CPC_MSG_T_STRING	   3  Normally to be ignored
//    //CPC_MSG_T_CONTI		   4  Normally to be ignored
//    //CPC_MSG_T_MEM		     7  Normally not to be handled
//    //CPC_MSG_T_RTR		     8  CAN remote frame
//    //CPC_MSG_T_TXACK		   9  Send acknowledge
//    //CPC_MSG_T_POWERUP	  10  Power-up message
//    //CPC_MSG_T_CMD_NO	  11  Normally to be ignored
//    //CPC_MSG_T_CAN_PRMS  12  Actual CAN parameters
//    //CPC_MSG_T_ABORTED	  13  Command aborted message
//    //CPC_MSG_T_CANSTATE  14  CAN state message
//    //CPC_MSG_T_RESET     15  used to reset CAN-Controller
//    //CPC_MSG_T_XCAN		  16  XCAN data frame
//    //CPC_MSG_T_XRTR      17  XCAN remote frame
//    //CPC_MSG_T_INFO      18  information strings
//    //CPC_MSG_T_CONTROL   19  used for control ofinterface/driver behaviour
//    //CPC_MSG_T_CONFIRM   20  response type for confirmed requests
//    //CPC_MSG_T_OVERRUN   21  response type for overrun conditions
//    //CPC_MSG_T_KEEPALIVE 22  response type for keep alive conditions
//
//    if (pCpcMsg->type != CPC_MSG_T_CAN) {
//        std::string strError = "CAN reply message type incorrect, type-nr: " + std::to_string(pCpcMsg->type);
//        m_pExceptionGenerator->onException(this, CANINTERFACE_REPLY_INCORRECT, eErrorUnknown, strError);
//    }
//
//    //fill the receiveCanMsg struct
//    pReceivedCanMsg->id = pCpcMsg->msg.canmsg.id;
//    pReceivedCanMsg->length = pCpcMsg->msg.canmsg.length;
//    for (int i = 0; i < 8; i++) {
//        pReceivedCanMsg->msg[i] = pCpcMsg->msg.canmsg.msg[i];
//    }

}
//-----------------------------------------------------
//Send_ReceiveCanMessage() sends 1 CAN-msg struct and waits for 1 reply-msg
//Max waiting time: parameter canTimeout [s], else exception
//Only the first reply is evaluated
//This reply-msg should be a CAN-msg type, else exception
//All other received reply-msg (CAN-msg type or other) are ignored/cleared

void CanInterface::Send_ReceiveCanMessage(CPC_CAN_MSG_T *pSendCanMsg, CPC_CAN_MSG_T *pReceivedCanMsg) {

    if (!m_fInitialised) Initialise();


    // empty the receive buffer before sending new msg by reading
    while (this->CountMessageInCanBuffer()) {
        CPC_CAN_MSG_T structSendCanMsgExtraMsg;
        this->ReceiveCanMessage(&structSendCanMsgExtraMsg);
    }

//  try
//  {
    /*
    std::string sHlp;
    if (pSendCanMsg->msg[0] == 0x21 ||
        pSendCanMsg->msg[0] == 0x22 ||
        pSendCanMsg->msg[0] == 0x23  )
    {
    pSendCanMsg->id;
    sHlp.sprintf("S: %X %X %X %X %X %X %X %X", pSendCanMsg->id     ,
                                               pSendCanMsg->length ,
                                               pSendCanMsg->msg[0] ,    //obj
                                               pSendCanMsg->msg[1] ,    //cmd
                                               pSendCanMsg->msg[2] ,    //data0
                                               pSendCanMsg->msg[3] ,    //data1
                                               pSendCanMsg->msg[4] ,    //data2
                                               pSendCanMsg->msg[5] );   //data3
    m_tLog.Write(sHlp);
    m_tLog.Flush();
    }
    */
    //send msg
    this->SendCanMessage(pSendCanMsg);

    //wait for one (or more) received messages in buffer
    this->WaitForCanReply();

    //get (first) received msg, this msg is returned to calling function
    this->ReceiveCanMessage(pReceivedCanMsg);
    /*
    if (pSendCanMsg->msg[0] == 0x21 ||
        pSendCanMsg->msg[0] == 0x22 ||
        pSendCanMsg->msg[0] == 0x23  )
    {
    sHlp.sprintf("R: %X %X %X %X %X %X %X %X", pReceivedCanMsg->id     ,
                                               pReceivedCanMsg->length ,
                                               pReceivedCanMsg->msg[0] ,    //obj
                                               pReceivedCanMsg->msg[1] ,    //cmd
                                               pReceivedCanMsg->msg[2] ,    //data0
                                               pReceivedCanMsg->msg[3] ,    //data1
                                               pReceivedCanMsg->msg[4] ,    //data2
                                               pReceivedCanMsg->msg[5] );   //data3

    m_tLog.Write(sHlp);
    m_tLog.Flush();
    }
     */
//  }//end try


//  __finally
//  {
//    try
//    {
//      //empty "too many msg" by reading, without raising exceptions
//      while( this->CountMessageInCanBuffer() )
//      {
//        CPC_CAN_MSG_T  structSendCanMsgExtraMsg;
//        this->ReceiveCanMessage(&structSendCanMsgExtraMsg);
//      }
//    }
//    catch (Exception &e)
//    {
//    }
//  } // end finally

}

//---------------------------------------------------------------------------


//-----------------------------------------------------------------

