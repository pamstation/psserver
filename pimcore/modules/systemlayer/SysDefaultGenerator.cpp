//---------------------------------------------------------------------------


#pragma hdrstop

#include <cstring>
#include <sstream>
#include "SysDefaultGenerator.h"  //for header-file
//#include <Math.hpp> //fot Sign
#include <math.h>   //for fabs
#include <stdio.h>  //for all cat_sprintf
//#include <DateUtils.hpp>
#include "../utils/util.h"

#include "../pimcore/PamStationFactory.h"          //for obtaining PamStation object
#include "../pimcore/PamStation.h"                 //for obtaining PamStation object
#include "StubPamStation.h"         //for obtaining StubPamStation object
#include "CANController.h"          //for obtaining CANController

#include "../pimcore/PamDefs_InitialValues.h"     //for initial values of initalise_protocolSettings
//---------------------------------------------------------------------------
#include <stdlib.h>
#include <iostream>
#include <string>
#include <fstream>
#include <streambuf>

#include "../../../server/json.hpp"


using json = nlohmann::json;

#pragma package(smart_init)
int SysDefaultGenerator::iInstanceCounter = 0;

SysDefaultGenerator::SysDefaultGenerator() {
    assert (++iInstanceCounter == 1);  //only one instance of this class allowed (singleton)'

    //get CANController, (from StubPamStation via PamStation, because PamStation is a singleton )
    PamStation *pPam = PamStationFactory::getCurrentPamStation();
    StubPamStation *pStubPamStation = dynamic_cast<StubPamStation *>(pPam);
    assert(pStubPamStation);
    m_pCAN = pStubPamStation->getCANController();
    assert(m_pCAN);

    //checks if sensor-data already retrieved from instrument
    m_iRefreshSensorCalibrationStruct = 0;
    m_iRefreshControlableSettingsStruct = 0;
    m_iRefreshWagonLogicalPositions = 0;
    m_iRefreshFilterLogicalPositions = 0;
    m_iRefreshFocusLogicalWellPositions = 0;
    m_iRefreshFocusLogicalPositions = 0;
    m_iRefreshLEDIntensity = 0;
    m_iWagonHeaterSettings = 0;
    m_iDispenseHeadHeaterSettings = 0;

    //empty entire array of calibration struct
    for (int i = 0; i < eCanObjMax; i++) memset(&m_rgtSensCalVal[i], 0, sizeof(m_rgtSensCalVal[i]));
    for (int i = 0; i < eCanObjMax; i++) memset(&m_rgtCtrlSett[i], 0, sizeof(m_rgtCtrlSett[i]));
    memset(&m_tWagonLogPos, 0, sizeof(m_tWagonLogPos));
    memset(&m_tFilterLogPos, 0, sizeof(m_tFilterLogPos));
    memset(&m_tFocusLogPos, 0, sizeof(m_tFocusLogPos));
    memset(&m_tLEDIntensity, 0, sizeof(m_tLEDIntensity));
    memset(&m_tWagonHeaterSett, 0, sizeof(m_tWagonHeaterSett));
    memset(&m_tDispenseHeadHeaterSett, 0, sizeof(m_tDispenseHeadHeaterSett));

    //PROTOCOL SETTINGS
    initialise_protocolSettings();
}

SysDefaultGenerator::~SysDefaultGenerator() {
    m_iRefreshSensorCalibrationStruct = 0;
    m_iRefreshControlableSettingsStruct = 0;
    m_iRefreshWagonLogicalPositions = 0;
    m_iRefreshFilterLogicalPositions = 0;
    m_iRefreshFocusLogicalWellPositions = 0;
    m_iRefreshFocusLogicalPositions = 0;
    m_iRefreshLEDIntensity = 0;

    m_pCAN = nullptr;
    assert (--iInstanceCounter == 0);  //only one instance of this class allowed (singleton)
}

//START CONVERSION SECTION
//The hardware does not measure in SI units, but in "Local Units" [LU]
//            "[SI]"                "[LU]"
// position    [mm]   is measured in [encoder pulses] (output of puls counter)
// temperature [�C]   is measured in [V]              (output of analog temperature sensor)
// pressure    [barg] is measured in [V]              (output of analog pressure sensor)

//the measurements are influenced by two sources and these can be calibrated away:
//- sensor calibration, in order to reduce sensor inaccuracies
//- system calibration, in order to reduce electronics influences
// both calibrations can coope with 2nd orde effects (offset, 1st order gain and 2nd order gain)

//So from temperature or pressure to display:
// process temperature [�C] -> analog sensor      -> [V]
// process voltages    [V]  -> system electronics -> [V]
// analog voltage      [V]  -> AD-conversion      -> a number [-] or "[LU]"
// the embedded software and the firmware use this number [LU] to control the system
// This number [LU] is presented to the software
// Software needs to convert this number from [LU] to [SI] by:
// - divide by AD-converson factor to obtain [V]
// - correct for system inaccuracies to obtain [V]
// - correct for sensor inaccuracies to obtain [�C] or [Barg]

// From setpoint to the number presented to the electronics:
// 1 sensor tranformation
// 2 system transformation
// 3 DA conversion factor
//--------------------------------------------------------
int SysDefaultGenerator::convertSItoLU(ECanObj eCanObj, float rValueInSI) {

    //SENSOR CALIBRATION TRANSFORMATION
    //The setpoint in SI is to be converted to a firware value
    // mm   to encoder pulsen,                        or
    // �C   to analog temperature sensor output in V, or
    // Barg to analog pressure    sensor output in V  ;

    //start with a value in "SI": �C, barg, �l, mm
    //conversion is SI = offset + gain*LU + gain2*LU� or
    //SI = gain2*LU� + gain*LU + offset  or...
    // 0 = gain2*LU� + gain*LU + offset - SI ; (0 = ax� + bx + c)

    //get sensor calibration data:
    Sensor_Calibration_Data_t *ptSensorData = getSensorCalibrationData(eCanObj);

    //SENSOR CALIBRATION TRANSFORMATION
    //for readability
    double rA = ptSensorData->rSensGain2;
    double rB = ptSensorData->rSensGain;
    double rC = ptSensorData->rSensOffset - rValueInSI;

    double rSens1 = 0.0;  //rSens1 represents sensor-output in [V]
    double rSens2 = 0.0;  //not used

    CalculateRootQuadraticEquation(rSens1, rSens2, rA, rB, rC);


    //SYSTEM CALIBRATION TRANSFORMATION
    //for readability
    rA = ptSensorData->rSysGain2;
    rB = ptSensorData->rSysGain;
    rC = ptSensorData->rSysOffset - rSens1;

    double rSys1 = 0.0;  //rSys1 represents system-output in [V]
    double rSys2 = 0.0;  //not used

    CalculateRootQuadraticEquation(rSys1, rSys2, rA, rB, rC);

    //AD CONVERSION
    float rValue3 = rSys1 * ptSensorData->rADConversion;  //multiply with AD-conversion factor [float]
    int iValue4 = (int) (rValue3 + (Util::Sign(rValue3) * 0.5));  //round to nearest integer [int]

    return iValue4;
}

//----------
float SysDefaultGenerator::convertLUtoSI(ECanObj eCanObj, int iValueInLU) {
    //conversion is SI = offset + gain*LU + gain2*LU�
    //step1: convert DA-factor back
    //step2: correct for system calibration
    //step3: correct for sensor calibration
    //get sensor calibration data:
    Sensor_Calibration_Data_t *ptSensorData = getSensorCalibrationData(eCanObj);

    double rValue1 = iValueInLU / ptSensorData->rADConversion;  //divide by AD-conversion factor
    double rValue2 = ptSensorData->rSysOffset + (ptSensorData->rSysGain * rValue1) +
                     (ptSensorData->rSysGain2 * rValue1 * rValue1); //correct for system-calibration:
    double rValue3 = ptSensorData->rSensOffset + (ptSensorData->rSensGain * rValue2) +
                     (ptSensorData->rSensGain2 * rValue2 * rValue2); //correct for sensor-calibration:
    return (float) rValue3;
}

//--------------------------------------------------------------------------------------
int SysDefaultGenerator::CalculateRootQuadraticEquation(double &rX1, double &rX2, double &rA, double &rB, double &rC) {
    //ax� + bx + c = 0; => x1,2 = (-b � SQRT(b^2 - 4ac) ) / 2a
    //tried to find so numerical recepies with cover all possibilities:
    // a  = 0 & b  = 0
    // a  = 0 & b != 0
    // a != 0 & b  = 0 (signB)
    // X1 = nul etc etc etc , precision loss etc etc
    // so now implemented straight-forward calculation, but hope on better and faster...


    int iErrorCode = 0; //no error

    if (rA == 0.0 && rB == 0.0) {
        iErrorCode = -1;
        assert (0);                              // if rA and rB are 0, there's no equation
    } else if (rA == 0.0 && rB != 0.0)          // not quadratic, but solve anyway
    {
        rX1 = -rC / rB;                        // X1 = X2 = -c/b
        rX2 = rX1;                        // don't calculate again
    } else  //rA != 0
    {
        //calculate discriminant:  b^2 - 4ac
        double rDiscriminant = (rB * rB) - 4 * rA * rC;

        if (rDiscriminant < 0.0)             // Equation has complex roots
        {
            iErrorCode = -2;
            assert (0);                           // implement later???
        } else                                    //at least one root
        {
            //remember rA ! = 0

            //if rB = 0, sign doesn't work, (stick with floats)
            double rSignB = 0.0;
            if (rB >= 0) rSignB = 1.0;
            else rSignB = -1.0;

            double rQ = (-rB + (rSignB * sqrt(rDiscriminant))) / 2.0;

            rX1 = rQ / rA;     //if you need one solution, this is often the one
            assert (rX1);      //what if rX1 = 0.0 ( if rC = 0.0, one root is 0, the other is -rB/rA, but which is x1 and which is x2?

            rX2 = rC / rX1;
        }
    }


    return iErrorCode;
}

//-------------------------------------------------------------------------------------
Sensor_Calibration_Data_t *SysDefaultGenerator::getSensorCalibrationData(ECanObj eCanObj) {
    //Sensor calibration data is stored on the PamStation-Instrument
    //all data is adressed by this sensor-object, because:
    // - it should have some adress, and the sensor-obj-adress is a logical choise
    // - in this way the sensor-data is stored on the board on which the sensor is connected (modularity)

    //All controllables use for their SItoLU conversion the same sensor-calibration data
    // so in this method controllables and sensors are  "connected"
    // in case a controllable doesn't have a anolog sensor a sensor-object is choosen
    // in case of encoders encoder-channelA
    // in case of apirateHeadPositioner the AspirateHeadUp Sensor
    /**
    *             SENSORS        & CONTROLLERS
    * TEMPERATURE
    * - eTempSensDispense        & eCtrlDispenseHeadHeater
    * - eTempSensBottomLeft      & eCtrlHtrBottomLeft
    * - eTempSensBottomMiddle    & eCtrlHtrBottomMiddle
    * - eTempSensBottomRight     & eCtrlHtrBottomRight
    * - eTempSensCoverLeft       & eCtrlHtrCoverLeft
    * - eTempSensCoverMiddle     & eCtrlHtrCoverMiddle
    * - eTempSensCoverRight      & eCtrlHtrCoverRight
    *
    * PRESSURE
    * - ePressSensOverPress      & eCtrlOverPressureSupplier
    * - ePressSensUnderPress     & eCtrlUnderPressureSupplier
    * - ePressSensAspirate       &  -
    * - ePressSensWell1          &  -
    * - ePressSensWell2          &  -
    * - ePressSensWell3          &  -
    * - ePressSensWell4          &  -
    * - ePressSensWell5          &  -
    * - ePressSensWell6          &  -
    * - ePressSensWell7          &  -
    * - ePressSensWell8          &  -
    * - ePressSensWell9          &  -
    * - ePressSensWell10         &  -
    * - ePressSensWell11         &  -
    * - ePressSensWell12         &  -
    *
    * POSITION
    * - eEncDispenseHead1A       & eCtrlDispenseHeadPositioner1
    * - eEncDispenseHead2A       & eCtrlDispenseHeadPositioner2
    * - eEncDispenseHead3A       & eCtrlDispenseHeadPositioner3
    * - eEncDispenseHead4A       & eCtrlDispenseHeadPositioner4
    * - eEncFilterA              & eCtrlFilterPositioner
    * - eEncFocusA               & eCtrlFocusPositioner
    * - eEncTransXA              & eCtrlTransporterX
    * - eEncTransYA              & eCtrlTransporterY
    *
    * CURRENT
    * - eCurrSensTransX          &  -
    * - eCurrSensTransY          &  -
    * - eCurrSensFocus           &  -
    * - eCurrSensFilter          &  -
    * - eCurrSensBottomLeft      &  -
    * - eCurrSensBottomMiddle    &  -
    * - eCurrSensBottomRight     &  -
    * - eCurrSensCoverLeft       &  -
    * - eCurrSensCoverMiddle     &  -
    * - eCurrSensCoverRight      &  -
    * - eCurrSensDispenseHeater  &  -
    * - eCurrSensAspirateMotor   &  -
    *
    * MISC
    * - eSensAspirateHeadUp      & eCtrlAspirateHeadPositioner
    */



    //get the data (once) from the instrument
    if (!m_iRefreshSensorCalibrationStruct)  //invoke only once
    {
        downloadSensorCalibrationStruct();    //download sensorcalibration data (once) from instrument
        m_iRefreshSensorCalibrationStruct++;
    }

    //connect the controllables to their sensor-object
    switch (eCanObj) {
        case ePressSensOverPress          :
            break;    //AI, board1
        case ePressSensUnderPress         :
            break;    //AI, board1
        case eCurrSensTransX              :
            break;    //CI, board1
        case eCurrSensTransY              :
            break;    //CI, board1
        case eCurrSensFocus               :
            break;    //CI, board1
        case eCurrSensFilter              :
            break;    //CI, board1
        case eEncTransXA                  :
            break;    //DI, board1
        case eEncTransYA                  :
            break;    //DI, board1
        case eEncFocusA                   :
            break;    //DI, board1
        case eEncFilterA                  :
            break;    //DI, board1
        case eCtrlOverPressureSupplier    :
            eCanObj = ePressSensOverPress;
            break; //CTRL, board1
        case eCtrlUnderPressureSupplier   :
            eCanObj = ePressSensUnderPress;
            break; //CTRL, board1
        case eCtrlTransporterX            :
            eCanObj = eEncTransXA;
            break; //CTRL, board1
        case eCtrlTransporterY            :
            eCanObj = eEncTransYA;
            break; //CTRL, board1
        case eCtrlFocusPositioner         :
            eCanObj = eEncFocusA;
            break; //CTRL, board1
        case eCtrlFilterPositioner        :
            eCanObj = eEncFilterA;
            break; //CTRL, board1
        case eTempSensCoverLeft           :
            break;    //AI, board2
        case eTempSensBottomLeft          :
            break;    //AI, board2
        case eTempSensCoverMiddle         :
            break;    //AI, board2
        case eTempSensBottomMiddle        :
            break;    //AI, board2
        case eTempSensCoverRight          :
            break;    //AI, board2
        case eTempSensBottomRight         :
            break;    //AI, board2
        case eTempSensCalibration         :
            break;    //AI, board2
        case ePressSensWell1              :
            break;    //AI, board2
        case ePressSensWell2              :
            break;    //AI, board2
        case ePressSensWell3              :
            break;    //AI, board2
        case ePressSensWell4              :
            break;    //AI, board2
        case ePressSensWell5              :
            break;    //AI, board2
        case ePressSensWell6              :
            break;    //AI, board2
        case ePressSensWell7              :
            break;    //AI, board2
        case ePressSensWell8              :
            break;    //AI, board2
        case ePressSensWell9              :
            break;    //AI, board2
        case ePressSensWell10             :
            break;    //AI, board2
        case ePressSensWell11             :
            break;    //AI, board2
        case ePressSensWell12             :
            break;    //AI, board2
        case eCurrSensBottomLeft          :
            break;    //CI, board2
        case eCurrSensBottomMiddle        :
            break;    //CI, board2
        case eCurrSensBottomRight         :
            break;    //CI, board2
        case eCurrSensCoverLeft           :
            break;    //CI, board2
        case eCurrSensCoverMiddle         :
            break;    //CI, board2
        case eCurrSensCoverRight          :
            break;    //CI, board2
        case eCtrlHtrCoverLeft            :
            eCanObj = eTempSensCoverLeft;
            break; //CTRL, board2
        case eCtrlHtrBottomLeft           :
            eCanObj = eTempSensBottomLeft;
            break; //CTRL, board2
        case eCtrlHtrCoverMiddle          :
            eCanObj = eTempSensCoverMiddle;
            break; //CTRL, board2
        case eCtrlHtrBottomMiddle         :
            eCanObj = eTempSensBottomMiddle;
            break; //CTRL, board2
        case eCtrlHtrCoverRight           :
            eCanObj = eTempSensCoverRight;
            break; //CTRL, board2
        case eCtrlHtrBottomRight          :
            eCanObj = eTempSensBottomRight;
            break; //CTRL, board2
        case eTempSensDispense            :
            break;    //AI, board3
        case ePressSensAspirate           :
            break;    //AI, board3
        case eCurrSensDispenseHeater      :
            break;    //CI, board3
        case eCurrSensAspirateMotor       :
            break;    //CI, board3
        case eEncDispenseHead1A           :
            break;    //DI, board3
        case eEncDispenseHead2A           :
            break;    //DI, board3
        case eEncDispenseHead3A           :
            break;    //DI, board3
        case eEncDispenseHead4A           :
            break;    //DI, board3
        case eSensAspirateHeadUp          :
            break;    //OI, board3 (needed to complete pattern for exception eCtrlAspirateHeadPositioner)
        case eCtrlAspirateHeadPositioner  :
            eCanObj = eSensAspirateHeadUp;
            break; //CTRL, board3
        case eCtrlDispenseHeadHeater      :
            eCanObj = eTempSensDispense;
            break; //CTRL, board3
        case eCtrlDispenseHeadPositioner1 :
            eCanObj = eEncDispenseHead1A;
            break; //CTRL, board3
        case eCtrlDispenseHeadPositioner2 :
            eCanObj = eEncDispenseHead2A;
            break; //CTRL, board3
        case eCtrlDispenseHeadPositioner3 :
            eCanObj = eEncDispenseHead3A;
            break; //CTRL, board3
        case eCtrlDispenseHeadPositioner4 :
            eCanObj = eEncDispenseHead4A;
            break; //CTRL, board3
        default:
            assert(0);  //this point should not be reached
    }

    return &m_rgtSensCalVal[eCanObj];

}

//------------------------------------------------------------------------------
Controllable_Settings_t *SysDefaultGenerator::getControllableSettings(ECanObj eCanObj) {
    //check function contract
    switch (eCanObj) {
        case eCtrlOverPressureSupplier    :
            break; //CTRL, board1
        case eCtrlUnderPressureSupplier   :
            break; //CTRL, board1
        case eCtrlTransporterX            :
            break; //CTRL, board1
        case eCtrlTransporterY            :
            break; //CTRL, board1
        case eCtrlFocusPositioner         :
            break; //CTRL, board1
        case eCtrlFilterPositioner        :
            break; //CTRL, board1
        case eCtrlHtrCoverLeft            :
            break; //CTRL, board2
        case eCtrlHtrBottomLeft           :
            break; //CTRL, board2
        case eCtrlHtrCoverMiddle          :
            break; //CTRL, board2
        case eCtrlHtrBottomMiddle         :
            break; //CTRL, board2
        case eCtrlHtrCoverRight           :
            break; //CTRL, board2
        case eCtrlHtrBottomRight          :
            break; //CTRL, board2
        case eCtrlAspirateHeadPositioner  :
            break; //CTRL, board3
        case eCtrlDispenseHeadHeater      :
            break; //CTRL, board3
        case eCtrlDispenseHeadPositioner1 :
            break; //CTRL, board3
        case eCtrlDispenseHeadPositioner2 :
            break; //CTRL, board3
        case eCtrlDispenseHeadPositioner3 :
            break; //CTRL, board3
        case eCtrlDispenseHeadPositioner4 :
            break; //CTRL, board3
        default:
            assert(0);  //this point should not be reached
    }

    if (!m_iRefreshControlableSettingsStruct)  //invoke only once
    {
        downloadControlableSettingsStruct();    //download ctrlsettings data (once) from instrument
        m_iRefreshControlableSettingsStruct++;
    }

    return &m_rgtCtrlSett[eCanObj];
}

//---------------------------------------------------------------
Wagon_LogicalXYPos_t *SysDefaultGenerator::getWagonLogicalPositions() {
    //get the data (once) from the instrument
    if (!m_iRefreshWagonLogicalPositions)      //invoke only once
    {
        downloadWagonLogicalPositionStruct();    //download sensorcalibration data (once) from instrument
        m_iRefreshWagonLogicalPositions++;
    }

    return &m_tWagonLogPos;
}

//-----------------------------------------------
Filter_LogicalPos_t *SysDefaultGenerator::getFilterLogicalPositions(void) {
    //get the data (once) from the instrument
    if (!m_iRefreshFilterLogicalPositions)      //invoke only once
    {
        downloadFilterLogicalPositionStruct();    //download data (once) from instrument
        m_iRefreshFilterLogicalPositions++;
    }

    return &m_tFilterLogPos;
}

//-------------------------------------------------
Focus_LogicalPos_t *SysDefaultGenerator::getFocusLogicalPositions(void) {
    //get the data (once) from the instrument
    if (!m_iRefreshFocusLogicalPositions)      //invoke only once
    {
        downloadFocusLogicalPositionStruct();    //download data (once) from instrument
        m_iRefreshFocusLogicalPositions++;
    }

    return &m_tFocusLogPos;
}

Focus_LogicalWellPos_t *SysDefaultGenerator::getFocusLogicalWellPositions(void) {
    //get the data (once) from the instrument
    if (!m_iRefreshFocusLogicalWellPositions)      //invoke only once
    {
        downloadFocusLogicalWellPositionStruct();    //download data (once) from instrument
        m_iRefreshFocusLogicalWellPositions++;
    }

    return &m_tFocusLogWellPos;
}
float SysDefaultGenerator::getFocusLogicalWellPosition(EWellNo eWell) {
    auto ptStruct = getFocusLogicalWellPositions();
    float value;
    switch ( eWell ) {
        case eWell1 : value = ptStruct->rOffsetForWell1; break;
        case eWell2 : value = ptStruct->rOffsetForWell2 = value; break;
        case eWell3 : value = ptStruct->rOffsetForWell3 = value; break;
        case eWell4 : value = ptStruct->rOffsetForWell4 = value; break;
        case eWell5 : value = ptStruct->rOffsetForWell5 = value; break;
        case eWell6 : value = ptStruct->rOffsetForWell6 = value; break;
        case eWell7 : value = ptStruct->rOffsetForWell7 = value; break;
        case eWell8 : value = ptStruct->rOffsetForWell8 = value; break;
        case eWell9 : value = ptStruct->rOffsetForWell9 = value; break;
        case eWell10 : value = ptStruct->rOffsetForWell10 = value; break;
        case eWell11 : value = ptStruct->rOffsetForWell11 = value; break;
        case eWell12 : value = ptStruct->rOffsetForWell12 = value; break;
        default : assert( 0 );
    }
    return value;
}
void SysDefaultGenerator::setFocusLogicalWellPosition(EWellNo eWell, float value) {

    auto ptStruct = getFocusLogicalWellPositions();

    switch ( eWell ) {
        case eWell1 : ptStruct->rOffsetForWell1 = value; break;
        case eWell2 : ptStruct->rOffsetForWell2 = value; break;
        case eWell3 : ptStruct->rOffsetForWell3 = value; break;
        case eWell4 : ptStruct->rOffsetForWell4 = value; break;
        case eWell5 : ptStruct->rOffsetForWell5 = value; break;
        case eWell6 : ptStruct->rOffsetForWell6 = value; break;
        case eWell7 : ptStruct->rOffsetForWell7 = value; break;
        case eWell8 : ptStruct->rOffsetForWell8 = value; break;
        case eWell9 : ptStruct->rOffsetForWell9 = value; break;
        case eWell10 : ptStruct->rOffsetForWell10 = value; break;
        case eWell11 : ptStruct->rOffsetForWell11 = value; break;
        case eWell12 : ptStruct->rOffsetForWell12 = value; break;
        default : assert( 0 );
    }
}

//----------
void SysDefaultGenerator::downloadFocusLogicalWellPositionStruct(void) {
    PamStation *pPam = PamStationFactory::getCurrentPamStation();
    StubPamStation *pStubPamStation = dynamic_cast<StubPamStation *>(pPam);
    m_tFocusLogWellPos = pStubPamStation->getConfig()->getFocusLogicalWellPos();
}

//---------------
LEDIntensity_t *SysDefaultGenerator::getLEDIntensity(void) {
    //get the data (once) from the instrument
    if (!m_iRefreshLEDIntensity)      //invoke only once
    {
        downloadLEDIntensityStruct();    //download data (once) from instrument
        m_iRefreshLEDIntensity++;
    }

    return &m_tLEDIntensity;
}

//---------------



//-----------------------------------------------------

void SysDefaultGenerator::downloadSensorCalibrationStruct(void)  //Get data from instrument
{
    //Get SensorCalibration structs from instrument and store them in a (member) array of structs: m_rgtSensCalVal
    //this array (m_rgtSensCalVal) is too big (big enough to contain all CanObjects), but only the indexes of sensors are used

    //in order to obtain them make an array rgSens of all "wanted" controllers
    //(if you want to download data for another sensor, add it to rgSens)
    //(make sure that the wanted data is uploaded too!!! )
    ECanObj rgSens[] =
            {eTempSensDispense, eTempSensCalibration,
             eTempSensBottomLeft, eTempSensBottomMiddle, eTempSensBottomRight,
             eTempSensCoverLeft, eTempSensCoverMiddle, eTempSensCoverRight,
             ePressSensOverPress, ePressSensUnderPress, ePressSensAspirate,
             ePressSensWell1, ePressSensWell2, ePressSensWell3, ePressSensWell4,
             ePressSensWell5, ePressSensWell6, ePressSensWell7, ePressSensWell8,
             ePressSensWell9, ePressSensWell10, ePressSensWell11, ePressSensWell12,
             eEncDispenseHead1A, eEncDispenseHead2A, eEncDispenseHead3A, eEncDispenseHead4A,
             eEncFilterA, eEncFocusA, eEncTransXA, eEncTransYA,
             eSensAspirateHeadUp,
             eCurrSensDispenseHeater, eCurrSensAspirateMotor,
             eCurrSensBottomLeft, eCurrSensBottomMiddle, eCurrSensBottomRight,
             eCurrSensCoverLeft, eCurrSensCoverMiddle, eCurrSensCoverRight,
             eCurrSensTransX, eCurrSensTransY, eCurrSensFocus, eCurrSensFilter
            };

    int iSize_rgSens = sizeof(rgSens) / sizeof(rgSens[0]);


    for (int i = 0; i < iSize_rgSens; i++) {
        Sensor_Calibration_Data_t scd = m_pCAN->getSensorCalibrationData(rgSens[i]);
        memcpy(&m_rgtSensCalVal[rgSens[i]],
               &scd,
               sizeof(m_rgtSensCalVal[rgSens[i]])
        );

        //the first value of the sensCalStruct is the sensor-obj
        //check if you ask the right sensor  on the right obj-adr
        assert (rgSens[i] == m_rgtSensCalVal[rgSens[i]].eSensorObj);
    }

}

//------------------------
void SysDefaultGenerator::downloadControlableSettingsStruct(void)     //Get Data from instrument:
{
    //Get ctrl-settings structs from instrument and store them in a (member) array of structs: m_rgtCtrlSett
    //this array (m_rgtCtrlSett) is too big (big enough to contain all CanObjects), but only the indexes of sensors are used

    //in order to obtain them make an array rgCtrl of all "wanted" controllers
    //(if you want to download data for another controller, add it to rgCtrl)
    //(make sure that the wanted data is uploaded too!!! )
    ECanObj rgCtrl[] = {
            eCtrlAspirateHeadPositioner, eCtrlDispenseHeadHeater,
            eCtrlDispenseHeadPositioner1, eCtrlDispenseHeadPositioner2,
            eCtrlDispenseHeadPositioner3, eCtrlDispenseHeadPositioner4,
            eCtrlFilterPositioner, eCtrlFocusPositioner,
            eCtrlOverPressureSupplier, eCtrlUnderPressureSupplier,
            eCtrlHtrCoverLeft, eCtrlHtrBottomLeft,
            eCtrlHtrCoverMiddle, eCtrlHtrBottomMiddle,
            eCtrlHtrCoverRight, eCtrlHtrBottomRight,
            eCtrlTransporterX, eCtrlTransporterY};

    //calculate nr of items in array (size)
    int iSize_rgCtrl = sizeof(rgCtrl) / sizeof(rgCtrl[0]);

    //loop through the array rgCtrl and get the data for the can-objects in this array
    // and store the obtained data in the array m_rgtCtrlSett
    for (int i = 0; i < iSize_rgCtrl; i++) {
        Controllable_Settings_t cs = m_pCAN->getControllerParameters(rgCtrl[i]);
        memcpy(&m_rgtCtrlSett[rgCtrl[i]],
               &cs,
               sizeof(m_rgtCtrlSett[rgCtrl[i]])
        );

        //the first value of the sensCalStruct is the sensor-obj
        //check if you ask the right data  on the right obj-adr
        assert (rgCtrl[i] == m_rgtCtrlSett[rgCtrl[i]].eCtrlObj);


        //convert fom LU to SI:
        //-
        m_rgtCtrlSett[rgCtrl[i]].rMaxProperty = convertLUtoSI(m_rgtCtrlSett[rgCtrl[i]].eSensObj,
                                                              m_rgtCtrlSett[rgCtrl[i]].max_setpoint);
        m_rgtCtrlSett[rgCtrl[i]].rMinProperty = convertLUtoSI(m_rgtCtrlSett[rgCtrl[i]].eSensObj,
                                                              m_rgtCtrlSett[rgCtrl[i]].min_setpoint);

        m_rgtCtrlSett[rgCtrl[i]].rTrackingError = convertLUtoSI(m_rgtCtrlSett[rgCtrl[i]].eSensObj,
                                                                m_rgtCtrlSett[rgCtrl[i]].min_setpoint +
                                                                m_rgtCtrlSett[rgCtrl[i]].max_tracking_error) -
                                                  m_rgtCtrlSett[rgCtrl[i]].rMinProperty;
        m_rgtCtrlSett[rgCtrl[i]].rTolerance = convertLUtoSI(m_rgtCtrlSett[rgCtrl[i]].eSensObj,
                                                            m_rgtCtrlSett[rgCtrl[i]].min_setpoint +
                                                            m_rgtCtrlSett[rgCtrl[i]].max_tolerance_error) -
                                              m_rgtCtrlSett[rgCtrl[i]].rMinProperty;

        if (m_rgtCtrlSett[rgCtrl[i]].eCurrSensObj != eCanObjMin) {
            m_rgtCtrlSett[rgCtrl[i]].rMaxCurrent = convertLUtoSI(m_rgtCtrlSett[rgCtrl[i]].eCurrSensObj,
                                                                 m_rgtCtrlSett[rgCtrl[i]].max_dc_i);
        } else {
            m_rgtCtrlSett[rgCtrl[i]].rMaxCurrent = 0;
        }


        //VELOCITY AND ACCELERATION
        //VELOCITY & ACCELERATION
        float rTSample = this->getSensorCalibrationData(m_rgtCtrlSett[rgCtrl[i]].eSensObj)->rTsample;
        if (rTSample > 0) {
            float ConvTime = 1.0; //most ctrl are SI/sec, but some are SI/min

            if (m_rgtCtrlSett[rgCtrl[i]].eCtrlObj == eCtrlDispenseHeadHeater ||
                m_rgtCtrlSett[rgCtrl[i]].eCtrlObj == eCtrlHtrBottomLeft ||
                m_rgtCtrlSett[rgCtrl[i]].eCtrlObj == eCtrlHtrBottomMiddle ||
                m_rgtCtrlSett[rgCtrl[i]].eCtrlObj == eCtrlHtrBottomRight ||
                m_rgtCtrlSett[rgCtrl[i]].eCtrlObj == eCtrlHtrCoverLeft ||
                m_rgtCtrlSett[rgCtrl[i]].eCtrlObj == eCtrlHtrCoverMiddle ||
                m_rgtCtrlSett[rgCtrl[i]].eCtrlObj == eCtrlHtrCoverRight) {
                ConvTime = 60.0;
            }

            m_rgtCtrlSett[rgCtrl[i]].rMinVelocity = convertLUtoSI(m_rgtCtrlSett[rgCtrl[i]].eSensObj,
                                                                  m_rgtCtrlSett[rgCtrl[i]].min_setpoint +
                                                                  (m_rgtCtrlSett[rgCtrl[i]].du_min_vel * ConvTime /
                                                                   rTSample)) - m_rgtCtrlSett[rgCtrl[i]].rMinProperty;
            m_rgtCtrlSett[rgCtrl[i]].rMaxVelocity = convertLUtoSI(m_rgtCtrlSett[rgCtrl[i]].eSensObj,
                                                                  m_rgtCtrlSett[rgCtrl[i]].min_setpoint +
                                                                  (m_rgtCtrlSett[rgCtrl[i]].du_max_vel * ConvTime /
                                                                   rTSample)) - m_rgtCtrlSett[rgCtrl[i]].rMinProperty;
            m_rgtCtrlSett[rgCtrl[i]].rDefaultVelocity = convertLUtoSI(m_rgtCtrlSett[rgCtrl[i]].eSensObj,
                                                                      m_rgtCtrlSett[rgCtrl[i]].min_setpoint +
                                                                      (m_rgtCtrlSett[rgCtrl[i]].DefaultVel * ConvTime /
                                                                       rTSample)) -
                                                        m_rgtCtrlSett[rgCtrl[i]].rMinProperty;

            m_rgtCtrlSett[rgCtrl[i]].rMinAcceleration = convertLUtoSI(m_rgtCtrlSett[rgCtrl[i]].eSensObj,
                                                                      m_rgtCtrlSett[rgCtrl[i]].min_setpoint +
                                                                      (m_rgtCtrlSett[rgCtrl[i]].du_min_acc * ConvTime /
                                                                       (rTSample * rTSample))) -
                                                        m_rgtCtrlSett[rgCtrl[i]].rMinProperty;
            m_rgtCtrlSett[rgCtrl[i]].rMaxAcceleration = convertLUtoSI(m_rgtCtrlSett[rgCtrl[i]].eSensObj,
                                                                      m_rgtCtrlSett[rgCtrl[i]].min_setpoint +
                                                                      (m_rgtCtrlSett[rgCtrl[i]].du_max_acc * ConvTime /
                                                                       (rTSample * rTSample))) -
                                                        m_rgtCtrlSett[rgCtrl[i]].rMinProperty;
            m_rgtCtrlSett[rgCtrl[i]].rDefaultAcceleration = convertLUtoSI(m_rgtCtrlSett[rgCtrl[i]].eSensObj,
                                                                          m_rgtCtrlSett[rgCtrl[i]].min_setpoint +
                                                                          (m_rgtCtrlSett[rgCtrl[i]].DefaultAcc *
                                                                           ConvTime / (rTSample * rTSample))) -
                                                            m_rgtCtrlSett[rgCtrl[i]].rMinProperty;
            //uSettleTime in [ms]
            m_rgtCtrlSett[rgCtrl[i]].uSettleTime =
                    m_rgtCtrlSett[rgCtrl[i]].T_Settletime * rTSample * 1000;   //[tik] * [sec/tik] * 1000 [ms/sec]



        } else {
            m_rgtCtrlSett[rgCtrl[i]].rMinVelocity = 0;
            m_rgtCtrlSett[rgCtrl[i]].rMaxVelocity = 0;
            m_rgtCtrlSett[rgCtrl[i]].rDefaultVelocity = 0;

            //Accelleration
            m_rgtCtrlSett[rgCtrl[i]].rMinAcceleration = 0;
            m_rgtCtrlSett[rgCtrl[i]].rMaxAcceleration = 0;
            m_rgtCtrlSett[rgCtrl[i]].rDefaultAcceleration = 0;

            //T_settleTime
            m_rgtCtrlSett[rgCtrl[i]].uSettleTime = 0;
        }


    }
}

//-------------------
void SysDefaultGenerator::setControllableSettings(Controllable_Settings_t tStruct) {
    //convert from SI to LU
    //- rMaxProperty => max_setpoint
    tStruct.max_setpoint = convertSItoLU(tStruct.eSensObj, tStruct.rMaxProperty);
    tStruct.min_setpoint = convertSItoLU(tStruct.eSensObj, tStruct.rMinProperty);

    //for some controllers the conversion is not linear,
    //the tolerance and tracking are in another range   (for 0.5 �C versus 20-80�C)
    //so calculate conversion somewhere in range
    //NB  tStruct.min_setpoint = convertSItoLU(tStruct.eSensObj , tStruct.rMinProperty);
    tStruct.max_tracking_error =
            convertSItoLU(tStruct.eSensObj, (tStruct.rMinProperty + tStruct.rTrackingError)) - tStruct.min_setpoint;
    tStruct.max_tolerance_error =
            convertSItoLU(tStruct.eSensObj, (tStruct.rMinProperty + tStruct.rTolerance)) - tStruct.min_setpoint;

    if (tStruct.eCurrSensObj != eCanObjMin) {
        tStruct.max_dc_i = convertSItoLU(tStruct.eCurrSensObj, tStruct.rMaxCurrent);
    } else {
        tStruct.max_dc_i = 0;
    }


    //VELOCITY & ACCELERATION
    float rTSample = this->getSensorCalibrationData(tStruct.eSensObj)->rTsample;
    if (rTSample > 0) {
        float ConvTime = 1.0; //most ctrl are SI/sec, but some are SI/min

        if (tStruct.eCtrlObj == eCtrlDispenseHeadHeater ||
            tStruct.eCtrlObj == eCtrlHtrBottomLeft ||
            tStruct.eCtrlObj == eCtrlHtrBottomMiddle ||
            tStruct.eCtrlObj == eCtrlHtrBottomRight ||
            tStruct.eCtrlObj == eCtrlHtrCoverLeft ||
            tStruct.eCtrlObj == eCtrlHtrCoverMiddle ||
            tStruct.eCtrlObj == eCtrlHtrCoverRight) {
            ConvTime = 60.0;
        }

        //Velocity
        tStruct.du_min_vel = (convertSItoLU(tStruct.eSensObj, (tStruct.rMinProperty + tStruct.rMinVelocity)) -
                              tStruct.min_setpoint) * rTSample / ConvTime;
        tStruct.du_max_vel = (convertSItoLU(tStruct.eSensObj, (tStruct.rMinProperty + tStruct.rMaxVelocity)) -
                              tStruct.min_setpoint) * rTSample / ConvTime;
        tStruct.DefaultVel = (convertSItoLU(tStruct.eSensObj, (tStruct.rMinProperty + tStruct.rDefaultVelocity)) -
                              tStruct.min_setpoint) * rTSample / ConvTime;

        //Accelleration
        tStruct.du_min_acc = (convertSItoLU(tStruct.eSensObj, (tStruct.rMinProperty + tStruct.rMinAcceleration)) -
                              tStruct.min_setpoint) * rTSample * rTSample / ConvTime;
        tStruct.du_max_acc = (convertSItoLU(tStruct.eSensObj, (tStruct.rMinProperty + tStruct.rMaxAcceleration)) -
                              tStruct.min_setpoint) * rTSample * rTSample / ConvTime;
        tStruct.DefaultAcc = (convertSItoLU(tStruct.eSensObj, (tStruct.rMinProperty + tStruct.rDefaultAcceleration)) -
                              tStruct.min_setpoint) * rTSample * rTSample / ConvTime;

        //T_settleTime (uSettleTime in [ms] + round TSample is fe 0.001000000omething) )
        tStruct.T_Settletime = int(
                (tStruct.uSettleTime / (1000 * rTSample)) + 0.5);   // [ms] / ( 1000 [ms/sec]  *  [sec/tik] )
    } else {
        tStruct.du_min_vel = 0;
        tStruct.du_max_vel = 0;
        tStruct.DefaultVel = 0;

        //Accelleration
        tStruct.du_min_acc = 0;
        tStruct.du_max_acc = 0;
        tStruct.DefaultAcc = 0;

        //T_settleTime
        tStruct.T_Settletime = 0;
    }

    m_pCAN->setControllerParameters(tStruct);
    m_iRefreshControlableSettingsStruct = 0;
}

//-----
void SysDefaultGenerator::downloadWagonLogicalPositionStruct(void) {
    m_tWagonLogPos = m_pCAN->getWagonLogicalPositions();
}

//--------
void SysDefaultGenerator::downloadFilterLogicalPositionStruct(void) {
    m_tFilterLogPos = m_pCAN->getFilterLogicalPositions();
}

//----------
void SysDefaultGenerator::downloadFocusLogicalPositionStruct(void) {
    m_tFocusLogPos = m_pCAN->getFocusLogicalPositions();
}

//-------
void SysDefaultGenerator::downloadLEDIntensityStruct(void) {
    m_tLEDIntensity = m_pCAN->getLEDIntensity();
}

//--------------------------------------------------------------------------------

void SysDefaultGenerator::setSensorCalibrationData(Sensor_Calibration_Data_t tStruct) {
    m_pCAN->setSensorCalibrationData(tStruct);
    m_iRefreshSensorCalibrationStruct = 0;
}


void SysDefaultGenerator::setWagonLogicalPositions(Wagon_LogicalXYPos_t tStruct) {
    m_pCAN->setWagonLogicalPostions(tStruct);
    m_iRefreshWagonLogicalPositions = 0;
}

void SysDefaultGenerator::setFilterLogicalPositions(Filter_LogicalPos_t tStruct) {
    m_pCAN->setFilterLogicalPostions(tStruct);
    m_iRefreshFilterLogicalPositions = 0;
}


void SysDefaultGenerator::setFocusLogicalPositions(Focus_LogicalPos_t tStruct) {
    m_pCAN->setFocusLogicalPostions(tStruct);
    m_iRefreshFocusLogicalPositions = 0;
}

void SysDefaultGenerator::setFocusLogicalWellPositions(Focus_LogicalWellPos_t tStruct) {
    m_tFocusLogWellPos = tStruct;
}

void SysDefaultGenerator::setLEDIntensity(LEDIntensity_t tStruct) {
    m_pCAN->setLEDIntensity(tStruct);
    m_iRefreshLEDIntensity = 0;
}

//---------------------------------------------------------
void SysDefaultGenerator::initialise_protocolSettings() {
    //see PamDefs_InitialValues.h
    PumpSettings_t tDefault1 =
            {
                    VALVE_OPEN_TIME,  //PumpSettings_t::uValveOpenTime
                    PRESS_SETTL_TIME,  //PumpSettings_t::uPressureSettlingDelay
                    FLUID_FLOW_TIME,  //PumpSettings_t::uFluidFlowDelay
                    BROKEN_MEMB_PRESS,  //PumpSettings_t::rBrokenMembranePressure
                    MAX_PMP_TIME         //PumpSettings_t::uMaximumWaitingTime
            };
    setPumpingDefaults(tDefault1);

    //see PamDefs_InitialValues.h
    AspirationSettings_t tDefault2 =
            {
                    ASP_MIN_PRS,      //AspirationSettings_t::rPressureUnderlimit
                    ASP_MAX_PRS,      //AspirationSettings_t::rPressureUpperLimit
                    ASP_DRY_TIME,      //AspirationSettings_t::uAspirateTubeCleanDelay
                    ASP_BUILD_TIME        //AspirationSettings_t::uAspPressBuildupDelay
            };
    setAspirationDefaults(tDefault2);

    //see PamDefs_InitialValues.h
    DispenseSettings_t tDefault3 =
            {
                    HTBL_BITSET,   //DispenseSettings_t::uHeatableHeadBitSet
                    PRIME_AMOUNT,   //DispenseSettings_t::rPrimeAmount
                    RETRACT_AMOUNT,   //DispenseSettings_t::rRetractAmount
                    RETRACT_DELAY      //DispenseSettings_t::uRetractDelay
            };
    setDispenseDefaults(tDefault3);

    //see PamDefs_InitialValues.h
    Load_Unload_Settings_t tDefault4 =
            {
                    SUPPLY_PRESS,        //Load_Unload_Settings_t::rDefaultOverPressure
                    SUPPLY_VAC,        //Load_Unload_Settings_t::rDefaultUnderPressure
                    INIT_TEMP,        //Load_Unload_Settings_t::rInitWagonTemperature
                    SAFE_UNLD_TEMP         //Load_Unload_Settings_t::rSafeUnloadTemperature
            };
    setLoad_Unload_Defaults(tDefault4);


}


//------------------------
void SysDefaultGenerator::setPumpingDefaults(PumpSettings_t tStruct) {
    m_tPumpSetting.uValveOpenTime = tStruct.uValveOpenTime; //[ms]
    m_tPumpSetting.uPressureSettlingDelay = tStruct.uPressureSettlingDelay; //[ms]
    m_tPumpSetting.uFluidFlowDelay = tStruct.uFluidFlowDelay; //[ms]
    m_tPumpSetting.rBrokenMembranePressure = tStruct.rBrokenMembranePressure; //[barg]
    m_tPumpSetting.uMaximumWaitingTime = tStruct.uMaximumWaitingTime; //[ms]
}

PumpSettings_t *SysDefaultGenerator::getPumpingDefaults(void) {
    return &m_tPumpSetting;
}

//----------------------------------------------------------------
void SysDefaultGenerator::setAspirationDefaults(AspirationSettings_t tStruct) {
    m_tAspirationSettings.rPressureUnderlimit = tStruct.rPressureUnderlimit;  //[barg]
    m_tAspirationSettings.rPressureUpperLimit = tStruct.rPressureUpperLimit;  //[barg]
    m_tAspirationSettings.uAspirateTubeCleanDelay = tStruct.uAspirateTubeCleanDelay;  //[ms]
    m_tAspirationSettings.uAspPressBuildupDelay = tStruct.uAspPressBuildupDelay;  //[ms]
}

//-----------------------------
AspirationSettings_t *SysDefaultGenerator::getAspirationDefaults(void) {
    return &m_tAspirationSettings;
}

//--
void SysDefaultGenerator::setDispenseDefaults(DispenseSettings_t tStruct) {
    m_tDispenseSettings.uHeatableHeadBitSet = tStruct.uHeatableHeadBitSet;    //[-]
    m_tDispenseSettings.rPrimeAmount = tStruct.rPrimeAmount;    //[�l]
    m_tDispenseSettings.rRetractAmount = tStruct.rRetractAmount;    //[�l]
    m_tDispenseSettings.uRetractDelay = tStruct.uRetractDelay;    //[ms]
}

DispenseSettings_t *SysDefaultGenerator::getDispenseDefaults(void) {
    return &m_tDispenseSettings;
}

//-----------------------
void SysDefaultGenerator::setLoad_Unload_Defaults(Load_Unload_Settings_t tStruct) {
    m_tLoad_Unload_Settings.rDefaultOverPressure = tStruct.rDefaultOverPressure; //[barg]
    m_tLoad_Unload_Settings.rDefaultUnderPressure = tStruct.rDefaultUnderPressure; //[barg]
    m_tLoad_Unload_Settings.rInitWagonTemperature = tStruct.rInitWagonTemperature; //[�C]
    m_tLoad_Unload_Settings.rSafeUnloadTemperature = tStruct.rSafeUnloadTemperature; //[�C]

}

//------
Load_Unload_Settings_t *SysDefaultGenerator::getLoad_Unload_Settings(void) {
    return &m_tLoad_Unload_Settings;
}

//---------------------------------
std::string SysDefaultGenerator::getVersionInfo(void) {
    //PS12_Version_t tStruct contains all the serial and versioning info from the 3 boards
    PS12_Version_t tStruct = m_pCAN->getBoardData();

    //strVersion is the user requested form of presenting this struct
    std::string strVersion;
    std::ostringstream stream;

    stream << "PS12_NR_" << tStruct.tSYCBInfo.iPS12_nr << ", ";
    stream << "SYCB_NR_" << tStruct.tSYCBInfo.iBrd_nr_high << tStruct.tSYCBInfo.iBrd_nr_medium
           << tStruct.tSYCBInfo.iBrd_nr_low;
    stream << "SYCB_FW_" << tStruct.tSYCBInfo.iFW_version;
    stream << "SYCB_ESW_" << tStruct.tSYCBInfo.iESW_version;

    return stream.str();

    //TODO

//    strVersion.sprintf("PS12_NR_%07d, ", tStruct.tSYCBInfo.iPS12_nr);
//    strVersion.cat_sprintf("SYCB_NR_%04X%04X%04X, ", tStruct.tSYCBInfo.iBrd_nr_high,
//                           tStruct.tSYCBInfo.iBrd_nr_medium,
//                           tStruct.tSYCBInfo.iBrd_nr_low);
//    strVersion.cat_sprintf("SYCB_FW_%03d, ", tStruct.tSYCBInfo.iFW_version);
//    strVersion.cat_sprintf("SYCB_ESW_%03d, ", tStruct.tSYCBInfo.iESW_version);
//    strVersion.cat_sprintf("SYCB_FLASH_%s, ", TDateTime(tStruct.tSYCBInfo.rFlashDate).FormatString("yyyymmdd"));
//
//    strVersion.cat_sprintf("PS12_NR_%07d, ", tStruct.tICCBInfo.iPS12_nr);
//    strVersion.cat_sprintf("ICCB_NR_%04X%04X%04X, ", tStruct.tICCBInfo.iBrd_nr_high,
//                           tStruct.tICCBInfo.iBrd_nr_medium,
//                           tStruct.tICCBInfo.iBrd_nr_low);
//    strVersion.cat_sprintf("ICCB_FW_%03d, ", tStruct.tICCBInfo.iFW_version);
//    strVersion.cat_sprintf("ICCB_ESW_%03d, ", tStruct.tICCBInfo.iESW_version);
//    strVersion.cat_sprintf("ICCB_FLASH_%s, ", TDateTime(tStruct.tICCBInfo.rFlashDate).FormatString("yyyymmdd"));
//
//    strVersion.cat_sprintf("PS12_NR_%07d, ", tStruct.tWSCBInfo.iPS12_nr);
//    strVersion.cat_sprintf("WSCB_NR_%04X%04X%04X, ", tStruct.tWSCBInfo.iBrd_nr_high,
//                           tStruct.tWSCBInfo.iBrd_nr_medium,
//                           tStruct.tWSCBInfo.iBrd_nr_low);
//    strVersion.cat_sprintf("WSCB_FW_%03d, ", tStruct.tWSCBInfo.iFW_version);
//    strVersion.cat_sprintf("WSCB_ESW_%03d, ", tStruct.tWSCBInfo.iESW_version);
//    strVersion.cat_sprintf("WSCB_FLASH_%s", TDateTime(tStruct.tWSCBInfo.rFlashDate).FormatString("yyyymmdd"));

    return strVersion;
}

//----------
void SysDefaultGenerator::setPS12SerialNumber(int iPS12_nr) {
    //only settable board data is serial number (for now)
    m_pCAN->setBoardData(iPS12_nr);
}

int SysDefaultGenerator::getPS12SerialNumber(void) {

    int iRetVal = 0;

    PS12_Version_t tStruct = m_pCAN->getBoardData();
    if ((tStruct.tSYCBInfo.iPS12_nr == tStruct.tICCBInfo.iPS12_nr) &&
        (tStruct.tICCBInfo.iPS12_nr == tStruct.tWSCBInfo.iPS12_nr)) {
        iRetVal = tStruct.tSYCBInfo.iPS12_nr;
    } else {
        iRetVal = -1;
    }

    return iRetVal;
}

//------------------------------------------------------------------------------
void SysDefaultGenerator::enableAllBoards(bool fEnable) {
    //set status of all boards
    m_pCAN->setBoardStatus(eBoard1Ctrl, fEnable);
    m_pCAN->setBoardStatus(eBoard2Ctrl, fEnable);
    m_pCAN->setBoardStatus(eBoard3Ctrl, fEnable);

    //check status
    assert (m_pCAN->getBoardStatus(eBoard1Ctrl) == fEnable);
    assert (m_pCAN->getBoardStatus(eBoard2Ctrl) == fEnable);
    assert (m_pCAN->getBoardStatus(eBoard3Ctrl) == fEnable);
}

void SysDefaultGenerator::flashAllBoards(void) {
    //write RAM to flash, after this command each board is dead for a certian time
    m_pCAN->flashBoard(eBoard1Ctrl);
    m_pCAN->flashBoard(eBoard2Ctrl);
    m_pCAN->flashBoard(eBoard3Ctrl);
}





