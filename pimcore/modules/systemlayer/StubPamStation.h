//---------------------------------------------------------------------------

#ifndef StubPamStationH
#define StubPamStationH
//---------------------------------------------------------------------------
#include <cassert>
#include <mutex>
#include "../pimcore/PamStation.h"             //for class inheritance
#include "SysEventSource.h"     //for class inheritance
#include "../utils/AMConfiguration.h"

//Interface Generators
class SysDefaultGenerator;
class SysExceptionGenerator;
//class SysImageGenerator;
class SysLogGenerator;

//Interface Hardware
class PropAspirateHeadPositioner;
class PropAspiratePump;
class PropCCD;
class PropDispenseHeadHeater;
class PropDispenseHeadPositioner;
class PropDisposable;
class PropFilterPositioner;
class PropFocusPositioner;
class PropLEDUnit;
class PropOverPressureSupplier;
class PropUnderPressureSupplier;
class PropUnloadButton;
class PropWagonHeater;
class PropWagonPositioner;
class PropWagonXYPositioner;
class PropLeds;

//Non Interface Items
class CANController;
class CanInterface;
class ThreadRefresh;
class PropIncubationChamberHeater;

class AMConfiguration;

//---------------------------------------------------------------------------
class StubPamStation: virtual public PamStation, virtual public SysEventSource
{

  public:
     StubPamStation();
    ~StubPamStation();

    //ALEX : to handle exception in case the instrument is not power on
    void initializeCanInterface() ;

    virtual AMConfiguration * getConfig();
    //INTERFACE GENERATORS
    virtual DefaultGenerator      * getDefaultGenerator()      ;
    virtual ExceptionGenerator    * getExceptionGenerator()    ;
//    virtual ImageGenerator        * getImageGenerator()        ;
    virtual LogGenerator          * getLogGenerator()          ;

    //INTERFACE HARDWARE
    virtual AspirateHeadPositioner * getAspirateHeadPositioner();
    virtual AspiratePump           * getAspiratePump()          ;
    virtual CCD                    * getCCD()                   ;
    virtual Disposable             * getDisposable()            ;
    virtual PropDisposable             * getPropDisposable()            ;
    virtual DispenseHeadHeater     * getDispenseHeadHeater()    ;
    virtual DispenseHeadPositioner * getDispenseHeadPositioner(EDispenseHeadNo eHeadNo);
    virtual FilterPositioner       * getFilterPositioner()      ;
    virtual FocusPositioner        * getFocusPositioner()       ;
    virtual LEDUnit                * getLEDUnit(ELEDNo eLedNo)  ;
    virtual OverPressureSupplier   * getOverPressureSupplier()  ;
    virtual UnderPressureSupplier  * getUnderPressureSupplier() ;
    virtual UnloadButton           * getUnloadButton()          ;
    virtual IncubationChamberHeater* getIncubationChamberHeater(EIncubationChamberHeater);
    virtual WagonHeater            * getWagonHeater()           ;
    virtual WagonPositioner        * getWagonPositioner(EWagonPositioner);
    virtual WagonXYPositioner      * getWagonXYPositioner()     ;

    //INTERFACE EXTRA FUNCTIONALITY
    virtual void abort(void);

    //NON INTERFACE OBJECTS
            CANController         * getCANController()         ;
            CanInterface          * getCanInterface()          ;

    //NON INTERFACE METHODS
    void updateProperties(void);

    //- JPau added 20090403
    // New interface added at end public section because Evolve is linked to old dll
    // otherwise abort function is mixed up with GetLeds();
    virtual PropLeds               * getLeds()     ;



  private:
    static int iInstanceCounter;   //for singleton behaviour test


    AMConfiguration * m_configuration;
    //Interface Generators
    SysDefaultGenerator       * m_pSysDefaultGenerator    ;
    SysExceptionGenerator     * m_pSysExceptionGenerator  ;
//    SysImageGenerator         * m_pSysImageGenerator      ;
    SysLogGenerator           * m_pSysLogGenerator        ;

    //Interface Hardware
    PropAspirateHeadPositioner * m_pPropAspirateHeadPositioner           ;
    PropAspiratePump           * m_pPropAspiratePump                     ;
    PropCCD                    * m_pPropCCD                              ;
    PropDispenseHeadHeater     * m_pPropDispenseHeadHeater               ;
    PropDispenseHeadPositioner * m_pPropDispenseHeadPositioner[eHeadMax] ; //put pointers in an array[0,1,2,3,4] , array[0] not used
    PropDisposable             * m_pPropDisposable                       ;
    PropFilterPositioner       * m_pPropFilterPositioner                 ;
    PropFocusPositioner        * m_pPropFocusPositioner                  ;
    PropLEDUnit                * m_pPropLEDUnit[eLEDNoMax]               ; //put pointers in an array[0,1,2,3]  , array[0] not used
    PropOverPressureSupplier   * m_pPropOverPressureSupplier             ;
    PropUnderPressureSupplier  * m_pPropUnderPressureSupplier            ;
    PropUnloadButton           * m_pPropUnloadButton                     ;
    PropIncubationChamberHeater* m_pPropIncubationChamberHeater[eIncChmbrHtrMax]; //put pointers in an array[0,1,2,3,4,5,6]  , array[0] not used
    PropWagonHeater            * m_pPropWagonHeater                      ;
    PropWagonPositioner        * m_pPropWagonPositioner[ePositionerMax]  ; //put pointers in an array[0,1,2] , array[0] not used
    PropWagonXYPositioner      * m_pPropWagonXYPositioner                ;
    PropLeds                   * m_pPropLeds;

    //Non Interface Items
    CANController               * m_pCANController                        ;
    CanInterface                * m_pCanInterface                         ;
    ThreadRefresh               * m_pThreadRefresh                        ;

    //properties
    unsigned                      m_uRefreshDelay;
    unsigned                      m_uWaitTime;

//    std::recursive_mutex m_pGuard;

    std::recursive_mutex m_pGuard;


    //abort Instrument hardware actions
    void onAbort(void);






};

#endif  //StubPamStationH
