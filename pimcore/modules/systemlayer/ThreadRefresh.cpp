//---------------------------------------------------------------------------


#pragma hdrstop

#include <thread>
#include <chrono>
#include <iostream>
#include <boost/exception/diagnostic_information.hpp>

#include "ThreadRefresh.h"   //for header-file
#include "StubPamStation.h"  //for using object
#include "../pimcore/PamStationException.h" //for using object

#pragma package(smart_init)


ThreadRefresh::ThreadRefresh() {
    running = false;
    m_pParent = nullptr;
    m_uThreadDelay = 0;
    numError = 0;
}

void ThreadRefresh::stop() {
    if (running){
        running = false;
        thread.join();
    }
}

void ThreadRefresh::start() {
    if (running) return;
    thread = std::thread([=]() {
        running = true;
        while (running) {
            try {
                m_pParent->updateProperties();
                std::this_thread::sleep_for(std::chrono::milliseconds(m_uThreadDelay));
            }
            catch (PamStationException &e) {
                //don't throw from update-loop, only exceptionlistener -if any- is notified
                //delete e;
                std::cout << "ThreadRefresh -- PamStationException -- " << e.getDescription() << std::endl;
                std::this_thread::sleep_for(std::chrono::milliseconds(m_uThreadDelay));
                if (e.getId() == CANINTERFACE_UNABLE_TO_SEND) {
                    numError++;
                }
                if (numError > 10) {
                    throw;
                }
            } catch (...) {
                std::cout << "ThreadRefresh -- unknown error -- " << boost::current_exception_diagnostic_information() << std::endl;
                throw ;
            }
        }
    });
}


//---------------------------------------------------------------------------
