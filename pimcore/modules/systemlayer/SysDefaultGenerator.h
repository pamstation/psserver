//---------------------------------------------------------------------------
#ifndef SysDefaultGeneratorH
#define SysDefaultGeneratorH
//---------------------------------------------------------------------------
#include "../pimcore/DefaultGenerator.h"      //for inheritance (interface)
#include "SysEventSource.h"   //for inheritance (base-implementation)

class CANController;

class SysDefaultGenerator : virtual public DefaultGenerator, virtual public SysEventSource {
public:
    SysDefaultGenerator();

    ~SysDefaultGenerator();

    /////////////////////
    /*INTERFACE SECTION*/
    /////////////////////

    //CONVERSIONS USING SENSOR_CALIBRATION DATA
    virtual int
    convertSItoLU(ECanObj eCanObj, float rValueInSI);   //input "SI": �C, barg, �l, mm, returns "LU": V, puls
    virtual float
    convertLUtoSI(ECanObj eCanObj, int iValueInLU);   //input "LU": V, puls, returns "SI": �C, barg, �l, mm
    virtual int CalculateRootQuadraticEquation(double &rX1, double &rX2, double &rA, double &rB, double &rC);

    //download data from instrument
    virtual Sensor_Calibration_Data_t *
    getSensorCalibrationData(ECanObj eCanObj); //eCanObj is either a sensor or the controller using this sensor
    virtual Controllable_Settings_t *
    getControllableSettings(ECanObj eCanObj); //CONTROLLABLE SETTINGS (min, max, default, timeout, resolution)
    virtual Wagon_LogicalXYPos_t *getWagonLogicalPositions(void);

    virtual Filter_LogicalPos_t *getFilterLogicalPositions(void);

    virtual Focus_LogicalWellPos_t *getFocusLogicalWellPositions(void);

    virtual Focus_LogicalPos_t *getFocusLogicalPositions(void);

    virtual LEDIntensity_t *getLEDIntensity(void);

    virtual WagonHeater_Settings_t *
    getWagonHeaterSettings(void) { return nullptr; }; //function became obsolete, but interface should remain intact
    virtual DispenseHeadHeater_Settings_t *getDispenseHeadHeaterSettings(
            void) { return nullptr; }; //function became obsolete, but interface should remain intact

    //upload data to instrument
    virtual void setSensorCalibrationData(Sensor_Calibration_Data_t);

    virtual void setControllableSettings(Controllable_Settings_t);

    virtual void setWagonLogicalPositions(Wagon_LogicalXYPos_t);

    virtual void setFilterLogicalPositions(Filter_LogicalPos_t);

    virtual void setFocusLogicalWellPosition(EWellNo well, float value);
    virtual float getFocusLogicalWellPosition     (EWellNo well) ;

    virtual void setFocusLogicalPositions(Focus_LogicalPos_t);

    virtual void setFocusLogicalWellPositions(Focus_LogicalWellPos_t);

    virtual void setLEDIntensity(LEDIntensity_t);

    virtual void setWagonHeaterSettings(WagonHeater_Settings_t) {};

    virtual void setDispenseHeadHeaterSettings(DispenseHeadHeater_Settings_t) {};


    virtual void setPumpingDefaults(PumpSettings_t);

    virtual void setAspirationDefaults(AspirationSettings_t);

    virtual void setDispenseDefaults(DispenseSettings_t);

    virtual void setLoad_Unload_Defaults(Load_Unload_Settings_t);

    virtual PumpSettings_t *getPumpingDefaults(void);

    virtual AspirationSettings_t *getAspirationDefaults(void);

    virtual DispenseSettings_t *getDispenseDefaults(void);

    virtual Load_Unload_Settings_t *getLoad_Unload_Settings(void);

    //version info data
    virtual std::string getVersionInfo(void);          //returns all relevant version info of all boards as a string
    virtual void setPS12SerialNumber(int iPS12_nr);  //flashes PS12 serial number (YYMMXX) on each board
    virtual int getPS12SerialNumber(
            void);          //reads PS12 serial number (YYMMXX) on each board, returns number if identical, else -1
    //flash related methods
    virtual void enableAllBoards(
            bool fEnable); //set all boards in enabled or disabled mode (when writing values to RAM, the board status should be disabled)
    virtual void flashAllBoards(void); //flashes all values from the RAM-memory to the flash-prom


private:
    static int iInstanceCounter;   //for singleton behaviour test

    //get data from instrument
    void downloadSensorCalibrationStruct(void); //store data in m_rgtSensCalVal[]
    void downloadControlableSettingsStruct(void); //store data in m_rgtCtrlSett[]
    void downloadWagonLogicalPositionStruct(void); //store data in m_tWagonLogPos
    void downloadFilterLogicalPositionStruct(void); //store data in m_tFilterLogPos
    void downloadFocusLogicalPositionStruct(void); //store data in m_tFocusLogPos
    void downloadFocusLogicalWellPositionStruct(void); //store data in m_tFocusLogPos
    void downloadLEDIntensityStruct(void); //store data in m_tLEDIntensity
    //void downloadWagonHeaterSettStruct(void)       ; //store data in m_tWagonHeaterSett
    //void downloadDispenseHeadHeaterSettStruct(void); //store data in m_tDispenseHeadHeaterSett



    //store data
    Sensor_Calibration_Data_t m_rgtSensCalVal[eCanObjMax];//array of pointers to a SensorCalibrationStruct
    Controllable_Settings_t m_rgtCtrlSett[eCanObjMax];//array of pointers to a ConytrollableSettingsStruct
    Wagon_LogicalXYPos_t m_tWagonLogPos;//struct with logical wagon XY-positions
    Filter_LogicalPos_t m_tFilterLogPos;//struct with logical filter-positions
    Focus_LogicalPos_t m_tFocusLogPos;//struct with logical focus-positions
    Focus_LogicalWellPos_t m_tFocusLogWellPos;
    LEDIntensity_t m_tLEDIntensity;//struct with LED intensities
    WagonHeater_Settings_t m_tWagonHeaterSett;//struct with WagonHeater process settings
    DispenseHeadHeater_Settings_t m_tDispenseHeadHeaterSett;//struct with DispenseHeadHeater process settings


    int m_iRefreshSensorCalibrationStruct;
    int m_iRefreshControlableSettingsStruct;
    int m_iRefreshWagonLogicalPositions;
    int m_iRefreshFilterLogicalPositions;
    int m_iRefreshFocusLogicalPositions;
    int m_iRefreshFocusLogicalWellPositions;
    int m_iRefreshLEDIntensity;
    int m_iWagonHeaterSettings;
    int m_iDispenseHeadHeaterSettings;

    void initialise_protocolSettings(void);

    PumpSettings_t m_tPumpSetting;
    AspirationSettings_t m_tAspirationSettings;
    DispenseSettings_t m_tDispenseSettings;
    Load_Unload_Settings_t m_tLoad_Unload_Settings;

    CANController *m_pCAN;

};

#endif  //SysDefaultGeneratorH
