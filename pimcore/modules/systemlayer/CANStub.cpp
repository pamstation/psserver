//---------------------------------------------------------------------------


#pragma hdrstop

#include "CANStub.h"
#include <cassert>
#include <cstdint>
#include <chrono>
#include <thread>

//---------------------------------------------------------------------------

#pragma package(smart_init)

CANStub::CANStub() { m_fInitialised = false; };

CANStub::~CANStub() { m_fInitialised = false; };

void CANStub::Send_ReceiveCanMessage(CPC_CAN_MSG_T *pSend_CpcMsg, CPC_CAN_MSG_T *pReply_CpcMsg) {
    //this method simulates the hardware behaviour:
    // - all commands are immediate executed and ready
    // - no errors occur


    if (!m_fInitialised)                   //m_fStubInitialised is set in CANController contructor
    {
        initialise();
        m_fInitialised = true;
    }



    //SIMULATION, FILL CAN REPLY: ID
    switch (pSend_CpcMsg->id) {
        case ePCtoBOARD1:
            pReply_CpcMsg->id = eBOARD1toPC;
            break;
        case ePCtoBOARD2:
            pReply_CpcMsg->id = eBOARD2toPC;
            break;
        case ePCtoBOARD3:
            pReply_CpcMsg->id = eBOARD3toPC;
            break;
        default         :
            assert(0);
    }

    //SIMULATION, FILL CAN REPLY: LENGTH
    switch (pSend_CpcMsg->msg[1]) {   //msg[1] contains the eCmd
        case eCmdSetEnabled :
        case eCmdSetTarget  :
        case eCmdGoHome     :
        case eCmdSetSpeed   :
            pReply_CpcMsg->length = 2;
            break;

        case eCmdGetStatus  :
        case eCmdGetTarget  :
        case eCmdGetActual  :
        case eCmdGetError   :
            pReply_CpcMsg->length = 6;
            break;

        case eCmdSetBoardStatus:
            pReply_CpcMsg->length = 2;
            break;
        case eCmdGetBoardStatus:
            pReply_CpcMsg->length = 6;
            break;
        case eCmdFlashBoard    :
            pReply_CpcMsg->length = 2;
            break;

        case eCmdSetValue1  :
        case eCmdSetValue2  :
        case eCmdSetValue3  :
        case eCmdSetValue4  :
        case eCmdSetValue5  :
        case eCmdSetValue6  :
        case eCmdSetValue7  :
        case eCmdSetValue8  :
        case eCmdSetValue9  :
        case eCmdSetValue10 :
        case eCmdSetValue11 :
        case eCmdSetValue12 :
        case eCmdSetValue13 :
        case eCmdSetValue14 :
        case eCmdSetValue15 :
        case eCmdSetValue16 :
        case eCmdSetValue17 :
        case eCmdSetValue18 :
        case eCmdSetValue19 :
        case eCmdSetValue20 :
        case eCmdSetValue21 :
        case eCmdSetValue22 :
        case eCmdSetValue23 :
        case eCmdSetValue24 :
        case eCmdSetValue25 :
        case eCmdSetValue26 :
        case eCmdSetValue27 :
        case eCmdSetValue28 :
        case eCmdSetValue29 :
        case eCmdSetValue30 :
        case eCmdSetValue31 :
        case eCmdSetValue32 :
        case eCmdSetValue33 :
        case eCmdSetValue34 :
        case eCmdSetValue35 :
        case eCmdSetValue36 :
        case eCmdSetValue37 :
        case eCmdSetValue38 :
        case eCmdSetValue39 :
        case eCmdSetValue40 :
        case eCmdSetValue41 :
        case eCmdSetValue42 :
        case eCmdSetValue43 :
        case eCmdSetValue44 :
        case eCmdSetValue45 :
        case eCmdSetValue46 :
        case eCmdSetValue47 :
        case eCmdSetValue48 :
        case eCmdSetValue49 :
        case eCmdSetValue50 :
            pReply_CpcMsg->length = 2;
            break;

        case eCmdGetValue1  :
        case eCmdGetValue2  :
        case eCmdGetValue3  :
        case eCmdGetValue4  :
        case eCmdGetValue5  :
        case eCmdGetValue6  :
        case eCmdGetValue7  :
        case eCmdGetValue8  :
        case eCmdGetValue9  :
        case eCmdGetValue10 :
        case eCmdGetValue11 :
        case eCmdGetValue12 :
        case eCmdGetValue13 :
        case eCmdGetValue14 :
        case eCmdGetValue15 :
        case eCmdGetValue16 :
        case eCmdGetValue17 :
        case eCmdGetValue18 :
        case eCmdGetValue19 :
        case eCmdGetValue20 :
        case eCmdGetValue21 :
        case eCmdGetValue22 :
        case eCmdGetValue23 :
        case eCmdGetValue24 :
        case eCmdGetValue25 :
        case eCmdGetValue26 :
        case eCmdGetValue27 :
        case eCmdGetValue28 :
        case eCmdGetValue29 :
        case eCmdGetValue30 :
        case eCmdGetValue31 :
        case eCmdGetValue32 :
        case eCmdGetValue33 :
        case eCmdGetValue34 :
        case eCmdGetValue35 :
        case eCmdGetValue36 :
        case eCmdGetValue37 :
        case eCmdGetValue38 :
        case eCmdGetValue39 :
        case eCmdGetValue40 :
        case eCmdGetValue41 :
        case eCmdGetValue42 :
        case eCmdGetValue43 :
        case eCmdGetValue44 :
        case eCmdGetValue45 :
        case eCmdGetValue46 :
        case eCmdGetValue47 :
        case eCmdGetValue48 :
        case eCmdGetValue49 :
        case eCmdGetValue50 :
            pReply_CpcMsg->length = 6;
            break;
        default             :
            assert(0);
    }

    //SIMULATION, FILL CAN REPLY: eObj
    pReply_CpcMsg->msg[0] = pSend_CpcMsg->msg[0];   //msg[0] contains the eObj

    //SIMULATION, FILL CAN REPLY: eCmd
    pReply_CpcMsg->msg[1] = pSend_CpcMsg->msg[1];   //msg[1] contains the eCmd

    //SIMULATION, (RE-)ASSEMBLE INTEGER DATA FROM 4 DATA-BYTES
    // by the way, if eCmd is a "getter" iData should be 0
    int iData = (int) ((int) (pSend_CpcMsg->msg[2]) +
                       (int) (pSend_CpcMsg->msg[3] << 8) +
                       (int) (pSend_CpcMsg->msg[4] << 16) +
                       (int) (pSend_CpcMsg->msg[5] << 24));


    //SIMULATION, CLEAR REPLY MSG
    pReply_CpcMsg->msg[2] = 0;
    pReply_CpcMsg->msg[3] = 0;
    pReply_CpcMsg->msg[4] = 0;
    pReply_CpcMsg->msg[5] = 0;
    pReply_CpcMsg->msg[6] = 0;  //Not Used
    pReply_CpcMsg->msg[7] = 0;  //Not Used

    //SIMULATION, ANSWER ON COMMANDS
    ECanObj eObj = (ECanObj) pSend_CpcMsg->msg[0];          //just for better reading
    ECanCmnd eCmd = (ECanCmnd) pSend_CpcMsg->msg[1];          //just for better reading

    //IF COMMAND IS A "SETTER", STORE WANTED RESULTS (in fields target,actual&status of simulation struct)
    if (eCmd == eCmdSetEnabled ||
        eCmd == eCmdSetTarget ||
        eCmd == eCmdGoHome ||
        eCmd == eCmdSetSpeed ||

        eCmd == eCmdSetBoardStatus ||
        eCmd == eCmdFlashBoard ||

        eCmd == eCmdSetValue1 ||
        eCmd == eCmdSetValue2 ||
        eCmd == eCmdSetValue3 ||
        eCmd == eCmdSetValue4 ||
        eCmd == eCmdSetValue5 ||
        eCmd == eCmdSetValue6 ||
        eCmd == eCmdSetValue7 ||
        eCmd == eCmdSetValue8 ||
        eCmd == eCmdSetValue9 ||
        eCmd == eCmdSetValue10 ||
        eCmd == eCmdSetValue11 ||
        eCmd == eCmdSetValue12 ||
        eCmd == eCmdSetValue13 ||
        eCmd == eCmdSetValue14 ||
        eCmd == eCmdSetValue15 ||
        eCmd == eCmdSetValue16 ||
        eCmd == eCmdSetValue17 ||
        eCmd == eCmdSetValue18 ||
        eCmd == eCmdSetValue19 ||
        eCmd == eCmdSetValue20 ||
        eCmd == eCmdSetValue21 ||
        eCmd == eCmdSetValue22 ||
        eCmd == eCmdSetValue23 ||
        eCmd == eCmdSetValue24 ||
        eCmd == eCmdSetValue25 ||
        eCmd == eCmdSetValue26 ||
        eCmd == eCmdSetValue27 ||
        eCmd == eCmdSetValue28 ||
        eCmd == eCmdSetValue29 ||
        eCmd == eCmdSetValue30 ||
        eCmd == eCmdSetValue31 ||
        eCmd == eCmdSetValue32 ||
        eCmd == eCmdSetValue33 ||
        eCmd == eCmdSetValue34 ||
        eCmd == eCmdSetValue35 ||
        eCmd == eCmdSetValue36 ||
        eCmd == eCmdSetValue37 ||
        eCmd == eCmdSetValue38 ||
        eCmd == eCmdSetValue39 ||
        eCmd == eCmdSetValue40 ||
        eCmd == eCmdSetValue41 ||
        eCmd == eCmdSetValue42 ||
        eCmd == eCmdSetValue43 ||
        eCmd == eCmdSetValue44 ||
        eCmd == eCmdSetValue45 ||
        eCmd == eCmdSetValue46 ||
        eCmd == eCmdSetValue47 ||
        eCmd == eCmdSetValue48 ||
        eCmd == eCmdSetValue49 ||
        eCmd == eCmdSetValue50) {
        int iNewCtrlStatus = 0;
        bool fBit2_Homed = (m_tCANData[eObj].iCtrlStatus >> 2) & 0x01;  //bit2 of ctrl status

        //FILL CTRL-STATUS BIT0&1
        //if cmd is "disable ctrl" => enabled&ready 0, in all other cases enabled&ready true
        if (eCmd == eCmdSetEnabled && !iData)
            iNewCtrlStatus = 0 + 0 + 0 + 0; //not enabled, not ready, not homed, no error
        else iNewCtrlStatus = 1 + 2 + 0 + 0; //    enabled,     ready, not homed, no error

        //FILL CTRL-STATUS BIT2
        //leave homed-status the same or set to true if cmd was goHome
        if (fBit2_Homed || eCmd == eCmdGoHome) iNewCtrlStatus += 4 + 0; //    enabled,     ready,     homed, no error

        switch (eCmd) {
            case eCmdSetEnabled :
                m_tCANData[eObj].iCtrlTarget = m_tCANData[eObj].iCtrlActual;   //target = actual
                m_tCANData[eObj].iCtrlActual = m_tCANData[eObj].iCtrlActual;   //don't change
                m_tCANData[eObj].iCtrlStatus = iNewCtrlStatus;
                break;
            case eCmdSetTarget  :
                m_tCANData[eObj].iCtrlTarget = iData;
                m_tCANData[eObj].iCtrlActual = iData;                      //immediate ready
                m_tCANData[eObj].iCtrlStatus = iNewCtrlStatus;
                //in case of a controller, pass new target to the matching sensor too
                //aspirateHeadPositioner is nonsense at this moment
                switch (eObj) {
                    case eCtrlOverPressureSupplier    :
                        m_tCANData[ePressSensOverPress].iCtrlActual = iData;
                        break; //CTRL, board1
                    case eCtrlUnderPressureSupplier   :
                        m_tCANData[ePressSensUnderPress].iCtrlActual = iData;
                        break; //CTRL, board1
                    case eCtrlTransporterX            :
                        m_tCANData[eEncTransXA].iCtrlActual = iData;
                        break; //CTRL, board1
                    case eCtrlTransporterY            :
                        m_tCANData[eEncTransYA].iCtrlActual = iData;
                        break; //CTRL, board1
                    case eCtrlFocusPositioner         :
                        m_tCANData[eEncFocusA].iCtrlActual = iData;
                        break; //CTRL, board1
                    case eCtrlFilterPositioner        :
                        m_tCANData[eEncFilterA].iCtrlActual = iData;
                        break; //CTRL, board1

                    case eCtrlHtrCoverLeft            :
                        m_tCANData[eTempSensCoverLeft].iCtrlActual = iData;
                        break; //CTRL, board2
                    case eCtrlHtrBottomLeft           :
                        m_tCANData[eTempSensBottomLeft].iCtrlActual = iData;
                        break; //CTRL, board2
                    case eCtrlHtrCoverMiddle          :
                        m_tCANData[eTempSensCoverMiddle].iCtrlActual = iData;
                        break; //CTRL, board2
                    case eCtrlHtrBottomMiddle         :
                        m_tCANData[eTempSensBottomMiddle].iCtrlActual = iData;
                        break; //CTRL, board2
                    case eCtrlHtrCoverRight           :
                        m_tCANData[eTempSensCoverRight].iCtrlActual = iData;
                        break; //CTRL, board2
                    case eCtrlHtrBottomRight          :
                        m_tCANData[eTempSensBottomRight].iCtrlActual = iData;
                        break; //CTRL, board2

                    case eCtrlAspirateHeadPositioner  :
                        m_tCANData[eSensAspirateHeadUp].iCtrlActual = iData;
                        break; //CTRL, board3
                    case eCtrlDispenseHeadHeater      :
                        m_tCANData[eTempSensDispense].iCtrlActual = iData;
                        break; //CTRL, board3
                    case eCtrlDispenseHeadPositioner1 :
                        m_tCANData[eEncDispenseHead1A].iCtrlActual = iData;
                        break; //CTRL, board3
                    case eCtrlDispenseHeadPositioner2 :
                        m_tCANData[eEncDispenseHead2A].iCtrlActual = iData;
                        break; //CTRL, board3
                    case eCtrlDispenseHeadPositioner3 :
                        m_tCANData[eEncDispenseHead3A].iCtrlActual = iData;
                        break; //CTRL, board3
                    case eCtrlDispenseHeadPositioner4 :
                        m_tCANData[eEncDispenseHead4A].iCtrlActual = iData;
                        break; //CTRL, board3
                }

                break;
            case eCmdGoHome     :
                m_tCANData[eObj].iCtrlTarget = HOMEPOS;
                m_tCANData[eObj].iCtrlActual = HOMEPOS;                    //immediate ready
                m_tCANData[eObj].iCtrlStatus = iNewCtrlStatus;
                break;
            case eCmdSetSpeed   :
                //do nothing
                break;

            case eCmdSetBoardStatus:
                m_tCANData[eObj].iBoardEnabled = iData;
                break;
            case eCmdFlashBoard: /*do nothing*/   break;

            case eCmdSetValue1 :
                m_tCANData[eObj].iValue1 = iData;
                break;
            case eCmdSetValue2 :
                m_tCANData[eObj].iValue2 = iData;
                break;
            case eCmdSetValue3 :
                m_tCANData[eObj].iValue3 = iData;
                break;
            case eCmdSetValue4 :
                m_tCANData[eObj].iValue4 = iData;
                break;
            case eCmdSetValue5 :
                m_tCANData[eObj].iValue5 = iData;
                break;
            case eCmdSetValue6 :
                m_tCANData[eObj].iValue6 = iData;
                break;
            case eCmdSetValue7 :
                m_tCANData[eObj].iValue7 = iData;
                break;
            case eCmdSetValue8 :
                m_tCANData[eObj].iValue8 = iData;
                break;
            case eCmdSetValue9 :
                m_tCANData[eObj].iValue9 = iData;
                break;
            case eCmdSetValue10:
                m_tCANData[eObj].iValue10 = iData;
                break;
            case eCmdSetValue11:
                m_tCANData[eObj].iValue11 = iData;
                break;
            case eCmdSetValue12:
                m_tCANData[eObj].iValue12 = iData;
                break;
            case eCmdSetValue13:
                m_tCANData[eObj].iValue13 = iData;
                break;
            case eCmdSetValue14:
                m_tCANData[eObj].iValue14 = iData;
                break;
            case eCmdSetValue15:
                m_tCANData[eObj].iValue15 = iData;
                break;
            case eCmdSetValue16:
                m_tCANData[eObj].iValue16 = iData;
                break;
            case eCmdSetValue17:
                m_tCANData[eObj].iValue17 = iData;
                break;
            case eCmdSetValue18:
                m_tCANData[eObj].iValue18 = iData;
                break;
            case eCmdSetValue19:
                m_tCANData[eObj].iValue19 = iData;
                break;
            case eCmdSetValue20:
                m_tCANData[eObj].iValue20 = iData;
                break;
            case eCmdSetValue21:
                m_tCANData[eObj].iValue21 = iData;
                break;
            case eCmdSetValue22:
                m_tCANData[eObj].iValue22 = iData;
                break;
            case eCmdSetValue23:
                m_tCANData[eObj].iValue23 = iData;
                break;
            case eCmdSetValue24:
                m_tCANData[eObj].iValue24 = iData;
                break;
            case eCmdSetValue25:
                m_tCANData[eObj].iValue25 = iData;
                break;
            case eCmdSetValue26:
                m_tCANData[eObj].iValue26 = iData;
                break;
            case eCmdSetValue27:
                m_tCANData[eObj].iValue27 = iData;
                break;
            case eCmdSetValue28:
                m_tCANData[eObj].iValue28 = iData;
                break;
            case eCmdSetValue29:
                m_tCANData[eObj].iValue29 = iData;
                break;
            case eCmdSetValue30:
                m_tCANData[eObj].iValue30 = iData;
                break;
            case eCmdSetValue31:
                m_tCANData[eObj].iValue31 = iData;
                break;
            case eCmdSetValue32:
                m_tCANData[eObj].iValue32 = iData;
                break;
            case eCmdSetValue33:
                m_tCANData[eObj].iValue33 = iData;
                break;
            case eCmdSetValue34:
                m_tCANData[eObj].iValue34 = iData;
                break;
            case eCmdSetValue35:
                m_tCANData[eObj].iValue35 = iData;
                break;
            case eCmdSetValue36:
                m_tCANData[eObj].iValue36 = iData;
                break;
            case eCmdSetValue37:
                m_tCANData[eObj].iValue37 = iData;
                break;
            case eCmdSetValue38:
                m_tCANData[eObj].iValue38 = iData;
                break;
            case eCmdSetValue39:
                m_tCANData[eObj].iValue39 = iData;
                break;
            case eCmdSetValue40:
                m_tCANData[eObj].iValue40 = iData;
                break;
            case eCmdSetValue41:
                m_tCANData[eObj].iValue41 = iData;
                break;
            case eCmdSetValue42:
                m_tCANData[eObj].iValue42 = iData;
                break;
            case eCmdSetValue43:
                m_tCANData[eObj].iValue43 = iData;
                break;
            case eCmdSetValue44:
                m_tCANData[eObj].iValue44 = iData;
                break;
            case eCmdSetValue45:
                m_tCANData[eObj].iValue45 = iData;
                break;
            case eCmdSetValue46:
                m_tCANData[eObj].iValue46 = iData;
                break;
            case eCmdSetValue47:
                m_tCANData[eObj].iValue47 = iData;
                break;
            case eCmdSetValue48:
                m_tCANData[eObj].iValue48 = iData;
                break;
            case eCmdSetValue49:
                m_tCANData[eObj].iValue49 = iData;
                break;
            case eCmdSetValue50:
                m_tCANData[eObj].iValue50 = iData;
                break;
            default :
                assert(0);
        }
    }//end "setter", when sending a "setter", the reply msg bytes remain "0"

        //IF COMMAND IS A "GETTER", RETURN WANTED RESULTS (from fields target,actual&status of simulation struct)
    else if (eCmd == eCmdGetStatus ||
             eCmd == eCmdGetTarget ||
             eCmd == eCmdGetActual ||
             eCmd == eCmdGetError ||

             eCmd == eCmdGetBoardStatus ||

             eCmd == eCmdGetValue1 ||
             eCmd == eCmdGetValue2 ||
             eCmd == eCmdGetValue3 ||
             eCmd == eCmdGetValue4 ||
             eCmd == eCmdGetValue5 ||
             eCmd == eCmdGetValue6 ||
             eCmd == eCmdGetValue7 ||
             eCmd == eCmdGetValue8 ||
             eCmd == eCmdGetValue9 ||
             eCmd == eCmdGetValue10 ||
             eCmd == eCmdGetValue11 ||
             eCmd == eCmdGetValue12 ||
             eCmd == eCmdGetValue13 ||
             eCmd == eCmdGetValue14 ||
             eCmd == eCmdGetValue15 ||
             eCmd == eCmdGetValue16 ||
             eCmd == eCmdGetValue17 ||
             eCmd == eCmdGetValue18 ||
             eCmd == eCmdGetValue19 ||
             eCmd == eCmdGetValue20 ||
             eCmd == eCmdGetValue21 ||
             eCmd == eCmdGetValue22 ||
             eCmd == eCmdGetValue23 ||
             eCmd == eCmdGetValue24 ||
             eCmd == eCmdGetValue25 ||
             eCmd == eCmdGetValue26 ||
             eCmd == eCmdGetValue27 ||
             eCmd == eCmdGetValue28 ||
             eCmd == eCmdGetValue29 ||
             eCmd == eCmdGetValue30 ||
             eCmd == eCmdGetValue31 ||
             eCmd == eCmdGetValue32 ||
             eCmd == eCmdGetValue33 ||
             eCmd == eCmdGetValue34 ||
             eCmd == eCmdGetValue35 ||
             eCmd == eCmdGetValue36 ||
             eCmd == eCmdGetValue37 ||
             eCmd == eCmdGetValue38 ||
             eCmd == eCmdGetValue39 ||
             eCmd == eCmdGetValue40 ||
             eCmd == eCmdGetValue41 ||
             eCmd == eCmdGetValue42 ||
             eCmd == eCmdGetValue43 ||
             eCmd == eCmdGetValue44 ||
             eCmd == eCmdGetValue45 ||
             eCmd == eCmdGetValue46 ||
             eCmd == eCmdGetValue47 ||
             eCmd == eCmdGetValue48 ||
             eCmd == eCmdGetValue49 ||
             eCmd == eCmdGetValue50) {
        int iRetVal = 0;

        //get wanted data
        switch (eCmd) {
            case eCmdGetStatus     :
                iRetVal = m_tCANData[eObj].iCtrlStatus;
                break;
            case eCmdGetTarget     :
                iRetVal = m_tCANData[eObj].iCtrlTarget;
                break;
            case eCmdGetActual     :
                iRetVal = m_tCANData[eObj].iCtrlActual;
                break;
            case eCmdGetError      :
                iRetVal = m_tCANData[eObj].iCtrlError;
                break;
            case eCmdGetBoardStatus:
                iRetVal = m_tCANData[eObj].iBoardEnabled;
                break;
            case eCmdGetValue1     :
                iRetVal = m_tCANData[eObj].iValue1;
                break;
            case eCmdGetValue2     :
                iRetVal = m_tCANData[eObj].iValue2;
                break;
            case eCmdGetValue3     :
                iRetVal = m_tCANData[eObj].iValue3;
                break;
            case eCmdGetValue4     :
                iRetVal = m_tCANData[eObj].iValue4;
                break;
            case eCmdGetValue5     :
                iRetVal = m_tCANData[eObj].iValue5;
                break;
            case eCmdGetValue6     :
                iRetVal = m_tCANData[eObj].iValue6;
                break;
            case eCmdGetValue7     :
                iRetVal = m_tCANData[eObj].iValue7;
                break;
            case eCmdGetValue8     :
                iRetVal = m_tCANData[eObj].iValue8;
                break;
            case eCmdGetValue9     :
                iRetVal = m_tCANData[eObj].iValue9;
                break;
            case eCmdGetValue10    :
                iRetVal = m_tCANData[eObj].iValue10;
                break;
            case eCmdGetValue11    :
                iRetVal = m_tCANData[eObj].iValue11;
                break;
            case eCmdGetValue12    :
                iRetVal = m_tCANData[eObj].iValue12;
                break;
            case eCmdGetValue13    :
                iRetVal = m_tCANData[eObj].iValue13;
                break;
            case eCmdGetValue14    :
                iRetVal = m_tCANData[eObj].iValue14;
                break;
            case eCmdGetValue15    :
                iRetVal = m_tCANData[eObj].iValue15;
                break;
            case eCmdGetValue16    :
                iRetVal = m_tCANData[eObj].iValue16;
                break;
            case eCmdGetValue17    :
                iRetVal = m_tCANData[eObj].iValue17;
                break;
            case eCmdGetValue18    :
                iRetVal = m_tCANData[eObj].iValue18;
                break;
            case eCmdGetValue19    :
                iRetVal = m_tCANData[eObj].iValue19;
                break;
            case eCmdGetValue20    :
                iRetVal = m_tCANData[eObj].iValue20;
                break;
            case eCmdGetValue21    :
                iRetVal = m_tCANData[eObj].iValue21;
                break;
            case eCmdGetValue22    :
                iRetVal = m_tCANData[eObj].iValue22;
                break;
            case eCmdGetValue23    :
                iRetVal = m_tCANData[eObj].iValue23;
                break;
            case eCmdGetValue24    :
                iRetVal = m_tCANData[eObj].iValue24;
                break;
            case eCmdGetValue25    :
                iRetVal = m_tCANData[eObj].iValue25;
                break;
            case eCmdGetValue26    :
                iRetVal = m_tCANData[eObj].iValue26;
                break;
            case eCmdGetValue27    :
                iRetVal = m_tCANData[eObj].iValue27;
                break;
            case eCmdGetValue28    :
                iRetVal = m_tCANData[eObj].iValue28;
                break;
            case eCmdGetValue29    :
                iRetVal = m_tCANData[eObj].iValue29;
                break;
            case eCmdGetValue30    :
                iRetVal = m_tCANData[eObj].iValue30;
                break;
            case eCmdGetValue31    :
                iRetVal = m_tCANData[eObj].iValue31;
                break;
            case eCmdGetValue32    :
                iRetVal = m_tCANData[eObj].iValue32;
                break;
            case eCmdGetValue33    :
                iRetVal = m_tCANData[eObj].iValue33;
                break;
            case eCmdGetValue34    :
                iRetVal = m_tCANData[eObj].iValue34;
                break;
            case eCmdGetValue35    :
                iRetVal = m_tCANData[eObj].iValue35;
                break;
            case eCmdGetValue36    :
                iRetVal = m_tCANData[eObj].iValue36;
                break;
            case eCmdGetValue37    :
                iRetVal = m_tCANData[eObj].iValue37;
                break;
            case eCmdGetValue38    :
                iRetVal = m_tCANData[eObj].iValue38;
                break;
            case eCmdGetValue39    :
                iRetVal = m_tCANData[eObj].iValue39;
                break;
            case eCmdGetValue40    :
                iRetVal = m_tCANData[eObj].iValue40;
                break;
            case eCmdGetValue41    :
                iRetVal = m_tCANData[eObj].iValue41;
                break;
            case eCmdGetValue42    :
                iRetVal = m_tCANData[eObj].iValue42;
                break;
            case eCmdGetValue43    :
                iRetVal = m_tCANData[eObj].iValue43;
                break;
            case eCmdGetValue44    :
                iRetVal = m_tCANData[eObj].iValue44;
                break;
            case eCmdGetValue45    :
                iRetVal = m_tCANData[eObj].iValue45;
                break;
            case eCmdGetValue46    :
                iRetVal = m_tCANData[eObj].iValue46;
                break;
            case eCmdGetValue47    :
                iRetVal = m_tCANData[eObj].iValue47;
                break;
            case eCmdGetValue48    :
                iRetVal = m_tCANData[eObj].iValue48;
                break;
            case eCmdGetValue49    :
                iRetVal = m_tCANData[eObj].iValue49;
                break;
            case eCmdGetValue50    :
                iRetVal = m_tCANData[eObj].iValue50;
                break;
            default            :
                assert(0);
        }

        //split (integer) data in msg-bytes
        pReply_CpcMsg->msg[2] = (uint8_t)((iRetVal) & 0xFF);   //LSB
        pReply_CpcMsg->msg[3] = (uint8_t)((iRetVal >> 8) & 0xFF);
        pReply_CpcMsg->msg[4] = (uint8_t)((iRetVal >> 16) & 0xFF);
        pReply_CpcMsg->msg[5] = (uint8_t)((iRetVal >> 24) & 0xFF);   //MSB

    } //end "getter"
    else
        assert (0); //no getter or setter

//    Sleep(0);


    std::this_thread::sleep_for(std::chrono::milliseconds(0));

}


void CANStub::initialise(void) {
    initialise_InstrumentStatus();
    initialise_SensorData();
    initialise_ControllerSettings();
    initialise_FilterLogicalPos();
    initialise_FocusLogicalPos();
    initialise_WagonLogicalPos();
    initialise_LEDIntensity();
    initialise_VersionInfo();
}

//------------------------
void CANStub::initialise_InstrumentStatus(void) {
    for (int i = 0; i < eCanObjMax; i++)     //initialise values of structs
    {
        m_tCANData[i].iCtrlTarget = 0;
        m_tCANData[i].iCtrlActual = 0;
        m_tCANData[i].iCtrlStatus = 0;
        m_tCANData[i].iCtrlError = 0;
        m_tCANData[i].iValue1 = 0;
        m_tCANData[i].iValue2 = 0;
        m_tCANData[i].iValue3 = 0;
        m_tCANData[i].iValue4 = 0;
        m_tCANData[i].iValue5 = 0;
        m_tCANData[i].iValue6 = 0;
        m_tCANData[i].iValue7 = 0;
        m_tCANData[i].iValue8 = 0;
        m_tCANData[i].iValue9 = 0;
        m_tCANData[i].iValue10 = 0;
        m_tCANData[i].iValue11 = 0;
        m_tCANData[i].iValue12 = 0;
        m_tCANData[i].iValue13 = 0;
        m_tCANData[i].iValue14 = 0;
        m_tCANData[i].iValue15 = 0;
        m_tCANData[i].iValue16 = 0;
        m_tCANData[i].iValue17 = 0;
        m_tCANData[i].iValue18 = 0;
        m_tCANData[i].iValue19 = 0;
        m_tCANData[i].iValue20 = 0;
        m_tCANData[i].iValue21 = 0;
        m_tCANData[i].iValue22 = 0;
        m_tCANData[i].iValue23 = 0;
        m_tCANData[i].iValue24 = 0;
        m_tCANData[i].iValue25 = 0;
        m_tCANData[i].iValue26 = 0;
        m_tCANData[i].iValue27 = 0;
        m_tCANData[i].iValue28 = 0;
        m_tCANData[i].iValue29 = 0;
        m_tCANData[i].iValue30 = 0;
        m_tCANData[i].iValue31 = 0;
        m_tCANData[i].iValue32 = 0;
        m_tCANData[i].iValue33 = 0;
        m_tCANData[i].iValue34 = 0;
        m_tCANData[i].iValue35 = 0;
        m_tCANData[i].iValue36 = 0;
        m_tCANData[i].iValue37 = 0;
        m_tCANData[i].iValue38 = 0;
        m_tCANData[i].iValue39 = 0;
        m_tCANData[i].iValue40 = 0;
        m_tCANData[i].iValue41 = 0;
        m_tCANData[i].iValue42 = 0;
        m_tCANData[i].iValue43 = 0;
        m_tCANData[i].iValue44 = 0;
        m_tCANData[i].iValue45 = 0;
        m_tCANData[i].iValue46 = 0;
        m_tCANData[i].iValue47 = 0;
        m_tCANData[i].iValue48 = 0;
        m_tCANData[i].iValue49 = 0;
        m_tCANData[i].iValue50 = 0;
    }

    //set some safety-sensors
    m_tCANData[eSensCoverClosed].iCtrlActual = 1; //cover is closed
    m_tCANData[eSensDispPresentLeft].iCtrlActual = 1; //Disposable1 (Well 1, 2, 3, 4)  is present
    m_tCANData[eSensDispPresentMiddle].iCtrlActual = 1; //Disposable2 (Well 5, 6, 7, 8)  is present
    m_tCANData[eSensDispPresentRight].iCtrlActual = 1; //Disposable3 (Well 9,10,11,12)  is present

    m_tCANData[eSensDispHead1Empty].iCtrlActual = 1; // dispenseHead present & not almost empty
    m_tCANData[eSensDispHead2Empty].iCtrlActual = 1; // dispenseHead present & not almost empty
    m_tCANData[eSensDispHead3Empty].iCtrlActual = 1; // dispenseHead present & not almost empty
    m_tCANData[eSensDispHead4Empty].iCtrlActual = 1; // dispenseHead present & not almost empty


    m_tCANData[ePressSensAspirate].iCtrlActual = 3277; //aspiratePress  � -0.1 barg

    m_tCANData[eBoard1Ctrl].iBoardEnabled = 1;    //on start up boards are enabled
    m_tCANData[eBoard2Ctrl].iBoardEnabled = 1;    //on start up boards are enabled
    m_tCANData[eBoard3Ctrl].iBoardEnabled = 1;    //on start up boards are enabled




}

//---
void CANStub::initialise_FilterLogicalPos(void) {
    ECanObj eObj = eEncFilterB;  //store logPosData "behind" eEncFilterB-channel (obj eEncFilterA-channel is used for sensor-calibration data)
    float rPosFilter1 = 5.6;
    float rPosFilter2 = -24.5;
    float rPosFilter3 = -54.5;

    m_tCANData[eObj].iValue1 = (int) eObj;  //ECanObj  eSensorObj       ;
    m_tCANData[eObj].iValue2 = *(int *) ((void *) &rPosFilter1);  //float rPosFilter1;
    m_tCANData[eObj].iValue3 = *(int *) ((void *) &rPosFilter2);  //float rPosFilter2;
    m_tCANData[eObj].iValue4 = *(int *) ((void *) &rPosFilter3);  //float rPosFilter3;

}

void CANStub::initialise_FocusLogicalPos(void) {
    ECanObj eObj = eEncFocusB;   //store logPosData "behind" eEncFocusB-channel (obj eEncFocusA-channel is used for sensor-calibration data)
    float rPosForFilter1 = 0.2;
    float rPosForFilter2 = 0.25;
    float rPosForFilter3 = 0.29;
    float rOffsetForDisp1 = 0.0;
    float rOffsetForDisp2 = 0.0;
    float rOffsetForDisp3 = 0.0;

    m_tCANData[eObj].iValue1 = (int) eObj;
    m_tCANData[eObj].iValue2 = *(int *) ((void *) &rPosForFilter1);
    m_tCANData[eObj].iValue3 = *(int *) ((void *) &rPosForFilter2);
    m_tCANData[eObj].iValue4 = *(int *) ((void *) &rPosForFilter3);
    m_tCANData[eObj].iValue5 = *(int *) ((void *) &rOffsetForDisp1);
    m_tCANData[eObj].iValue6 = *(int *) ((void *) &rOffsetForDisp2);
    m_tCANData[eObj].iValue7 = *(int *) ((void *) &rOffsetForDisp3);

}

void CANStub::initialise_WagonLogicalPos(void) {
    ECanObj eObjX = eEncTransXB;  //store logPosData "behind" eEncTransXB-channel (obj eEncTransXA-channel is used for sensor-calibration data)
    ECanObj eObjY = eEncTransYB;  //store logPosData "behind" eEncTransYB-channel (obj eEncTransYA-channel is used for sensor-calibration data)


    Wagon_LogicalXYPos_t tStruct =
            {
                    {165.0, 0.0},   //XYPos xyLoadPosition
                    {-210.0, 170.0},   //XYPos xyIncubationPosition
                    {0.0, 0.0},   //XYPos xyFrontPanelPosition
                    {-182.0, 89.0},   //XYPos xyReadPosition
                    {-1.5, 138.0},   //XYPos xyAspiratePosition
                    {0.0, 91.0},   //XYPos xyDispensePosition

                    {9.0, 0.0},   //XYPos xyOffset_Well
                    {0.0, 72.0},   //XYPos xyOffset_Disposable
                    {-20.0, 0.0},   //XYPos xyOffset_FocusGlass
                    {0.0, 0.0},   //XYPos xyOffset_DispenseHead1
                    {0.0, 3.0},   //XYPos xyOffset_DispenseHead2
                    {0.0, 78.0},   //XYPos xyOffset_DispenseHead3
                    {0.0, 81.0},   //XYPos xyOffset_DispenseHead4
                    {63.0, -35.5},   //XYPos xyOffset_Prime1
                    {63.0, 179.5}    //XYPos xyOffset_Prime2
            };

    m_tCANData[eObjX].iValue1 = (int) eObjX;  //ECanObj eSensorObj
    m_tCANData[eObjY].iValue1 = (int) eObjY;  //ECanObj eSensorObj
    m_tCANData[eObjX].iValue2 = *(int *) ((void *) &tStruct.xyLoadPosition.X);  //float  (XYPos xyLoadPosition).X
    m_tCANData[eObjY].iValue2 = *(int *) ((void *) &tStruct.xyLoadPosition.Y);  //float  (XYPos xyLoadPosition).Y
    m_tCANData[eObjX].iValue3 = *(int *) ((void *) &tStruct.xyIncubationPosition.X);  //float  (XYPos xyIncubationPosition).X
    m_tCANData[eObjY].iValue3 = *(int *) ((void *) &tStruct.xyIncubationPosition.Y);  //float  (XYPos xyIncubationPosition).Y
    m_tCANData[eObjX].iValue4 = *(int *) ((void *) &tStruct.xyFrontPanelPosition.X);  //float  (XYPos xyFrontPanelPosition).X
    m_tCANData[eObjY].iValue4 = *(int *) ((void *) &tStruct.xyFrontPanelPosition.Y);  //float  (XYPos xyFrontPanelPosition).Y
    m_tCANData[eObjX].iValue5 = *(int *) ((void *) &tStruct.xyReadPosition.X);  //float  (XYPos xyReadPosition).X
    m_tCANData[eObjY].iValue5 = *(int *) ((void *) &tStruct.xyReadPosition.Y);  //float  (XYPos xyReadPosition).Y
    m_tCANData[eObjX].iValue6 = *(int *) ((void *) &tStruct.xyAspiratePosition.X);  //float  (XYPos xyAspiratePosition).X
    m_tCANData[eObjY].iValue6 = *(int *) ((void *) &tStruct.xyAspiratePosition.Y);  //float  (XYPos xyAspiratePosition).Y
    m_tCANData[eObjX].iValue7 = *(int *) ((void *) &tStruct.xyDispensePosition.X);  //float  (XYPos xyDispensePosition).X
    m_tCANData[eObjY].iValue7 = *(int *) ((void *) &tStruct.xyDispensePosition.Y);  //float  (XYPos xyDispensePosition).Y
    m_tCANData[eObjX].iValue8 = *(int *) ((void *) &tStruct.xyOffset_Well.X);  //float  (XYPos xyOffset_Well).X
    m_tCANData[eObjY].iValue8 = *(int *) ((void *) &tStruct.xyOffset_Well.Y);  //float  (XYPos xyOffset_Well).Y
    m_tCANData[eObjX].iValue9 = *(int *) ((void *) &tStruct.xyOffset_Disposable.X);  //float  (XYPos xyOffset_Disposable).X
    m_tCANData[eObjY].iValue9 = *(int *) ((void *) &tStruct.xyOffset_Disposable.Y);  //float  (XYPos xyOffset_Disposable).Y
    m_tCANData[eObjX].iValue10 = *(int *) ((void *) &tStruct.xyOffset_FocusGlass.X);  //float  (XYPos xyOffset_FocusGlass).X
    m_tCANData[eObjY].iValue10 = *(int *) ((void *) &tStruct.xyOffset_FocusGlass.Y);  //float  (XYPos xyOffset_FocusGlass).Y
    m_tCANData[eObjX].iValue11 = *(int *) ((void *) &tStruct.xyOffset_DispenseHead1.X);  //float  (XYPos xyOffset_DispenseHead1).X
    m_tCANData[eObjY].iValue11 = *(int *) ((void *) &tStruct.xyOffset_DispenseHead1.Y);  //float  (XYPos xyOffset_DispenseHead1).Y
    m_tCANData[eObjX].iValue12 = *(int *) ((void *) &tStruct.xyOffset_DispenseHead2.X);  //float  (XYPos xyOffset_DispenseHead2).X
    m_tCANData[eObjY].iValue12 = *(int *) ((void *) &tStruct.xyOffset_DispenseHead2.Y);  //float  (XYPos xyOffset_DispenseHead2).Y
    m_tCANData[eObjX].iValue13 = *(int *) ((void *) &tStruct.xyOffset_DispenseHead3.X);  //float  (XYPos xyOffset_DispenseHead3).X
    m_tCANData[eObjY].iValue13 = *(int *) ((void *) &tStruct.xyOffset_DispenseHead3.Y);  //float  (XYPos xyOffset_DispenseHead3).Y
    m_tCANData[eObjX].iValue14 = *(int *) ((void *) &tStruct.xyOffset_DispenseHead4.X);  //float  (XYPos xyOffset_DispenseHead4).X
    m_tCANData[eObjY].iValue14 = *(int *) ((void *) &tStruct.xyOffset_DispenseHead4.Y);  //float  (XYPos xyOffset_DispenseHead4).Y
    m_tCANData[eObjX].iValue15 = *(int *) ((void *) &tStruct.xyOffset_Prime1.X);  //float  (XYPos xyOffset_Prime1).X
    m_tCANData[eObjY].iValue15 = *(int *) ((void *) &tStruct.xyOffset_Prime1.Y);  //float  (XYPos xyOffset_Prime1).Y
    m_tCANData[eObjX].iValue16 = *(int *) ((void *) &tStruct.xyOffset_Prime2.X);  //float  (XYPos xyOffset_Prime2).X
    m_tCANData[eObjY].iValue16 = *(int *) ((void *) &tStruct.xyOffset_Prime2.Y);  //float  (XYPos xyOffset_Prime2).Y

}

void CANStub::initialise_LEDIntensity(void) {
    unsigned uIntensity_LED1 = 70;  //PWM 0-200%, 0 is off, 200 is max intensity
    unsigned uIntensity_LED2 = 70;  //PWM 0-200%, 0 is off, 200 is max intensity
    unsigned uIntensity_LED3 = 50;  //PWM 0-200%, 0 is off, 200 is max intensity

    m_tCANData[eLEDUnit1].iValue1 = (int) eLEDUnit1;  //ECanObj eSensorObj
    m_tCANData[eLEDUnit1].iValue2 = (int) uIntensity_LED1;  //PWM
    m_tCANData[eLEDUnit2].iValue1 = (int) eLEDUnit2;  //ECanObj eSensorObj
    m_tCANData[eLEDUnit2].iValue2 = (int) uIntensity_LED2;  //PWM
    m_tCANData[eLEDUnit3].iValue1 = (int) eLEDUnit3;  //ECanObj eSensorObj
    m_tCANData[eLEDUnit3].iValue2 = (int) uIntensity_LED3;  //PWM
}

//--------------------------------------------------------------------------
void CANStub::initialise_VersionInfo(void) {

    m_tCANData[eBoard1Ctrl].iValue1 = (int) eBoard1Ctrl;   //eObj
    m_tCANData[eBoard1Ctrl].iValue2 = (int) 505001;   //PS12_nr
    m_tCANData[eBoard1Ctrl].iValue3 = (int) 38495;   //FlashDate
    m_tCANData[eBoard1Ctrl].iValue4 = (int) 0xFFFF;   //Brd_nr_high
    m_tCANData[eBoard1Ctrl].iValue5 = (int) 0xFFFF;   //Brd_nr_medium
    m_tCANData[eBoard1Ctrl].iValue6 = (int) 0xFFF0;   //Brd_nr_low
    m_tCANData[eBoard1Ctrl].iValue7 = (int) 2;   //FW_version
    m_tCANData[eBoard1Ctrl].iValue8 = (int) 3;   //ESW_version

    m_tCANData[eBoard2Ctrl].iValue1 = (int) eBoard2Ctrl;   //eObj
    m_tCANData[eBoard2Ctrl].iValue2 = (int) 505001;   //PS12_nr
    m_tCANData[eBoard2Ctrl].iValue3 = (int) 38495;   //FlashDate
    m_tCANData[eBoard2Ctrl].iValue4 = (int) 0xFFFF;   //Brd_nr_high
    m_tCANData[eBoard2Ctrl].iValue5 = (int) 0xFFFF;   //Brd_nr_medium
    m_tCANData[eBoard2Ctrl].iValue6 = (int) 0xFFF1;   //Brd_nr_low
    m_tCANData[eBoard2Ctrl].iValue7 = (int) 2;   //FW_version
    m_tCANData[eBoard2Ctrl].iValue8 = (int) 3;   //ESW_version

    m_tCANData[eBoard3Ctrl].iValue1 = (int) eBoard3Ctrl;   //eObj
    m_tCANData[eBoard3Ctrl].iValue2 = (int) 505001;   //PS12_nr
    m_tCANData[eBoard3Ctrl].iValue3 = (int) 38495;   //FlashDate
    m_tCANData[eBoard3Ctrl].iValue4 = (int) 0xFFFF;   //Brd_nr_high
    m_tCANData[eBoard3Ctrl].iValue5 = (int) 0xFFFF;   //Brd_nr_medium
    m_tCANData[eBoard3Ctrl].iValue6 = (int) 0xFFF2;   //Brd_nr_low
    m_tCANData[eBoard3Ctrl].iValue7 = (int) 2;   //FW_version
    m_tCANData[eBoard3Ctrl].iValue8 = (int) 3;   //ESW_version

}

//---------
void CANStub::initialise_SensorData(void) {
    //ANALOG SENSORS&ENCODERS STRUCT:
//ECanObj eSensorObj
    float rSysOffset;
    float rSysGain;
    float rSysGain2;
    float rSensOffset;
    float rSensGain;
    float rSensGain2;
    float rADConversion;
    float rTsample;

    //create an index
    ECanObj rgIndex[] =
            {
                    eTempSensDispense, eTempSensCalibration,
                    eTempSensBottomLeft, eTempSensBottomMiddle, eTempSensBottomRight,
                    eTempSensCoverLeft, eTempSensCoverMiddle, eTempSensCoverRight,
                    ePressSensUnderPress, ePressSensAspirate,
                    ePressSensOverPress,
                    ePressSensWell1, ePressSensWell2, ePressSensWell3, ePressSensWell4,
                    ePressSensWell5, ePressSensWell6, ePressSensWell7, ePressSensWell8,
                    ePressSensWell9, ePressSensWell10, ePressSensWell11, ePressSensWell12,
                    eEncDispenseHead1A, eEncDispenseHead2A, eEncDispenseHead3A, eEncDispenseHead4A,
                    eEncFilterA, eEncFocusA, eEncTransXA, eEncTransYA,
                    eSensAspirateHeadUp,
                    eCurrSensDispenseHeater, eCurrSensAspirateMotor,
                    eCurrSensBottomLeft, eCurrSensBottomMiddle, eCurrSensBottomRight,
                    eCurrSensCoverLeft, eCurrSensCoverMiddle, eCurrSensCoverRight,
                    eCurrSensTransX, eCurrSensTransY, eCurrSensFocus, eCurrSensFilter
            };


    int iSize_rgIndex = sizeof(rgIndex) / sizeof(rgIndex[0]);

    //loop over this index and fill in the correct values
    for (int i = 0; i < iSize_rgIndex; i++) {
        //as controllable settings are to be converted from SI to LU, using sensor-calibration settings
        //they are dependent (if sensor is calibrated, the controllable settings must be converted and flashed too)

        switch (rgIndex[i]) {
            case eTempSensCoverLeft     :
                rSysOffset = 100.1801;
                rSysGain = 8.1142;
                rSysGain2 = -0.0034087;
                rSensOffset = -252.1748;
                rSensGain = 2.4638;
                rSensGain2 = 0.00055214;
                rADConversion = 6400.0;
                rTsample = 0.1;
                break;
            case eTempSensCoverMiddle   :
                rSysOffset = 100.216;
                rSysGain = 8.083;
                rSysGain2 = -0.0024891;
                rSensOffset = -251.5025;
                rSensGain = 2.4531;
                rSensGain2 = 0.00060048;
                rADConversion = 6400.0;
                rTsample = 0.1;
                break;
            case eTempSensCoverRight    :
                rSysOffset = 100.0995;
                rSysGain = 8.0953;
                rSysGain2 = -0.0033641;
                rSensOffset = -252.8979;
                rSensGain = 2.4843;
                rSensGain2 = 0.00047173;
                rADConversion = 6400.0;
                rTsample = 0.1;
                break;
            case eTempSensBottomLeft    :
                rSysOffset = 99.6178;
                rSysGain = 8.0991;
                rSysGain2 = -0.0047066;
                rSensOffset = -248.7332;
                rSensGain = 2.4064;
                rSensGain2 = 0.00079894;
                rADConversion = 6400.0;
                rTsample = 0.1;
                break;
            case eTempSensBottomMiddle  :
                rSysOffset = 99.524;
                rSysGain = 8.0399;
                rSysGain2 = 0.0070656;
                rSensOffset = -250.2554;
                rSensGain = 2.4344;
                rSensGain2 = 0.00068033;
                rADConversion = 6400.0;
                rTsample = 0.1;
                break;
            case eTempSensBottomRight   :
                rSysOffset = 99.5533;
                rSysGain = 8.0421;
                rSysGain2 = 0.010128;
                rSensOffset = -252.4172;
                rSensGain = 2.4687;
                rSensGain2 = 0.00055562;
                rADConversion = 6400.0;
                rTsample = 0.1;
                break;
            case eTempSensCalibration   :
                rSysOffset = 100.0265;
                rSysGain = 8.1118;
                rSysGain2 = -0.0042183;
                rSensOffset = -245.68;
                rSensGain = 2.3577;
                rSensGain2 = 0.001;
                rADConversion = 6400.0;
                rTsample = 0.1;
                break;
            case eTempSensDispense      :
                rSysOffset = 100.4019;
                rSysGain = 8.0885;
                rSysGain2 = -0.0018786;
                rSensOffset = -247.2038;
                rSensGain = 2.3878;
                rSensGain2 = 0.00084381;
                rADConversion = 6400.0;
                rTsample = 0.1;
                break;
            case ePressSensUnderPress   :
                rSysOffset = -0.20909;
                rSysGain = 1.0;
                rSysGain2 = 0.0;
                rSensOffset = 0.0;
                rSensGain = -0.22;
                rSensGain2 = 0.0;
                rADConversion = 6400.0;
                rTsample = 0;   //don't care
                break;
            case ePressSensAspirate     :
                rSysOffset = -0.11818;
                rSysGain = 1.0;
                rSysGain2 = 0.0;
                rSensOffset = 0.0;
                rSensGain = -0.22;
                rSensGain2 = 0.0;
                rADConversion = 6400.0;
                rTsample = 0;   //don't care
                break;
            case ePressSensOverPress    :
                rSysOffset = -0.20909;
                rSysGain = 1.0;
                rSysGain2 = 0.0;
                rSensOffset = 0.0;
                rSensGain = 0.22;
                rSensGain2 = 0.0;
                rADConversion = 6400.0;
                rTsample = 0;   //don't care
                break;
            case ePressSensWell1        :
                rSysOffset = -2.4045;
                rSysGain = 1.0;
                rSysGain2 = 0.0;
                rSensOffset = 0.0;
                rSensGain = 0.44;
                rSensGain2 = 0.0;
                rADConversion = 6400.0;
                rTsample = 0;    //don't care
                break;
            case ePressSensWell2        :
                rSysOffset = -2.4045;
                rSysGain = 1.0;
                rSysGain2 = 0.0;
                rSensOffset = 0.0;
                rSensGain = 0.44;
                rSensGain2 = 0.0;
                rADConversion = 6400.0;
                rTsample = 0;    //don't care
                break;
            case ePressSensWell3        :
                rSysOffset = -2.4045;
                rSysGain = 1.0;
                rSysGain2 = 0.0;
                rSensOffset = 0.0;
                rSensGain = 0.44;
                rSensGain2 = 0.0;
                rADConversion = 6400.0;
                rTsample = 0;    //don't care
                break;
            case ePressSensWell4        :
                rSysOffset = -2.3818;
                rSysGain = 1.0;
                rSysGain2 = 0.0;
                rSensOffset = 0.0;
                rSensGain = 0.44;
                rSensGain2 = 0.0;
                rADConversion = 6400.0;
                rTsample = 0;    //don't care
                break;
            case ePressSensWell5        :
                rSysOffset = -2.4045;
                rSysGain = 1.0;
                rSysGain2 = 0.0;
                rSensOffset = 0.0;
                rSensGain = 0.44;
                rSensGain2 = 0.0;
                rADConversion = 6400.0;
                rTsample = 0;    //don't care
                break;
            case ePressSensWell6        :
                rSysOffset = -2.4045;
                rSysGain = 1.0;
                rSysGain2 = 0.0;
                rSensOffset = 0.0;
                rSensGain = 0.44;
                rSensGain2 = 0.0;
                rADConversion = 6400.0;
                rTsample = 0;    //don't care
                break;
            case ePressSensWell7        :
                rSysOffset = -2.4045;
                rSysGain = 1.0;
                rSysGain2 = 0.0;
                rSensOffset = 0.0;
                rSensGain = 0.44;
                rSensGain2 = 0.0;
                rADConversion = 6400.0;
                rTsample = 0;    //don't care
                break;
            case ePressSensWell8        :
                rSysOffset = -2.3818;
                rSysGain = 1.0;
                rSysGain2 = 0.0;
                rSensOffset = 0.0;
                rSensGain = 0.44;
                rSensGain2 = 0.0;
                rADConversion = 6400.0;
                rTsample = 0;    //don't care
                break;
            case ePressSensWell9        :
                rSysOffset = -2.4273;
                rSysGain = 1.0;
                rSysGain2 = 0.0;
                rSensOffset = 0.0;
                rSensGain = 0.44;
                rSensGain2 = 0.0;
                rADConversion = 6400.0;
                rTsample = 0;    //don't care
                break;
            case ePressSensWell10       :
                rSysOffset = -2.4273;
                rSysGain = 1.0;
                rSysGain2 = 0.0;
                rSensOffset = 0.0;
                rSensGain = 0.44;
                rSensGain2 = 0.0;
                rADConversion = 6400.0;
                rTsample = 0;    //don't care
                break;
            case ePressSensWell11       :
                rSysOffset = -2.4045;
                rSysGain = 1.0;
                rSysGain2 = 0.0;
                rSensOffset = 0.0;
                rSensGain = 0.44;
                rSensGain2 = 0.0;
                rADConversion = 6400.0;
                rTsample = 0;    //don't care
                break;
            case ePressSensWell12       :
                rSysOffset = -2.4045;
                rSysGain = 1.0;
                rSysGain2 = 0.0;
                rSensOffset = 0.0;
                rSensGain = 0.44;
                rSensGain2 = 0.0;
                rADConversion = 6400.0;
                rTsample = 0;    //don't care
                break;
            case eEncDispenseHead1A     :
            case eEncDispenseHead2A     :
            case eEncDispenseHead3A     :
            case eEncDispenseHead4A     :
                rSysOffset = 0.0;
                rSysGain = 1.0;
                rSysGain2 = 0.0;
                rSensOffset = 0.0;
                rSensGain = 1.0;
                rSensGain2 = 0.0;
                rADConversion = 275.1;
                rTsample = 0;    //don't care
                break;
            case eEncFocusA             :
                rSysOffset = 0.0;
                rSysGain = 1.0;
                rSysGain2 = 0.0;
                rSensOffset = 0.0;
                rSensGain = 1.0;
                rSensGain2 = 0.0;
                rADConversion = 14000.0;
                rTsample = 0.001;
                break;
            case eEncFilterA            :
                rSysOffset = 0.0;
                rSysGain = 1.0;
                rSysGain2 = 0.0;
                rSensOffset = 0.0;
                rSensGain = 1.0;
                rSensGain2 = 0.0;
                rADConversion = 666.67;
                rTsample = 0.001;
                break;
            case eEncTransXA            :
                rSysOffset = 1.5;
                rSysGain = 1.0;
                rSysGain2 = 0.0;
                rSensOffset = 0.0;
                rSensGain = 1.0;
                rSensGain2 = 0.0;
                rADConversion = 666.67;
                rTsample = 0.001;
                break;
            case eEncTransYA            :
                rSysOffset = 0.6;
                rSysGain = 1.0;
                rSysGain2 = 0.0;
                rSensOffset = 0.0;
                rSensGain = 1.0;
                rSensGain2 = 0.0;
                rADConversion = 666.67;
                rTsample = 0.001;
                break;
            case eSensAspirateHeadUp    :
                rSysOffset = 0.0;
                rSysGain = 1.0;
                rSysGain2 = 0.0;
                rSensOffset = 0.0;   //no conversion
                rSensGain = 1.0;
                rSensGain2 = 0.0;
                rADConversion = 1.0;
                rTsample = 0;    //don't care, not zero
                break;
            case eCurrSensAspirateMotor :
            case eCurrSensBottomLeft    :
            case eCurrSensBottomMiddle  :
            case eCurrSensBottomRight   :
            case eCurrSensCoverLeft     :
            case eCurrSensCoverMiddle   :
            case eCurrSensCoverRight    :
            case eCurrSensDispenseHeater:
                rSysOffset = 0.0;
                rSysGain = 1.0;
                rSysGain2 = 0.0;
                rSensOffset = 0.0;
                rSensGain = 1.0;
                rSensGain2 = 0.0;
                rADConversion = 249.9;
                rTsample = 0.0005;
                break;
            case eCurrSensFilter        :
            case eCurrSensFocus         :
            case eCurrSensTransX        :
            case eCurrSensTransY        :
                rSysOffset = 0.0;
                rSysGain = 1.0;
                rSysGain2 = 0.0;
                rSensOffset = 0.0;
                rSensGain = 1.0;
                rSensGain2 = 0.0;
                rADConversion = 249.9;
                rTsample = 0;
                break;
            default:
                assert(0);
        }
        //store in the CANDATA[] and convert to integers
        m_tCANData[rgIndex[i]].iValue1 = (int) rgIndex[i];  //ECanObj eSensorObj   ;
        m_tCANData[rgIndex[i]].iValue2 = *(int *) ((void *) &rSysOffset);  //float   rSysOffset   ;
        m_tCANData[rgIndex[i]].iValue3 = *(int *) ((void *) &rSysGain);  //float   rSysGain     ;
        m_tCANData[rgIndex[i]].iValue4 = *(int *) ((void *) &rSysGain2);  //float   rSysGain2    ;
        m_tCANData[rgIndex[i]].iValue5 = *(int *) ((void *) &rSensOffset);  //float   rSensOffset  ;
        m_tCANData[rgIndex[i]].iValue6 = *(int *) ((void *) &rSensGain);  //float   rSensGain    ;
        m_tCANData[rgIndex[i]].iValue7 = *(int *) ((void *) &rSensGain2);  //float   rSensGain2   ;
        m_tCANData[rgIndex[i]].iValue8 = *(int *) ((void *) &rADConversion);  //float   rADConversion;
        m_tCANData[rgIndex[i]].iValue9 = *(int *) ((void *) &rTsample);  //float   rTsample     ;

    }
}

//----------
void CANStub::initialise_ControllerSettings(void) {
    //create an index
    ECanObj rgIndex[] = {
            eCtrlAspirateHeadPositioner, eCtrlDispenseHeadHeater,
            eCtrlDispenseHeadPositioner1, eCtrlDispenseHeadPositioner2,
            eCtrlDispenseHeadPositioner3, eCtrlDispenseHeadPositioner4,
            eCtrlFilterPositioner, eCtrlFocusPositioner,
            eCtrlOverPressureSupplier, eCtrlUnderPressureSupplier,
            eCtrlHtrCoverLeft, eCtrlHtrBottomLeft,
            eCtrlHtrCoverMiddle, eCtrlHtrBottomMiddle,
            eCtrlHtrCoverRight, eCtrlHtrBottomRight,
            eCtrlTransporterX, eCtrlTransporterY};

    int iSize_rgIndex = sizeof(rgIndex) / sizeof(rgIndex[0]);

    //loop over this index and fill in the correct values
    for (int i = 0; i < iSize_rgIndex; i++) {
        int sd_KPp = 0;
        int sd_KIp = 0;
        int sd_KDp = 0;
        int sd_Integ_Lim_max = 0;
        int sd_Integ_Lim_min = 0;
        int KPi = 0;
        int KIi = 0;
        float DefaultVel = 1;
        float DefaultAcc = 0.1;
        int sf_KPp = 0;
        int sf_KIp = 0;
        int sf_KDp = 0;
        int va_scale = 0;
        int T_Settletime = 0;
        unsigned Allow_Homing = 0;
        int Homing_tick_count = 0;
        int Homing_Speed_PWM = 0;
        int Homing_Seeking_PWM = 0;
        int max_output = 0;
        int min_output = 0;
        int max_setpoint = 0;
        int min_setpoint = 0;
        int max_tracking_error = 0;
        int max_tolerance_error = 0;
        int max_dc_i = 0;
        int max_dc_pwm = 0;
        float du_max_vel = 30;
        float du_min_vel = 10;
        float du_max_acc = 0.2;
        float du_min_acc = 0.05;

        ECanObj eSensorObj = (ECanObj) 0;  //overwrite
        ECanObj eCurrSnsObj = (ECanObj) 0;  //overwrite
        unsigned uWaitReadyTimeout = 0;
        int iDefaultPwm = 0;


        switch (rgIndex[i]) {
            case eCtrlOverPressureSupplier             :
                eSensorObj = ePressSensOverPress;// eCanObj, storage location of conversion data (=calibration data)
                eCurrSnsObj = eCanObjMin;
                uWaitReadyTimeout = 10000;             // [ms]
                max_setpoint = 16000;             // 0.5 barg in [LU]
                min_setpoint = 1300;             // 0 barg in [LU]
                max_tracking_error = 1455;             // 0.09 barg in [LU]
                max_tolerance_error = 1455;             // 0.09 barg in [LU]
                break;
            case eCtrlUnderPressureSupplier             :
                eSensorObj = ePressSensUnderPress;// eCanObj, storage location of conversion data (=calibration data)
                eCurrSnsObj = eCanObjMin;
                uWaitReadyTimeout = 10000;             // [ms]
                max_setpoint = 1300;              // 0 barg in [LU]
                min_setpoint = 16000;             // -0.5 barg in [LU]
                max_tracking_error = 1455;             // -0.09 barg in [LU]
                max_tolerance_error = 1455;             // -0.09 barg in [LU]
                break;
            case eCtrlTransporterX             :
                eSensorObj = eEncTransXA;        // eCanObj, storage location of conversion data (=calibration data)
                eCurrSnsObj = eCurrSensTransX;
                uWaitReadyTimeout = 30000;             // [ms]
                sd_KPp = 3000;
                sd_KIp = 50;
                sd_KDp = 75;
                sd_Integ_Lim_max = 16384000;
                sd_Integ_Lim_min = -16384000;
                KPi = 0;
                KIi = 0;
                DefaultVel = 20.3;             //   30 mm/s  in [LU]
                DefaultAcc = 0.1;             //  150 mm/s2 in [LU]
                sf_KPp = 32768;
                sf_KIp = 32768;
                sf_KDp = 15;
                va_scale = 12;
                T_Settletime = 500;             //  500  ms    in [LU]
                Allow_Homing = 1;
                Homing_tick_count = 500;
                Homing_Speed_PWM = -120;
                Homing_Seeking_PWM = 50;
                max_output = 200;
                min_output = -200;
                max_setpoint = 120000;             //  167 mm    in [LU]
                min_setpoint = -150000;             // -211 mm    in [LU]
                max_tracking_error = 2000;             //  3   mm    in [LU]
                max_tolerance_error = 333;             // 0.5  mm    in [LU]
                max_dc_i = 1024;             // 4.1  A     in [LU]
                max_dc_pwm = 200;
                du_max_vel = 30;             //  45 mm/s  in [LU]
                du_min_vel = 10;             //  15 mm/s  in [LU]
                du_max_acc = 0.2;             // 300 mm/s2 in [LU]
                du_min_acc = 0.05;             //  75 mm/s2 in [LU]
                break;
            case eCtrlTransporterY             :
                eSensorObj = eEncTransYA;        // eCanObj, storage location of conversion data (=calibration data)
                eCurrSnsObj = eCurrSensTransY;
                uWaitReadyTimeout = 30000;             // [ms]
                sd_KPp = 3000;
                sd_KIp = 50;
                sd_KDp = 75;
                sd_Integ_Lim_max = 16384000;
                sd_Integ_Lim_min = -16384000;
                KPi = 0;
                KIi = 0;
                DefaultVel = 20.3;             //   30 mm/s  in [LU]
                DefaultAcc = 0.1;             //  150 mm/s2 in [LU]
                sf_KPp = 32768;
                sf_KIp = 32768;
                sf_KDp = 15;
                va_scale = 12;
                T_Settletime = 500;             // 3000  ms    in [LU]
                Allow_Homing = 1;
                Homing_tick_count = 500;
                Homing_Speed_PWM = -80;
                Homing_Seeking_PWM = 50;
                max_output = 200;
                min_output = -200;
                max_setpoint = 120000;             // 173 mm in [LU]
                min_setpoint = -40000;             // -59 mm in [LU]
                max_tracking_error = 2000;             //  3 mm in [LU]
                max_tolerance_error = 333;             // 0.5 mm in [LU]
                max_dc_i = 1024;             // 2    A in [LU]
                max_dc_pwm = 200;
                du_max_vel = 30;             //  45 mm/s  in [LU]
                du_min_vel = 10;             //  15 mm/s  in [LU]
                du_max_acc = 0.2;             // 300 mm/s2 in [LU]
                du_min_acc = 0.05;             //  75 mm/s2 in [LU]
                break;
            case eCtrlFocusPositioner             :
                eSensorObj = eEncFocusA;         // eCanObj, storage location of conversion data (=calibration data)
                eCurrSnsObj = eCurrSensFocus;
                uWaitReadyTimeout = 10000;             // [ms]
                sd_KPp = 3000;
                sd_KIp = 50;
                sd_KDp = 75;
                sd_Integ_Lim_max = 16384000;
                sd_Integ_Lim_min = -16384000;
                KPi = 0;
                KIi = 0;
                DefaultVel = 5.0;             //   1.15 mm/s  in [LU]
                DefaultAcc = 1.0;             //   0.05 mm/s2 in [LU]
                sf_KPp = 32768;
                sf_KIp = 32768;
                sf_KDp = 15;
                va_scale = 12;
                T_Settletime = 500;             //  1000  ms    in [LU]
                Allow_Homing = 1;
                Homing_tick_count = 500;
                Homing_Speed_PWM = -10;
                Homing_Seeking_PWM = 10;
                max_output = 50;
                min_output = -50;
                max_setpoint = 28000;             // 2  mm in [LU]
                min_setpoint = -2001;             // -0.1  mm in [LU]
                max_tracking_error = 5000;             // 0.5 mm in [LU]
                max_tolerance_error = 250;             // 0.17 mm in [LU]
                max_dc_i = 1024;             // 2    A in [LU]
                max_dc_pwm = 50;
                du_max_vel = 10;             // 2.5 mm/s  in [LU]
                du_min_vel = 0.05;             // 0.0025 mm/s  in [LU]
                du_max_acc = 0.2;             // 100 mm/s2 in [LU]
                du_min_acc = 0.05;             //  25 mm/s2 in [LU]
                break;
            case eCtrlFilterPositioner             :
                eSensorObj = eEncFilterA;        // eCanObj, storage location of conversion data (=calibration data)
                eCurrSnsObj = eCurrSensFilter;
                uWaitReadyTimeout = 10000;             // [ms]
                sd_KPp = 64000;
                sd_KIp = 200;
                sd_KDp = 0;
                sd_Integ_Lim_max = 16384000;
                sd_Integ_Lim_min = -16384000;
                KPi = 0;
                KIi = 0;
                DefaultVel = 20;             //   30 mm/s  in [LU]
                DefaultAcc = 0.1;             //  150 mm/s2 in [LU]
                sf_KPp = 32768;
                sf_KIp = 32768;
                sf_KDp = 15;
                va_scale = 12;
                T_Settletime = 500;             //  2000  ms    in [LU]
                Allow_Homing = 1;
                Homing_tick_count = 500;
                Homing_Speed_PWM = 60;
                Homing_Seeking_PWM = -60;
                max_output = 200;
                min_output = -200;
                max_setpoint = 6000;             // 6 mm in [LU]
                min_setpoint = -40000;             // -54.1 mm in [LU]
                max_tracking_error = 2000;             // 3 mm in [LU]
                max_tolerance_error = 333;             // 0.5 mm in [LU]
                max_dc_i = 1024;             // 2    A in [LU]
                max_dc_pwm = 200;
                du_max_vel = 30;             // 45 mm/s  in [LU]
                du_min_vel = 15;             // 0.2 mm/s  in [LU]
                du_max_acc = 0.3;             // 300 mm/s2 in [LU]
                du_min_acc = 0.05;            //  75 mm/s2 in [LU]
                break;
            case eCtrlHtrCoverLeft                 :
                eSensorObj = eTempSensCoverLeft; // eCanObj, storage location of conversion data (=calibration data)
                eCurrSnsObj = eCurrSensCoverLeft;
                uWaitReadyTimeout = 900000;             // [ms]
                sd_KPp = 4000;
                sd_KIp = 22;
                sd_KDp = 0;
                sd_Integ_Lim_max = 16384000;
                sd_Integ_Lim_min = 0;
                KPi = 50;
                KIi = 1;
                DefaultVel = 4.3133;             //     8   �C/min  in [LU]
                sf_KPp = 32768;
                sf_KIp = 32768;
                sf_KDp = 32768;
                va_scale = 12;
                T_Settletime = 300;             //10000  ms       in [LU]
                max_output = 200;
                min_output = 0;
                max_setpoint = 27200;             //   85   �C      in [LU]
                min_setpoint = 3200;             //    10  �C      in [LU]
                max_tracking_error = 16000;             //    5 �C      in [LU]
                max_tolerance_error = 480;             //     0.5 �C      in [LU]
                max_dc_i = 512;             //     2    A      in [LU]
                max_dc_pwm = 200;
                du_max_vel = 4.3133;             //     8   �C/min  in [LU]
                du_min_vel = 0.54;             //     1   �C/min  in [LU]
                break;
            case eCtrlHtrCoverMiddle               :
                eSensorObj = eTempSensCoverMiddle;// eCanObj, storage location of conversion data (=calibration data)
                eCurrSnsObj = eCurrSensCoverMiddle;
                uWaitReadyTimeout = 900000;             // [ms]
                sd_KPp = 4000;
                sd_KIp = 22;
                sd_KDp = 0;
                sd_Integ_Lim_max = 16384000;
                sd_Integ_Lim_min = 0;
                KPi = 50;
                KIi = 1;
                DefaultVel = 4.3133;             //     8   �C/min  in [LU]
                sf_KPp = 32768;
                sf_KIp = 32768;
                sf_KDp = 32768;
                va_scale = 12;
                T_Settletime = 300;             //10000  ms       in [LU]
                max_output = 200;
                min_output = 0;
                max_setpoint = 27200;             //   85   �C      in [LU]
                min_setpoint = 3200;             //    10  �C      in [LU]
                max_tracking_error = 16000;             //    5 �C      in [LU]
                max_tolerance_error = 480;             //     0.5 �C      in [LU]
                max_dc_i = 512;             //     2    A      in [LU]
                max_dc_pwm = 200;
                du_max_vel = 4.3133;             //     8   �C/min  in [LU]
                du_min_vel = 0.54;             //     1   �C/min  in [LU]
                break;
            case eCtrlHtrCoverRight                :
                eSensorObj = eTempSensCoverRight;// eCanObj, storage location of conversion data (=calibration data)
                eCurrSnsObj = eCurrSensCoverRight;
                uWaitReadyTimeout = 900000;             // [ms]
                sd_KPp = 4000;
                sd_KIp = 22;
                sd_KDp = 0;
                sd_Integ_Lim_max = 16384000;
                sd_Integ_Lim_min = 0;
                KPi = 50;
                KIi = 1;
                DefaultVel = 4.3133;             //     8   �C/min  in [LU]
                sf_KPp = 32768;
                sf_KIp = 32768;
                sf_KDp = 32768;
                va_scale = 12;
                T_Settletime = 300;             //10000  ms       in [LU]
                max_output = 200;
                min_output = 0;
                max_setpoint = 27200;             //   85   �C      in [LU]
                min_setpoint = 3200;             //    10  �C      in [LU]
                max_tracking_error = 16000;             //    5 �C      in [LU]
                max_tolerance_error = 480;             //     0.5 �C      in [LU]
                max_dc_i = 512;             //     2    A      in [LU]
                max_dc_pwm = 200;
                du_max_vel = 4.3133;             //     8   �C/min  in [LU]
                du_min_vel = 0.54;             //     1   �C/min  in [LU]
                break;
            case eCtrlHtrBottomLeft                :
                eSensorObj = eTempSensBottomLeft;// eCanObj, storage location of conversion data (=calibration data)
                eCurrSnsObj = eCurrSensBottomLeft;
                uWaitReadyTimeout = 900000;             // [ms]
                sd_KPp = 12000;
                sd_KIp = 90;
                sd_KDp = 0;
                sd_Integ_Lim_max = 16384000;
                sd_Integ_Lim_min = -16384000;
                KPi = 50;
                KIi = 1;
                DefaultVel = 4.3133;             //     8   �C/min  in [LU]
                sf_KPp = 32768;
                sf_KIp = 32768;
                sf_KDp = 32768;
                va_scale = 12;
                T_Settletime = 300;             //10000  ms       in [LU]
                max_output = 500;
                min_output = -500;
                max_setpoint = 27200;             //   85   �C      in [LU]
                min_setpoint = 3200;             //    10  �C      in [LU]
                max_tracking_error = 3200;             //    5 �C      in [LU]
                max_tolerance_error = 320;             //     0.5 �C      in [LU]
                max_dc_i = 512;             //     2    A      in [LU]
                max_dc_pwm = 200;
                du_max_vel = 4.3133;             //     8   �C/min  in [LU]
                du_min_vel = 0.54;             //     1   �C/min  in [LU]
                break;
            case eCtrlHtrBottomMiddle              :
                eSensorObj = eTempSensBottomMiddle;// eCanObj, storage location of conversion data (=calibration data)
                eCurrSnsObj = eCurrSensBottomMiddle;
                uWaitReadyTimeout = 900000;             // [ms]
                sd_KPp = 12000;
                sd_KIp = 90;
                sd_KDp = 0;
                sd_Integ_Lim_max = 16384000;
                sd_Integ_Lim_min = -16384000;
                KPi = 50;
                KIi = 1;
                DefaultVel = 4.3133;             //     8   �C/min  in [LU]
                sf_KPp = 32768;
                sf_KIp = 32768;
                sf_KDp = 32768;
                va_scale = 12;
                T_Settletime = 300;             //10000  ms       in [LU]
                max_output = 500;
                min_output = -500;
                max_setpoint = 27200;             //   85   �C      in [LU]
                min_setpoint = 3200;             //    10  �C      in [LU]
                max_tracking_error = 3200;             //    5 �C      in [LU]
                max_tolerance_error = 320;             //     0.5 �C      in [LU]
                max_dc_i = 512;             //     2    A      in [LU]
                max_dc_pwm = 200;
                du_max_vel = 4.3133;             //     8   �C/min  in [LU]
                du_min_vel = 0.54;             //     1   �C/min  in [LU]
                break;
            case eCtrlHtrBottomRight               :
                eSensorObj = eTempSensBottomRight;// eCanObj, storage location of conversion data (=calibration data)
                eCurrSnsObj = eCurrSensBottomRight;
                uWaitReadyTimeout = 900000;             // [ms]
                sd_KPp = 12000;
                sd_KIp = 90;
                sd_KDp = 0;
                sd_Integ_Lim_max = 16384000;
                sd_Integ_Lim_min = -16384000;
                KPi = 50;
                KIi = 1;
                DefaultVel = 4.3133;             //     8   �C/min  in [LU]
                sf_KPp = 32768;
                sf_KIp = 32768;
                sf_KDp = 32768;
                va_scale = 12;
                T_Settletime = 300;             //10000  ms       in [LU]
                max_output = 500;
                min_output = -500;
                max_setpoint = 27200;             //   85   �C      in [LU]
                min_setpoint = 3200;             //    10  �C      in [LU]
                max_tracking_error = 3200;             //    5 �C      in [LU]
                max_tolerance_error = 320;             //     0.5 �C      in [LU]
                max_dc_i = 512;             //     2    A      in [LU]
                max_dc_pwm = 200;
                du_max_vel = 4.3133;             //     8   �C/min  in [LU]
                du_min_vel = 0.54;             //     1   �C/min  in [LU]
                break;
            case eCtrlAspirateHeadPositioner      :
                eSensorObj = eSensAspirateHeadUp;// eCanObj, storage location of conversion data (=calibration data)
                eCurrSnsObj = eCanObjMin;
                uWaitReadyTimeout = 10000;              // [ms]
                iDefaultPwm = 100;              // halve kracht vooruit
                max_setpoint = 2;              // Up in [LU]
                min_setpoint = 1;              // Down in [LU]
                max_tracking_error = 0;              //
                max_tolerance_error = 0;              //
                break;
            case eCtrlDispenseHeadHeater           :
                eSensorObj = eTempSensDispense;  // eCanObj, storage location of conversion data (=calibration data)
                eCurrSnsObj = eCurrSensDispenseHeater;
                uWaitReadyTimeout = 900000;             // [ms]
                sd_KPp = 5000;
                sd_KIp = 21.7;
                sd_KDp = 0;
                sd_Integ_Lim_max = 16384000;
                sd_Integ_Lim_min = 0;
                KPi = 50;
                KIi = 1;
                DefaultVel = 4.3133;             //     8   �C/min  in [LU]
                sf_KPp = 32768;
                sf_KIp = 32768;
                sf_KDp = 32768;
                va_scale = 12;
                T_Settletime = 300;             //10000  ms       in [LU]
                max_output = 157;
                min_output = 0;
                max_setpoint = 27200;             //   85   �C      in [LU]
                min_setpoint = 9600;             //    30  �C      in [LU]
                max_tracking_error = 16000;             //    5 �C      in [LU]
                max_tolerance_error = 800;             //     0.5 �C      in [LU]
                max_dc_i = 511;             //     2    A      in [LU]
                max_dc_pwm = 157;
                du_max_vel = 4.3133;             //     8   �C/min  in [LU]
                du_min_vel = 0.54;             //     1   �C/min  in [LU]
                break;
            case eCtrlDispenseHeadPositioner1      :
                eSensorObj = eEncDispenseHead1A; // eCanObj, storage location of conversion data (=calibration data)
                eCurrSnsObj = eCanObjMin;
                uWaitReadyTimeout = 10000;             // [ms]
                iDefaultPwm = 26;              // achtste kracht vooruit
                max_setpoint = 27510;             // 100 �l in [LU]
                min_setpoint = -13755;             // 50 �l in [LU]
                max_tracking_error = 688;             // 2.5 �l in [LU]
                max_tolerance_error = 688;             // 2.5 �l in [LU]
                break;
            case eCtrlDispenseHeadPositioner2      :
                eSensorObj = eEncDispenseHead2A; // eCanObj, storage location of conversion data (=calibration data)
                eCurrSnsObj = eCanObjMin;
                uWaitReadyTimeout = 10000;             // [ms]
                iDefaultPwm = 26;              // achtste kracht vooruit
                max_setpoint = 27510;             // 100 �l in [LU]
                min_setpoint = -13755;             // 50 �l in [LU]
                max_tracking_error = 688;             // 2.5 �l in [LU]
                max_tolerance_error = 688;             // 2.5 �l in [LU]
                break;
            case eCtrlDispenseHeadPositioner3      :
                eSensorObj = eEncDispenseHead3A; // eCanObj, storage location of conversion data (=calibration data)
                eCurrSnsObj = eCanObjMin;
                uWaitReadyTimeout = 10000;             // [ms]
                iDefaultPwm = 26;              // achtste kracht vooruit
                max_setpoint = 27510;             // 100 �l in [LU]
                min_setpoint = -13755;             // 50 �l in [LU]
                max_tracking_error = 688;             // 2.5 �l in [LU]
                max_tolerance_error = 688;             // 2.5 �l in [LU]
                break;
            case eCtrlDispenseHeadPositioner4      :
                eSensorObj = eEncDispenseHead4A; // eCanObj, storage location of conversion data (=calibration data)
                eCurrSnsObj = eCanObjMin;
                uWaitReadyTimeout = 10000;             // [ms]
                iDefaultPwm = 26;              // achtste kracht vooruit
                max_setpoint = 27510;             // 100 �l in [LU]
                min_setpoint = -13755;             // 50 �l in [LU]
                max_tracking_error = 688;             // 2.5 �l in [LU]
                max_tolerance_error = 688;             // 2.5 �l in [LU]
                break;
            default:
                assert(0);
        }

        //store in the CANDATA[] and convert to integers
        m_tCANData[rgIndex[i]].iValue1 = sd_KPp;  //int       sd_KPp
        m_tCANData[rgIndex[i]].iValue2 = sd_KIp;  //int       sd_KIp
        m_tCANData[rgIndex[i]].iValue3 = sd_KDp;  //int       sd_KDp
        m_tCANData[rgIndex[i]].iValue4 = sd_Integ_Lim_max;  //int       sd_Integ_Lim_max
        m_tCANData[rgIndex[i]].iValue5 = KPi;  //int       KPi
        m_tCANData[rgIndex[i]].iValue6 = KIi;  //int       KIi
        m_tCANData[rgIndex[i]].iValue7 = *(int *) ((void *) &DefaultVel);  //float     DefaultVel
        m_tCANData[rgIndex[i]].iValue8 = *(int *) ((void *) &DefaultAcc);  //float     DefaultAcc
        m_tCANData[rgIndex[i]].iValue9 = sf_KPp;  //int       sf_KPp
        m_tCANData[rgIndex[i]].iValue10 = sf_KIp;  //int       sf_KIp
        m_tCANData[rgIndex[i]].iValue11 = sf_KDp;  //int       sf_KDp
        m_tCANData[rgIndex[i]].iValue12 = va_scale;  //int       va_scale
        m_tCANData[rgIndex[i]].iValue13 = T_Settletime;  //int       T_Settletime
        m_tCANData[rgIndex[i]].iValue14 = (int) Allow_Homing;  //unsigned  Allow_Homing
        m_tCANData[rgIndex[i]].iValue15 = Homing_tick_count;  //int       Homing_tick_count
        m_tCANData[rgIndex[i]].iValue16 = Homing_Speed_PWM;  //int       Homing_Speed_PWM
        m_tCANData[rgIndex[i]].iValue17 = Homing_Seeking_PWM;  //int       Homing_Seeking_PWM
        m_tCANData[rgIndex[i]].iValue18 = max_output;  //int       max_output
        m_tCANData[rgIndex[i]].iValue19 = min_output;  //int       min_output
        m_tCANData[rgIndex[i]].iValue20 = max_setpoint;  //int       max_setpoint
        m_tCANData[rgIndex[i]].iValue21 = min_setpoint;  //int       min_setpoint
        m_tCANData[rgIndex[i]].iValue22 = max_tracking_error;  //int       max_tracking_error
        m_tCANData[rgIndex[i]].iValue23 = max_tolerance_error;  //int       max_tolerance_error
        m_tCANData[rgIndex[i]].iValue24 = max_dc_i;  //int       max_dc_i
        m_tCANData[rgIndex[i]].iValue25 = max_dc_pwm;  //int       max_dc_pwm
        m_tCANData[rgIndex[i]].iValue26 = *(int *) ((void *) &du_max_vel);  //float     du_max_vel
        m_tCANData[rgIndex[i]].iValue27 = *(int *) ((void *) &du_min_vel);  //float     du_min_vel
        m_tCANData[rgIndex[i]].iValue28 = *(int *) ((void *) &du_max_acc);  //float     du_max_acc
        m_tCANData[rgIndex[i]].iValue29 = *(int *) ((void *) &du_min_acc);  //float     du_min_acc

        m_tCANData[rgIndex[i]].iValue30 = sd_Integ_Lim_min;  //int       sd_Integ_Lim_min

        m_tCANData[rgIndex[i]].iValue31 = (int) rgIndex[i];  //ECanObj  eCtrlObj
        m_tCANData[rgIndex[i]].iValue32 = (int) eSensorObj;  //ECanObj  eSensObj containing the conversion factors
        m_tCANData[rgIndex[i]].iValue33 = (int) eCurrSnsObj;  //ECanObj  eCurrSnsObj containing the conversion factors  for [A]
        m_tCANData[rgIndex[i]].iValue34 = (int) uWaitReadyTimeout;  //unsigned uWaitReadyTimeout;
        m_tCANData[rgIndex[i]].iValue35 = iDefaultPwm;  //int      iDefaultPwm;

    }

}
