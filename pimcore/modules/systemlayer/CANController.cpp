//---------------------------------------------------------------------------
#pragma hdrstop

#include <iostream>
#include <cstdint>
#include <cassert>
#include "CANController.h"   //for header-file

#include "../pimcore/PamStationFactory.h"
#include "../pimcore/PamStation.h"
#include "StubPamStation.h"

#include "CANStub.h"         //for getting stub
#include "CanInterface.h"    //for sending messages with CanInterface

#include "SysExceptionGenerator.h" //for passing Exception to ExceptionGenerator
#include "../utils/AMConfiguration.h"
#include "boost/date_time/gregorian/gregorian.hpp"
#pragma package(smart_init)



//--------------------------------------------------------------------

int CANController::iInstanceCounter = 0;

CANController::CANController() {
    ++iInstanceCounter;
    assert (iInstanceCounter == 1);  //only one instance of this class allowed (singleton)

    m_pCANStub = nullptr;
    m_pCanInterface = nullptr;

    m_pCANStub = new CANStub();
    assert(m_pCANStub);

    PamStation *pPam = PamStationFactory::getCurrentPamStation();
    auto *pStubPam = dynamic_cast<StubPamStation *>(pPam);
    m_pCanInterface = pStubPam->getCanInterface();
    assert (m_pCanInterface);

}

CANController::~CANController() {
    --iInstanceCounter;
    assert (iInstanceCounter == 0);  //only one instance of this class allowed (singleton)
    m_pCanInterface = nullptr;      //destruct is done in ~(Stub)Pamalyser
    FREEPOINTER(m_pCANStub);
}

bool CANController::isStub() {
    PamStation *pPam = PamStationFactory::getCurrentPamStation();
    auto *pStubPam = dynamic_cast<StubPamStation *>(pPam);
    return pStubPam->getConfig()->isStub();
}

bool CANController::isStubBoard1() {
    PamStation *pPam = PamStationFactory::getCurrentPamStation();
    auto *pStubPam = dynamic_cast<StubPamStation *>(pPam);
    return pStubPam->getConfig()->isStubBoard1();
}

bool CANController::isStubBoard2() {
    PamStation *pPam = PamStationFactory::getCurrentPamStation();
    auto *pStubPam = dynamic_cast<StubPamStation *>(pPam);
    return pStubPam->getConfig()->isStubBoard2();
}

bool CANController::isStubBoard3() {
    PamStation *pPam = PamStationFactory::getCurrentPamStation();
    auto *pStubPam = dynamic_cast<StubPamStation *>(pPam);
    return pStubPam->getConfig()->isStubBoard3();
}

//-----------------------------------------------
// return value of  send_rcv_CanMsg is ignored
void CANController::setEnabled(ECanObj eObj, bool fTrueFalse) {
    send_rcv_CanMsg(eObj, eCmdSetEnabled, (int) fTrueFalse);
}

void CANController::setTarget(ECanObj eObj, int iTargetinLU) { send_rcv_CanMsg(eObj, eCmdSetTarget, iTargetinLU); }

void CANController::goHome(ECanObj eObj) { send_rcv_CanMsg(eObj, eCmdGoHome, 0); }

void CANController::setSpeed(ECanObj eObj, float rSpeed) {
    send_rcv_CanMsg(eObj, eCmdSetSpeed, *(int *) ((void *) &rSpeed));
} //cast float to int

int CANController::getStatus(ECanObj eObj) { return send_rcv_CanMsg(eObj, eCmdGetStatus, 0); }

int CANController::getTarget(ECanObj eObj) { return send_rcv_CanMsg(eObj, eCmdGetTarget, 0); }

int CANController::getActual(ECanObj eObj) { return send_rcv_CanMsg(eObj, eCmdGetActual, 0); }

int CANController::getError(ECanObj eObj) { return send_rcv_CanMsg(eObj, eCmdGetError, 0); }



//---------------------------------------------------------------------------

int CANController::send_rcv_CanMsg(ECanObj eObj, ECanCmnd eCmd, int iData) {
    int iRetValue = -1;


    std::lock_guard<std::recursive_mutex> guard(m_pcsCANGuard);

    //create structures for a send and a receive message
    CPC_CAN_MSG_T pSend_CpcMsg;
    CPC_CAN_MSG_T pReply_CpcMsg;

    //FILL CAN MSG ID
    //send object to corresponding board (enum eObj are ordered to board ID)
    if (eObj > eCanObjMin && eObj <= eBoard1Ctrl) pSend_CpcMsg.id = ePCtoBOARD1;   //Board1 = SYCB
    else if (eObj > eBoard1Ctrl && eObj <= eBoard2Ctrl) pSend_CpcMsg.id = ePCtoBOARD2;   //Board2 = ICCB
    else if (eObj > eBoard2Ctrl && eObj <= eBoard3Ctrl) pSend_CpcMsg.id = ePCtoBOARD3;   //Board3 = WSCB
    else
        assert (0);


    //FILL CAN MSG LENGTH
    switch (eCmd) {
        case eCmdSetEnabled    :
        case eCmdSetTarget     :
        case eCmdGoHome        :
        case eCmdSetSpeed      :
            pSend_CpcMsg.length = 6;
            break;

        case eCmdGetStatus     :
        case eCmdGetTarget     :
        case eCmdGetActual     :
        case eCmdGetError      :
            pSend_CpcMsg.length = 2;
            break;

        case eCmdFlashBoard    :
        case eCmdSetBoardStatus:
            pSend_CpcMsg.length = 6;
            break;
        case eCmdGetBoardStatus:
            pSend_CpcMsg.length = 2;
            break;

        case eCmdSetValue1     :
        case eCmdSetValue2     :
        case eCmdSetValue3     :
        case eCmdSetValue4     :
        case eCmdSetValue5     :
        case eCmdSetValue6     :
        case eCmdSetValue7     :
        case eCmdSetValue8     :
        case eCmdSetValue9     :
        case eCmdSetValue10    :
        case eCmdSetValue11    :
        case eCmdSetValue12    :
        case eCmdSetValue13    :
        case eCmdSetValue14    :
        case eCmdSetValue15    :
        case eCmdSetValue16    :
        case eCmdSetValue17    :
        case eCmdSetValue18    :
        case eCmdSetValue19    :
        case eCmdSetValue20    :
        case eCmdSetValue21    :
        case eCmdSetValue22    :
        case eCmdSetValue23    :
        case eCmdSetValue24    :
        case eCmdSetValue25    :
        case eCmdSetValue26    :
        case eCmdSetValue27    :
        case eCmdSetValue28    :
        case eCmdSetValue29    :
        case eCmdSetValue30    :
        case eCmdSetValue31    :
        case eCmdSetValue32    :
        case eCmdSetValue33    :
        case eCmdSetValue34    :
        case eCmdSetValue35    :
        case eCmdSetValue36    :
        case eCmdSetValue37    :
        case eCmdSetValue38    :
        case eCmdSetValue39    :
        case eCmdSetValue40    :
        case eCmdSetValue41    :
        case eCmdSetValue42    :
        case eCmdSetValue43    :
        case eCmdSetValue44    :
        case eCmdSetValue45    :
        case eCmdSetValue46    :
        case eCmdSetValue47    :
        case eCmdSetValue48    :
        case eCmdSetValue49    :
        case eCmdSetValue50    :
            pSend_CpcMsg.length = 6;
            break;

        case eCmdGetValue1     :
        case eCmdGetValue2     :
        case eCmdGetValue3     :
        case eCmdGetValue4     :
        case eCmdGetValue5     :
        case eCmdGetValue6     :
        case eCmdGetValue7     :
        case eCmdGetValue8     :
        case eCmdGetValue9     :
        case eCmdGetValue10    :
        case eCmdGetValue11    :
        case eCmdGetValue12    :
        case eCmdGetValue13    :
        case eCmdGetValue14    :
        case eCmdGetValue15    :
        case eCmdGetValue16    :
        case eCmdGetValue17    :
        case eCmdGetValue18    :
        case eCmdGetValue19    :
        case eCmdGetValue20    :
        case eCmdGetValue21    :
        case eCmdGetValue22    :
        case eCmdGetValue23    :
        case eCmdGetValue24    :
        case eCmdGetValue25    :
        case eCmdGetValue26    :
        case eCmdGetValue27    :
        case eCmdGetValue28    :
        case eCmdGetValue29    :
        case eCmdGetValue30    :
        case eCmdGetValue31    :
        case eCmdGetValue32    :
        case eCmdGetValue33    :
        case eCmdGetValue34    :
        case eCmdGetValue35    :
        case eCmdGetValue36    :
        case eCmdGetValue37    :
        case eCmdGetValue38    :
        case eCmdGetValue39    :
        case eCmdGetValue40    :
        case eCmdGetValue41    :
        case eCmdGetValue42    :
        case eCmdGetValue43    :
        case eCmdGetValue44    :
        case eCmdGetValue45    :
        case eCmdGetValue46    :
        case eCmdGetValue47    :
        case eCmdGetValue48    :
        case eCmdGetValue49    :
        case eCmdGetValue50    :
            pSend_CpcMsg.length = 2;
            break;

        default             :
            assert(0);
    }

    //FILL CAN MSG DATA BYTES
    pSend_CpcMsg.msg[0] = eObj;                           //FILL OBJ
    pSend_CpcMsg.msg[1] = eCmd;                           //FILL CMD
    pSend_CpcMsg.msg[2] = (uint8_t) ((iData) & 0xFF);   //LSB
    pSend_CpcMsg.msg[3] = (uint8_t) ((iData >> 8) & 0xFF);
    pSend_CpcMsg.msg[4] = (uint8_t) ((iData >> 16) & 0xFF);
    pSend_CpcMsg.msg[5] = (uint8_t) ((iData >> 24) & 0xFF);   //MSB
    pSend_CpcMsg.msg[6] = 0;                              //Not Used
    pSend_CpcMsg.msg[7] = 0;                              //Not Used


    //"sensor"-parameters for aspirateUpsensor
    if (eObj == 0x80 && eCmd > eCmdGetError) {
        //send all messages for these commands to STUB
        m_pCANStub->Send_ReceiveCanMessage(&pSend_CpcMsg, &pReply_CpcMsg);
    } else if (eObj == 0x24 || eObj == 0x29) {
        //board1 ePWMSpare1 en eDiagSYCBPOError
        //todo: eventually enable this in ESW
        m_pCANStub->Send_ReceiveCanMessage(&pSend_CpcMsg, &pReply_CpcMsg);
    } else {
        if (isStub()) {
            //send all messages to STUB
            m_pCANStub->Send_ReceiveCanMessage(&pSend_CpcMsg, &pReply_CpcMsg);
        } else {
            switch (pSend_CpcMsg.id) {
                case ePCtoBOARD1: {
                    if (isStubBoard1()) {
                        m_pCANStub->Send_ReceiveCanMessage(&pSend_CpcMsg, &pReply_CpcMsg);
                    } else {
                        m_pCanInterface->Send_ReceiveCanMessage(&pSend_CpcMsg, &pReply_CpcMsg);
                    }
                    break;
                }
                case ePCtoBOARD2: {
                    if (isStubBoard2()) {
                        m_pCANStub->Send_ReceiveCanMessage(&pSend_CpcMsg, &pReply_CpcMsg);
                    } else {
                        m_pCanInterface->Send_ReceiveCanMessage(&pSend_CpcMsg, &pReply_CpcMsg);
                    }
                    break;
                }
                case ePCtoBOARD3: {
                    if (isStubBoard3()) {
                        m_pCANStub->Send_ReceiveCanMessage(&pSend_CpcMsg, &pReply_CpcMsg);
                    } else {
                        m_pCanInterface->Send_ReceiveCanMessage(&pSend_CpcMsg, &pReply_CpcMsg);
                    }
                    break;
                }
                default:
                    assert (0);
            }
        }
    }

    //CHECK CAN REPLY ID
    switch (pSend_CpcMsg.id) {
        case ePCtoBOARD1:
            assert(pReply_CpcMsg.id == eBOARD1toPC);
            break;
        case ePCtoBOARD2:
            assert(pReply_CpcMsg.id == eBOARD2toPC);
            break;
        case ePCtoBOARD3:
            assert(pReply_CpcMsg.id == eBOARD3toPC);
            break;
        default         :
            assert(0);
    }

    //CHECK CAN REPLY LENGTH
    //replied cmd
    switch (pReply_CpcMsg.msg[1]) {
        case eCmdSetEnabled    :
        case eCmdSetTarget     :
        case eCmdGoHome        :
        case eCmdSetSpeed      :
            assert(pReply_CpcMsg.length == 2);
            break;
        case eCmdGetStatus     :
        case eCmdGetTarget     :
        case eCmdGetActual     :
        case eCmdGetError      :
            assert(pReply_CpcMsg.length == 6);
            break;

        case eCmdFlashBoard    :
        case eCmdSetBoardStatus:
            assert(pReply_CpcMsg.length == 2);
            break;

        case eCmdGetBoardStatus:
            assert(pReply_CpcMsg.length == 6);
            break;

        case eCmdSetValue1     :
        case eCmdSetValue2     :
        case eCmdSetValue3     :
        case eCmdSetValue4     :
        case eCmdSetValue5     :
        case eCmdSetValue6     :
        case eCmdSetValue7     :
        case eCmdSetValue8     :
        case eCmdSetValue9     :
        case eCmdSetValue10    :
        case eCmdSetValue11    :
        case eCmdSetValue12    :
        case eCmdSetValue13    :
        case eCmdSetValue14    :
        case eCmdSetValue15    :
        case eCmdSetValue16    :
        case eCmdSetValue17    :
        case eCmdSetValue18    :
        case eCmdSetValue19    :
        case eCmdSetValue20    :
        case eCmdSetValue21    :
        case eCmdSetValue22    :
        case eCmdSetValue23    :
        case eCmdSetValue24    :
        case eCmdSetValue25    :
        case eCmdSetValue26    :
        case eCmdSetValue27    :
        case eCmdSetValue28    :
        case eCmdSetValue29    :
        case eCmdSetValue30    :
        case eCmdSetValue31    :
        case eCmdSetValue32    :
        case eCmdSetValue33    :
        case eCmdSetValue34    :
        case eCmdSetValue35    :
        case eCmdSetValue36    :
        case eCmdSetValue37    :
        case eCmdSetValue38    :
        case eCmdSetValue39    :
        case eCmdSetValue40    :
        case eCmdSetValue41    :
        case eCmdSetValue42    :
        case eCmdSetValue43    :
        case eCmdSetValue44    :
        case eCmdSetValue45    :
        case eCmdSetValue46    :
        case eCmdSetValue47    :
        case eCmdSetValue48    :
        case eCmdSetValue49    :
        case eCmdSetValue50    :
            assert(pReply_CpcMsg.length == 2);
            break;

        case eCmdGetValue1     :
        case eCmdGetValue2     :
        case eCmdGetValue3     :
        case eCmdGetValue4     :
        case eCmdGetValue5     :
        case eCmdGetValue6     :
        case eCmdGetValue7     :
        case eCmdGetValue8     :
        case eCmdGetValue9     :
        case eCmdGetValue10    :
        case eCmdGetValue11    :
        case eCmdGetValue12    :
        case eCmdGetValue13    :
        case eCmdGetValue14    :
        case eCmdGetValue15    :
        case eCmdGetValue16    :
        case eCmdGetValue17    :
        case eCmdGetValue18    :
        case eCmdGetValue19    :
        case eCmdGetValue20    :
        case eCmdGetValue21    :
        case eCmdGetValue22    :
        case eCmdGetValue23    :
        case eCmdGetValue24    :
        case eCmdGetValue25    :
        case eCmdGetValue26    :
        case eCmdGetValue27    :
        case eCmdGetValue28    :
        case eCmdGetValue29    :
        case eCmdGetValue30    :
        case eCmdGetValue31    :
        case eCmdGetValue32    :
        case eCmdGetValue33    :
        case eCmdGetValue34    :
        case eCmdGetValue35    :
        case eCmdGetValue36    :
        case eCmdGetValue37    :
        case eCmdGetValue38    :
        case eCmdGetValue39    :
        case eCmdGetValue40    :
        case eCmdGetValue41    :
        case eCmdGetValue42    :
        case eCmdGetValue43    :
        case eCmdGetValue44    :
        case eCmdGetValue45    :
        case eCmdGetValue46    :
        case eCmdGetValue47    :
        case eCmdGetValue48    :
        case eCmdGetValue49    :
        case eCmdGetValue50    :
            assert(pReply_CpcMsg.length == 6);
            break;

        case eCmdError         :
            assert(pReply_CpcMsg.length == 3);
            break;     //a system or communication error occurred

        default                :
            assert(0);
    }


    //CHECK OBJ, object should always be mirrored
    assert(pReply_CpcMsg.msg[0] == pSend_CpcMsg.msg[0]); //msg[0] contains the eObj

    //CHECK CMD, cmd should always be mirrored unless an error occurred
    //reply cmd is not an error
    if (pReply_CpcMsg.msg[1] != eCmdError) {
        //msg[1] contains the eCmd
        assert(pReply_CpcMsg.msg[1] == pSend_CpcMsg.msg[1]);
    } else {
        // a system or communication error occured
        PamStation *pPam = PamStationFactory::getCurrentPamStation();
        ExceptionGenerator *pExc = pPam->getExceptionGenerator();
        auto *m_pExceptionGenerator = dynamic_cast<SysExceptionGenerator *>(pExc);
        assert (m_pExceptionGenerator);

        int iObj = (int) (pReply_CpcMsg.msg[0]);
        int iErrorCode = (int) (pReply_CpcMsg.msg[2]);
        std::string strDesc = "System error notification, Obj: " + std::to_string(iObj) + ", Error-code: " +
                              std::to_string(iErrorCode);
        m_pExceptionGenerator->onException(this, CANCONTROLLER_ESW_ERROR_NOTIFICATION, eErrorUnknown, strDesc);
    }

    //RE-ASSEMBLE CAN DATA BYTES INTO AN INTEGER
    //if eCmd was "setter" iRetValue should be 0
    //if eCmd was "getter" iRetValue is the requested value in (int)LU (local units: [V] , [enc] etc)
    iRetValue = ((int) (pReply_CpcMsg.msg[2]) +    //LSB
                 (int) (pReply_CpcMsg.msg[3] << 8) +
                 (int) (pReply_CpcMsg.msg[4] << 16) +
                 (int) (pReply_CpcMsg.msg[5] << 24));   //MSB

    //RETURN DATA
    return iRetValue;

}

//---------------------------------------------------------------
void CANController::setSensorCalibrationData(Sensor_Calibration_Data_t tStruct) {
    ECanObj eObj = tStruct.eSensorObj;
    //obj must be a analog sensor or encoder (or aspirateSensor) or current sensor
    assert (eObj == eTempSensDispense ||
            eObj == eTempSensBottomLeft ||
            eObj == eTempSensBottomMiddle ||
            eObj == eTempSensBottomRight ||
            eObj == eTempSensCoverLeft ||
            eObj == eTempSensCoverMiddle ||
            eObj == eTempSensCoverRight ||
            eObj == eTempSensCalibration ||
            eObj == ePressSensOverPress ||
            eObj == ePressSensUnderPress ||
            eObj == ePressSensAspirate ||
            eObj == ePressSensWell1 ||
            eObj == ePressSensWell2 ||
            eObj == ePressSensWell3 ||
            eObj == ePressSensWell4 ||
            eObj == ePressSensWell5 ||
            eObj == ePressSensWell6 ||
            eObj == ePressSensWell7 ||
            eObj == ePressSensWell8 ||
            eObj == ePressSensWell9 ||
            eObj == ePressSensWell10 ||
            eObj == ePressSensWell11 ||
            eObj == ePressSensWell12 ||
            eObj == eEncDispenseHead1A ||
            eObj == eEncDispenseHead2A ||
            eObj == eEncDispenseHead3A ||
            eObj == eEncDispenseHead4A ||
            eObj == eEncFilterA ||
            eObj == eEncFocusA ||
            eObj == eEncTransXA ||
            eObj == eEncTransYA ||
            eObj == eSensAspirateHeadUp ||
            eObj == eCurrSensAspirateMotor ||
            eObj == eCurrSensBottomLeft ||
            eObj == eCurrSensBottomMiddle ||
            eObj == eCurrSensBottomRight ||
            eObj == eCurrSensCoverLeft ||
            eObj == eCurrSensCoverMiddle ||
            eObj == eCurrSensCoverRight ||
            eObj == eCurrSensDispenseHeater ||
            eObj == eCurrSensFilter ||
            eObj == eCurrSensFocus ||
            eObj == eCurrSensTransX ||
            eObj == eCurrSensTransY
    );

    //The SensorCalibrationStruct consists of 7 floats
    //These are send to the instrument via CAN
    //as objectnr the sensor-adress is used
    //as cmd nr value1 to 7 are used
    //the data (floats) are converted to the IEEE integer representation:
    // - first cast  (the adress of ) the  (4 bytes) float to a neutral void (pointer)
    // - then cast this neutral to a signed integer (also 4 bytes) pointer
    // - so: int iInteger = * (int*) ((void*) &rFloat);

    send_rcv_CanMsg(eObj, eCmdSetValue1, (int) tStruct.eSensorObj);   //ECanObj eSensorObj   ;
    send_rcv_CanMsg(eObj, eCmdSetValue2, *(int *) ((void *) &tStruct.rSysOffset));   //float   rSysOffset   ;
    send_rcv_CanMsg(eObj, eCmdSetValue3, *(int *) ((void *) &tStruct.rSysGain));   //float   rSysGain     ;
    send_rcv_CanMsg(eObj, eCmdSetValue4, *(int *) ((void *) &tStruct.rSysGain2));   //float   rSysGain2    ;
    send_rcv_CanMsg(eObj, eCmdSetValue5, *(int *) ((void *) &tStruct.rSensOffset));   //float   rSensOffset  ;
    send_rcv_CanMsg(eObj, eCmdSetValue6, *(int *) ((void *) &tStruct.rSensGain));   //float   rSensGain    ;
    send_rcv_CanMsg(eObj, eCmdSetValue7, *(int *) ((void *) &tStruct.rSensGain2));   //float   rSensGain2   ;
    send_rcv_CanMsg(eObj, eCmdSetValue8, *(int *) ((void *) &tStruct.rADConversion));   //float   rADConversion;
    send_rcv_CanMsg(eObj, eCmdSetValue9, *(int *) ((void *) &tStruct.rTsample));   //float   rTsample     ;


    //START TEST if conversion from float to integer and back is ok
    int iData1 = send_rcv_CanMsg(eObj, eCmdGetValue1, 0);                             //(int)   eSensorObj   ;
    int iData2 = send_rcv_CanMsg(eObj, eCmdGetValue2, 0);                             //(int)   rSysOffset   ;
    int iData3 = send_rcv_CanMsg(eObj, eCmdGetValue3, 0);                             //(int)   rSysGain     ;
    int iData4 = send_rcv_CanMsg(eObj, eCmdGetValue4, 0);                             //(int)   rSysGain2    ;
    int iData5 = send_rcv_CanMsg(eObj, eCmdGetValue5, 0);                             //(int)   rSensOffset  ;
    int iData6 = send_rcv_CanMsg(eObj, eCmdGetValue6, 0);                             //(int)   rSensGain    ;
    int iData7 = send_rcv_CanMsg(eObj, eCmdGetValue7, 0);                             //(int)   rSensGain2   ;
    int iData8 = send_rcv_CanMsg(eObj, eCmdGetValue8, 0);                             //(int)   rADConversion;
    int iData9 = send_rcv_CanMsg(eObj, eCmdGetValue9, 0);                             //(int)   rTsample     ;

    assert (tStruct.eSensorObj == (ECanObj) iData1);                //ECanObj eSensorObj   ;
    assert (tStruct.rSysOffset == *(float *) ((void *) &iData2));                //float   rSysOffset   ;
    assert (tStruct.rSysGain == *(float *) ((void *) &iData3));                //float   rSysGain     ;
    assert (tStruct.rSysGain2 == *(float *) ((void *) &iData4));                //float   rSysGain2    ;
    assert (tStruct.rSensOffset == *(float *) ((void *) &iData5));                //float   rSensOffset  ;
    assert (tStruct.rSensGain == *(float *) ((void *) &iData6));                //float   rSensGain    ;
    assert (tStruct.rSensGain2 == *(float *) ((void *) &iData7));                //float   rSensGain2   ;
    assert (tStruct.rADConversion == *(float *) ((void *) &iData8));                //float   rADConversion;
    assert (tStruct.rTsample == *(float *) ((void *) &iData9));                //float   rTsample     ;
    //END TEST
}

Sensor_Calibration_Data_t CANController::getSensorCalibrationData(ECanObj eObj) {
    //obj must be a analog sensor or encoder (or aspirateSensor)
    assert (eObj == eTempSensDispense ||
            eObj == eTempSensBottomLeft ||
            eObj == eTempSensBottomMiddle ||
            eObj == eTempSensBottomRight ||
            eObj == eTempSensCoverLeft ||
            eObj == eTempSensCoverMiddle ||
            eObj == eTempSensCoverRight ||
            eObj == eTempSensCalibration ||
            eObj == ePressSensOverPress ||
            eObj == ePressSensUnderPress ||
            eObj == ePressSensAspirate ||
            eObj == ePressSensWell1 ||
            eObj == ePressSensWell2 ||
            eObj == ePressSensWell3 ||
            eObj == ePressSensWell4 ||
            eObj == ePressSensWell5 ||
            eObj == ePressSensWell6 ||
            eObj == ePressSensWell7 ||
            eObj == ePressSensWell8 ||
            eObj == ePressSensWell9 ||
            eObj == ePressSensWell10 ||
            eObj == ePressSensWell11 ||
            eObj == ePressSensWell12 ||
            eObj == eEncDispenseHead1A ||
            eObj == eEncDispenseHead2A ||
            eObj == eEncDispenseHead3A ||
            eObj == eEncDispenseHead4A ||
            eObj == eEncFilterA ||
            eObj == eEncFocusA ||
            eObj == eEncTransXA ||
            eObj == eEncTransYA ||
            eObj == eSensAspirateHeadUp ||
            eObj == eCurrSensAspirateMotor ||
            eObj == eCurrSensBottomLeft ||
            eObj == eCurrSensBottomMiddle ||
            eObj == eCurrSensBottomRight ||
            eObj == eCurrSensCoverLeft ||
            eObj == eCurrSensCoverMiddle ||
            eObj == eCurrSensCoverRight ||
            eObj == eCurrSensDispenseHeater ||
            eObj == eCurrSensFilter ||
            eObj == eCurrSensFocus ||
            eObj == eCurrSensTransX ||
            eObj == eCurrSensTransY
    );


    Sensor_Calibration_Data_t tStruct;

    //get all calibration data
    int iData1 = send_rcv_CanMsg(eObj, eCmdGetValue1, 0);                //(int)   eSensorObj   ;
    int iData2 = send_rcv_CanMsg(eObj, eCmdGetValue2, 0);                //(int)   rSysOffset   ;
    int iData3 = send_rcv_CanMsg(eObj, eCmdGetValue3, 0);                //(int)   rSysGain     ;
    int iData4 = send_rcv_CanMsg(eObj, eCmdGetValue4, 0);                //(int)   rSysGain2    ;
    int iData5 = send_rcv_CanMsg(eObj, eCmdGetValue5, 0);                //(int)   rSensOffset  ;
    int iData6 = send_rcv_CanMsg(eObj, eCmdGetValue6, 0);                //(int)   rSensGain    ;
    int iData7 = send_rcv_CanMsg(eObj, eCmdGetValue7, 0);                //(int)   rSensGain2   ;
    int iData8 = send_rcv_CanMsg(eObj, eCmdGetValue8, 0);                //(int)   rADConversion;
    int iData9 = send_rcv_CanMsg(eObj, eCmdGetValue9, 0);                //(int)   rTsample     ;


    //convert (back) to float and set in calibration struct
    tStruct.eSensorObj = (ECanObj) iData1;           //ECanObj eSensorObj   ;
    tStruct.rSysOffset = *(float *) ((void *) &iData2);           //float   rSysOffset   ;
    tStruct.rSysGain = *(float *) ((void *) &iData3);           //float   rSysGain     ;
    tStruct.rSysGain2 = *(float *) ((void *) &iData4);           //float   rSysGain2    ;
    tStruct.rSensOffset = *(float *) ((void *) &iData5);           //float   rSensOffset  ;
    tStruct.rSensGain = *(float *) ((void *) &iData6);           //float   rSensGain    ;
    tStruct.rSensGain2 = *(float *) ((void *) &iData7);           //float   rSensGain2   ;
    tStruct.rADConversion = *(float *) ((void *) &iData8);           //float   rADConversion;
    tStruct.rTsample = *(float *) ((void *) &iData9);           //float   rTsample     ;

    return tStruct;
}

void CANController::setControllerParameters(Controllable_Settings_t tStruct) {
    ECanObj eObj = tStruct.eCtrlObj;

    send_rcv_CanMsg(eObj, eCmdSetValue1, tStruct.sd_KPp);  //int       sd_KPp
    send_rcv_CanMsg(eObj, eCmdSetValue2, tStruct.sd_KIp);  //int       sd_KIp
    send_rcv_CanMsg(eObj, eCmdSetValue3, tStruct.sd_KDp);  //int       sd_KDp
    send_rcv_CanMsg(eObj, eCmdSetValue4, tStruct.sd_Integ_Lim_max);  //int       sd_Integ_Lim_max
    send_rcv_CanMsg(eObj, eCmdSetValue5, tStruct.KPi);  //int       KPi
    send_rcv_CanMsg(eObj, eCmdSetValue6, tStruct.KIi);  //int       KIi
    send_rcv_CanMsg(eObj, eCmdSetValue7, *(int *) ((void *) &tStruct.DefaultVel));  //float     DefaultVel
    send_rcv_CanMsg(eObj, eCmdSetValue8, *(int *) ((void *) &tStruct.DefaultAcc));  //float     DefaultAcc
    send_rcv_CanMsg(eObj, eCmdSetValue9, tStruct.sf_KPp);  //int       sf_KPp
    send_rcv_CanMsg(eObj, eCmdSetValue10, tStruct.sf_KIp);  //int       sf_KIp
    send_rcv_CanMsg(eObj, eCmdSetValue11, tStruct.sf_KDp);  //int       sf_KDp
    send_rcv_CanMsg(eObj, eCmdSetValue12, tStruct.va_scale);  //int       va_scale
    send_rcv_CanMsg(eObj, eCmdSetValue13, tStruct.T_Settletime);  //int       T_Settletime
    send_rcv_CanMsg(eObj, eCmdSetValue14, tStruct.Allow_Homing);  //unsigned  Allow_Homing
    send_rcv_CanMsg(eObj, eCmdSetValue15, tStruct.Homing_tick_count);  //int       Homing_tick_count
    send_rcv_CanMsg(eObj, eCmdSetValue16, tStruct.Homing_Speed_PWM);  //int       Homing_Speed_PWM
    send_rcv_CanMsg(eObj, eCmdSetValue17, tStruct.Homing_Seeking_PWM);  //int       Homing_Seeking_PWM
    send_rcv_CanMsg(eObj, eCmdSetValue18, tStruct.max_output);  //int       max_output
    send_rcv_CanMsg(eObj, eCmdSetValue19, tStruct.min_output);  //int       min_output
    send_rcv_CanMsg(eObj, eCmdSetValue20, tStruct.max_setpoint);  //int       max_setpoint
    send_rcv_CanMsg(eObj, eCmdSetValue21, tStruct.min_setpoint);  //int       min_setpoint
    send_rcv_CanMsg(eObj, eCmdSetValue22, tStruct.max_tracking_error);  //int       max_tracking_error
    send_rcv_CanMsg(eObj, eCmdSetValue23, tStruct.max_tolerance_error);  //int       max_tolerance_error
    send_rcv_CanMsg(eObj, eCmdSetValue24, tStruct.max_dc_i);  //int       max_dc_i
    send_rcv_CanMsg(eObj, eCmdSetValue25, tStruct.max_dc_pwm);  //int       max_dc_pwm
    send_rcv_CanMsg(eObj, eCmdSetValue26, *(int *) ((void *) &tStruct.du_max_vel));  //float     du_max_vel
    send_rcv_CanMsg(eObj, eCmdSetValue27, *(int *) ((void *) &tStruct.du_min_vel));  //float     du_min_vel
    send_rcv_CanMsg(eObj, eCmdSetValue28, *(int *) ((void *) &tStruct.du_max_acc));  //float     du_max_acc
    send_rcv_CanMsg(eObj, eCmdSetValue29, *(int *) ((void *) &tStruct.du_min_acc));  //float     du_min_acc

    send_rcv_CanMsg(eObj, eCmdSetValue30, tStruct.sd_Integ_Lim_min);  //int       sd_Integ_Lim_min

    send_rcv_CanMsg(eObj, eCmdSetValue31, tStruct.eCtrlObj);  //ECanObj   eCtrlObj
    send_rcv_CanMsg(eObj, eCmdSetValue32,
                    tStruct.eSensObj);  //ECanObj   eSensObj containing the conversion factors
    send_rcv_CanMsg(eObj, eCmdSetValue33,
                    tStruct.eCurrSensObj);  //ECanObj   eCurrSensObj containing the conversion factors for current [A]
    send_rcv_CanMsg(eObj, eCmdSetValue34, tStruct.uWaitReadyTimeout);  //unsigned  uWaitReadyTimeout
    send_rcv_CanMsg(eObj, eCmdSetValue35, tStruct.iDefaultPwm);  //int       iDefaultPwm


}

Controllable_Settings_t CANController::getControllerParameters(ECanObj eObj) {
    Controllable_Settings_t tStruct;

    //get data from instrument
    int iData1 = send_rcv_CanMsg(eObj, eCmdGetValue1, 0);           //     sd_KPp
    int iData2 = send_rcv_CanMsg(eObj, eCmdGetValue2, 0);           //(int)     sd_KIp
    int iData3 = send_rcv_CanMsg(eObj, eCmdGetValue3, 0);           //(int)     sd_KDp
    int iData4 = send_rcv_CanMsg(eObj, eCmdGetValue4, 0);           //(int)     sd_Integ_Lim_max
    int iData5 = send_rcv_CanMsg(eObj, eCmdGetValue5, 0);           //(int)     KPi
    int iData6 = send_rcv_CanMsg(eObj, eCmdGetValue6, 0);           //(int)     KIi
    int iData7 = send_rcv_CanMsg(eObj, eCmdGetValue7, 0);           //(int)     DefaultVel
    int iData8 = send_rcv_CanMsg(eObj, eCmdGetValue8, 0);           //(int)     DefaultAcc
    int iData9 = send_rcv_CanMsg(eObj, eCmdGetValue9, 0);           //(int)     sf_KPp
    int iData10 = send_rcv_CanMsg(eObj, eCmdGetValue10, 0);           //(int)     sf_KIp
    int iData11 = send_rcv_CanMsg(eObj, eCmdGetValue11, 0);           //(int)     sf_KDp
    int iData12 = send_rcv_CanMsg(eObj, eCmdGetValue12, 0);           //(int)     va_scale
    int iData13 = send_rcv_CanMsg(eObj, eCmdGetValue13, 0);           //(int)     T_Settletime
    int iData14 = send_rcv_CanMsg(eObj, eCmdGetValue14, 0);           //(int)     Allow_Homing
    int iData15 = send_rcv_CanMsg(eObj, eCmdGetValue15, 0);           //(int)     Homing_tick_count
    int iData16 = send_rcv_CanMsg(eObj, eCmdGetValue16, 0);           //(int)     Homing_Speed_PWM
    int iData17 = send_rcv_CanMsg(eObj, eCmdGetValue17, 0);           //(int)     Homing_Seeking_PWM
    int iData18 = send_rcv_CanMsg(eObj, eCmdGetValue18, 0);           //(int)     max_output
    int iData19 = send_rcv_CanMsg(eObj, eCmdGetValue19, 0);           //(int)     min_output
    int iData20 = send_rcv_CanMsg(eObj, eCmdGetValue20, 0);           //(int)     max_setpoint
    int iData21 = send_rcv_CanMsg(eObj, eCmdGetValue21, 0);           //(int)     min_setpoint
    int iData22 = send_rcv_CanMsg(eObj, eCmdGetValue22, 0);           //(int)     max_tracking_error
    int iData23 = send_rcv_CanMsg(eObj, eCmdGetValue23, 0);           //(int)     max_tolerance_error
    int iData24 = send_rcv_CanMsg(eObj, eCmdGetValue24, 0);           //(int)     max_dc_i
    int iData25 = send_rcv_CanMsg(eObj, eCmdGetValue25, 0);           //(int)     max_dc_pwm
    int iData26 = send_rcv_CanMsg(eObj, eCmdGetValue26, 0);           //(int)     du_max_vel
    int iData27 = send_rcv_CanMsg(eObj, eCmdGetValue27, 0);           //(int)     du_min_vel
    int iData28 = send_rcv_CanMsg(eObj, eCmdGetValue28, 0);           //(int)     du_max_acc
    int iData29 = send_rcv_CanMsg(eObj, eCmdGetValue29, 0);           //(int)     du_min_acc

    int iData30 = send_rcv_CanMsg(eObj, eCmdGetValue30, 0);           //(int)     sd_Integ_Lim_min

    int iData31 = send_rcv_CanMsg(eObj, eCmdGetValue31, 0);           //(int)     eCtrlObj
    int iData32 = send_rcv_CanMsg(eObj, eCmdGetValue32, 0);           //(int)     eSensObj
    int iData33 = send_rcv_CanMsg(eObj, eCmdGetValue33, 0);           //(int)     eCurrSensObj
    int iData34 = send_rcv_CanMsg(eObj, eCmdGetValue34, 0);           //(int)     uWaitReadyTimeout
    int iData35 = send_rcv_CanMsg(eObj, eCmdGetValue35, 0);           //(int)     iDefaultPwm


    //convert to original type
    tStruct.sd_KPp = iData1;      //int       sd_KPp
    tStruct.sd_KIp = iData2;      //int       sd_KIp
    tStruct.sd_KDp = iData3;      //int       sd_KDp
    tStruct.sd_Integ_Lim_max = iData4;      //int       sd_Integ_Lim_max
    tStruct.KPi = iData5;      //int       KPi
    tStruct.KIi = iData6;      //int       KIi
    tStruct.DefaultVel = *(float *) ((void *) &iData7);      //float     DefaultVel
    tStruct.DefaultAcc = *(float *) ((void *) &iData8);      //float     DefaultAcc
    tStruct.sf_KPp = iData9;      //int       sf_KPp
    tStruct.sf_KIp = iData10;      //int       sf_KIp
    tStruct.sf_KDp = iData11;      //int       sf_KDp
    tStruct.va_scale = iData12;      //int       va_scale
    tStruct.T_Settletime = iData13;      //int       T_Settletime
    tStruct.Allow_Homing = (unsigned) iData14;      //unsigned  Allow_Homing
    tStruct.Homing_tick_count = iData15;      //int       Homing_tick_count
    tStruct.Homing_Speed_PWM = iData16;      //int       Homing_Speed_PWM
    tStruct.Homing_Seeking_PWM = iData17;      //int       Homing_Seeking_PWM
    tStruct.max_output = iData18;      //int       max_output
    tStruct.min_output = iData19;      //int       min_output
    tStruct.max_setpoint = iData20;      //int       max_setpoint
    tStruct.min_setpoint = iData21;      //int       min_setpoint
    tStruct.max_tracking_error = iData22;      //int       max_tracking_error
    tStruct.max_tolerance_error = iData23;      //int       max_tolerance_error
    tStruct.max_dc_i = iData24;      //int       max_dc_i
    tStruct.max_dc_pwm = iData25;      //int       max_dc_pwm
    tStruct.du_max_vel = *(float *) ((void *) &iData26);      //float     du_max_vel
    tStruct.du_min_vel = *(float *) ((void *) &iData27);      //float     du_min_vel
    tStruct.du_max_acc = *(float *) ((void *) &iData28);      //float     du_max_acc
    tStruct.du_min_acc = *(float *) ((void *) &iData29);      //float     du_min_acc

    tStruct.sd_Integ_Lim_min = iData30;      //int       sd_Integ_Lim_min

    tStruct.eCtrlObj = (ECanObj) iData31;      //ECanObj   eCtrlObj
    tStruct.eSensObj = (ECanObj) iData32;      //ECanObj   eSensObj
    tStruct.eCurrSensObj = (ECanObj) iData33;      //ECanObj   eCurrSensObj
    tStruct.uWaitReadyTimeout = (unsigned) iData34;      //unsigned  uWaitReadyTimeout
    tStruct.iDefaultPwm = iData35;      //int       iDefaultPwm


    return tStruct;

}

//------------------------------------------------------------------------------
void CANController::setFilterLogicalPostions(Filter_LogicalPos_t tStruct) {
    ECanObj eObj = eEncFilterB;  //store logPosData "behind" eEncFilterB-channel (obj eEncFilterA-channel is used for sensor-calibration data)

    send_rcv_CanMsg(eObj, eCmdSetValue1, (int) eObj); //ECanObj eSensorObj
    send_rcv_CanMsg(eObj, eCmdSetValue2, *(int *) ((void *) &tStruct.rPosFilter1)); //float   rPosFilter1
    send_rcv_CanMsg(eObj, eCmdSetValue3, *(int *) ((void *) &tStruct.rPosFilter2)); //float   rPosFilter2
    send_rcv_CanMsg(eObj, eCmdSetValue4, *(int *) ((void *) &tStruct.rPosFilter3)); //float   rPosFilter3

    //START TEST if conversion from float to integer and back is ok
    int iData1 = send_rcv_CanMsg(eObj, eCmdGetValue1, 0);                          //(int)   eSensorObj
    int iData2 = send_rcv_CanMsg(eObj, eCmdGetValue2, 0);                          //(int)   rPosFilter1
    int iData3 = send_rcv_CanMsg(eObj, eCmdGetValue3, 0);                          //(int)   rPosFilter2
    int iData4 = send_rcv_CanMsg(eObj, eCmdGetValue4, 0);                          //(int)   rPosFilter3

    assert (eObj == (ECanObj) iData1);              //ECanObj eSensorObj
    assert (tStruct.rPosFilter1 == *(float *) ((void *) &iData2));              //float   rPosFilter1
    assert (tStruct.rPosFilter2 == *(float *) ((void *) &iData3));              //float   rPosFilter2
    assert (tStruct.rPosFilter3 == *(float *) ((void *) &iData4));              //float   rPosFilter3
    //END TEST

}

Filter_LogicalPos_t CANController::getFilterLogicalPositions() {
    ECanObj eObj = eEncFilterB;  //store logPosData "behind" eEncFilterB-channel (obj eEncFilterA-channel is used for sensor-calibration data)
    Filter_LogicalPos_t tStruct;

    int iData1 = send_rcv_CanMsg(eObj, eCmdGetValue1, 0);       //(int)   eSensorObj
    int iData2 = send_rcv_CanMsg(eObj, eCmdGetValue2, 0);       //(int)   rPosFilter1
    int iData3 = send_rcv_CanMsg(eObj, eCmdGetValue3, 0);       //(int)   rPosFilter2
    int iData4 = send_rcv_CanMsg(eObj, eCmdGetValue4, 0);       //(int)   rPosFilter3

//    eObj = (ECanObj) iData1;     //ECanObj eSensorObj
    tStruct.rPosFilter1 = *(float *) ((void *) &iData2);     //float   rPosFilter1
    tStruct.rPosFilter2 = *(float *) ((void *) &iData3);     //float   rPosFilter2
    tStruct.rPosFilter3 = *(float *) ((void *) &iData4);     //float   rPosFilter3

    return tStruct;

}

//----------------------------
void CANController::setFocusLogicalPostions(Focus_LogicalPos_t tStruct) {
    ECanObj eObj = eEncFocusB;  //store logPosData "behind" eEncFocusB-channel (obj eEncFocusA-channel is used for sensor-calibration data)

    send_rcv_CanMsg(eObj, eCmdSetValue1, (int) eObj);   //ECanObj eSensorObj
    send_rcv_CanMsg(eObj, eCmdSetValue2, *(int *) ((void *) &tStruct.rPosForFilter1));   //float   rPosForFilter1;
    send_rcv_CanMsg(eObj, eCmdSetValue3, *(int *) ((void *) &tStruct.rPosForFilter2));   //float   rPosForFilter2;
    send_rcv_CanMsg(eObj, eCmdSetValue4, *(int *) ((void *) &tStruct.rPosForFilter3));   //float   rPosForFilter3;
    send_rcv_CanMsg(eObj, eCmdSetValue5, *(int *) ((void *) &tStruct.rOffsetForDisp1));   //float   rOffsetForDisp1;
    send_rcv_CanMsg(eObj, eCmdSetValue6, *(int *) ((void *) &tStruct.rOffsetForDisp2));   //float   rOffsetForDisp2;
    send_rcv_CanMsg(eObj, eCmdSetValue7, *(int *) ((void *) &tStruct.rOffsetForDisp3));   //float   rOffsetForDisp3;

    //START TEST if conversion from float to integer and back is ok
    int iData1 = send_rcv_CanMsg(eObj, eCmdGetValue1, 0);                               //(int)   eSensorObj
    int iData2 = send_rcv_CanMsg(eObj, eCmdGetValue2, 0);                               //(int)   rPosForFilter1;
    int iData3 = send_rcv_CanMsg(eObj, eCmdGetValue3, 0);                               //(int)   rPosForFilter2;
    int iData4 = send_rcv_CanMsg(eObj, eCmdGetValue4, 0);                               //(int)   rPosForFilter3;
    int iData5 = send_rcv_CanMsg(eObj, eCmdGetValue5, 0);                               //(int)   rOffsetForDisp1;
    int iData6 = send_rcv_CanMsg(eObj, eCmdGetValue6, 0);                               //(int)   rOffsetForDisp2;
    int iData7 = send_rcv_CanMsg(eObj, eCmdGetValue7, 0);                               //(int)   rOffsetForDisp3;

    assert (eObj == (ECanObj) iData1);              //ECanObj eSensorObj
    assert (tStruct.rPosForFilter1 == *(float *) ((void *) &iData2));              //float   rPosForFilter1;
    assert (tStruct.rPosForFilter2 == *(float *) ((void *) &iData3));              //float   rPosForFilter2;
    assert (tStruct.rPosForFilter3 == *(float *) ((void *) &iData4));              //float   rPosForFilter3;
    assert (tStruct.rOffsetForDisp1 == *(float *) ((void *) &iData5));              //float   rOffsetForDisp1;
    assert (tStruct.rOffsetForDisp2 == *(float *) ((void *) &iData6));              //float   rOffsetForDisp2;
    assert (tStruct.rOffsetForDisp3 == *(float *) ((void *) &iData7));              //float   rOffsetForDisp3;
    //END TEST
}

Focus_LogicalPos_t CANController::getFocusLogicalPositions(void) {
    ECanObj eObj = eEncFocusB;  //store logPosData "behind" eEncFocusB-channel (obj eEncFocusA-channel is used for sensor-calibration data)
    Focus_LogicalPos_t tStruct;

    int iData1 = send_rcv_CanMsg(eObj, eCmdGetValue1, 0);          //(int)   eSensorObj
    int iData2 = send_rcv_CanMsg(eObj, eCmdGetValue2, 0);          //(int)   rPosForFilter1;
    int iData3 = send_rcv_CanMsg(eObj, eCmdGetValue3, 0);          //(int)   rPosForFilter2;
    int iData4 = send_rcv_CanMsg(eObj, eCmdGetValue4, 0);          //(int)   rPosForFilter3;
    int iData5 = send_rcv_CanMsg(eObj, eCmdGetValue5, 0);          //(int)   rOffsetForDisp1;
    int iData6 = send_rcv_CanMsg(eObj, eCmdGetValue6, 0);          //(int)   rOffsetForDisp2;
    int iData7 = send_rcv_CanMsg(eObj, eCmdGetValue7, 0);          //(int)   rOffsetForDisp3;

    eObj = (ECanObj) iData1;    //          eSensorObj
    tStruct.rPosForFilter1 = *(float *) ((void *) &iData2);    //float   rPosForFilter1;
    tStruct.rPosForFilter2 = *(float *) ((void *) &iData3);    //float   rPosForFilter2;
    tStruct.rPosForFilter3 = *(float *) ((void *) &iData4);    //float   rPosForFilter3;
    tStruct.rOffsetForDisp1 = *(float *) ((void *) &iData5);    //float   rOffsetForDisp1;
    tStruct.rOffsetForDisp2 = *(float *) ((void *) &iData6);    //float   rOffsetForDisp2;
    tStruct.rOffsetForDisp3 = *(float *) ((void *) &iData7);    //float   rOffsetForDisp3;

    return tStruct;

}
//---------------------------------------

void CANController::setWagonLogicalPostions(Wagon_LogicalXYPos_t tStruct) {

    ECanObj eObjX = eEncTransXB;  //store logPosData "behind" eEncTransXB-channel (obj eEncTransXA-channel is used for sensor-calibration data)
    ECanObj eObjY = eEncTransYB;  //store logPosData "behind" eEncTransYB-channel (obj eEncTransYA-channel is used for sensor-calibration data)

    int i = 0;
     send_rcv_CanMsg(eObjX, eCmdSetValue1, (int) eObjX);
     send_rcv_CanMsg(eObjY, eCmdSetValue1, (int) eObjY);
     send_rcv_CanMsg(eObjX, eCmdSetValue2, *(int *) ((void *) &tStruct.xyLoadPosition.X));
     send_rcv_CanMsg(eObjY, eCmdSetValue2, *(int *) ((void *) &tStruct.xyLoadPosition.Y));
     send_rcv_CanMsg(eObjX, eCmdSetValue3, *(int *) ((void *) &tStruct.xyIncubationPosition.X));
     send_rcv_CanMsg(eObjY, eCmdSetValue3, *(int *) ((void *) &tStruct.xyIncubationPosition.Y));
     send_rcv_CanMsg(eObjX, eCmdSetValue4, *(int *) ((void *) &tStruct.xyFrontPanelPosition.X));
     send_rcv_CanMsg(eObjY, eCmdSetValue4, *(int *) ((void *) &tStruct.xyFrontPanelPosition.Y));
     send_rcv_CanMsg(eObjX, eCmdSetValue5, *(int *) ((void *) &tStruct.xyReadPosition.X));
     send_rcv_CanMsg(eObjY, eCmdSetValue5, *(int *) ((void *) &tStruct.xyReadPosition.Y));
     send_rcv_CanMsg(eObjX, eCmdSetValue6, *(int *) ((void *) &tStruct.xyAspiratePosition.X));
     send_rcv_CanMsg(eObjY, eCmdSetValue6, *(int *) ((void *) &tStruct.xyAspiratePosition.Y));
     send_rcv_CanMsg(eObjX, eCmdSetValue7, *(int *) ((void *) &tStruct.xyDispensePosition.X));
     send_rcv_CanMsg(eObjY, eCmdSetValue7, *(int *) ((void *) &tStruct.xyDispensePosition.Y));
     send_rcv_CanMsg(eObjX, eCmdSetValue8, *(int *) ((void *) &tStruct.xyOffset_Well.X));
     send_rcv_CanMsg(eObjY, eCmdSetValue8, *(int *) ((void *) &tStruct.xyOffset_Well.Y));
     send_rcv_CanMsg(eObjX, eCmdSetValue9, *(int *) ((void *) &tStruct.xyOffset_Disposable.X));
     send_rcv_CanMsg(eObjY, eCmdSetValue9, *(int *) ((void *) &tStruct.xyOffset_Disposable.Y));
     send_rcv_CanMsg(eObjX, eCmdSetValue10, *(int *) ((void *) &tStruct.xyOffset_FocusGlass.X));
     send_rcv_CanMsg(eObjY, eCmdSetValue10, *(int *) ((void *) &tStruct.xyOffset_FocusGlass.Y));
     send_rcv_CanMsg(eObjX, eCmdSetValue11, *(int *) ((void *) &tStruct.xyOffset_DispenseHead1.X));
     send_rcv_CanMsg(eObjY, eCmdSetValue11, *(int *) ((void *) &tStruct.xyOffset_DispenseHead1.Y));
     send_rcv_CanMsg(eObjX, eCmdSetValue12, *(int *) ((void *) &tStruct.xyOffset_DispenseHead2.X));
     send_rcv_CanMsg(eObjY, eCmdSetValue12, *(int *) ((void *) &tStruct.xyOffset_DispenseHead2.Y));
     send_rcv_CanMsg(eObjX, eCmdSetValue13, *(int *) ((void *) &tStruct.xyOffset_DispenseHead3.X));
     send_rcv_CanMsg(eObjY, eCmdSetValue13, *(int *) ((void *) &tStruct.xyOffset_DispenseHead3.Y));
     send_rcv_CanMsg(eObjX, eCmdSetValue14, *(int *) ((void *) &tStruct.xyOffset_DispenseHead4.X));
     send_rcv_CanMsg(eObjY, eCmdSetValue14, *(int *) ((void *) &tStruct.xyOffset_DispenseHead4.Y));
     send_rcv_CanMsg(eObjX, eCmdSetValue15, *(int *) ((void *) &tStruct.xyOffset_Prime1.X));
     send_rcv_CanMsg(eObjY, eCmdSetValue15, *(int *) ((void *) &tStruct.xyOffset_Prime1.Y));
     send_rcv_CanMsg(eObjX, eCmdSetValue16, *(int *) ((void *) &tStruct.xyOffset_Prime2.X));
     send_rcv_CanMsg(eObjY, eCmdSetValue16, *(int *) ((void *) &tStruct.xyOffset_Prime2.Y));


    //START TEST if conversion from float to integer and back is ok
    int iData1X = send_rcv_CanMsg(eObjX, eCmdGetValue1, 0);
    int iData1Y = send_rcv_CanMsg(eObjY, eCmdGetValue1, 0);
    int iData2X = send_rcv_CanMsg(eObjX, eCmdGetValue2, 0);
    int iData2Y = send_rcv_CanMsg(eObjY, eCmdGetValue2, 0);
    int iData3X = send_rcv_CanMsg(eObjX, eCmdGetValue3, 0);
    int iData3Y = send_rcv_CanMsg(eObjY, eCmdGetValue3, 0);
    int iData4X = send_rcv_CanMsg(eObjX, eCmdGetValue4, 0);
    int iData4Y = send_rcv_CanMsg(eObjY, eCmdGetValue4, 0);
    int iData5X = send_rcv_CanMsg(eObjX, eCmdGetValue5, 0);
    int iData5Y = send_rcv_CanMsg(eObjY, eCmdGetValue5, 0);
    int iData6X = send_rcv_CanMsg(eObjX, eCmdGetValue6, 0);
    int iData6Y = send_rcv_CanMsg(eObjY, eCmdGetValue6, 0);
    int iData7X = send_rcv_CanMsg(eObjX, eCmdGetValue7, 0);
    int iData7Y = send_rcv_CanMsg(eObjY, eCmdGetValue7, 0);
    int iData8X = send_rcv_CanMsg(eObjX, eCmdGetValue8, 0);
    int iData8Y = send_rcv_CanMsg(eObjY, eCmdGetValue8, 0);
    int iData9X = send_rcv_CanMsg(eObjX, eCmdGetValue9, 0);
    int iData9Y = send_rcv_CanMsg(eObjY, eCmdGetValue9, 0);
    int iData10X = send_rcv_CanMsg(eObjX, eCmdGetValue10, 0);
    int iData10Y = send_rcv_CanMsg(eObjY, eCmdGetValue10, 0);
    int iData11X = send_rcv_CanMsg(eObjX, eCmdGetValue11, 0);
    int iData11Y = send_rcv_CanMsg(eObjY, eCmdGetValue11, 0);
    int iData12X = send_rcv_CanMsg(eObjX, eCmdGetValue12, 0);
    int iData12Y = send_rcv_CanMsg(eObjY, eCmdGetValue12, 0);
    int iData13X = send_rcv_CanMsg(eObjX, eCmdGetValue13, 0);
    int iData13Y = send_rcv_CanMsg(eObjY, eCmdGetValue13, 0);
    int iData14X = send_rcv_CanMsg(eObjX, eCmdGetValue14, 0);
    int iData14Y = send_rcv_CanMsg(eObjY, eCmdGetValue14, 0);
    int iData15X = send_rcv_CanMsg(eObjX, eCmdGetValue15, 0);
    int iData15Y = send_rcv_CanMsg(eObjY, eCmdGetValue15, 0);
    int iData16X = send_rcv_CanMsg(eObjX, eCmdGetValue16, 0);
    int iData16Y = send_rcv_CanMsg(eObjY, eCmdGetValue16, 0);


    assert (eObjX == (ECanObj) iData1X);      //ECanObj eSensorObj
    assert (eObjY == (ECanObj) iData1Y);      //ECanObj eSensorObj
    assert (tStruct.xyLoadPosition.X == *(float *) ((void *) &iData2X));      //float   (XYPos xyLoadPosition).X
    assert (tStruct.xyLoadPosition.Y == *(float *) ((void *) &iData2Y));      //float   (XYPos xyLoadPosition).Y
    assert (tStruct.xyIncubationPosition.X ==
            *(float *) ((void *) &iData3X));      //float   (XYPos xyIncubationPosition).X
    assert (tStruct.xyIncubationPosition.Y ==
            *(float *) ((void *) &iData3Y));      //float   (XYPos xyIncubationPosition).Y
    assert (tStruct.xyFrontPanelPosition.X ==
            *(float *) ((void *) &iData4X));      //float   (XYPos xyFrontPanelPosition).X
    assert (tStruct.xyFrontPanelPosition.Y ==
            *(float *) ((void *) &iData4Y));      //float   (XYPos xyFrontPanelPosition).Y
    assert (tStruct.xyReadPosition.X == *(float *) ((void *) &iData5X));      //float   (XYPos xyReadPosition).X
    assert (tStruct.xyReadPosition.Y == *(float *) ((void *) &iData5Y));      //float   (XYPos xyReadPosition).Y
    assert (tStruct.xyAspiratePosition.X == *(float *) ((void *) &iData6X));      //float   (XYPos xyAspiratePosition).X
    assert (tStruct.xyAspiratePosition.Y == *(float *) ((void *) &iData6Y));      //float   (XYPos xyAspiratePosition).Y
    assert (tStruct.xyDispensePosition.X == *(float *) ((void *) &iData7X));      //float   (XYPos xyDispensePosition).X
    assert (tStruct.xyDispensePosition.Y == *(float *) ((void *) &iData7Y));      //float   (XYPos xyDispensePosition).Y
    assert (tStruct.xyOffset_Well.X == *(float *) ((void *) &iData8X));      //float   (XYPos xyOffset_Well).X
    assert (tStruct.xyOffset_Well.Y == *(float *) ((void *) &iData8Y));      //float   (XYPos xyOffset_Well).Y
    assert (tStruct.xyOffset_Disposable.X ==
            *(float *) ((void *) &iData9X));      //float   (XYPos xyOffset_Disposable).X
    assert (tStruct.xyOffset_Disposable.Y ==
            *(float *) ((void *) &iData9Y));      //float   (XYPos xyOffset_Disposable).Y
    assert (tStruct.xyOffset_FocusGlass.X ==
            *(float *) ((void *) &iData10X));      //float   (XYPos xyOffset_FocusGlass).X
    assert (tStruct.xyOffset_FocusGlass.Y ==
            *(float *) ((void *) &iData10Y));      //float   (XYPos xyOffset_FocusGlass).Y
    assert (tStruct.xyOffset_DispenseHead1.X ==
            *(float *) ((void *) &iData11X));      //float   (XYPos xyOffset_DispenseHead1).X
    assert (tStruct.xyOffset_DispenseHead1.Y ==
            *(float *) ((void *) &iData11Y));      //float   (XYPos xyOffset_DispenseHead1).Y
    assert (tStruct.xyOffset_DispenseHead2.X ==
            *(float *) ((void *) &iData12X));      //float   (XYPos xyOffset_DispenseHead2).X
    assert (tStruct.xyOffset_DispenseHead2.Y ==
            *(float *) ((void *) &iData12Y));      //float   (XYPos xyOffset_DispenseHead2).Y
    assert (tStruct.xyOffset_DispenseHead3.X ==
            *(float *) ((void *) &iData13X));      //float   (XYPos xyOffset_DispenseHead3).X
    assert (tStruct.xyOffset_DispenseHead3.Y ==
            *(float *) ((void *) &iData13Y));      //float   (XYPos xyOffset_DispenseHead3).Y
    assert (tStruct.xyOffset_DispenseHead4.X ==
            *(float *) ((void *) &iData14X));      //float   (XYPos xyOffset_DispenseHead4).X
    assert (tStruct.xyOffset_DispenseHead4.Y ==
            *(float *) ((void *) &iData14Y));      //float   (XYPos xyOffset_DispenseHead4).Y
    assert (tStruct.xyOffset_Prime1.X == *(float *) ((void *) &iData15X));      //float   (XYPos xyOffset_Prime1).X
    assert (tStruct.xyOffset_Prime1.Y == *(float *) ((void *) &iData15Y));      //float   (XYPos xyOffset_Prime1).Y
    assert (tStruct.xyOffset_Prime2.X == *(float *) ((void *) &iData16X));      //float   (XYPos xyOffset_Prime2).X
    assert (tStruct.xyOffset_Prime2.Y == *(float *) ((void *) &iData16Y));      //float   (XYPos xyOffset_Prime2).Y
    //END TEST

}

Wagon_LogicalXYPos_t CANController::getWagonLogicalPositions(void) {
    Wagon_LogicalXYPos_t tStruct; //a return struct
    ECanObj eObjX = eEncTransXB;  //store logPosData "behind" eEncTransXB-channel (obj eEncTransXA-channel is used for sensor-calibration data)
    ECanObj eObjY = eEncTransYB;  //store logPosData "behind" eEncTransYB-channel (obj eEncTransYA-channel is used for sensor-calibration data)

    int iData1X = send_rcv_CanMsg(eObjX, eCmdGetValue1, 0);                //(int) eSensorObj
    int iData1Y = send_rcv_CanMsg(eObjY, eCmdGetValue1, 0);                //(int) eSensorObj
    int iData2X = send_rcv_CanMsg(eObjX, eCmdGetValue2, 0);                //(int) (XYPos xyLoadPosition).X
    int iData2Y = send_rcv_CanMsg(eObjY, eCmdGetValue2, 0);                //(int) (XYPos xyLoadPosition).Y
    int iData3X = send_rcv_CanMsg(eObjX, eCmdGetValue3, 0);                //(int) (XYPos xyIncubationPosition).X
    int iData3Y = send_rcv_CanMsg(eObjY, eCmdGetValue3, 0);                //(int) (XYPos xyIncubationPosition).Y
    int iData4X = send_rcv_CanMsg(eObjX, eCmdGetValue4, 0);                //(int) (XYPos xyFrontPanelPosition).X
    int iData4Y = send_rcv_CanMsg(eObjY, eCmdGetValue4, 0);                //(int) (XYPos xyFrontPanelPosition).Y
    int iData5X = send_rcv_CanMsg(eObjX, eCmdGetValue5, 0);                //(int) (XYPos xyReadPosition).X
    int iData5Y = send_rcv_CanMsg(eObjY, eCmdGetValue5, 0);                //(int) (XYPos xyReadPosition).Y
    int iData6X = send_rcv_CanMsg(eObjX, eCmdGetValue6, 0);                //(int) (XYPos xyAspiratePosition).X
    int iData6Y = send_rcv_CanMsg(eObjY, eCmdGetValue6, 0);                //(int) (XYPos xyAspiratePosition).Y
    int iData7X = send_rcv_CanMsg(eObjX, eCmdGetValue7, 0);                //(int) (XYPos xyDispensePosition).X
    int iData7Y = send_rcv_CanMsg(eObjY, eCmdGetValue7, 0);                //(int) (XYPos xyDispensePosition).Y
    int iData8X = send_rcv_CanMsg(eObjX, eCmdGetValue8, 0);                //(int) (XYPos xyOffset_Well).X
    int iData8Y = send_rcv_CanMsg(eObjY, eCmdGetValue8, 0);                //(int) (XYPos xyOffset_Well).Y
    int iData9X = send_rcv_CanMsg(eObjX, eCmdGetValue9, 0);                //(int) (XYPos xyOffset_Disposable).X
    int iData9Y = send_rcv_CanMsg(eObjY, eCmdGetValue9, 0);                //(int) (XYPos xyOffset_Disposable).Y
    int iData10X = send_rcv_CanMsg(eObjX, eCmdGetValue10, 0);                //(int) (XYPos xyOffset_FocusGlass).X
    int iData10Y = send_rcv_CanMsg(eObjY, eCmdGetValue10, 0);                //(int) (XYPos xyOffset_FocusGlass).Y
    int iData11X = send_rcv_CanMsg(eObjX, eCmdGetValue11, 0);                //(int) (XYPos xyOffset_DispenseHead1).X
    int iData11Y = send_rcv_CanMsg(eObjY, eCmdGetValue11, 0);                //(int) (XYPos xyOffset_DispenseHead1).Y
    int iData12X = send_rcv_CanMsg(eObjX, eCmdGetValue12, 0);                //(int) (XYPos xyOffset_DispenseHead2).X
    int iData12Y = send_rcv_CanMsg(eObjY, eCmdGetValue12, 0);                //(int) (XYPos xyOffset_DispenseHead2).Y
    int iData13X = send_rcv_CanMsg(eObjX, eCmdGetValue13, 0);                //(int) (XYPos xyOffset_DispenseHead3).X
    int iData13Y = send_rcv_CanMsg(eObjY, eCmdGetValue13, 0);                //(int) (XYPos xyOffset_DispenseHead3).Y
    int iData14X = send_rcv_CanMsg(eObjX, eCmdGetValue14, 0);                //(int) (XYPos xyOffset_DispenseHead4).X
    int iData14Y = send_rcv_CanMsg(eObjY, eCmdGetValue14, 0);                //(int) (XYPos xyOffset_DispenseHead4).Y
    int iData15X = send_rcv_CanMsg(eObjX, eCmdGetValue15, 0);                //(int) (XYPos xyOffset_Prime1).X
    int iData15Y = send_rcv_CanMsg(eObjY, eCmdGetValue15, 0);                //(int) (XYPos xyOffset_Prime1).Y
    int iData16X = send_rcv_CanMsg(eObjX, eCmdGetValue16, 0);                //(int) (XYPos xyOffset_Prime2).X
    int iData16Y = send_rcv_CanMsg(eObjY, eCmdGetValue16, 0);                //(int) (XYPos xyOffset_Prime2).Y


    eObjX = (ECanObj) iData1X;   //ECanObj eSensorObj
    eObjY = (ECanObj) iData1Y;   //ECanObj eSensorObj
    tStruct.xyLoadPosition.X = *(float *) ((void *) &iData2X);   //float   (XYPos xyLoadPosition).X
    tStruct.xyLoadPosition.Y = *(float *) ((void *) &iData2Y);   //float   (XYPos xyLoadPosition).Y
    tStruct.xyIncubationPosition.X = *(float *) ((void *) &iData3X);   //float   (XYPos xyIncubationPosition).X
    tStruct.xyIncubationPosition.Y = *(float *) ((void *) &iData3Y);   //float   (XYPos xyIncubationPosition).Y
    tStruct.xyFrontPanelPosition.X = *(float *) ((void *) &iData4X);   //float   (XYPos xyFrontPanelPosition).X
    tStruct.xyFrontPanelPosition.Y = *(float *) ((void *) &iData4Y);   //float   (XYPos xyFrontPanelPosition).Y
    tStruct.xyReadPosition.X = *(float *) ((void *) &iData5X);   //float   (XYPos xyReadPosition).X
    tStruct.xyReadPosition.Y = *(float *) ((void *) &iData5Y);   //float   (XYPos xyReadPosition).Y
    tStruct.xyAspiratePosition.X = *(float *) ((void *) &iData6X);   //float   (XYPos xyAspiratePosition).X
    tStruct.xyAspiratePosition.Y = *(float *) ((void *) &iData6Y);   //float   (XYPos xyAspiratePosition).Y
    tStruct.xyDispensePosition.X = *(float *) ((void *) &iData7X);   //float   (XYPos xyDispensePosition).X
    tStruct.xyDispensePosition.Y = *(float *) ((void *) &iData7Y);   //float   (XYPos xyDispensePosition).Y
    tStruct.xyOffset_Well.X = *(float *) ((void *) &iData8X);   //float   (XYPos xyOffset_Well).X
    tStruct.xyOffset_Well.Y = *(float *) ((void *) &iData8Y);   //float   (XYPos xyOffset_Well).Y
    tStruct.xyOffset_Disposable.X = *(float *) ((void *) &iData9X);   //float   (XYPos xyOffset_Disposable).X
    tStruct.xyOffset_Disposable.Y = *(float *) ((void *) &iData9Y);   //float   (XYPos xyOffset_Disposable).Y
    tStruct.xyOffset_FocusGlass.X = *(float *) ((void *) &iData10X);   //float   (XYPos xyOffset_FocusGlass).X
    tStruct.xyOffset_FocusGlass.Y = *(float *) ((void *) &iData10Y);   //float   (XYPos xyOffset_FocusGlass).Y
    tStruct.xyOffset_DispenseHead1.X = *(float *) ((void *) &iData11X);   //float   (XYPos xyOffset_DispenseHead1).X
    tStruct.xyOffset_DispenseHead1.Y = *(float *) ((void *) &iData11Y);   //float   (XYPos xyOffset_DispenseHead1).Y
    tStruct.xyOffset_DispenseHead2.X = *(float *) ((void *) &iData12X);   //float   (XYPos xyOffset_DispenseHead2).X
    tStruct.xyOffset_DispenseHead2.Y = *(float *) ((void *) &iData12Y);   //float   (XYPos xyOffset_DispenseHead2).Y
    tStruct.xyOffset_DispenseHead3.X = *(float *) ((void *) &iData13X);   //float   (XYPos xyOffset_DispenseHead3).X
    tStruct.xyOffset_DispenseHead3.Y = *(float *) ((void *) &iData13Y);   //float   (XYPos xyOffset_DispenseHead3).Y
    tStruct.xyOffset_DispenseHead4.X = *(float *) ((void *) &iData14X);   //float   (XYPos xyOffset_DispenseHead4).X
    tStruct.xyOffset_DispenseHead4.Y = *(float *) ((void *) &iData14Y);   //float   (XYPos xyOffset_DispenseHead4).Y
    tStruct.xyOffset_Prime1.X = *(float *) ((void *) &iData15X);   //float   (XYPos xyOffset_Prime1).X
    tStruct.xyOffset_Prime1.Y = *(float *) ((void *) &iData15Y);   //float   (XYPos xyOffset_Prime1).Y
    tStruct.xyOffset_Prime2.X = *(float *) ((void *) &iData16X);   //float   (XYPos xyOffset_Prime2).X
    tStruct.xyOffset_Prime2.Y = *(float *) ((void *) &iData16Y);   //float   (XYPos xyOffset_Prime2).Y

    return tStruct;
}

//--------------------------
void CANController::setLEDIntensity(LEDIntensity_t tStruct) {
    send_rcv_CanMsg(eLEDUnit1, eCmdSetValue1, (int) eLEDUnit1);  //ECanObj  eSensorObj
    send_rcv_CanMsg(eLEDUnit1, eCmdSetValue2, (int) tStruct.uIntensity_LED1);  //unsigned uIntensity_LED1

    send_rcv_CanMsg(eLEDUnit2, eCmdSetValue1, (int) eLEDUnit2);  //ECanObj  eSensorObj
    send_rcv_CanMsg(eLEDUnit2, eCmdSetValue2, (int) tStruct.uIntensity_LED2);  //unsigned uIntensity_LED2

    send_rcv_CanMsg(eLEDUnit3, eCmdSetValue1, (int) eLEDUnit3);  //ECanObj  eSensorObj
    send_rcv_CanMsg(eLEDUnit3, eCmdSetValue2, (int) tStruct.uIntensity_LED3);  //unsigned uIntensity_LED3

    //START TEST if conversion from float to integer and back is ok
    int iData1 = send_rcv_CanMsg(eLEDUnit1, eCmdGetValue1, 0);                  //(int)   eSensorObj
    int iData2 = send_rcv_CanMsg(eLEDUnit1, eCmdGetValue2, 0);                  //(int)   uIntensity_LED1;
    int iData3 = send_rcv_CanMsg(eLEDUnit2, eCmdGetValue1, 0);                  //(int)   eSensorObj
    int iData4 = send_rcv_CanMsg(eLEDUnit2, eCmdGetValue2, 0);                  //(int)   uIntensity_LED2;
    int iData5 = send_rcv_CanMsg(eLEDUnit3, eCmdGetValue1, 0);                  //(int)   eSensorObj
    int iData6 = send_rcv_CanMsg(eLEDUnit3, eCmdGetValue2, 0);                  //(int)   uIntensity_LED3;

    assert (eLEDUnit1 == (ECanObj) iData1);              //ECanObj  eSensorObj
    assert (tStruct.uIntensity_LED1 == (unsigned) iData2);              //unsigned uIntensity_LED1
    assert (eLEDUnit2 == (ECanObj) iData3);              //ECanObj  eSensorObj
    assert (tStruct.uIntensity_LED2 == (unsigned) iData4);              //unsigned uIntensity_LED2
    assert (eLEDUnit3 == (ECanObj) iData5);              //ECanObj  eSensorObj
    assert (tStruct.uIntensity_LED3 == (unsigned) iData6);              //unsigned uIntensity_LED3
    //END TEST
}

LEDIntensity_t CANController::getLEDIntensity(void) {
    LEDIntensity_t tStruct;

//int iData1 = send_rcv_CanMsg(eLEDUnit1, eCmdGetValue1, 0);                  //(int)   eSensorObj
    int iData2 = send_rcv_CanMsg(eLEDUnit1, eCmdGetValue2, 0);                  //(int)   uIntensity_LED1;
//int iData3 = send_rcv_CanMsg(eLEDUnit2, eCmdGetValue1, 0);                  //(int)   eSensorObj
    int iData4 = send_rcv_CanMsg(eLEDUnit2, eCmdGetValue2, 0);                  //(int)   uIntensity_LED2;
//int iData5 = send_rcv_CanMsg(eLEDUnit3, eCmdGetValue1, 0);                  //(int)   eSensorObj
    int iData6 = send_rcv_CanMsg(eLEDUnit3, eCmdGetValue2, 0);                  //(int)   uIntensity_LED3;

//eLEDUnit1               =(ECanObj)  iData1;                                 //ECanObj  eSensorObj
    tStruct.uIntensity_LED1 = (unsigned) iData2;                                 //unsigned uIntensity_LED1
//eLEDUnit2               =(ECanObj)  iData3;                                 //ECanObj  eSensorObj
    tStruct.uIntensity_LED2 = (unsigned) iData4;                                 //unsigned uIntensity_LED2
//eLEDUnit3               =(ECanObj)  iData5;                                 //ECanObj  eSensorObj
    tStruct.uIntensity_LED3 = (unsigned) iData6;                                 //unsigned uIntensity_LED3

    return tStruct;
}

//------------------------------------------------------------
void CANController::setBoardData(int iPS12_nr)   //yymmxx
{
    //send serial number on all three boards
    send_rcv_CanMsg(eBoard1Ctrl, eCmdSetValue2, iPS12_nr);
    send_rcv_CanMsg(eBoard2Ctrl, eCmdSetValue2, iPS12_nr);
    send_rcv_CanMsg(eBoard3Ctrl, eCmdSetValue2, iPS12_nr);

    int iData1 = send_rcv_CanMsg(eBoard1Ctrl, eCmdGetValue1, 0);                  //(int)   eBoardCtrl
    int iData2 = send_rcv_CanMsg(eBoard1Ctrl, eCmdGetValue2, 0);                  //(int)   iSerialNumber;
    int iData3 = send_rcv_CanMsg(eBoard2Ctrl, eCmdGetValue1, 0);                  //(int)   eBoardCtrl
    int iData4 = send_rcv_CanMsg(eBoard2Ctrl, eCmdGetValue2, 0);                  //(int)   iSerialNumber;
    int iData5 = send_rcv_CanMsg(eBoard3Ctrl, eCmdGetValue1, 0);                  //(int)   eBoardCtrl
    int iData6 = send_rcv_CanMsg(eBoard3Ctrl, eCmdGetValue2, 0);                  //(int)   iSerialNumber;

    assert (eBoard1Ctrl == iData1);
    assert (iPS12_nr == iData2);
    assert (eBoard2Ctrl == iData3);
    assert (iPS12_nr == iData4);
    assert (eBoard3Ctrl == iData5);
    assert (iPS12_nr == iData6);
}

PS12_Version_t CANController::getBoardData(void) {
    //"PS12_NR_050501, SYCB_NR_FFFFFFFFFFF0, SYCB_FW_002, SYCB_ESW_003, SYCB_FLASH_20050523,
    // PS12_NR_050501, ICCB_NR_FFFFFFFFFFF1, ICCB_FW_002, ICCB_ESW_003, ICCB_FLASH_20050523,
    // PS12_NR_050501, WSCB_NR_FFFFFFFFFFF2, WSCB_FW_002, WSCB_ESW_003, WSCB_FLASH_20050523";

    PS12_Version_t tStruct;

    assert(eBoard1Ctrl == send_rcv_CanMsg(eBoard1Ctrl, eCmdGetValue1, 0));
    tStruct.tSYCBInfo.iPS12_nr = send_rcv_CanMsg(eBoard1Ctrl, eCmdGetValue2, 0);
    tStruct.tSYCBInfo.rFlashDate = send_rcv_CanMsg(eBoard1Ctrl, eCmdGetValue3,
                                                   0);  // integer part is sufficient to display the date;
    tStruct.tSYCBInfo.iBrd_nr_high = send_rcv_CanMsg(eBoard1Ctrl, eCmdGetValue4, 0);
    tStruct.tSYCBInfo.iBrd_nr_medium = send_rcv_CanMsg(eBoard1Ctrl, eCmdGetValue5, 0);
    tStruct.tSYCBInfo.iBrd_nr_low = send_rcv_CanMsg(eBoard1Ctrl, eCmdGetValue6, 0);
    tStruct.tSYCBInfo.iFW_version = send_rcv_CanMsg(eBoard1Ctrl, eCmdGetValue7, 0);
    tStruct.tSYCBInfo.iESW_version = send_rcv_CanMsg(eBoard1Ctrl, eCmdGetValue8, 0);

    assert(eBoard2Ctrl == send_rcv_CanMsg(eBoard2Ctrl, eCmdGetValue1, 0));
    tStruct.tICCBInfo.iPS12_nr = send_rcv_CanMsg(eBoard2Ctrl, eCmdGetValue2, 0);
    tStruct.tICCBInfo.rFlashDate = send_rcv_CanMsg(eBoard2Ctrl, eCmdGetValue3,
                                                   0);   // integer part is sufficient to display the date;
    tStruct.tICCBInfo.iBrd_nr_high = send_rcv_CanMsg(eBoard2Ctrl, eCmdGetValue4, 0);
    tStruct.tICCBInfo.iBrd_nr_medium = send_rcv_CanMsg(eBoard2Ctrl, eCmdGetValue5, 0);
    tStruct.tICCBInfo.iBrd_nr_low = send_rcv_CanMsg(eBoard2Ctrl, eCmdGetValue6, 0);
    tStruct.tICCBInfo.iFW_version = send_rcv_CanMsg(eBoard2Ctrl, eCmdGetValue7, 0);
    tStruct.tICCBInfo.iESW_version = send_rcv_CanMsg(eBoard2Ctrl, eCmdGetValue8, 0);

    assert(eBoard3Ctrl == send_rcv_CanMsg(eBoard3Ctrl, eCmdGetValue1, 0));
    tStruct.tWSCBInfo.iPS12_nr = send_rcv_CanMsg(eBoard3Ctrl, eCmdGetValue2, 0);
    tStruct.tWSCBInfo.rFlashDate = send_rcv_CanMsg(eBoard3Ctrl, eCmdGetValue3,
                                                   0);    // integer part is sufficient to display the date;
    tStruct.tWSCBInfo.iBrd_nr_high = send_rcv_CanMsg(eBoard3Ctrl, eCmdGetValue4, 0);
    tStruct.tWSCBInfo.iBrd_nr_medium = send_rcv_CanMsg(eBoard3Ctrl, eCmdGetValue5, 0);
    tStruct.tWSCBInfo.iBrd_nr_low = send_rcv_CanMsg(eBoard3Ctrl, eCmdGetValue6, 0);
    tStruct.tWSCBInfo.iFW_version = send_rcv_CanMsg(eBoard3Ctrl, eCmdGetValue7, 0);
    tStruct.tWSCBInfo.iESW_version = send_rcv_CanMsg(eBoard3Ctrl, eCmdGetValue8, 0);


    return tStruct;
}


//--------------
//BOARD COMMANDS (ECanObj = eBoard1Ctrl, eBoard2Ctrl or eBoard3Ctrl )
void CANController::setBoardStatus(ECanObj eObj, bool fEnabled) {
    assert (eObj == eBoard1Ctrl ||
            eObj == eBoard2Ctrl ||
            eObj == eBoard3Ctrl);

    send_rcv_CanMsg(eObj, eCmdSetBoardStatus, fEnabled);
}

bool CANController::getBoardStatus(ECanObj eObj) {
    assert (eObj == eBoard1Ctrl ||
            eObj == eBoard2Ctrl ||
            eObj == eBoard3Ctrl);

    bool fEnabled = send_rcv_CanMsg(eObj, eCmdGetBoardStatus, 0);
    return fEnabled;
}

void CANController::flashBoard(ECanObj eObj) {
    assert (eObj == eBoard1Ctrl ||
            eObj == eBoard2Ctrl ||
            eObj == eBoard3Ctrl);

    //(int) Now() is the date part of TDateTime
    //the NIOS boards will store the date-of-flash
//    send_rcv_CanMsg(eObj, eCmdFlashBoard, (int) Now());
//It gives the number of days that have passed since 12/30/1899.

    using namespace boost::gregorian;

    std::string s("1899-12-30");
    date origin(from_simple_string(s));
    date today = day_clock::local_day();
    days nDays = today - origin;

    send_rcv_CanMsg(eObj, eCmdFlashBoard, (int)nDays.days());

}


