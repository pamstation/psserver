//---------------------------------------------------------------------------

#pragma hdrstop

#include "StubPamStationFactory.h"  //for header-file
#include "StubPamStation.h"         //for getting PamStation
#include "../pimcore/PamStationException.h" //for using object
#include "../propertylayer/PropLeds.h"
#include <stdlib.h>
//---------------------------------------------------------------------------

#pragma package(smart_init)

StubPamStation* StubPamStationFactory::pCurrent = nullptr;

StubPamStation* StubPamStationFactory::getCurrentPamStation()
{
    if (!pCurrent)
    {


        pCurrent = new  StubPamStation();
        assert(pCurrent != nullptr);


        try
        {
            pCurrent->initializeCanInterface();
            //ALEX test if msg can send to the can 
            PropLeds * pLeds = pCurrent->getLeds();
            pLeds->SetRedLed(false);
        }
        catch (PamStationException &e)
        {
            delete pCurrent;
            pCurrent = nullptr;
            throw;
        }
    }
    return pCurrent;
}

void StubPamStationFactory::shutdown()
{
    assert(pCurrent != nullptr);
    delete pCurrent;
    pCurrent = nullptr;
}
//------------------------------------------------------------------------
//Implementation of interface::getCurrentPamStation
//------------------------------------------------------------------------
PamStation * PamStationFactory::getCurrentPamStation()
{
    return StubPamStationFactory::getCurrentPamStation();
}

void PamStationFactory::shutdown()
{
    StubPamStationFactory::shutdown();
}
