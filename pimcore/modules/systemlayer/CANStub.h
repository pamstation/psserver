//---------------------------------------------------------------------------

#ifndef CANStubH
#define CANStubH

#include "../pimcore/PamDefs.h"             //for enums etc
//#include "../drivers/cpcwin/cpc.h"                  //for CANmsg declaration
#include "cpc.h"
//---------------------------------------------------------------------------
class CANStub
{
  public:
     CANStub();
    ~CANStub();

     void Send_ReceiveCanMessage(CPC_CAN_MSG_T* pSend_CpcMsg, CPC_CAN_MSG_T* pReply_CpcMsg);

  private:
      //struct to keep the simulated values
      typedef struct {
      int iCtrlTarget;
      int iCtrlActual;
      int iCtrlStatus;
      int iCtrlError;

      int iBoardEnabled;

      int iValue1 ;
      int iValue2 ;
      int iValue3 ;
      int iValue4 ;
      int iValue5 ;
      int iValue6 ;
      int iValue7 ;
      int iValue8 ;
      int iValue9 ;
      int iValue10;
      int iValue11;
      int iValue12;
      int iValue13;
      int iValue14;
      int iValue15;
      int iValue16;
      int iValue17;
      int iValue18;
      int iValue19;
      int iValue20;
      int iValue21;
      int iValue22;
      int iValue23;
      int iValue24;
      int iValue25;
      int iValue26;
      int iValue27;
      int iValue28;
      int iValue29;
      int iValue30;
      int iValue31;
      int iValue32;
      int iValue33;
      int iValue34;
      int iValue35;
      int iValue36;
      int iValue37;
      int iValue38;
      int iValue39;
      int iValue40;
      int iValue41;
      int iValue42;
      int iValue43;
      int iValue44;
      int iValue45;
      int iValue46;
      int iValue47;
      int iValue48;
      int iValue49;
      int iValue50;
      }Ctrl_Sim_t;

      Ctrl_Sim_t m_tCANData[eCanObjMax];  //enough space for each object number

      bool m_fInitialised;
      void initialise(void);            //set all necessary values in m_tCANData;
      void initialise_InstrumentStatus  (void);
      void initialise_SensorData        (void);
      void initialise_ControllerSettings(void);
      void initialise_FilterLogicalPos  (void);
      void initialise_FocusLogicalPos   (void);
      void initialise_WagonLogicalPos   (void);
      void initialise_LEDIntensity      (void);
      void initialise_VersionInfo       (void);


};




#endif
