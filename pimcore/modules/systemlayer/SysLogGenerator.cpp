//---------------------------------------------------------------------------
#include <fstream>   //for  ofstream

#pragma hdrstop

#include <cassert>

#include "SysLogGenerator.h"  //for header-file
#include "../pimcore/LogHandler.h"           //for listener

//---------------------------------------------------------------------------

#pragma package(smart_init)

int SysLogGenerator::iInstanceCounter=0;

SysLogGenerator::SysLogGenerator()
{
    ++iInstanceCounter;
  assert (iInstanceCounter == 1);  //only one instance of this class allowed (singleton)
}
SysLogGenerator::~SysLogGenerator()
{
    --iInstanceCounter;
  assert (iInstanceCounter == 0);  //only one instance of this class allowed (singleton)
}
//---------------------------------------------------
void SysLogGenerator::addLogHandler(LogHandler * h)
{
    handler = h;
}
void SysLogGenerator::removeLogHandler(LogHandler * h)
{
    handler = nullptr;
}

void SysLogGenerator::logInfo(std::string aMsg){
   if (handler) handler->logInfo(aMsg);
}

void SysLogGenerator::logError(std::string aMsg){
   if (handler) handler->logError(aMsg);
}

//----------------------------------------------------------------------
void SysLogGenerator::OnLog(const std::string& strWhen, const std::string& strWho, const std::string& strWhat)
{

  //TEST VERSION
//  std::ofstream logStream;
//
//  std::string logfile = "Log_" + FormatDateTime( "yyyymmdd", Now() )+ ".txt";   //create file name
//  std::string logLine = strWhen+"  "+strWho+"  "+strWhat+"\n";                  //create log-entry
//
//
//  logStream.open(logfile.c_str(), std::ofstream::out|std::ofstream::app);
//  logStream <<   logLine.c_str();
}
