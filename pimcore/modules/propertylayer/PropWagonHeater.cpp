//---------------------------------------------------------------------------


#pragma hdrstop

#include <cassert>
#include "PropWagonHeater.h"     //for header-file
#include "../pimcore/WagonHeaterListener.h"       //for listener

#include "../systemlayer/SysExceptionGenerator.h" //for passing Exception to ExceptionGenerator

#include "../pimcore/PamStationFactory.h"         //for obtaining and using IncChmbrHtr's
#include "../pimcore/PamStation.h"                //for obtaining and using IncChmbrHtr's
#include "../pimcore/IncubationChamberHeater.h"   //for obtaining and using IncChmbrHtr's
#include "../pimcore/DefaultGenerator.h"          //for obtaining Defaults

#include "../pimcore/PamDefs_InitialValues.h"     //for initial values of initalise_protocolSettings


#include <values.h>                                    //for MINFLOAT etc.
//#include <Math.hpp>                                    //for Sign
#include <math.h>                                      //for sqrt

//---------------------------------------------------------------------------

#pragma package(smart_init)


int PropWagonHeater::iInstanceCounter = 0;


PropWagonHeater::PropWagonHeater() {
    initWagonHeaterMemberValues();       //init member variables
    ++iInstanceCounter;
    assert (iInstanceCounter == 1);     //only one instance of this class allowed (singleton)
}

PropWagonHeater::~PropWagonHeater() {

    terminateWagonHeaterMemberValues();  //reset member variables
    --iInstanceCounter;
    assert (iInstanceCounter == 0);     //only one instance of this class allowed (singleton)
}

void PropWagonHeater::initWagonHeaterMemberValues() {
    m_pPam = PamStationFactory::getCurrentPamStation();

    m_rBottomOffset = BOTTOM_OFFSET; //�C
    m_rCoverOffset = COVER_OFFSET;  //�C


}

void PropWagonHeater::terminateWagonHeaterMemberValues(void) {
    m_pPam = nullptr; //actual destruction is done in PamStation & PamStationFactory
}

//--------------------------
void PropWagonHeater::addWagonHeaterListener(WagonHeaterListener *h) {
    addListenerToList(h);
}

void PropWagonHeater::removeWagonHeaterListener(WagonHeaterListener *h) {
    removeListenerFromList(h);
}
//--------------------------

// Check on safe temperature is only implemented in wagonHeater
void PropWagonHeater::checkUnloadTemperatureSafe(void) {
    //get default setting SafeUnloadTemperature
    float rSafeUnloadTemperature = m_pDefaultGenerator->getLoad_Unload_Settings()->rSafeUnloadTemperature;

    if (getActualValue() > rSafeUnloadTemperature) {
        m_pExceptionGenerator->onException(this, WAGONHEATER_UNLOAD_TEMPERATURE_NOT_SAFE, eErrorSafety,
                                           "WAGONHEATER_UNLOAD_TEMPERATURE_NOT_SAFE");
    }
}

//---------------------------------------------------------
//OVERRIDES FROM CONTROLLABLE AND HEATABLE
//SETTERS
//Setter, overrides Controllable method:
void PropWagonHeater::setTargetValue(float rTarget) {
    //check range (for now his way:)
    if (rTarget < getMinValue() - 0.001 ||
        rTarget > getMaxValue() + 0.001) {
        std::string strDesc =
                "WagonHeater Range Error, TargetValue: " + std::to_string(rTarget) +
                ", MinValue: " + std::to_string(getMinValue()) +
                ", MaxValue: " + std::to_string(getMaxValue());;
        m_pExceptionGenerator->onException(this, CONTROLLABLE_RANGE_ERROR, eErrorRange, strDesc);
    }

    //pass these setpoints
    (m_pPam->getIncubationChamberHeater(eIncChmbrHtrCoverLeft))->setTargetValue(rTarget + m_rCoverOffset);
    (m_pPam->getIncubationChamberHeater(eIncChmbrHtrCoverMiddle))->setTargetValue(rTarget + m_rCoverOffset);
    (m_pPam->getIncubationChamberHeater(eIncChmbrHtrCoverRight))->setTargetValue(rTarget + m_rCoverOffset);

    (m_pPam->getIncubationChamberHeater(eIncChmbrHtrBottomLeft))->setTargetValue(rTarget + m_rBottomOffset);
    (m_pPam->getIncubationChamberHeater(eIncChmbrHtrBottomMiddle))->setTargetValue(rTarget + m_rBottomOffset);
    (m_pPam->getIncubationChamberHeater(eIncChmbrHtrBottomRight))->setTargetValue(rTarget + m_rBottomOffset);

}

//Setter, overrides Controllable method:
void PropWagonHeater::setEnabled(bool fTrueFalse) {
    //pass boolean to all
    for (int iHtr = eIncChmbrHtrMin + 1; iHtr < eIncChmbrHtrMax; iHtr++) {
        (m_pPam->getIncubationChamberHeater((EIncubationChamberHeater) iHtr))->setEnabled(fTrueFalse);
    }
}

//Setter, overrides Heatable method:
void PropWagonHeater::setTemperatureGain(float rTemperatureGain) {
    //pass same value to all heaters
    for (int iHtr = eIncChmbrHtrMin + 1; iHtr < eIncChmbrHtrMax; iHtr++) {
        (m_pPam->getIncubationChamberHeater((EIncubationChamberHeater) iHtr))->setTemperatureGain(rTemperatureGain);
    }

    //update property with TemperatureGain from IncChmbrHeaters
    setTemperatureGainProperty(getTemperatureGain());
}

//GETTERS
//Getter, overrides Controllable method:
float PropWagonHeater::getTargetValue() {
    //calculate the disposable setpoint temperature using the middle bottom and cover temperature

    //1 get target temperatures of middle heaters
    double rBottomTemp = (m_pPam->getIncubationChamberHeater(eIncChmbrHtrBottomMiddle))->getTargetValue();
    double rCoverTemp = (m_pPam->getIncubationChamberHeater(eIncChmbrHtrCoverMiddle))->getTargetValue();


    rBottomTemp = rBottomTemp - m_rBottomOffset;  //correct for offset
    rCoverTemp = rCoverTemp - m_rCoverOffset;   //correct for offset

    return (rBottomTemp + rCoverTemp) / 2.0;  //for now get average of both middle heater values

}

//Getter, overrides Controllable method:
float PropWagonHeater::getActualValue() {
    //calculate the disposable actual temperature using the middle bottom and cover temperature

    //1 get actual temperatures of middle heaters
    double rBottomTemp = (m_pPam->getIncubationChamberHeater(eIncChmbrHtrBottomMiddle))->getActualValue();
    double rCoverTemp = (m_pPam->getIncubationChamberHeater(eIncChmbrHtrCoverMiddle))->getActualValue();

    rBottomTemp = rBottomTemp - m_rBottomOffset;  //correct for offset
    rCoverTemp = rCoverTemp - m_rCoverOffset;   //correct for offset


    return (rBottomTemp + rCoverTemp) / 2.0;  //for now get average of both middle heater values

}

//Getter, overrides Controllable method:
bool PropWagonHeater::getEnabled() {
    bool fEnabled = true;

    //enabled if all enabled
    for (int iHtr = eIncChmbrHtrMin + 1; iHtr < eIncChmbrHtrMax; iHtr++) {
        bool fAnswer = (m_pPam->getIncubationChamberHeater((EIncubationChamberHeater) iHtr))->getEnabled();
        if (fAnswer == false) fEnabled = false;
    }

    return fEnabled;
}

//Getter, overrides Controllable method:
bool PropWagonHeater::getReady() {
    bool fReady = true;

    //ready if all ready
    for (int iHtr = eIncChmbrHtrMin + 1; iHtr < eIncChmbrHtrMax; iHtr++) {
        bool fAnswer = (m_pPam->getIncubationChamberHeater((EIncubationChamberHeater) iHtr))->getReady();
        if (fAnswer == false) fReady = false;
    }

    return fReady;
}

//Getter, overrides Heatable method:
float PropWagonHeater::getTemperatureGain() {
    float rBottom = (m_pPam->getIncubationChamberHeater(eIncChmbrHtrBottomMiddle))->getTemperatureGain();
    float rCover = (m_pPam->getIncubationChamberHeater(eIncChmbrHtrCoverMiddle))->getTemperatureGain();

    //for now get average of both middle heater values
    return (rBottom + rCover) / 2;

}

//WAITREADY, overrides Controllable method:
void PropWagonHeater::waitReady() {
    //unblocks if all waitEvents are unblocking
    for (int iHtr = eIncChmbrHtrMin + 1; iHtr < eIncChmbrHtrMax; iHtr++) {
        (m_pPam->getIncubationChamberHeater((EIncubationChamberHeater) iHtr))->waitReady();
    }
}

//READers of default user defined values
//Default value reader, overrides Controllable Method:
float PropWagonHeater::getMinValue() {

    float rMinValue;
    float rHeatMin = 0.0;

    //get maximal MinValue for bottom heaters, correct for  m_rBottomOffset
    rMinValue = ((m_pPam->getIncubationChamberHeater(eIncChmbrHtrBottomLeft))->getMinValue()) - m_rBottomOffset;
    if (rMinValue > rHeatMin) rHeatMin = rMinValue;

    rMinValue = ((m_pPam->getIncubationChamberHeater(eIncChmbrHtrBottomMiddle))->getMinValue()) - m_rBottomOffset;
    if (rMinValue > rHeatMin) rHeatMin = rMinValue;

    rMinValue = ((m_pPam->getIncubationChamberHeater(eIncChmbrHtrBottomRight))->getMinValue()) - m_rBottomOffset;
    if (rMinValue > rHeatMin) rHeatMin = rMinValue;

    //get maximal MinValue for cover heaters, correct for  m_rCoverOffset
    rMinValue = ((m_pPam->getIncubationChamberHeater(eIncChmbrHtrCoverLeft))->getMinValue()) - m_rCoverOffset;
    if (rMinValue > rHeatMin) rHeatMin = rMinValue;

    rMinValue = ((m_pPam->getIncubationChamberHeater(eIncChmbrHtrCoverMiddle))->getMinValue()) - m_rCoverOffset;
    if (rMinValue > rHeatMin) rHeatMin = rMinValue;

    rMinValue = ((m_pPam->getIncubationChamberHeater(eIncChmbrHtrCoverRight))->getMinValue()) - m_rCoverOffset;
    if (rMinValue > rHeatMin) rHeatMin = rMinValue;

    return rHeatMin;

}

//Default value reader, overrides Controllable Method:
float PropWagonHeater::getMaxValue() {
    float rMaxValue;
    float rHeatMax = 1000.0;

    //get minimal MaxValue for bottom heaters, correct for  m_rBottomOffset
    rMaxValue = ((m_pPam->getIncubationChamberHeater(eIncChmbrHtrBottomLeft))->getMaxValue()) - m_rBottomOffset;
    if (rMaxValue < rHeatMax) rHeatMax = rMaxValue;

    rMaxValue = ((m_pPam->getIncubationChamberHeater(eIncChmbrHtrBottomMiddle))->getMaxValue()) - m_rBottomOffset;
    if (rMaxValue < rHeatMax) rHeatMax = rMaxValue;

    rMaxValue = ((m_pPam->getIncubationChamberHeater(eIncChmbrHtrBottomRight))->getMaxValue()) - m_rBottomOffset;
    if (rMaxValue < rHeatMax) rHeatMax = rMaxValue;


    //get minimal MaxValue for cover heaters, correct for  m_rCoverOffset
    rMaxValue = ((m_pPam->getIncubationChamberHeater(eIncChmbrHtrCoverLeft))->getMaxValue()) - m_rCoverOffset;
    if (rMaxValue < rHeatMax) rHeatMax = rMaxValue;

    rMaxValue = ((m_pPam->getIncubationChamberHeater(eIncChmbrHtrCoverMiddle))->getMaxValue()) - m_rCoverOffset;
    if (rMaxValue < rHeatMax) rHeatMax = rMaxValue;

    rMaxValue = ((m_pPam->getIncubationChamberHeater(eIncChmbrHtrCoverRight))->getMaxValue()) - m_rCoverOffset;
    if (rMaxValue < rHeatMax) rHeatMax = rMaxValue;

    return rHeatMax;

}

bool PropWagonHeater::getErrorStatus() {
    bool fError = false;

    //in error if at least one in error
    for (int iHtr = eIncChmbrHtrMin + 1; iHtr < eIncChmbrHtrMax; iHtr++) {
        bool fAnswer = (m_pPam->getIncubationChamberHeater((EIncubationChamberHeater) iHtr))->getErrorStatus();
        if (fAnswer == true) fError = true;
    }

    return fError;
}

int PropWagonHeater::getErrorNumber() {
    int iErrorNumber = 0;

    //for now, return error-code of highest heater number
    // each heater can be adressed seperately to gets its own errornumber
    for (int iHtr = eIncChmbrHtrMin + 1; iHtr < eIncChmbrHtrMax; iHtr++) {
        int iAnswer = (m_pPam->getIncubationChamberHeater((EIncubationChamberHeater) iHtr))->getErrorNumber();
        if (iAnswer != 0) iErrorNumber = iAnswer;
    }

    return iErrorNumber;
}


//Default value reader, overrides Heatable Method:
//////////////////////
//START GAIN SECTION//
//////////////////////
float PropWagonHeater::getMinTemperatureGain() {
    float rMinGain = MINFLOAT;

    //get from all heaters the HIGHEST minimum temperature gain
    for (int iHtr = eIncChmbrHtrMin + 1; iHtr < eIncChmbrHtrMax; iHtr++) {
        float rValue = (m_pPam->getIncubationChamberHeater((EIncubationChamberHeater) iHtr))->getMinTemperatureGain();
        if (rValue > rMinGain) rMinGain = rValue;
    }

    return rMinGain;
}

//Default value reader, overrides Heatable Method:
float PropWagonHeater::getMaxTemperatureGain() {
    float rMaxGain = MAXFLOAT;

    //get from all heaters the LOWEST maximum temperature gain
    for (int iHtr = eIncChmbrHtrMin + 1; iHtr < eIncChmbrHtrMax; iHtr++) {
        float rValue = (m_pPam->getIncubationChamberHeater((EIncubationChamberHeater) iHtr))->getMaxTemperatureGain();
        if (rValue < rMaxGain) rMaxGain = rValue;
    }

    return rMaxGain;
}
//////////////////////
//END GAIN SECTION  //
//////////////////////



//This method is called by the StubPamStations updateProperties-loop
void PropWagonHeater::updateProperties(void) {
    setTargetProperty(getTargetValue());  // in [SI]
    setActualProperty(getActualValue());  // in [SI]
    setReadyProperty(getReady());

    //- JPau added 20090403 for aborting the heating

//setTemperatureGainProperty( getTemperatureGain() );  //TempGain property updated once in the setter
}

//The next sections is implemented for the Controllable & Heatable listener functionality
//-------------------------
void PropWagonHeater::setReadyProperty(bool fTrueFalse) {
    if (m_fReady != fTrueFalse)              //on changes only
    {
        m_fReady = fTrueFalse;               //set property
        onReadyChanged();                    //notify listener
    }
}

void PropWagonHeater::setTargetProperty(float rTargetValue) {
    if (m_rTargetProperty != rTargetValue)   //on changes only
    {
        m_rTargetProperty = rTargetValue;    //set property
        onTargetValueChanged();              //notify listener
    }
}

void PropWagonHeater::setActualProperty(float rActualValue) {
    if (m_rActualProperty != rActualValue)   //on changes only
    {
        m_rActualProperty = rActualValue;    //set property
        onActualValueChanged();              //notify listener
    }
}

void PropWagonHeater::setTemperatureGainProperty(float rTemperatureGain) {
    if (m_rGainProperty != rTemperatureGain) //on changes only
    {
        m_rGainProperty = rTemperatureGain;  //set property
        onTemperatureGainChanged();          //notify listener
    }
}

//----
void PropWagonHeater::onReadyChanged() {

    std::lock_guard<std::recursive_mutex> guard(m_pcsListenerGuard);

    for (int index = 0; index < m_pListOfListeners.size(); index++) {
        EventHandler *pBase = (EventHandler *) m_pListOfListeners[index];
        ControllableListener *pCtrl = dynamic_cast<ControllableListener *>(pBase);
        pCtrl->onReadyChanged(this);
    }

}

void PropWagonHeater::onTargetValueChanged() {

    std::lock_guard<std::recursive_mutex> guard(m_pcsListenerGuard);

    for (int index = 0; index < m_pListOfListeners.size(); index++) {
        EventHandler *pBase = (EventHandler *) m_pListOfListeners[index];
        ControllableListener *pCtrl = dynamic_cast<ControllableListener *>(pBase);
        pCtrl->onTargetValueChanged(this);
    }

}

void PropWagonHeater::onActualValueChanged() {

    std::lock_guard<std::recursive_mutex> guard(m_pcsListenerGuard);

    for (int index = 0; index < m_pListOfListeners.size(); index++) {
        EventHandler *pBase = (EventHandler *) m_pListOfListeners[index];
        ControllableListener *pCtrl = dynamic_cast<ControllableListener *>(pBase);
        pCtrl->onActualValueChanged(this);
    }

}

void PropWagonHeater::onTemperatureGainChanged() {

    std::lock_guard<std::recursive_mutex> guard(m_pcsListenerGuard);

    for (int index = 0; index < m_pListOfListeners.size(); index++) {
        EventHandler *pBase = (EventHandler *) m_pListOfListeners[index];
        HeatableListener *pCtrl = dynamic_cast<HeatableListener *>(pBase);
        pCtrl->onTemperatureGainChanged(this);
    }

}



