//---------------------------------------------------------------------------


#pragma hdrstop

#include <cassert>

#include "PropFilterPositioner.h"   //header-file
#include "../pimcore/FilterPositionerListener.h"     //for listener

#include "../systemlayer/SysDefaultGenerator.h"       //for using DefaultGenerator

//#include <Math.hpp> //for SameValue
#include "../utils/util.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

int PropFilterPositioner::iInstanceCounter=0;

//------
PropFilterPositioner::PropFilterPositioner()
{
  assert (++iInstanceCounter == 1); //only one instance of this class allowed (singleton)
  m_eObj = eCtrlFilterPositioner;   //identifier
}
PropFilterPositioner::~PropFilterPositioner()
{
  m_eObj = eCanObjMin;              //reset identifier
  assert (--iInstanceCounter == 0); //only one instance of this class allowed (singleton)
}
//------------------------------------------------------------------
void PropFilterPositioner::addFilterPositionerListener(FilterPositionerListener* h)
{
  addListenerToList(h);
}
void PropFilterPositioner::removeFilterPositionerListener(FilterPositionerListener* h)
{
  removeListenerFromList(h);
}
//---------------------------------------------------------------------------
void PropFilterPositioner::goPositionForFilter(EFilterNo eFilter)
{
  handleGoPositionForFilter(eFilter); //interface call, just redirect
}
void PropFilterPositioner::handleGoPositionForFilter(EFilterNo eFilter)
{
  setTargetValue( getPositionForFilter(eFilter) );
}
//-----------------------------------------------
bool PropFilterPositioner::getInPositionForFilter(EFilterNo eFilter)
{
  bool fTrueFalse = Util::SameValue
                      (  getPositionForFilter(eFilter)                                      ,  //[mm]
                         getActualValue()                                                   ,  //[mm]
                       ( m_pDefaultGenerator->getControllableSettings(m_eObj) )->rTolerance ); //[mm]

  return fTrueFalse ;  //only true on exact match
}


float PropFilterPositioner::getPositionForFilter(EFilterNo eFilter)
{
  float rReturnValue;

  Filter_LogicalPos_t* ptStruct = m_pDefaultGenerator->getFilterLogicalPositions();

  switch ( eFilter ) {
    case eFilter1 : rReturnValue = ptStruct->rPosFilter1; break;
    case eFilter2 : rReturnValue = ptStruct->rPosFilter2; break;
    case eFilter3 : rReturnValue = ptStruct->rPosFilter3; break;
    default : assert( 0 );
  }

  return rReturnValue;
}
