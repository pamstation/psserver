//---------------------------------------------------------------------------


#pragma hdrstop
#include <cassert>
#include "PropWagonPositioner.h"   //header-file
#include "../pimcore/WagonPositionerListener.h"    //for listener

#include "../systemlayer/SysExceptionGenerator.h" //for passing Exception to ExceptionGenerator
#include "../systemlayer/CANController.h"         //for using CAN

//#include <Math.hpp> //for SameValue
#include "../utils/util.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)

int PropWagonPositioner::iInstanceCounter=0;
//-----
PropWagonPositioner::PropWagonPositioner(EWagonPositioner ePositioner)
{
  switch (ePositioner){
  case ePositionerX: m_eObj = eCtrlTransporterX; break;
  case ePositionerY: m_eObj = eCtrlTransporterY; break;
  default          : assert(0);
  }

  ++iInstanceCounter;

  assert (iInstanceCounter <= 2); //only X or Y positioner
}
PropWagonPositioner::~PropWagonPositioner()
{
  m_eObj = eCanObjMin;
  --iInstanceCounter ; //reset identifier
  assert (iInstanceCounter <= 1); //only X or Y positioner
}
//--------------------------------------------------------------------------
void PropWagonPositioner::addWagonPositionerListener(WagonPositionerListener* h)
{
  addListenerToList(h);
}
void PropWagonPositioner::removeWagonPositionerListener(WagonPositionerListener* h)
{
  removeListenerFromList(h);
}
//--------------------------------------------------------------------
bool PropWagonPositioner::getCoverClosed(void)
{
  return m_pCAN->getActual(eSensCoverClosed);   // 0 = open, 1 = closed
}
//-------------------------------------------------------------------------------
