//---------------------------------------------------------------------------

#ifndef PropHomablePositionableH
#define PropHomablePositionableH
//---------------------------------------------------------------------------

#include "../pimcore/HomablePositionable.h"       //for inheritance (interface)
#include "PropControllable.h"   //for inheritance (base-implementation)

class PropHomablePositionable:  virtual public HomablePositionable,  virtual public PropControllable
{
  public:
   PropHomablePositionable();
  ~PropHomablePositionable();

    //--INTERFACE METHODS--
    virtual void goHome  (void);
    virtual bool getHomed(void) {return getHomedProperty(); };

  private:
    virtual void    addHomablePositionableListener(HomablePositionableListener* h);
    virtual void removeHomablePositionableListener(HomablePositionableListener* h);

    void  handleGoHomeCommand(void);
};

#endif   //PropHomablePositionableH
