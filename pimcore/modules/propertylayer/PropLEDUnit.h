//---------------------------------------------------------------------------

#ifndef PropLEDUnitH
#define PropLEDUnitH
//---------------------------------------------------------------------------

#include "../pimcore/LEDUnit.h"               //for inheritance (interface)
#include "PropSwitchable.h"  //for inheritance (base-implementation)

class PropLEDUnit: virtual public LEDUnit , virtual public PropSwitchable
{
  public:
     PropLEDUnit(ELEDNo eLedNo);
    ~PropLEDUnit();

    virtual void    addLEDUnitListener(LEDUnitListener * h);
    virtual void removeLEDUnitListener(LEDUnitListener * h);

    void  setLedWaitTime ( int   iLedWaitTime );  // in [LU], has listener
    //getters
    int   getLedWaitTime();   //[LU]

  private:
    static int iInstanceCounter;   //for singleton behaviour test
        int m_iLedWaitTime;
};

#endif  // PropLEDUnitH
