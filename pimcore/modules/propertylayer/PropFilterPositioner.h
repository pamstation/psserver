//---------------------------------------------------------------------------

#ifndef PropFilterPositionerH
#define PropFilterPositionerH
//---------------------------------------------------------------------------

#include "../pimcore/FilterPositioner.h"              //for inheritance (interface)
#include "PropHomablePositionable.h" //for inheritance (base-implementation)

class PropFilterPositioner: virtual public FilterPositioner , virtual public PropHomablePositionable
{
  public:
     PropFilterPositioner();
    ~PropFilterPositioner();

    virtual void    addFilterPositionerListener (FilterPositionerListener * h);
    virtual void removeFilterPositionerListener (FilterPositionerListener * h);

    virtual void     goPositionForFilter(EFilterNo eFilter);
    virtual bool  getInPositionForFilter(EFilterNo eFilter);
    virtual float   getPositionForFilter(EFilterNo eFilter);

  private:
    static int iInstanceCounter;   //for singleton behaviour test

    void  handleGoPositionForFilter(EFilterNo eFilter);

};

#endif   //PropFilterPositionerH
