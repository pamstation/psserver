//---------------------------------------------------------------------------

#include <cassert>

#include "PropAspirateHeadPositioner.h"   //header-file
#include "../pimcore/AspirateHeadPositionerListener.h"     //for listener

#include "../systemlayer/SysExceptionGenerator.h" //for passing Exception to ExceptionGenerator
//---------------------------------------------------------------------------


int PropAspirateHeadPositioner::iInstanceCounter=0;
//--------------
PropAspirateHeadPositioner::PropAspirateHeadPositioner()
{
  assert (++iInstanceCounter == 1);         //only one instance of this class allowed (singleton)
  m_eObj = eCtrlAspirateHeadPositioner;     //identifier
}
PropAspirateHeadPositioner::~PropAspirateHeadPositioner()
{
  m_eObj = eCanObjMin;                      //reset identifier
  assert (--iInstanceCounter == 0);         //only one instance of this class allowed (singleton)
}
//-------------------------------------------------------
void PropAspirateHeadPositioner::addAspirateHeadPositionerListener(AspirateHeadPositionerListener * h)
{
 this->addListenerToList(h);
}
void PropAspirateHeadPositioner::removeAspirateHeadPositionerListener(AspirateHeadPositionerListener * h)
{
  this->removeListenerFromList(h);
}
//----------------------------------------
EAspirateHeadPosition PropAspirateHeadPositioner::getAspirateHeadPosition()
{
//  getActualValue();
//  return EAspirateHeadPosition::eAspHeadPosMax;
  return (EAspirateHeadPosition)(int)getActualValue();
}
//------------------
void PropAspirateHeadPositioner::setAspirateHeadPosition(EAspirateHeadPosition ePos)
{
  //interface method, just redirect
  handleSetAspirateHeadPosition(ePos);  
}
//---
void PropAspirateHeadPositioner::handleSetAspirateHeadPosition(EAspirateHeadPosition ePos)
{
  this->setTargetValue( ( float)ePos );

}
//-----
void PropAspirateHeadPositioner::checkAspirateHeadPositionerUp()
{
  if ( getAspirateHeadPosition() != eUp )
  {
    m_pExceptionGenerator->onException(this, ASPIRATEHEADPOSTIONER_NOT_UP, eErrorSafety, "ASPIRATEHEADPOSTIONER_NOT_UP" + getAspirateHeadPosition());
  }
}
//--------------------------------------------------

