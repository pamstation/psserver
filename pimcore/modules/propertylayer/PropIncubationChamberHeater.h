//---------------------------------------------------------------------------

#ifndef PropIncubationChamberHeaterH
#define PropIncubationChamberHeaterH

#include "../pimcore/IncubationChamberHeater.h"     //for inheritance (interface)
#include "PropHeatable.h"         //for inheritance (base-implementation)
//---------------------------------------------------------------------------


class PropIncubationChamberHeater: virtual public IncubationChamberHeater, virtual public PropHeatable
{
  public:
     PropIncubationChamberHeater(EIncubationChamberHeater);
    ~PropIncubationChamberHeater();

   virtual void     addIncubationChamberHeaterListener(IncubationChamberHeaterListener *h); ///<adds listener
   virtual void  removeIncubationChamberHeaterListener(IncubationChamberHeaterListener *h); ///<removes listener

  private:
    static int iInstanceCounter;   //for singleton behaviour test

};

#endif //PropIncubationChamberHeaterH
