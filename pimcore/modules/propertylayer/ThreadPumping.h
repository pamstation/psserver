//---------------------------------------------------------------------------

#ifndef ThreadPumpingH
#define ThreadPumpingH
//---------------------------------------------------------------------------
#include <thread>
#include "../pimcore/PamDefs.h"

//---------------------------------------------------------------------------
class PropDisposable;

class ThreadPumping {


public:
    ThreadPumping();

    void start();

    void waitDone();

    PropDisposable *m_pParent;
    EDropletPosition m_eUpDown;        //Pump up or down
    unsigned m_uWellBitSet;    //selected wells
    signed m_sPumpTimeMs;

    bool running;
    std::thread thread;
};
//---------------------------------------------------------------------------
#endif
