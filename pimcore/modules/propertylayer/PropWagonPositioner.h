//---------------------------------------------------------------------------

#ifndef PropWagonPositionerH
#define PropWagonPositionerH
//---------------------------------------------------------------------------

#include "../pimcore/WagonPositioner.h"               //for inheritance (interface)
#include "PropHomablePositionable.h" //for inheritance (base-implementation)

class PropWagonPositioner: virtual public WagonPositioner , virtual public PropHomablePositionable
{
  public:
     PropWagonPositioner(EWagonPositioner);
    ~PropWagonPositioner();

    virtual void    addWagonPositionerListener (WagonPositionerListener * h);
    virtual void removeWagonPositionerListener (WagonPositionerListener * h);

    virtual bool   getCoverClosed(void);

  private:
    static int iInstanceCounter;   //for singleton behaviour test

};

#endif   //PropWagonPositionerH
