//---------------------------------------------------------------------------


#pragma hdrstop

#include "PropPressurable.h"  //for header-file
#include "../pimcore/PressurableListener.h"    //for listener

//---------------------------------------------------------------------------

#pragma package(smart_init)

PropPressurable::PropPressurable()
{
  //empty
}
PropPressurable::~PropPressurable()
{
  //empty
}

void PropPressurable::addPressurableListener(PressurableListener* h)
{
  //empty
}
void PropPressurable::removePressurableListener(PressurableListener* h)
{
  //empty
}

