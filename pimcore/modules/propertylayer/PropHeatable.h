//---------------------------------------------------------------------------

#ifndef PropHeatableH
#define PropHeatableH
//---------------------------------------------------------------------------

#include "../pimcore/Heatable.h"                 //for inheritance (interface)
#include "PropControllable.h"   //for inheritance (base implementation)

class PropHeatable:  virtual public Heatable,  virtual public PropControllable
{
  public:
   PropHeatable();
  ~PropHeatable();

    //--INTERFACE METHODS--
    virtual void  setTemperatureGain(float rTemperatureGain);         //[�C/min]
    virtual float getTemperatureGain()                     ;          //[�C/min]

    virtual float getMinTemperatureGain(); //returns user defined default value [�C/min]
    virtual float getMaxTemperatureGain(); //returns user defined default value [�C/min]

  private:
    virtual void    addHeatableListener(HeatableListener* h);
    virtual void removeHeatableListener(HeatableListener* h);

    void   handleTemperatureGainCommand(float rTemperatureGain);      //[�C/min]

  //START PROPERTY SECTION
    //setters
    void  setTemperatureGainProperty( float rGainLuSample );         //[LU / sample]
    //getters
    float getTemperatureGainProperty(){ return m_rGainProperty;  };  //[LU / sample]
    //properties
    float m_rGainProperty   ;                                        //[LU / sample]
    //listener events
    void  onTemperatureGainChanged();
  //END PROPERTY SECTION

};

#endif  //PropHeatableH
