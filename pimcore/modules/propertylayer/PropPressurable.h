//---------------------------------------------------------------------------

#ifndef PropPressurableH
#define PropPressurableH
//---------------------------------------------------------------------------

#include "../pimcore/Pressurable.h"              //for inheritance (interface)
#include "PropControllable.h"   //for inheritance (base implementation)

class PropPressurable:  virtual public Pressurable,  virtual public PropControllable
{
  public:
   PropPressurable();
  ~PropPressurable();

  //--INTERFACE METHODS--
  private:
    virtual void    addPressurableListener(PressurableListener* h);
    virtual void removePressurableListener(PressurableListener* h);

};

#endif //PropPressurableH
