//---------------------------------------------------------------------------

#ifndef PropDispenseHeadHeaterH
#define PropDispenseHeadHeaterH
//---------------------------------------------------------------------------

#include "../pimcore/DispenseHeadHeater.h"   //for inheritance (interface)
#include "PropHeatable.h"  //for inheritance (base-implementation)

class PropDispenseHeadHeater:  virtual public DispenseHeadHeater,  virtual public PropHeatable
{
  public:
     PropDispenseHeadHeater();
    ~PropDispenseHeadHeater();

    virtual void    addDispenseHeadHeaterListener(DispenseHeadHeaterListener * h);
    virtual void removeDispenseHeadHeaterListener(DispenseHeadHeaterListener * h);
    
    //OVERRIDE FROM CONTROLLABLE (to correct for process calibration)
    virtual void  setTargetValue(float rTargetValue);
    virtual float getTargetValue()                  ;
    virtual float getActualValue()                  ;
    
    virtual float getMinValue   ()                  ;
    virtual float getMaxValue   ()                  ;

    virtual void updateProperties(void)      ;  //override from controllable, not an interface function

   /* */

  protected:

  private:
    static int iInstanceCounter;   //for singleton behaviour test

    float m_rOffsetValue; //hardcoded set at construction
};


#endif //PropWagonHeaterH
