//---------------------------------------------------------------------------


#pragma hdrstop

#include "PropPositionable.h"  //header-file
#include "../pimcore/PositionableListener.h"    //for listener
//---------------------------------------------------------------------------

#pragma package(smart_init)
PropPositionable::PropPositionable()
{
  //empty
}
PropPositionable::~PropPositionable()
{
  //empty
}

void PropPositionable::addPositionableListener(PositionableListener* h)
{
  //empty
}
void PropPositionable::removePositionableListener(PositionableListener* h)
{
  //empty
}
