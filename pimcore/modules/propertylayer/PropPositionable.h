//---------------------------------------------------------------------------

#ifndef PropPositionableH
#define PropPositionableH
//---------------------------------------------------------------------------

#include "../pimcore/Positionable.h"             //for inheritance (interface)
#include "PropControllable.h"   //for inheritance (base-implementation)

class PropPositionable:  virtual public Positionable,  virtual public PropControllable
{
  public:
   PropPositionable();
  ~PropPositionable();

  //--INTERFACE METHODS--
  private:
    virtual void    addPositionableListener(PositionableListener* h);
    virtual void removePositionableListener(PositionableListener* h);

};

#endif   //PropPositionableH
