//---------------------------------------------------------------------------


#pragma hdrstop
#include <cassert>
#include "PropOverPressureSupplier.h"  //for header-file
#include "../pimcore/OverPressureSupplierListener.h"    //for listener
//---------------------------------------------------------------------------

#pragma package(smart_init)

int PropOverPressureSupplier::iInstanceCounter=0;
//----------
PropOverPressureSupplier::PropOverPressureSupplier()
{
  assert (++iInstanceCounter == 1);   //only one instance of this class allowed (singleton)
  m_eObj = eCtrlOverPressureSupplier; //identifier
}
PropOverPressureSupplier::~PropOverPressureSupplier()
{
  m_eObj = eCanObjMin;                //reset identifier
  assert (--iInstanceCounter == 0);   //only one instance of this class allowed (singleton)
}
//-----------------------------------------------
void PropOverPressureSupplier::addOverPressureSupplierListener(OverPressureSupplierListener * h)
{
  addListenerToList(h);
}
void PropOverPressureSupplier::removeOverPressureSupplierListener(OverPressureSupplierListener * h)
{
  removeListenerFromList(h);
}
