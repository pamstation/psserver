//---------------------------------------------------------------------------

#ifndef PropDispenseHeadPositionerH
#define PropDispenseHeadPositionerH
//---------------------------------------------------------------------------

#include "../pimcore/DispenseHeadPositioner.h"        //for inheritance (interface)
#include "PropPositionable.h" //for inheritance (base-implementation)

class PropDispenseHeadPositioner: virtual public DispenseHeadPositioner , virtual public PropPositionable
{
  public:
     PropDispenseHeadPositioner(EDispenseHeadNo eHeadNo);
    ~PropDispenseHeadPositioner();

    //INTERFACE METHODS
    virtual void    addDispenseHeadPositionerListener (DispenseHeadPositionerListener * h);
    virtual void removeDispenseHeadPositionerListener (DispenseHeadPositionerListener * h);

  //virtual void  checkAlmostEmpty();
    virtual void  checkDispenseHeadPresent();

  private:
    static int iInstanceCounter;   //for singleton behaviour test

    ECanObj m_eAlmostEmptySensorObj;

};

#endif   //PropDispenseHeadPositionerH
