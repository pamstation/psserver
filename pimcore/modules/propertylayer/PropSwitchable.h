//---------------------------------------------------------------------------

#ifndef PropSwitchableH
#define PropSwitchableH

//for publics
#include "../pimcore/Switchable.h"           //for inheritance  (interface)
#include "../systemlayer/SysChangeSource.h"  //for inheritance  (base-implementation)

//---------------------------------------------------------------------------

class PropSwitchable: virtual public Switchable, virtual public SysChangeSource
{
  public:
    PropSwitchable();
    ~PropSwitchable();

    virtual void setSwitchStatus(bool fTrueFalse);
    virtual bool getSwitchStatus(void) {return getSwitchedProperty(); };

    //StubPamStation's call to update properties
    void updateProperties(void);

  private:
    virtual void addSwitchableListener   (SwitchableListener* h);
    virtual void removeSwitchableListener(SwitchableListener* h);

    void handleSetSwitchStatusCommand(bool fTrueFalse);

    //getter
    void setSwitchedProperty(bool fTrueFalse); //implemented in cpp, has listener
    //setter
    bool getSwitchedProperty() { return m_fSwitched; };
    //property
    bool  m_fSwitched;
    //listener events
    void  onSwitchedChanged();

    //UPDATE methods to add as callback to can-loop
    void updateSwitchedProperty();

};

#endif // PropSwitchableH
