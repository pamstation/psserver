//---------------------------------------------------------------------------

#ifndef PropUnloadButtonH
#define PropUnloadButtonH
//---------------------------------------------------------------------------
#include "../utils/event.h"
#include "../pimcore/UnloadButton.h"                  //for inheritance (interface)
#include "../systemlayer/SysChangeSource.h"  //for inheritance  (base-implementation)

class PropUnloadButton: virtual public UnloadButton , virtual public SysChangeSource
{
  public:
     PropUnloadButton();
    ~PropUnloadButton();

    virtual void    addUnloadButtonListener(UnloadButtonListener * h);
    virtual void removeUnloadButtonListener(UnloadButtonListener * h);

    virtual void press(void);
    virtual void waitPressedEvent(void);

    virtual void setEnabled(bool);
    virtual bool getEnabled(void);
    virtual int getStateId();

  private:
    static int iInstanceCounter;   //for singleton behaviour test

    void initUnloadButtonMemberValues(ECanObj m_eObj);
    void terminateUnloadButtonMemberValues(void);

    bool m_fEnabled;
    int m_stateId;
    Event   m_pWaitButtonPressedEvent;

    void onPressed(void);
    void onEnabled(void);

};

#endif  // PropUnloadButtonH
