//---------------------------------------------------------------------------

#ifndef PropCCDH
#define PropCCDH
//---------------------------------------------------------------------------
#include <future>
#include "../utils/event.h"
#include "../pimcore/CCD.h"                   //for inheritance (interface)
#include "../systemlayer/SysChangeSource.h"  //for inheritance  (base-implementation)
//#include "../../../../../../../../../opt/pylon5/include/pylon/InstantCamera.h"

#include <pylon/PylonIncludes.h>
#include <pylon/BaslerUniversalInstantCamera.h>
//using namespace Basler_UniversalCameraParams;
// using namespace Pylon;

class CCMImage;

typedef struct DXINFOHEADER_tag {
    int nWidth;            // CCD Size
    int nHeight;        //
    int nDepth;            // 10 or 12 bit
    int nFormat;        // Sensorformat 0:Monochrome   1-4:DX_FORMAT_COLOR_xx - Color of the first Pixel
    int nInfoX;            // Offset
    int nInfoY;
    int nInfoCX;        // Image Info Size
    int nInfoCY;
} DXINFOHEADER, *PDXINFOHEADER;

class PropCCD : virtual public CCD, virtual public SysChangeSource {
public:
    PropCCD();

    ~PropCCD();


    //PIMCORE INTERFACE CALLS
    virtual void addCCDListener(CCDListener *h);

    virtual void removeCCDListener(CCDListener *h);

    virtual void setEnabled(bool fTrueFalse);

    virtual bool getEnabled() { return getEnabledProperty(); };

    virtual void setExposureTime(unsigned rExpTime);                             //time in [ms]
    virtual unsigned getExposureTime() { return getExposureTimeProperty(); };  //time in [ms]
    virtual unsigned getMaxExposureTime() { return getMaxExposureTimeProperty(); };  //time in [ms]

    virtual Image *acquireImage();

    virtual void setImageInfo(EWellNo eWell, EFilterNo eFilter);

    virtual void doLog(const std::string &strWhat);

    virtual bool isEmulator();


private:
    static int iInstanceCounter;   //for singleton behaviour test

    //PROPERTY SECTION
    //setters
    void setEnabledProperty(bool fTrueFalse) {m_fEnabled = fTrueFalse;};

    void setExposureTimeProperty(unsigned rExpTime);
    //getters
    bool getEnabledProperty() { return m_fEnabled; };

    unsigned getExposureTimeProperty() { return m_uExposureTime; };

    unsigned getMaxExposureTimeProperty() { return m_uMaxExposureTime; };
    //properties
    bool m_fEnabled;
    unsigned m_uExposureTime; //[ms]
    unsigned m_uMaxExposureTime; //[ms]

    std::mutex mutex;

    //LAST IMAGE INFO
    EWellNo m_eWellInfo;
    EFilterNo m_eFilterInfo;

//    CCMImage *image;
    Pylon::CBaslerUniversalInstantCamera *camera;
};

#endif  // PropCCDH
