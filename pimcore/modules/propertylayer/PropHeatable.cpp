//---------------------------------------------------------------------------


#pragma hdrstop

#include <cassert>
#include "PropHeatable.h"  //for header-file
#include "../pimcore/HeatableListener.h"    //for listener

#include "../systemlayer/SysDefaultGenerator.h"   //for converting to and from SI units
#include "../systemlayer/SysExceptionGenerator.h" //for passing Exception to ExceptionGenerator
#include "../systemlayer/CANController.h"         //for using CANController m_pCAN

//#include <Math.hpp> //for Sign()
//---------------------------------------------------------------------------

#pragma package(smart_init)

PropHeatable::PropHeatable() {
    m_rGainProperty = -1.0;
}

PropHeatable::~PropHeatable() {
}

//---------------------------------
void PropHeatable::addHeatableListener(HeatableListener *h) {
    addListenerToList(h);
}

void PropHeatable::removeHeatableListener(HeatableListener *h) {
    removeListenerFromList(h);
}

//------GETGAIN-------------
float PropHeatable::getTemperatureGain() {
    //get necessary conversion factor rTSample [sec/sample]
    float rTSample = m_pDefaultGenerator->getSensorCalibrationData(m_eObj)->rTsample;
    assert (rTSample > 0);   //should have a value

    // conversion is only accurate in range of sensor calibration, so provide offset (in this case min value)
    int iMinValueLU = m_pDefaultGenerator->convertSItoLU(m_eObj, getMinValue());

    //convert from LU/sample to LU/sec to LU/min
    float rGainLuMin =
            getTemperatureGainProperty() * 60.0 / rTSample;  // [LU/sample] * [sec/min] / [sec/sample]   = [LU/min]

    //concert from LU/min to �C/min, this is done within the range of the sensor-calibration
    float rTempGainSI = m_pDefaultGenerator->convertLUtoSI(m_eObj, iMinValueLU + rGainLuMin) -
                        m_pDefaultGenerator->convertLUtoSI(m_eObj, iMinValueLU);

    return rTempGainSI;
}

//------------------SETGAIN-----------------------
void PropHeatable::setTemperatureGain(float rTemperatureGain) {
    handleTemperatureGainCommand(rTemperatureGain); //interface-call, do nothing just redirect
}

void PropHeatable::handleTemperatureGainCommand(float rTemperatureGain) {
    //check range  in [SI]
    if (rTemperatureGain < getMinTemperatureGain() || rTemperatureGain > getMaxTemperatureGain()) {
        std::string strDesc = "Temperature Gain Range Error, Obj: " + std::to_string(m_eObj) + ", Temperature Gain Value: " +
                              std::to_string(rTemperatureGain);
        m_pExceptionGenerator->onException(this, CONTROLLABLE_RANGE_ERROR, eErrorRange, strDesc);
    }

    //get necessary conversion factor rTSample [sec/sample]
    float rTSample = m_pDefaultGenerator->getSensorCalibrationData(m_eObj)->rTsample;
    assert (rTSample > 0);   //should have a value

    //convert from �C/min to LU/min, this is done within the range of the sensor-calibration (although result is an integer, put in float)
    float rGainLuMin = m_pDefaultGenerator->convertSItoLU(m_eObj, getMinValue() + rTemperatureGain) -
                       m_pDefaultGenerator->convertSItoLU(m_eObj, getMinValue());

    //convert from LU/min to LU/sec to LU/sample
    float rGainLuSample = (rGainLuMin / 60.0) * rTSample;  // ( [LU/min] / [sec/min] ) * [sec/sample] = [LU/sample]

    //send mssg to CANController
    m_pCAN->setSpeed(m_eObj, rGainLuSample);       //in LU/sample

    //self update property, there is no getSpeed in CANController (yet?)
    setTemperatureGainProperty(rGainLuSample);   //in  LU/sample

}

//-------------------------END SETGAIN---------------

//--START SECTION PROPERTIES------------------

//in [LU]
void PropHeatable::setTemperatureGainProperty(float rGainLuSample) {
    if (m_rGainProperty !=
        rGainLuSample)       //on changes only  (even the smallest, only the user is able to update this property)
    {
        m_rGainProperty = rGainLuSample;      //change property
        onTemperatureGainChanged();              //notify listener
    }
}

//--
void PropHeatable::onTemperatureGainChanged() {

    std::lock_guard<std::recursive_mutex> guard(m_pcsListenerGuard);

    for (int index = 0; index < m_pListOfListeners.size(); index++) {
        EventHandler *pBase = (EventHandler *) m_pListOfListeners[index];
        HeatableListener *pHtbl = dynamic_cast<HeatableListener *>(pBase);
        pHtbl->onTemperatureGainChanged(this);
    }

}


//--END SECTION PROPERTIES-------------------

float PropHeatable::getMinTemperatureGain() {
    return m_pDefaultGenerator->getControllableSettings(m_eObj)->rMinVelocity;    //in SI [�C/min]
}

float PropHeatable::getMaxTemperatureGain() {
    return m_pDefaultGenerator->getControllableSettings(m_eObj)->rMaxVelocity;    //in SI [�C/min]
}


