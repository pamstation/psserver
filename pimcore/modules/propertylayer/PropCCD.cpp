//---------------------------------------------------------------------------
#pragma hdrstop

#include <cassert>
#include "PropCCD.h"   //header-file
#include "../pimcore/CCDListener.h"     //for listener
#include "../pimcore/PamStationFactory.h"  //for obtaining PamStation object
#include "../pimcore/PamStation.h"         //for obtaining PamStation object
#include "../pimcore/LogGenerator.h"         //for obtaining PamStation object
#include "../pimcore/PamStationException.h"
#include "../systemlayer/CCMImage.h"    //for handling Image
#include "../systemlayer/SysLogGenerator.h" //for passing Exception to ExceptionGenerator
#include "../systemlayer/SysExceptionGenerator.h"
//#include "../utils/event.h"

#pragma package(smart_init)

int PropCCD::iInstanceCounter = 0;


PropCCD::PropCCD() {
    m_eObj = eCtrlCCD;
    ++iInstanceCounter;
    assert (iInstanceCounter == 1);  //only one instance of this class allowed (singleton)

    m_fEnabled = false;
    m_uExposureTime = 0;
    m_uMaxExposureTime = 10000; //[ms]

    m_eWellInfo = eWellNoUnknown;
    m_eFilterInfo = eFilterNoUnknown;

    try {
        Pylon::PylonInitialize();

        camera = new Pylon::CBaslerUniversalInstantCamera(Pylon::CTlFactory::GetInstance().CreateFirstDevice());

        // Print the model name of the camera.
        std::cout << "Using device " << camera->GetDeviceInfo().GetModelName() << std::endl;
        std::cout << "Using device " << camera->GetDeviceInfo().GetModelName() << " class "
                  << camera->GetDeviceInfo().GetDeviceClass() << std::endl;

        if (!isEmulator()) {

            camera->Open();

            const char Filename[] = "config/camera-settings.pfs";

//            try {
//                Pylon::CFeaturePersistence::Load(Filename, &camera->GetNodeMap(), true);
//            }
//            catch (Pylon::GenericException &e) {
//                m_pExceptionGenerator->onException(this, CCD_SETTINGS, eErrorUnknown, e.GetDescription());
//            }


            if (camera->ExposureTimeMode.IsWritable()) {
                camera->ExposureTimeMode.SetValue(Basler_UniversalCameraParams::ExposureTimeMode_Standard);
                std::cout << "setting ExposureTimeMode " << std::endl;
            }

            if (camera->GainAuto.IsWritable()) {
                camera->GainAuto.SetValue(Basler_UniversalCameraParams::GainAuto_Off);
                std::cout << "setting GainAuto " << std::endl;
            }

            if (camera->Gain.IsWritable()) {
                camera->Gain.SetValue(7.0);
                std::cout << "setting Gain " << std::endl;
            }

            if (camera->ExposureAuto.IsWritable()) {
                camera->ExposureAuto.SetValue(Basler_UniversalCameraParams::ExposureAuto_Off);
                std::cout << "setting ExposureAuto " << std::endl;
            }

            if (camera->ExposureTime.IsWritable()) {
                camera->ExposureTime.SetValue(200000.0);
                std::cout << "setting ExposureTime " << std::endl;
            }

            if (camera->PixelFormat.IsWritable()) {
                camera->PixelFormat.SetValue(Basler_UniversalCameraParams::PixelFormat_Mono12);
                std::cout << "setting PixelFormat " << "Mono12" << std::endl;
            }

            // setting binning first is important, see doc
            if (camera->BinningHorizontal.IsWritable()) {
                camera->BinningHorizontal.SetValue(2);
                std::cout << "setting BinningHorizontal " << 2 << std::endl;
            }

            if (camera->BinningVertical.IsWritable()) {
                camera->BinningVertical.SetValue(2);
                std::cout << "setting BinningVertical " << 2 << std::endl;
            }

            if (camera->OffsetX.IsWritable()) {
                camera->OffsetX.SetValue(0);
                std::cout << "setting OffsetX " << 0 << std::endl;
            }

            if (camera->OffsetY.IsWritable()) {
                camera->OffsetY.SetValue(0);
                std::cout << "setting OffsetY " << 0 << std::endl;
            }

            if (camera->CenterX.IsWritable()) {
                camera->CenterX.SetValue(true);
                std::cout << "setting CenterX " << true << std::endl;
            }

            if (camera->CenterY.IsWritable()) {
                camera->CenterY.SetValue(true);
                std::cout << "setting CenterY " << true << std::endl;
            }

            if (camera->Width.IsWritable()) {
                camera->Width.SetValue(552);
                std::cout << "setting Width " << 552 << std::endl;
            }

            if (camera->Height.IsWritable()) {
                camera->Height.SetValue(413);
                std::cout << "setting Height " << 413 << std::endl;
            }


            if (camera->AutoFunctionROISelector.IsWritable()) {
                camera->AutoFunctionROISelector.SetValue(Basler_UniversalCameraParams::AutoFunctionROISelector_ROI1);
                std::cout << "setting AutoFunctionROISelector " << "ROI1" << std::endl;
            }

//            if (camera->AutoFunctionROIOffsetX.IsWritable()) {
//                camera->AutoFunctionROIOffsetX.SetValue(4);
//                std::cout << "setting AutoFunctionROIOffsetX " << 4 << std::endl;
//            }
//
//            if (camera->AutoFunctionROIOffsetY.IsWritable()) {
//                camera->AutoFunctionROIOffsetY.SetValue(4);
//                std::cout << "setting AutoFunctionROIOffsetY " << 4 << std::endl;
//            }

            if (camera->AutoFunctionROIOffsetX.IsWritable()) {
                camera->AutoFunctionROIOffsetX.SetValue(0);
                std::cout << "setting AutoFunctionROIOffsetX " << 0 << std::endl;
            }

            if (camera->AutoFunctionROIOffsetY.IsWritable()) {
                camera->AutoFunctionROIOffsetY.SetValue(0);
                std::cout << "setting AutoFunctionROIOffsetY " << 0 << std::endl;
            }

            if (camera->AutoFunctionROIWidth.IsWritable()) {
                camera->AutoFunctionROIWidth.SetValue(960);
                std::cout << "setting AutoFunctionROIWidth " << 960 << std::endl;
            }

            if (camera->AutoFunctionROIHeight.IsWritable()) {
                camera->AutoFunctionROIHeight.SetValue(600);
                std::cout << "setting AutoFunctionROIHeight " << 600 << std::endl;
            }

            if (camera->AutoFunctionROIUseBrightness.IsWritable()) {
                camera->AutoFunctionROIUseBrightness.SetValue(true);
                std::cout << "setting AutoFunctionROIUseBrightness " << true << std::endl;
            }

            // Save the content of the camera's node map into the file
            if (!isEmulator()) {
                const char Filename[] = "config/current-camera-settings.pfs";
                try {
                    Pylon::CFeaturePersistence::Save(Filename, &camera->GetNodeMap());
                }
                catch (Pylon::GenericException &e) {
                    // Error handling
                    m_pExceptionGenerator->onException(this, CCD_SETTINGS, eErrorUnknown, e.GetDescription());
                }
            }

//            camera->Close();
        }
    } catch (const Pylon::GenericException &e) {
        m_pExceptionGenerator->onException(this, CCD_OPEN_DEVICE, eErrorUnknown, e.GetDescription());
    }
}

bool PropCCD::isEmulator() {
    std::string deviceClass = "BaslerCamEmu";
    return deviceClass == camera->GetDeviceInfo().GetDeviceClass().c_str();
}

Image *PropCCD::acquireImage() {
    std::lock_guard<std::mutex> guard(mutex);

    Pylon::CGrabResultPtr ptrGrabResult;
    CCMImage *anImage = nullptr;

    try {

        if (!isEmulator()) {
//            camera->Open();
            camera->ExposureTime.SetValue(1000.0 * getExposureTime());
//            camera->Close();
        }

        camera->StartGrabbing(1);

        camera->RetrieveResult(5000, ptrGrabResult, Pylon::TimeoutHandling_ThrowException);

        if (ptrGrabResult->GrabSucceeded()) {

            anImage = new CCMImage();

            uint32_t width = ptrGrabResult->GetWidth();
            uint32_t height = ptrGrabResult->GetHeight();

            anImage->m_iSizeX = width;
            anImage->m_iSizeY = height;
            anImage->m_iBitsPerPixel = 12;
            anImage->m_uSaturationLimit = (unsigned) (1 << 12) - 1;
            anImage->m_uExposureTime = getExposureTime(); //[ms]
            anImage->m_eWell = m_eWellInfo;
            anImage->m_eFilter = m_eFilterInfo;
            anImage->createArrayOfPixels(width, height);

            if (!isEmulator()) {
                Pylon::CPylonImage targetImage;
                targetImage.AttachGrabResultBuffer(ptrGrabResult);
                auto *pCCDBuffer = reinterpret_cast<const unsigned short *>(targetImage.GetBuffer());
                unsigned short *pArrayOfPixels = anImage->getArrayOfPixels();

                for (unsigned i = 0; i < width * height; i++) {
                    if (pCCDBuffer[i] > anImage->m_uSaturationLimit) {
                        m_pExceptionGenerator->onException(this, CCD_IMAGE_FORMAT, eErrorRange, "bad image format");
                    }
                }

                memcpy(pArrayOfPixels,
                       pCCDBuffer,
                       targetImage.GetAllocatedBufferSize()
                );
                //        targetImage.Save(Pylon::ImageFileFormat_Tiff, "/tmp/ps2.tiff");
            }
        } else {
            m_pExceptionGenerator->onException(this, CCD_NULLIMAGE, eErrorUnknown,
                                               ptrGrabResult->GetErrorDescription().c_str());
        }

    } catch (const Pylon::GenericException &e) {
        m_pExceptionGenerator->onException(this, CCD_NULLIMAGE, eErrorUnknown, e.GetDescription());
    }

    return anImage;
}


PropCCD::~PropCCD() {
    --iInstanceCounter;
    assert (iInstanceCounter == 0);
    Pylon::PylonTerminate();
}

//-------------------------------------------------------
void PropCCD::addCCDListener(CCDListener *h) {
    addListenerToList(h);
}

void PropCCD::removeCCDListener(CCDListener *h) { removeListenerFromList(h); }

//------------------------------------------------------
void PropCCD::setEnabled(bool fEnable) { setEnabledProperty(fEnable); }


//-------------------------------------------------------
void PropCCD::setExposureTime(unsigned rExpTime) {
    if (rExpTime > getMaxExposureTime()) {
        this->m_pExceptionGenerator->onException(this, CCD_RANGE_ERROR, eErrorRange, "");
    }
    setExposureTimeProperty(rExpTime);
}


//---
void PropCCD::setExposureTimeProperty(unsigned rExpTime) {
    if (m_uExposureTime != rExpTime) {
        m_uExposureTime = rExpTime;
        std::lock_guard<std::recursive_mutex> guard(m_pcsListenerGuard);

        for (int index = 0; index < m_pListOfListeners.size(); index++) {
            auto *pBase = (EventHandler *) m_pListOfListeners[index];
            auto *pCCD = dynamic_cast<CCDListener *>(pBase);
            pCCD->onExposureTimeChanged(this);
        }
    }
}


//-----------------------
void PropCCD::setImageInfo(EWellNo eWell, EFilterNo eFilter) {
    m_eWellInfo = eWell;
    m_eFilterInfo = eFilter;
}

void PropCCD::doLog(const std::string &strWhat) {
    PamStation *pPam = PamStationFactory::getCurrentPamStation();
    LogGenerator *aLog = pPam->getLogGenerator();
    auto *aSysLog = dynamic_cast<SysLogGenerator *>(aLog);
    aSysLog->logInfo(strWhat);
}
