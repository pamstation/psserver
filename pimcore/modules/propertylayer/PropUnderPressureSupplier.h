//---------------------------------------------------------------------------

#ifndef PropUnderPressureSupplierH
#define PropUnderPressureSupplierH
//---------------------------------------------------------------------------

#include "../pimcore/UnderPressureSupplier.h"    //for inheritance (interface)
#include "PropPressurable.h"    //for inheritance (base-implementation)

class PropUnderPressureSupplier: virtual public UnderPressureSupplier, virtual public PropPressurable
{
  public:
     PropUnderPressureSupplier();
    ~PropUnderPressureSupplier();

    virtual void    addUnderPressureSupplierListener(UnderPressureSupplierListener * h);
    virtual void removeUnderPressureSupplierListener(UnderPressureSupplierListener * h);

  private:
    static int iInstanceCounter;   //for singleton behaviour test
};


#endif   //PropUnderPressureSupplierH
