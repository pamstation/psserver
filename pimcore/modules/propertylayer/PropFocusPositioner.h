//---------------------------------------------------------------------------

#ifndef PropFocusPositionerH
#define PropFocusPositionerH
//---------------------------------------------------------------------------

#include "../pimcore/FocusPositioner.h"               //for inheritance (interface)
#include "PropHomablePositionable.h" //for inheritance (base-implementation)

class PropFocusPositioner: virtual public FocusPositioner , virtual public PropHomablePositionable
{
  public:
     PropFocusPositioner();
    ~PropFocusPositioner();

    virtual void    addFocusPositionerListener (FocusPositionerListener * h);
    virtual void removeFocusPositionerListener (FocusPositionerListener * h);

    virtual void     goPositionForFilterAndWell(EFilterNo eFilter, EWellNo eWell);
    virtual bool  getInPositionForFilterAndWell(EFilterNo eFilter, EWellNo eWell);
    virtual float   getPositionForFilterAndWell(EFilterNo eFilter, EWellNo eWell);

  private:
    static int iInstanceCounter;   //for singleton behaviour test

    void  handleGoPositionForFilterAndWell(EFilterNo eFilter, EWellNo eWell);

};

#endif   //PropFocusPositionerH
