//---------------------------------------------------------------------------


#pragma hdrstop

#include "PropHomablePositionable.h"  //header-file
#include "../pimcore/HomablePositionableListener.h"    //for listener

#include "../systemlayer/CANController.h"          //for using handle to CANController
//---------------------------------------------------------------------------

#pragma package(smart_init)

PropHomablePositionable::PropHomablePositionable() {}

PropHomablePositionable::~PropHomablePositionable() {}
//-----------------------

void PropHomablePositionable::addHomablePositionableListener(HomablePositionableListener *h) {
    addListenerToList(h);
}

void PropHomablePositionable::removeHomablePositionableListener(HomablePositionableListener *h) {
    removeListenerFromList(h);
}

//--------
void PropHomablePositionable::goHome(void) {
    handleGoHomeCommand(); //interface-call, do nothing just redirect
}

void PropHomablePositionable::handleGoHomeCommand(void) {

    std::lock_guard<std::recursive_mutex> guard(m_pcsPropertyGuard);

    //setEnabled:
    // stops - if any- current movement, resets -if any- current error, enables servo
    setEnabled(true);

    //Send Msg to CANController
    m_pCAN->goHome(m_eObj);

    //synchronise:
    //It takes some time for the Can UpdateLoop to notice the controller is not ready anymore
    //waitReady doesn't block until that time
    //to prevent that the waitReadyEvent is resetted
    //and the status-property is set to an impossible value to force an update
    // (this takes also care for situation if controller was already ready)
    setStatusProperty(m_iCtrlStatus + 128);  //force update of StatusProperty
    m_pWaitReadyEvent.ResetEvent();           //block waitReady

}



