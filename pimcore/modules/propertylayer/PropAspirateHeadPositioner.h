//---------------------------------------------------------------------------

#ifndef PropAspirateHeadPositionerH
#define PropAspirateHeadPositionerH
//---------------------------------------------------------------------------

#include "../pimcore/AspirateHeadPositioner.h"   //for inheritance (interface)
#include "PropPositionable.h"   //for inheritance (base-implementation)

class PropAspirateHeadPositioner: virtual public AspirateHeadPositioner , virtual public PropPositionable
{
  public:
     PropAspirateHeadPositioner();
    ~PropAspirateHeadPositioner();

    virtual void    addAspirateHeadPositionerListener(AspirateHeadPositionerListener * h);
    virtual void removeAspirateHeadPositionerListener(AspirateHeadPositionerListener * h);

    virtual void                  setAspirateHeadPosition      (EAspirateHeadPosition ePos);
    virtual EAspirateHeadPosition getAspirateHeadPosition      (void);
    virtual void                  checkAspirateHeadPositionerUp(void);

  private:
    static int iInstanceCounter;   //for singleton behaviour test

    void  handleSetAspirateHeadPosition(EAspirateHeadPosition ePos);
};

#endif  // PropAspirateHeadPositionerH
