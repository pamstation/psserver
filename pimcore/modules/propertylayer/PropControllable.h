//---------------------------------------------------------------------------

#ifndef PropControllableH
#define PropControllableH

#include <mutex>
#include "../utils/event.h"

#include "../pimcore/Controllable.h"         //inheritance (interface)
#include "../systemlayer/SysChangeSource.h"  //inheritance (implementation base-class)

//---------------------------------------------------------------------------

class PropControllable: virtual public Controllable, virtual public SysChangeSource
{
  public:
     PropControllable();
    ~PropControllable();

    //interface setters
    virtual void setTargetValue(float rTargetValue);                      //controls hardware
    virtual void setEnabled    (bool  fTrueFalse  );                      //controls hardware

    //interface getters
    virtual float getTargetValue()     ;  //returns hardware status
    virtual float getActualValue()     ;  //returns hardware status
    virtual bool  getEnabled()         { return getEnabledProperty(); };  //returns controller status
    virtual bool  getReady()           { return getReadyProperty();   };  //returns controller status
    virtual bool  getErrorStatus()     { return getErrorProperty();   };  //returns controller status
    virtual int   getErrorNumber()     { return getErrNrProperty();   };  //returns controller error number


    virtual float getMinValue()        ;  //returns user defined default value
    virtual float getMaxValue()        ;  //returns user defined default value

    //interface waitReady
    virtual void  waitReady();
    virtual void stopWaitReady();

    //StubPamStation's call to update properties
    virtual void updateProperties(void);       //overridable (f.e. wagonHeater)

  protected:   //for actual hardware-class
    int   m_iResolution;     // wanted actualValueChanged-listener resolution in SI translated in LU

    //START PROPERTY SECTION
    //setters
    void  setStatusProperty ( int   iCtrlStatus  );  //bitwise Ctrl-status, split into:
    void  setEnabledProperty( bool  fTrueFalse   );  //Ctrl-status bit0, has no listener (yet?)
    void  setReadyProperty  ( bool  fTrueFalse   );  //Ctrl-status bit1, has listener
    void  setHomedProperty  ( bool  fTrueFalse   );  //Ctrl-status bit2, has listener
    void  setErrorProperty  ( bool  fTrueFalse   );  //Ctrl-status bit3, throws error
    void  setTargetProperty ( int   iTargetValue );  // in [LU], has listener
    void  setActualProperty ( int   iActualValue );  // in [LU], has listener
    void  setErrorNumber    ( int   iErrorNumber );  // Ctrl's error number, has no listener
    //getters
    bool  getEnabledProperty()  { return m_fEnabled;         };
    bool  getReadyProperty()    { return m_fReady;           };
    bool  getHomedProperty()    { return m_fHomed;           };
    bool  getErrorProperty()    { return m_fError;           };
    int   getTargetProperty()   { return m_iTargetProperty;  };   //[LU]
    int   getActualProperty()   { return m_iActualProperty;  };   //[LU]
    int   getErrNrProperty()    { return m_iErrorNumber;     };   //Ctrl error number

    //properties
    int   m_iCtrlStatus;        //bitwise Ctrl-status as obtained from CANController
    bool  m_fEnabled;           //Ctrl-status bit0
    bool  m_fReady;             //Ctrl-status bit1
    bool  m_fHomed;             //Ctrl-status bit2
    bool  m_fError;             //Ctrl-status bit3
    int   m_iTargetProperty;    //hardware status value in [LU]!
    int   m_iActualProperty;    //hardware status value in [LU]!
    int   m_iErrorNumber;       //Ctrl's error number
    //listener events
    void  onReadyChanged();
    void  onHomedChanged();
    void  onTargetValueChanged();
    void  onActualValueChanged(int iDelta);

    //END PROPERTY SECTION

    //UPDATE methods to call by REFRESH-PROPERTIES loop
    //OVERRIDe-able (by f.e. wagonHeater)

    virtual void updateStatusProperty();
    virtual void updateTargetProperty();
    virtual void updateActualProperty();
    virtual void updateErrorNumber   ();

    Event m_pWaitReadyEvent;      //protected because accessibility for homables
    std::recursive_mutex m_pcsPropertyGuard; //protected because accessibility for homables

  private:
    virtual void addControllableListener   (ControllableListener* h);
    virtual void removeControllableListener(ControllableListener* h);

    void  handleSetTargetValueCommand(float rTargetValue);
    void  handleSetEnabledCommand(bool fTrueFalse);

    void initialise(void);
    void terminate (void);
    bool fEnteredBefore;   //indicates whether or not m_iResolution is calculated

};

#endif // PropControllableH
