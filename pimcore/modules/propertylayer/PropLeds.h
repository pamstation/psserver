//---------------------------------------------------------------------------

#ifndef PropLedsH
#define PropLedsH
//---------------------------------------------------------------------------

//#include "modules/pimcore/Disposable.h"           //for inheritance (interface)
#include "../systemlayer/SysChangeSource.h"  //for inheritance  (base-implementation)

class PropLeds : virtual public SysChangeSource
{
  public:
     PropLeds();
    ~PropLeds();

    void SetRedLed(bool fOnOff);
    void SetGreenLed(bool fOnOff);
  private:
    static int iInstanceCounter;   //for singleton behaviour test

};

#endif  // PropDisposableH
