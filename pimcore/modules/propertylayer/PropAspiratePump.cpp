//---------------------------------------------------------------------------


#include "PropAspiratePump.h"   //header-file
#include "../pimcore/AspiratePumpListener.h"     //for listener

#include "../systemlayer/CANController.h"         //for use of CANController
#include "../systemlayer/SysDefaultGenerator.h"   //for using DefaultGenerator
#include "../systemlayer/SysExceptionGenerator.h" //for passing Exception to ExceptionGenerator
//---------------------------------------------------------------------------

#include <cassert>

int PropAspiratePump::iInstanceCounter=0;

//--------------------------------------------------------------------------------
PropAspiratePump::PropAspiratePump()
{
  m_eObj = ePmpAspirate;                     //identifier
  assert (++iInstanceCounter == 1);          //only one instance of this class allowed (singleton)
}
//----
PropAspiratePump::~PropAspiratePump()
{
  m_eObj = eCanObjMin;                    //reset identifier
  assert (--iInstanceCounter == 0);       //only one instance of this class allowed (singleton)
}
//---------------------------------------------------------
void PropAspiratePump::addAspiratePumpListener(AspiratePumpListener * h)
{
  this->addListenerToList(h);
}
void PropAspiratePump::removeAspiratePumpListener(AspiratePumpListener * h)
{
  this->removeListenerFromList(h);
}
//-----------------------------------------------------------------------
void PropAspiratePump::checkAspiratePressureOk(void)
{
  float rAspiratePressure   = getAspiratePressure();                                             //[Barg]
  float rPressureUpperLimit = m_pDefaultGenerator->getAspirationDefaults()->rPressureUpperLimit; //[Barg]
  float rPressureUnderlimit = m_pDefaultGenerator->getAspirationDefaults()->rPressureUnderlimit; //[Barg]

  //Upper and under limit: both negative value, upper value is closest to zero

  if ( rAspiratePressure > rPressureUpperLimit || rAspiratePressure < rPressureUnderlimit)
  {
    m_pExceptionGenerator->onException(this, ASPIRATE_PRESSURE_NOT_OK, eErrorSafety, "");
  }
}
float PropAspiratePump::getAspiratePressure(void)
{
  int     iPressLU =              m_pCAN->getActual    (ePressSensAspirate          );  //[LU]
  float   rPressSi = m_pDefaultGenerator->convertLUtoSI(ePressSensAspirate, iPressLU);
  return  rPressSi;                                                                     //[Barg]
}

