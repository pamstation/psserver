//---------------------------------------------------------------------------


#include <cassert>

#include "PropDispenseHeadPositioner.h"   //header-file
#include "../pimcore/DispenseHeadPositionerListener.h"    //for listener
#include "../systemlayer/CANController.h"                 //for using object
#include "../systemlayer/SysExceptionGenerator.h"         //for passing Exception to ExceptionGenerator
//---------------------------------------------------------------------------



int PropDispenseHeadPositioner::iInstanceCounter=0;


PropDispenseHeadPositioner::PropDispenseHeadPositioner(EDispenseHeadNo eHeadNo)
{
  //identifier
  if      (eHeadNo == eHead1) {m_eObj = eCtrlDispenseHeadPositioner1; m_eAlmostEmptySensorObj = eSensDispHead1Empty; }
  else if (eHeadNo == eHead2) {m_eObj = eCtrlDispenseHeadPositioner2; m_eAlmostEmptySensorObj = eSensDispHead2Empty; }
  else if (eHeadNo == eHead3) {m_eObj = eCtrlDispenseHeadPositioner3; m_eAlmostEmptySensorObj = eSensDispHead3Empty; }
  else if (eHeadNo == eHead4) {m_eObj = eCtrlDispenseHeadPositioner4; m_eAlmostEmptySensorObj = eSensDispHead4Empty; }
  else      assert(0);

  assert (++iInstanceCounter <= 4);                 //only one instance of this class allowed (singleton)
}
PropDispenseHeadPositioner::~PropDispenseHeadPositioner()
{
  m_eObj = eCanObjMin;                              //reset identifier
  assert (--iInstanceCounter < 4);                  //only one instance of this class allowed (singleton)
}
//------------------------------------------------------------------------------
void PropDispenseHeadPositioner::addDispenseHeadPositionerListener(DispenseHeadPositionerListener* h)
{
  addListenerToList(h);
}
void PropDispenseHeadPositioner::removeDispenseHeadPositionerListener(DispenseHeadPositionerListener* h)
{
  removeListenerFromList(h);
}
//---------------------------------------------------------------------------
//checkAlmostEmpty becomes obsolete after beta
/*
void PropDispenseHeadPositioner::checkAlmostEmpty()
{
  //get AlmostEmpty sensor status
  //if sensor is "true" throw exception
  bool fAlmostEmptySensor = m_pCAN->getActual(m_eAlmostEmptySensorObj);

  if (fAlmostEmptySensor)    
  {
    m_pExceptionGenerator->onException(this, DISPENSEHEAD_ALMOST_EMPTY, eErrorSafety, "DISPENSEHEAD_ALMOST_EMPTY");
  }
}
*/
//------------------------------------------------------------------------
//CHANGED IMPLEMENTATION OF ALMOST-EMPTY-CHECK to DISPENSE-HEAD-PRESENT
void PropDispenseHeadPositioner::checkDispenseHeadPresent()
{
  //get AlmostEmpty sensor status aka inverse dispenseHeadPresent
  //if sensor is "true", dispenseHead present, don't throw exception
  //if sensor is "false", dispenseHead not present, throw exception
  //naming conventions are from "old" implementation

  bool fAlmostEmptySensor = m_pCAN->getActual(m_eAlmostEmptySensorObj);

  if (!fAlmostEmptySensor)
  {
    m_pExceptionGenerator->onException(this, DISPENSEHEAD_NOT_PRESENT, eErrorSafety, "DISPENSEHEAD_NOT_PRESENT");
  }
}
