//---------------------------------------------------------------------------


#pragma hdrstop

#include "PropSwitchable.h"  //for header-file
#include "../pimcore/SwitchableListener.h"   //for listener methods on"Property"Changed(this);

#include "../systemlayer/CANController.h"    //for using CANController m_pCAN
//---------------------------------------------------------------------------

#pragma package(smart_init)

PropSwitchable::PropSwitchable() {
    m_fSwitched = false;
}

PropSwitchable::~PropSwitchable() {
    m_fSwitched = false;
}

void PropSwitchable::addSwitchableListener(SwitchableListener *h) {
    addListenerToList(h);
}

void PropSwitchable::removeSwitchableListener(SwitchableListener *h) {
    removeListenerFromList(h);
}


//------------------------------
void PropSwitchable::setSwitchStatus(bool fTrueFalse) {
    handleSetSwitchStatusCommand(fTrueFalse); //interface-call, do nothing just redirect
}

void PropSwitchable::handleSetSwitchStatusCommand(bool fTrueFalse) {
    //send command to CANController
    m_pCAN->setTarget(m_eObj, fTrueFalse);

    //update properties:
    updateSwitchedProperty();
}

//----------------------------
//START UPDATE SECTION
//place here the methods to be called regulary
void PropSwitchable::updateProperties(void) {
    //empty  updateSwitchedProperty is called directly
}

void PropSwitchable::updateSwitchedProperty() { setSwitchedProperty(m_pCAN->getActual(m_eObj)); }

//END UPDATE SECTION

//-----------------------------------

//START SECTION PROPERTIES
void PropSwitchable::setSwitchedProperty(bool fTrueFalse) {
    if (m_fSwitched != fTrueFalse)               //on changes only
    {
        m_fSwitched = fTrueFalse;                  //change property

        onSwitchedChanged();                       //self notify listener
    }
}

//---
void PropSwitchable::onSwitchedChanged() {

    std::lock_guard<std::recursive_mutex> guard(m_pcsListenerGuard);

    for (int index = 0; index < m_pListOfListeners.size(); index++) {
        EventHandler *pBase = (EventHandler *) m_pListOfListeners[index];
        SwitchableListener *pSwi = dynamic_cast<SwitchableListener *>(pBase);
        pSwi->onSwitchedChanged(this);
    }

}
