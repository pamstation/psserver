//---------------------------------------------------------------------------


#pragma hdrstop
#include <cassert>
#include "PropLeds.h"   //header-file

#include "../systemlayer/CANController.h"         //for use of CANController
#include "../systemlayer/SysDefaultGenerator.h" //for using object
#include "../systemlayer/SysExceptionGenerator.h" //for passing Exception to ExceptionGenerator

//---------------------------------------------------------------------------

#pragma package(smart_init)

int PropLeds::iInstanceCounter=0;


PropLeds::PropLeds()
{
  ++iInstanceCounter;
  assert (iInstanceCounter == 1);  //only one instance of this class allowed (singleton)
}
PropLeds::~PropLeds()
{
  --iInstanceCounter;
  assert (iInstanceCounter == 0); //only one instance of this class allowed (singleton)
}
//-----------------START VALVE SECTION----------------------
void PropLeds::SetRedLed(bool fOnOff)
{
  m_pCAN->setTarget( ePowerOutSpare2 , fOnOff ); // this is the red led
}
void PropLeds::SetGreenLed(bool fOnOff)
{
  m_pCAN->setTarget( ePowerOutSpare0 , fOnOff ); // this is the green led
}

