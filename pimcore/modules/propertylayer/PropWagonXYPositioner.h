//---------------------------------------------------------------------------

#ifndef PropWagonXYPositionerH
#define PropWagonXYPositionerH
//---------------------------------------------------------------------------

#include "../pimcore/WagonXYPositioner.h"               //for inheritance (interface)
#include "../systemlayer/SysChangeSource.h"  //for inheritance  (base-implementation)

/**
*      -> Y
*  |   1   5    9
* \/   2   6   10
*  X   3   7   11
*      4   8   12
*/

class WagonPositioner;              //for getting pointers to WagonPositioner X  & Y
class AspirateHeadPositioner;       //for getting pointers to AspirateHeadPositioner
class PropWagonXYPositioner: virtual public WagonXYPositioner , virtual public SysChangeSource
{
  public:
     PropWagonXYPositioner();
    ~PropWagonXYPositioner();

    virtual void    addWagonXYPositionerListener (WagonXYPositionerListener * h);  //WagonPositionXY uses a WagonPositioner listener
    virtual void removeWagonXYPositionerListener (WagonXYPositionerListener * h);

    //INTERFACE METHODS
    virtual void  goLoadPosition          ()                            ;
    virtual bool  getInLoadPosition       ()                            ;
    virtual XYPos getLoadPosition         ()                            ;

    virtual void  goFrontPanelPosition    ()                            ;
    virtual bool  getInFrontPanelPosition ()                            ;
    virtual XYPos getFrontPanelPosition   ()                            ;

    virtual void  goIncubationPosition    ()                            ;
    virtual bool  getInIncubationPosition ()                            ;
    virtual XYPos getIncubationPosition   ()                            ;

    virtual void  goReadPosition          (EWellNo eWell)               ;
    virtual bool  getInReadPosition       (EWellNo eWell)               ;
    virtual XYPos getReadPosition         (EWellNo eWell)               ;

    virtual void  goAspiratePosition      (EWellNo eWell)               ;
    virtual bool  getInAspiratePosition   (EWellNo eWell)               ;
    virtual XYPos getAspiratePosition     (EWellNo eWell)               ;

    virtual void  goDispensePosition      (EWellNo eWell, EDispenseHeadNo eHead);
    virtual bool  getInDispensePosition   (EWellNo eWell, EDispenseHeadNo eHead);
    virtual XYPos getDispensePosition     (EWellNo eWell, EDispenseHeadNo eHead);

    virtual void  goPrimePosition         (EDispenseHeadNo eHead)       ;
    virtual bool  getInPrimePosition      (EDispenseHeadNo eHead)       ;
    virtual XYPos getPrimePosition        (EDispenseHeadNo eHead)       ;

    virtual void  goFocusGlassPosition    (EDisposableNo eDisposable)   ;
    virtual bool  getInFocusGlassPosition (EDisposableNo eDisposable)   ;
    virtual XYPos getFocusGlassPosition   (EDisposableNo eDisposable)   ;

    virtual void checkCoverClosed         (void)                        ; //throw safetyexception
    virtual bool   getCoverClosed         (void)                        ;

    //OVERRIDE FROM HOMABLEPOSITIONABLE
    virtual void  goHome                  ()                            ;
    virtual bool  getHomed                ()                            ;

    //OVERRIDE FROM CONTROLLABLE
    virtual void  waitReady               ()                            ;
    virtual void  setEnabled              (bool  fTrueFalse)            ;
    virtual void  setTargetValue          (XYPos xyPos)                 ;
    virtual bool  getEnabled              ()                            ;
    virtual bool  getReady                ()                            ;
    virtual XYPos getTargetValue          ()                            ;
    virtual XYPos getActualValue          ()                            ;
    virtual XYPos getMinValue             ()                            ;
    virtual XYPos getMaxValue             ()                            ;
    virtual bool  getErrorStatus          ()                            ;
    virtual XYPos getErrorNumber          ()                            ;

    //for listener functionality
    void updateProperties(void);


  private:
    static int iInstanceCounter;   //for singleton behaviour test

    void      initClassMembers(void);
    void terminateClassMembers(void);

    //START PROPERTY SECTION (USING THE NECESSARY CONTROLLABLE & HEATABLE METHODS & PROPERTIES)
    //FOR IMPLEMENTING CONTROLLABLE & HEATABLE LISTENER FUNCTIONALITY
    //setters
    void  setReadyProperty          ( bool  fTrueFalse    );      //Ctrl-status bit1, has listener
    void  setHomedProperty          ( bool  fTrueFalse    );      //Ctrl-status bit1, has listener
    void  setTargetProperty         ( XYPos xyTargetValue );      // in [SI], has listener
    void  setActualProperty         ( XYPos xyActualValue );      // in [SI], has listener
    //properties
    bool  m_fReady;              //Ctrl-status bit1  , for listener-implementation
    bool  m_fHomed;              //Ctrl-status bit2  , for listener-implementation
    XYPos m_xyTargetProperty;    //hardware status value , for listener-implementation
    XYPos m_xyActualProperty;    //hardware status value , for listener-implementation
    //listener events
    void  onReadyChanged();
    void  onHomedChanged();
    void  onTargetValueChanged();
    void  onActualValueChanged();
    //END PROPERTY SECTION


    ECanObj  m_eObjX;
    ECanObj  m_eObjY;

    WagonPositioner*        m_pX;
    WagonPositioner*        m_pY;
    AspirateHeadPositioner* m_pAspHead;
};

#endif   //PropWagonXYPositionerH
