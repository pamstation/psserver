//---------------------------------------------------------------------------


#pragma hdrstop
#include <cassert>
#include "PropLEDUnit.h"   //header-file
#include "../pimcore/LEDUnitListener.h"     //for listener
//---------------------------------------------------------------------------

#pragma package(smart_init)

int PropLEDUnit::iInstanceCounter=0;
//---------------
PropLEDUnit::PropLEDUnit(ELEDNo eLedNo)
{
  //identifier
  if      (eLedNo == eLED1) m_eObj = eLEDUnit1;
  else if (eLedNo == eLED2) m_eObj = eLEDUnit2;
  else if (eLedNo == eLED3) m_eObj = eLEDUnit3;
  else      assert(0);

  // default waittime
  m_iLedWaitTime = 1000;

  assert (++iInstanceCounter <= 3); //max 3 instances of this class allowed (singleton)
}
PropLEDUnit::~PropLEDUnit()
{
  m_eObj = eCanObjMin;              //reset identifier
  assert (--iInstanceCounter < 3 ); //max 3 instances of this class allowed (singleton)
}
//-----------------------------
void PropLEDUnit::addLEDUnitListener(LEDUnitListener * h)
{
  addListenerToList(h);
}
void PropLEDUnit::removeLEDUnitListener(LEDUnitListener * h)
{
  removeListenerFromList(h);
}
//- JPau added 20090403 for setting the waittime 
void PropLEDUnit::setLedWaitTime ( int   iLedWaitTime )
{
  if(iLedWaitTime >= 0)
  {
    m_iLedWaitTime = iLedWaitTime;
  }
}

int   PropLEDUnit::getLedWaitTime()   { return m_iLedWaitTime;  };

