//---------------------------------------------------------------------------

#ifndef PropOverPressureSupplierH
#define PropOverPressureSupplierH
//---------------------------------------------------------------------------

#include "../pimcore/OverPressureSupplier.h"   //for inheritance (interface)
#include "PropPressurable.h"  //for inheritance (base-implementation)

class PropOverPressureSupplier:  virtual public OverPressureSupplier,  virtual public PropPressurable
{
  public:
     PropOverPressureSupplier();
    ~PropOverPressureSupplier();

    virtual void    addOverPressureSupplierListener(OverPressureSupplierListener * h);
    virtual void removeOverPressureSupplierListener(OverPressureSupplierListener * h);

  private:
    static int iInstanceCounter;   //for singleton behaviour test

};


#endif //PropOverPressureSupplierH
