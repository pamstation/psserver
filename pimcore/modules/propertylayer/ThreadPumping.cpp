//---------------------------------------------------------------------------


#pragma hdrstop

#include "ThreadPumping.h"   //for header file
#include "PropDisposable.h"  //for using object

#pragma package(smart_init)

ThreadPumping::ThreadPumping() {
    m_pParent = nullptr;
    m_eUpDown = eDropletUnknown;
    m_uWellBitSet = 0;
    m_sPumpTimeMs = -1;
}

void ThreadPumping::waitDone() {
    thread.join();
}

void ThreadPumping::start() {
    thread = std::thread([=]() {
        m_pParent->processPumpCommand(m_eUpDown, m_uWellBitSet, m_sPumpTimeMs);
    });
}


//---------------------------------------------------------------------------

