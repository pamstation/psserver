//---------------------------------------------------------------------------

#ifndef PropDisposableH
#define PropDisposableH
//---------------------------------------------------------------------------
#include <mutex>
#include "../utils/event.h"

#include "../pimcore/Disposable.h"           //for inheritance (interface)
#include "../systemlayer/SysChangeSource.h"  //for inheritance  (base-implementation)

class ThreadPumping;

class PropDisposable : virtual public Disposable, virtual public SysChangeSource {
public:
    PropDisposable();

    ~PropDisposable();

    //INTERFACE METHODS
    virtual void addDisposableListener(DisposableListener *h);

    virtual void removeDisposableListener(DisposableListener *h);

    virtual void pumpUp(unsigned uWellBitSet, signed sPumpTimeMs = -1);

    virtual std::future<int> pumpUpAsync(unsigned uWellBitSet, signed sPumpTimeMs = -1);

    virtual void pumpDown(unsigned uWellBitSet, signed sPumpTimeMs = -1);

    virtual std::future<int> pumpDownAsync(unsigned uWellBitSet, signed sPumpTimeMs = -1);


//    virtual void waitPumpReady(void);  //throws timeout-exception

    virtual EDropletPosition getDropletPosition() { return getDropletPositionProperty(); };

    virtual void getPumpUpSemaphore(void);

    virtual void releasePumpUpSemaphore(void);

    virtual void setFluidDownCallBack(void (*pFluidDownCallBack)(void));

    virtual void checkBrokenMembranes(unsigned uWellBitSet);    //throws exception
    virtual unsigned
    getBrokenMembranes(unsigned uWellBitSet);   //returns a (subset) wellbitset of "suspicious" membranes

    virtual void checkDisposablesLoaded(unsigned uWellBitSet); //throws PamStationException
    virtual unsigned getDisposablesLoaded(void);

    void processPumpCommand(EDropletPosition eUpDown, unsigned uWellBitSet,
                            signed sPumpTimeMs); //public, to be called by thread

    // need public access for pamservice
    void closeAllValves(void);   //closes the valves belonging to Well 1-12
    void openSelectedWellValves(unsigned uWellBitSet);   //opens the valves belonging to the selection of well 1-12
    void closeSelectedWellValves(unsigned uWellBitSet);   //close the valves belonging to the selection of well 1-12
    void openSelectedPressureValves(
            EDropletPosition eUpDown);   //opens the valves leading to the over- or underpressure supply
    void closeSelectedPressureValves(
            EDropletPosition eUpDown);   //close the valves leading to the over- or underpressure supply
    float getWellPressure(unsigned well);

private:
    int m_iCountPumpStart;
    int m_iCountPumpReady;
    static int iInstanceCounter;   //for singleton behaviour test

    void initDisposableMemberValues(ECanObj m_eObj);

    void terminateDisposableMemberValues(void);

    void handlePumpCommand(EDropletPosition eUpDown, unsigned uWellBitSet,
                           signed sPumpTimeMs); //redirection of interface call


//    void closeAllValves            (void                         );   //closes the valves belonging to Well 1-12
//    void openSelectedWellValves    (unsigned         uWellBitSet );   //opens the valves belonging to the selection of well 1-12
//    void openSelectedPressureValves(EDropletPosition eUpDown     );   //opens the valves leading to the over- or underpressure supply


    //PROPERTY SECTION
    //setters
    void setDropletPositionProperty(EDropletPosition eUpDown);

    //getters
    EDropletPosition getDropletPositionProperty() { return m_eDropletPosition; };
    //property
    EDropletPosition m_eDropletPosition;

    //listener events
    void onDropletPositionChanged(void);

    //WAIT READY SECTION
//    Event m_pWaitPumpReadyEvent;
    std::recursive_mutex m_PumpCommandSemaphore;

    //SYNCHRONISATIONS: SEMAPHORE  & CALLBACK
    std::recursive_mutex m_pDisposableSemaphore;

    void (*m_pOnFluidDownCallBack)(void);
    //void (__closure *m_pOnFluidDownCallBack)(void);


    //array's of canobjects
    ECanObj m_rgePressValveWell[eWellNoMax];  //array of valve can-objects
    ECanObj m_rgePressSensorWell[eWellNoMax];  //array of sensor can-objects
};

#endif  // PropDisposableH
