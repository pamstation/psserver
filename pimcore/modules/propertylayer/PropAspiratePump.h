//---------------------------------------------------------------------------

#ifndef PropAspiratePumpH
#define PropAspiratePumpH
//---------------------------------------------------------------------------

#include "../pimcore/AspiratePump.h"           //for inheritance (interface)
#include "PropSwitchable.h"  //for inheritance (base-implementation)

class PropAspiratePump: virtual public AspiratePump , virtual public PropSwitchable
{
  public:
     PropAspiratePump();
    ~PropAspiratePump();

    virtual void    addAspiratePumpListener(AspiratePumpListener * h);
    virtual void removeAspiratePumpListener(AspiratePumpListener * h);

    virtual void  checkAspiratePressureOk(void); //throw safetyexception
    virtual float   getAspiratePressure  (void);

  private:
    static int iInstanceCounter;   //for singleton behaviour test


};

#endif  // PropAspiratePumpH
