//---------------------------------------------------------------------------


#include <cassert>
#include "PropDisposable.h"   //header-file
#include "../pimcore/DisposableListener.h"     //for listener

#include "../systemlayer/CANController.h"         //for use of CANController
#include "../systemlayer/SysDefaultGenerator.h" //for using object
#include "../systemlayer/SysExceptionGenerator.h" //for passing Exception to ExceptionGenerator

#include "ThreadPumping.h"

#include <math.h>  //for fabs and pow
#include "../utils/util.h"
//---------------------------------------------------------------------------



int PropDisposable::iInstanceCounter = 0;


PropDisposable::PropDisposable() {
    m_eObj = eCtrlDisposable;                        //init identifier
    initDisposableMemberValues(m_eObj);            //init member variables

    ++iInstanceCounter;

    assert (iInstanceCounter == 1);  //only one instance of this class allowed (singleton)

    m_iCountPumpStart = 0;
    m_iCountPumpReady = 0;
}

PropDisposable::~PropDisposable() {
    terminateDisposableMemberValues();            //reset member variables
    m_eObj = eCanObjMin;                           //reset identifier

    assert (m_iCountPumpReady == m_iCountPumpStart);
    --iInstanceCounter;

    assert (iInstanceCounter == 0); //only one instance of this class allowed (singleton)
}

//---------
void PropDisposable::initDisposableMemberValues(ECanObj m_eObj) {
    assert (m_eObj == eCtrlDisposable);
    m_eDropletPosition = eDropletUnknown;

    //INIT SEMAPHORE
//    m_pDisposableSemaphore = nullptr;
//    m_pDisposableSemaphore = new TCriticalSection();
//    assert(m_pDisposableSemaphore);

    //INIT SYNCREAD CALLBACK
    m_pOnFluidDownCallBack = nullptr;

//    //INIT WAITREADY EVENT
//    m_pWaitPumpReadyEvent = nullptr;
//    m_pWaitPumpReadyEvent = new TSimpleEvent();
//    assert (m_pWaitPumpReadyEvent);
//    m_pWaitPumpReadyEvent.SetEvent(); //unblock

    //make array of valve CANObj's
    m_rgePressValveWell[eWell1] = eValveWell1;
    m_rgePressValveWell[eWell2] = eValveWell2;
    m_rgePressValveWell[eWell3] = eValveWell3;
    m_rgePressValveWell[eWell4] = eValveWell4;
    m_rgePressValveWell[eWell5] = eValveWell5;
    m_rgePressValveWell[eWell6] = eValveWell6;
    m_rgePressValveWell[eWell7] = eValveWell7;
    m_rgePressValveWell[eWell8] = eValveWell8;
    m_rgePressValveWell[eWell9] = eValveWell9;
    m_rgePressValveWell[eWell10] = eValveWell10;
    m_rgePressValveWell[eWell11] = eValveWell11;
    m_rgePressValveWell[eWell12] = eValveWell12;

    m_rgePressSensorWell[eWell1] = ePressSensWell1;
    m_rgePressSensorWell[eWell2] = ePressSensWell2;
    m_rgePressSensorWell[eWell3] = ePressSensWell3;
    m_rgePressSensorWell[eWell4] = ePressSensWell4;
    m_rgePressSensorWell[eWell5] = ePressSensWell5;
    m_rgePressSensorWell[eWell6] = ePressSensWell6;
    m_rgePressSensorWell[eWell7] = ePressSensWell7;
    m_rgePressSensorWell[eWell8] = ePressSensWell8;
    m_rgePressSensorWell[eWell9] = ePressSensWell9;
    m_rgePressSensorWell[eWell10] = ePressSensWell10;
    m_rgePressSensorWell[eWell11] = ePressSensWell11;
    m_rgePressSensorWell[eWell12] = ePressSensWell12;
}

void PropDisposable::terminateDisposableMemberValues(void) {
    m_eDropletPosition = eDropletUnknown;

    //TERMINATE SEMAPHORE
//    m_pDisposableSemaphore->Release(); //unblock
//    FREEPOINTER(m_pDisposableSemaphore);
    m_pDisposableSemaphore.unlock();

    //TERMINATE SYNCREAD CALLBACK
    m_pOnFluidDownCallBack = nullptr;

    //TERMINATE WAITREADY EVENT
//    m_pWaitPumpReadyEvent.SetEvent(); //unblock
//    FREEPOINTER(m_pWaitPumpReadyEvent);
}

//------------------------------------------------------------
void PropDisposable::addDisposableListener(DisposableListener *h) {
    addListenerToList(h);
}

void PropDisposable::removeDisposableListener(DisposableListener *h) {
    removeListenerFromList(h);
}

std::future<int> PropDisposable::pumpUpAsync(unsigned uWellBitSet, signed sPumpTimeMs) {
    return std::async(std::launch::async, [&] {
        pumpUp(uWellBitSet, sPumpTimeMs);
        return m_iCountPumpStart;
    });
}


//----------------------------------------------------------------
void PropDisposable::pumpUp(unsigned uWellBitSet, signed sPumpTimeMs) {
    handlePumpCommand(eDropletUp, uWellBitSet, sPumpTimeMs); //interface-call, do nothing just redirect
}

std::future<int> PropDisposable::pumpDownAsync(unsigned uWellBitSet, signed sPumpTimeMs) {
    return std::async(std::launch::async, [&] {
        pumpDown(uWellBitSet, sPumpTimeMs);
        return m_iCountPumpStart;
    });
}

//--
void PropDisposable::pumpDown(unsigned uWellBitSet, signed sPumpTimeMs) {
    handlePumpCommand(eDropletDown, uWellBitSet, sPumpTimeMs);//interface-call, do nothing just redirect
}

//---
void PropDisposable::handlePumpCommand(EDropletPosition eUpDown, unsigned uWellBitSet, signed sPumpTimeMs) {

    assert(eUpDown == eDropletDown || eUpDown == eDropletUp);

    std::lock_guard<std::recursive_mutex> guard(m_PumpCommandSemaphore);


    m_iCountPumpStart++;
    //0 reset waitPumpEvent
//    waitPumpReady();                     //wait earlier Pump actions are completed
//    m_pWaitPumpReadyEvent.ResetEvent(); //block


    processPumpCommand(eUpDown, uWellBitSet, sPumpTimeMs);


//    ThreadPumping *pThreadPumping = new ThreadPumping();  //create a new thread (FreeOnTerminate, so thread destroys itself after completion)
//    pThreadPumping->m_pParent = this;
//    pThreadPumping->m_eUpDown = eUpDown;
//    pThreadPumping->m_uWellBitSet = uWellBitSet;
//    pThreadPumping->m_sPumpTimeMs = sPumpTimeMs;
//
//    pThreadPumping->start();   //call from thread to: processPumpCommand( eUpDown,  uWellBitSet);
//    //handlePumpCommand can return, so Pump calls are non-blocking

}

//---
void PropDisposable::processPumpCommand(EDropletPosition eUpDown, unsigned uWellBitSet, signed sPumpTimeMs) {

    //1 close all valves
    closeAllValves();

    //2 select over- or underpressure (fUpDown)
    openSelectedPressureValves(eUpDown);

    //3 delay to allow pressure building up
    Util::Sleep(m_pDefaultGenerator->getPumpingDefaults()->uPressureSettlingDelay);

    //4 open selected valves
    openSelectedWellValves(uWellBitSet);

    //5 wait "valve_open_time"
    Util::Sleep(m_pDefaultGenerator->getPumpingDefaults()->uValveOpenTime);

    //6 close all valves
    closeAllValves();

    if (sPumpTimeMs < 0) {
        //7 give fluid time to move up or down
        Util::Sleep(m_pDefaultGenerator->getPumpingDefaults()->uFluidFlowDelay);
    } else {
        Util::Sleep(sPumpTimeMs);
    }

    //8 setDropletPosition Up or Down
    setDropletPositionProperty(eUpDown);

    //9 set waitPumpEvent
    m_iCountPumpReady++;
//    m_pWaitPumpReadyEvent.SetEvent(); //unblock
}

//-----------------START VALVE SECTION----------------------
void PropDisposable::closeAllValves(void) {
    for (int i = eWell1; i < eWellNoMax; i++)         //loop for all wells, i: 1,2,3,4,5,etc
    {
        m_pCAN->setTarget(m_rgePressValveWell[i], 0);  //close all wells
    }
    m_pCAN->setTarget(eValvePrsSelection, 0);        //close PressureSupply selection valve
    m_pCAN->setTarget(eValveVacSelection, 0);        //close VacuumSupply   selection valve
}

//--
void PropDisposable::openSelectedWellValves(unsigned uWellBitSet) {
    for (int i = eWell1; i < eWellNoMax; i++)   //loop for all wells, i: 1,2,3,4,5,etc
    {
        if ((1 << (i - 1)) & uWellBitSet)             //check if well is selected, 1<<(i-1): 1,2,4,8,16,etc
        {
            m_pCAN->setTarget(m_rgePressValveWell[i], 1);
        }
    }
}

void PropDisposable::closeSelectedWellValves(unsigned uWellBitSet) {
    for (int i = eWell1; i < eWellNoMax; i++)   //loop for all wells, i: 1,2,3,4,5,etc
    {
        if ((1 << (i - 1)) & uWellBitSet)             //check if well is selected, 1<<(i-1): 1,2,4,8,16,etc
        {
            m_pCAN->setTarget(m_rgePressValveWell[i], 0);
        }
    }
}

//--
void PropDisposable::openSelectedPressureValves(EDropletPosition eUpDown) {
    if (eUpDown == eDropletUp) m_pCAN->setTarget(eValvePrsSelection, 1);  //open PressureSupply selection
    else m_pCAN->setTarget(eValveVacSelection, 1);  //open VacuumSupply selection
}

void PropDisposable::closeSelectedPressureValves(EDropletPosition eUpDown) {
    if (eUpDown == eDropletUp) m_pCAN->setTarget(eValvePrsSelection, 0);  //open PressureSupply selection
    else m_pCAN->setTarget(eValveVacSelection, 0);  //open VacuumSupply selection
}
//-----------------END VALVE SECTION----------------------


//-----------------START BROKEN MEMBRANE SECTION----------------------
void PropDisposable::checkBrokenMembranes(unsigned uWellBitSet) {
    unsigned uBrokenMembraneWellBitSet = 0;

    //check if pressure in any of the given WellBitSet is too low on this moment
    uBrokenMembraneWellBitSet = getBrokenMembranes(uWellBitSet);

    if (uBrokenMembraneWellBitSet) // at least 1 broken membrane detected
    {
        //pass the uBrokenMembraneWellBitSet in case the "catcher" of the error wants to take action on it
        // f.e. proceed without using the suspicious wells
        m_pExceptionGenerator->onException(this, DISPOSABLE_BROKEN_MEMBRANE_ERROR, eErrorSafety,
                                           std::to_string(uBrokenMembraneWellBitSet));
    }
}

float PropDisposable::getWellPressure(unsigned well) {
    if (well >= eWellNoMax) return -42.0;
    if (well < eWell1) return -42.0;
    int iWellPressLU = m_pCAN->getActual(m_rgePressSensorWell[well]);
    return m_pDefaultGenerator->convertLUtoSI(m_rgePressSensorWell[well], iWellPressLU);
}

//----
unsigned PropDisposable::getBrokenMembranes(unsigned uWellBitSet) {
    unsigned uBrokenMembraneWellBitSet = 0;
    for (int i = eWell1; i < eWellNoMax; i++)  //loop for all wells, i: 1,2,3,4,5,etc
    {
        if ((1 << (i - 1)) & uWellBitSet)           //check if well is selected, 1<<(i-1): 1,2,4,8,16,etc
        {
            //get actual pressure in well (pressure could be over- or underpressure)
            int iWellPressLU = m_pCAN->getActual(m_rgePressSensorWell[i]);
            float rWellPressSI = m_pDefaultGenerator->convertLUtoSI(m_rgePressSensorWell[i], iWellPressLU);

            //compare with reference press
            float rBrknMembrRefPress = m_pDefaultGenerator->getPumpingDefaults()->rBrokenMembranePressure;
            if ((fabs(rWellPressSI)) < rBrknMembrRefPress) //use fabs because pressure is over- or under-pressure
            {
                uBrokenMembraneWellBitSet += pow(2, (i - 1)); //i-1, because WellBitSet is 0-based
            }
        }
    }
    //uBrokenMembraneWellBitSet contains the subset of the given uWellBitSet
    //where the pressure on moment of checking is below a preset value
    return uBrokenMembraneWellBitSet;
}
//-----------------END BROKEN MEMBRANE SECTION----------------------


//-------------------------------------------------------------------------
void PropDisposable::setDropletPositionProperty(EDropletPosition eUpDown) {
    if (m_eDropletPosition != eUpDown)               //on changes only
    {
        m_eDropletPosition = eUpDown;                  //change property
        onDropletPositionChanged();                    //notify listener
    }
}

void PropDisposable::onDropletPositionChanged() {
    std::lock_guard<std::recursive_mutex> guard(m_pcsListenerGuard);

    for (int index = 0; index < m_pListOfListeners.size(); index++) {
        EventHandler *pBase = (EventHandler *) m_pListOfListeners[index];
        DisposableListener *pDis = dynamic_cast<DisposableListener *>(pBase);
        pDis->onDropletPositionChanged(this);
    }
}

//------------------------------------------------------------------------------
//void PropDisposable::waitPumpReady(void) {
//    unsigned uMaximumWaitingTime = m_pDefaultGenerator->getPumpingDefaults()->uMaximumWaitingTime;
//
//    bool tResult = m_pWaitPumpReadyEvent.WaitFor(uMaximumWaitingTime);
//    if (!tResult) {
//        m_pExceptionGenerator->onException(this, DISPOSABLE_TIMEOUT, eErrorTimeOut, "");
//    }
//}

//-------------------------------------------------------------------------------
void PropDisposable::getPumpUpSemaphore(void) {
//    m_pDisposableSemaphore->Acquire(); //block

    m_pDisposableSemaphore.lock();
}

void PropDisposable::releasePumpUpSemaphore(void) {
    m_pDisposableSemaphore.unlock();
//    m_pDisposableSemaphore->Release();                      //unblock
    if (m_pOnFluidDownCallBack) m_pOnFluidDownCallBack();     //notify SyncReadStep that another pump-cycle has finished
}
//-------------------------------------------------------------------------------

void PropDisposable::setFluidDownCallBack(void(*pFluidDownCallBack)
        (void)) {
    m_pOnFluidDownCallBack = pFluidDownCallBack;
}

//-----START DISPOSABLES LOADED SECTION
void PropDisposable::checkDisposablesLoaded(unsigned WellBitSet) {

    bool fDisposable1Present = m_pCAN->getActual(eSensDispPresentLeft);  //contains Well1,2,3,4
    bool fDisposable2Present = m_pCAN->getActual(eSensDispPresentMiddle);  //contains Well5,6,7,8
    bool fDisposable3Present = m_pCAN->getActual(eSensDispPresentRight);  //contains Well9,10,11,12

    bool fUseWellOnDisp1 = (WellBitSet & 0x00F);     // xxxx xxxx xxxx & 0000 0000 1111  = 0000 0000 xxxx
    bool fUseWellOnDisp2 = (WellBitSet & 0x0F0);     // xxxx xxxx xxxx & 0000 1111 0000  = 0000 xxxx 0000
    bool fUseWellOnDisp3 = (WellBitSet & 0xF00);     // xxxx xxxx xxxx & 1111 0000 0000  = xxxx 0000 0000

    if (fUseWellOnDisp1 == true && fDisposable1Present == false ||
        fUseWellOnDisp2 == true && fDisposable2Present == false ||
        fUseWellOnDisp3 == true && fDisposable3Present == false) {
        m_pExceptionGenerator->onException(this, DISPOSABLE_NOT_LOADED, eErrorUnknown, "DISPOSABLE_NOT_LOADED");
    }
}

//-----
unsigned PropDisposable::getDisposablesLoaded(void) {
    bool fDisposable1Present = m_pCAN->getActual(eSensDispPresentLeft);  //contains Well 1, 2, 3, 4
    bool fDisposable2Present = m_pCAN->getActual(eSensDispPresentMiddle);  //contains Well 5, 6, 7, 8
    bool fDisposable3Present = m_pCAN->getActual(eSensDispPresentRight);  //contains Well 9,10,11,12

    return fDisposable1Present +      // 0 or 1
           2 * fDisposable2Present +      // 0 or 2
           4 * fDisposable3Present;      // 0 or 4
}
//----------------END DISPOSABLES LOADED SECTION------------------------------------------
