//---------------------------------------------------------------------------



#include <cassert>

#include "PropDispenseHeadHeater.h"  //for header-file
#include "../pimcore/DispenseHeadHeaterListener.h"    //for listener

#include "../pimcore/PamDefs_InitialValues.h" //for offset value
//---------------------------------------------------------------------------



 

int PropDispenseHeadHeater::iInstanceCounter=0;
//---------------
PropDispenseHeadHeater::PropDispenseHeadHeater()
{
  assert (++iInstanceCounter == 1); //only one instance of this class allowed (singleton)
  m_eObj = eCtrlDispenseHeadHeater; //identifier

  //the spec is for dispenseHEadTemperature = +0, -5 �C
  // to avoid any overshoot the InstrumentTarget is set to userTarget - offset.
  m_rOffsetValue = DISPHTR_OFFSET;

}
//--------------------
PropDispenseHeadHeater::~PropDispenseHeadHeater()
{
  m_eObj = eCanObjMin;              //reset identifier
  assert (--iInstanceCounter == 0); //only one instance of this class allowed (singleton)
}
//----------------------------------------------------------
void PropDispenseHeadHeater::addDispenseHeadHeaterListener(DispenseHeadHeaterListener * h)
{
  addListenerToList(h);
}
//--
void PropDispenseHeadHeater::removeDispenseHeadHeaterListener(DispenseHeadHeaterListener * h)
{
  removeListenerFromList(h);
}

//--------------------------
void  PropDispenseHeadHeater::setTargetValue(float rTargetValue)
{
  PropControllable::setTargetValue(rTargetValue + m_rOffsetValue);
}
float PropDispenseHeadHeater::getTargetValue()
{
  return ( PropControllable::getTargetValue() - m_rOffsetValue );
}
float PropDispenseHeadHeater::getActualValue()
{
  return ( PropControllable::getActualValue() - m_rOffsetValue );
}

float PropDispenseHeadHeater::getMinValue()
{
  return ( PropControllable::getMinValue() - m_rOffsetValue );
}
float PropDispenseHeadHeater::getMaxValue()
{
  return  ( PropControllable::getMaxValue() - m_rOffsetValue );
}
//- JPau added 20090403 for aborting the heating 
//This method is called by the StubPamStations updateProperties-loop
void PropDispenseHeadHeater::updateProperties(void)
{
  PropControllable::updateProperties();
   
//setTemperatureGainProperty( getTemperatureGain() );  //TempGain property updated once in the setter
}


/**/
