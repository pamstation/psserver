//---------------------------------------------------------------------------


#pragma hdrstop

#include <cassert>

#include "PropFocusPositioner.h"   //header-file
#include "../pimcore/FocusPositionerListener.h"    //for listener

#include "../systemlayer/SysDefaultGenerator.h"       //for using DefaultGenerator

//#include <Math.hpp> //for SameValue
#include "../utils/util.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)

int PropFocusPositioner::iInstanceCounter=0;
//---
PropFocusPositioner::PropFocusPositioner()
{
  assert (++iInstanceCounter == 1); //only one instance of this class allowed (singleton)
  m_eObj = eCtrlFocusPositioner;    //identifier
}
PropFocusPositioner::~PropFocusPositioner()
{
  m_eObj = eCanObjMin;              //reset identifier
  assert (--iInstanceCounter == 0); //only one instance of this class allowed (singleton)
}
//-----------------------------------------------------------------------------
void PropFocusPositioner::addFocusPositionerListener(FocusPositionerListener* h)
{
  addListenerToList(h);
}
void PropFocusPositioner::removeFocusPositionerListener(FocusPositionerListener* h)
{
  removeListenerFromList(h);
}
//---------------------------------------------------------------------------
void PropFocusPositioner::goPositionForFilterAndWell(EFilterNo eFilter, EWellNo eWell)
{
  handleGoPositionForFilterAndWell( eFilter, eWell ); //interface call, just redirect
}
void PropFocusPositioner:: handleGoPositionForFilterAndWell(EFilterNo eFilter, EWellNo eWell)
{
  setTargetValue( getPositionForFilterAndWell(eFilter,eWell) );
}
//------------------------------------------------------
bool PropFocusPositioner::getInPositionForFilterAndWell(EFilterNo eFilter, EWellNo eWell)
{
  bool fTrueFalse = Util::SameValue
                      (  getPositionForFilterAndWell(eFilter,eWell)                         ,  //[mm]
                         getActualValue()                                                   ,  //[mm]
                       ( m_pDefaultGenerator->getControllableSettings(m_eObj) )->rTolerance ); //[mm]

  return fTrueFalse ;  //only true on exact match
}
//----------------------------------------------------------
//float PropFocusPositioner::getPositionForFilterAndWell(EFilterNo eFilter, EWellNo eWell)
//{
//  float rReturnValue;
//
//  Focus_LogicalPos_t* ptStruct = m_pDefaultGenerator->getFocusLogicalPositions();
//
//  switch ( eFilter ) {
//    case eFilter1 : rReturnValue = ptStruct->rPosForFilter1; break;
//    case eFilter2 : rReturnValue = ptStruct->rPosForFilter2; break;
//    case eFilter3 : rReturnValue = ptStruct->rPosForFilter3; break;
//    default : assert( 0 );
//  }
//
//  switch ( eWell ) {
//    case eWell1 :
//    case eWell2 :
//    case eWell3 :
//    case eWell4 : rReturnValue += ptStruct->rOffsetForDisp1; break;
//    case eWell5 :
//    case eWell6 :
//    case eWell7 :
//    case eWell8 : rReturnValue += ptStruct->rOffsetForDisp2; break;
//    case eWell9 :
//    case eWell10 :
//    case eWell11 :
//    case eWell12 : rReturnValue += ptStruct->rOffsetForDisp3; break;
//    default : assert( 0 );
//  }
//
//  return rReturnValue;
//};

float PropFocusPositioner::getPositionForFilterAndWell(EFilterNo eFilter, EWellNo eWell)
{
  float rReturnValue;

  auto ptStruct = m_pDefaultGenerator->getFocusLogicalWellPositions();

  switch ( eFilter ) {
    case eFilter1 : rReturnValue = ptStruct->rPosForFilter1; break;
    case eFilter2 : rReturnValue = ptStruct->rPosForFilter2; break;
    case eFilter3 : rReturnValue = ptStruct->rPosForFilter3; break;
    default : assert( 0 );
  }

  switch ( eWell ) {
    case eWell1 : rReturnValue += ptStruct->rOffsetForWell1; break;
    case eWell2 : rReturnValue += ptStruct->rOffsetForWell2; break;
    case eWell3 : rReturnValue += ptStruct->rOffsetForWell3; break;
    case eWell4 : rReturnValue += ptStruct->rOffsetForWell4; break;
    case eWell5 : rReturnValue += ptStruct->rOffsetForWell5; break;
    case eWell6 : rReturnValue += ptStruct->rOffsetForWell6; break;
    case eWell7 : rReturnValue += ptStruct->rOffsetForWell7; break;
    case eWell8 : rReturnValue += ptStruct->rOffsetForWell8; break;
    case eWell9 : rReturnValue += ptStruct->rOffsetForWell9; break;
    case eWell10 : rReturnValue += ptStruct->rOffsetForWell10; break;
    case eWell11 : rReturnValue += ptStruct->rOffsetForWell11; break;
    case eWell12 : rReturnValue += ptStruct->rOffsetForWell12; break;
    default : assert( 0 );
  }

  return rReturnValue;
};



