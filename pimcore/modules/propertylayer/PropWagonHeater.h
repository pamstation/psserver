//---------------------------------------------------------------------------

#ifndef PropWagonHeaterH
#define PropWagonHeaterH
//---------------------------------------------------------------------------

#include "../pimcore/WagonHeater.h"         //for inheritance (interface)
#include "PropHeatable.h"  //for inheritance (base-implementation)

/**
* WagonHeater is a  wrapper around six heaters. The user only sees one wagonHeater,
* all calls are redirected and translated to 3 CoverHeaterCtrl and 3 BottomHeaterCtrl
* All instrument related values "belong" to actual hardware, so WagonHeater cannot contain
* instrument related data and therefore all values are in SI.
*
*/

class PamStation;
class PropWagonHeater:  virtual public WagonHeater,  virtual public PropHeatable
{
  public:
     PropWagonHeater();
    ~PropWagonHeater();

    virtual void     addWagonHeaterListener(WagonHeaterListener * h);
    virtual void  removeWagonHeaterListener(WagonHeaterListener * h);

    virtual void  checkUnloadTemperatureSafe(void);

    //OVERRIDE FROM CONTROLLABLE
    virtual void  setEnabled(bool fTrueFalse);
    virtual void  setTargetValue(float)      ; 
    virtual bool  getEnabled()               ; 
    virtual bool  getReady()                 ; 
    virtual float getTargetValue()           ; 
    virtual float getActualValue()           ; 
    virtual void  waitReady()                ; 
    virtual float getMinValue()              ; 
    virtual float getMaxValue()              ; 
    virtual bool  getErrorStatus()           ;  
    virtual int   getErrorNumber()           ;


    virtual void updateProperties(void)      ;  //override from controllable, not an interface function

    //OVERRIDE FROM HEATABLE
    virtual void  setTemperatureGain(float rTemperatureGain);
    virtual float getTemperatureGain()       ;
    virtual float getMinTemperatureGain()    ; 
    virtual float getMaxTemperatureGain()    ; 

    protected:
      void initWagonHeaterMemberValues();
      void terminateWagonHeaterMemberValues(void);

  private:
    static int iInstanceCounter;   //for singleton behaviour test
    PamStation* m_pPam ;


    //START PROPERTY SECTION (USING THE NECESSARY CONTROLLABLE & HEATABLE METHODS & PROPERTIES)
    //FOR IMPLEMENTING CONTROLLABLE & HEATABLE LISTENER FUNCTIONALITY
    //setters
    void  setReadyProperty          ( bool  fTrueFalse   );      //Ctrl-status bit1, has listener
    void  setTargetProperty         ( float rTargetValue );      // in [SI], has listener
    void  setActualProperty         ( float rActualValue );      // in [SI], has listener
    void  setTemperatureGainProperty( float rTemperatureGain );  // in [SI], has listener
    //properties
    bool  m_fReady;             //Ctrl-status bit1  , for listener-implementation
    float m_rTargetProperty;    //hardware status value , for listener-implementation
    float m_rActualProperty;    //hardware status value , for listener-implementation
    float m_rGainProperty   ;   //current user defined hardware status  , for listener-implementation
    //listener events
    void  onReadyChanged();
    void  onTargetValueChanged();
    void  onActualValueChanged();
    void  onTemperatureGainChanged();
    //END PROPERTY SECTION



    //CONVERSION SECTION :
    float m_rBottomOffset;  //offset for the BottomHeaters-group, for now (?) hardcoded in constructor
    float m_rCoverOffset ;  //offset for the CoverHeaters-group , for now (?) hardcoded in constructor




};
#endif //PropWagonHeaterH
