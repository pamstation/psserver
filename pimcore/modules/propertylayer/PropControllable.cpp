//---------------------------------------------------------------------------


#include <cassert>

#include "PropControllable.h"    //for header-file
#include "../pimcore/ControllableListener.h"      //for listener methods on"Property"Changed(this);

#include "../systemlayer/SysDefaultGenerator.h"   //for using DefaultGenerator
#include "../systemlayer/SysExceptionGenerator.h" //for passing Exception to ExceptionGenerator

#include "../systemlayer/CANController.h"          //for using CANController m_pCAN

//#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------


PropControllable::PropControllable() {
    initialise();
}

PropControllable::~PropControllable() {
    terminate();
}

void PropControllable::initialise() {
    fEnteredBefore = false; //indicates whether or not m_iResolution is calculated

    //---INITIALISE PROPERTIES-----
    //1a: set initial value hardware Properties * user defined defaults
    //used direct property access, because set-methods could invoke a listener
    m_iCtrlStatus = 128;   // an impossible value, but disabled, not ready, not homed, no error
    m_fEnabled = false;
    m_fReady = false;
    m_fHomed = false;    // m_fHomed is only used by the HomablePositionables
    m_iTargetProperty = -1000;    //[LU]
    m_iActualProperty = -1000;    //[LU]
    m_pWaitReadyEvent.SetEvent();

}

void PropControllable::terminate(void) {
    fEnteredBefore = false;
    m_pDefaultGenerator = nullptr;                 //destruct is done in PamStation

}

void PropControllable::stopWaitReady() {
    m_pWaitReadyEvent.SetEvent();
}
//------------------------------------------------------------------

void PropControllable::waitReady() {

    unsigned uMaxWaitTimeinMs = (m_pDefaultGenerator->getControllableSettings(m_eObj))->uWaitReadyTimeout;

    bool tResult = m_pWaitReadyEvent.WaitFor(uMaxWaitTimeinMs);

    if (!tResult) {
        std::string strDesc = "Ctrl Timeout, Obj: " + std::to_string(m_eObj) + ", Waited Time [ms]: " +
                              std::to_string(uMaxWaitTimeinMs);
        m_pExceptionGenerator->onException(this, CONTROLLABLE_TIMEOUT, eErrorTimeOut, strDesc);
    }
}

//INTERFACE GETTER's OF ANALOG SENSOR VALUES, CONTAIN "LU"->"SI" CONVERSION ROUNDING CONSEQUENCES
float PropControllable::getTargetValue() {
    double rReturn = m_pDefaultGenerator->convertLUtoSI(m_eObj, getTargetProperty());
    return rReturn;
}

float PropControllable::getActualValue() {
    double rReturn = m_pDefaultGenerator->convertLUtoSI(m_eObj, getActualProperty());
    return rReturn;
}


//BEGIN SECTION: TARGET VALUE
// 1: setTargetValue = the interface call, redirected to:
// 2: handleSetTargetValueCommand with:
//    -critical section
//    -range check
// 3: to be overridden.
//    - put simulation in stub-version (PropControllable)
//        - setEnabled (true)
//        - set target value
//        - reset ready property & ready Event
//        - set actual value
//        - set ready property & ready Event
//    - put actual hardware calls in CCMControllable

void PropControllable::setTargetValue(float rTargetValue) {
    //interface-call, do nothing just redirect
    handleSetTargetValueCommand(rTargetValue);
}

//-------
void PropControllable::handleSetTargetValueCommand(float rTargetValue) {
    //check range; getMinValue & getMaxValue return values in [SI](�C, �l, mm, barg etc)

    //use controllable::getMinValue, special for dispenseHeadHeater and other derived classes with offsets

    if (rTargetValue < PropControllable::getMinValue() ||
        rTargetValue > PropControllable::getMaxValue()) {
        std::string strDesc = "Ctrl Range Error , Obj: " + std::to_string(m_eObj) +
                              ", TargetValue: " + std::to_string(rTargetValue) +
                              ", MinValue: " + std::to_string(getMinValue()) +
                              ", MaxValue: " + std::to_string(getMaxValue());
        m_pExceptionGenerator->onException(this, CONTROLLABLE_RANGE_ERROR, eErrorRange, strDesc);
    }


    std::lock_guard<std::recursive_mutex> guard(m_pcsPropertyGuard);

    //setEnabled:
    // stops - if any- current movement, resets -if any- current error, enables servo
    handleSetEnabledCommand(true);

    //convert to LocalUnits "LU" [V], [EncoderPulses]
    int iTargetinLU = m_pDefaultGenerator->convertSItoLU(m_eObj, rTargetValue);

    //Send Msg to CANController
    //m_pCAN->setTarget(m_eObj, convertSItoLU(rTargetValue) );
    m_pCAN->setTarget(m_eObj, iTargetinLU);

    //synchronise:
    //It takes some time for the Can UpdateLoop to notice the controller is not ready anymore
    //waitReady doesn't block until that time
    //to prevent that the waitReadyEvent is resetted
    //and the status-property is set to an impossible value to force an update
    // (this takes also care for situation if controller was already ready)
    setStatusProperty(m_iCtrlStatus + 128);  //force update of StatusProperty
    m_pWaitReadyEvent.ResetEvent();           //block waitReady
}
//END SECTION: TARGET VALUE


//BEGIN SECTION: ENABLE
// 1: setEnabled  = the interface call, redirected to:
// 2: handleSetEnabledcommand
//      -critical section
// 3: to be overridden
//    - put simulation in stub-version (PropControllable)
//        - set Enabled property
//        - if true (1): targetValue = actualValue
//        - if true (2): ready property is true
//        - if true (3): ready event true
//        - if false(1): targetValue = NOTARGET
//        - if false(2): ready property is false
//        - if false(3): ready event is true
//    - put actual hardware calls in CCMControllable
void PropControllable::setEnabled(bool fTrueFalse) {
    //interface-call, do nothing just redirect
    handleSetEnabledCommand(fTrueFalse);
}

void PropControllable::handleSetEnabledCommand(bool fTrueFalse) {

    std::lock_guard<std::recursive_mutex> guard(m_pcsPropertyGuard);
    //Send Msg to CANController
    m_pCAN->setEnabled(m_eObj, fTrueFalse);

}
//END SECTION: ENABLE


//START UPDATE SECTION
//place here the methods to be called regulary
void PropControllable::updateProperties(void) {
    std::lock_guard<std::recursive_mutex> guard(m_pcsPropertyGuard);
    updateTargetProperty();
    updateActualProperty();
    updateStatusProperty();
    updateErrorNumber();

}

void PropControllable::updateTargetProperty() { setTargetProperty(m_pCAN->getTarget(m_eObj)); }

void PropControllable::updateActualProperty() { setActualProperty(m_pCAN->getActual(m_eObj)); }

void PropControllable::updateStatusProperty() { setStatusProperty(m_pCAN->getStatus(m_eObj)); }

void PropControllable::updateErrorNumber() { setErrorNumber(m_pCAN->getError(m_eObj)); }

//END UPDATE SECTION

//START SECTION PROPERTIES
//----
//NB: the LU -> SI conversion is done in the interface "getter"
//TargetProperty & ActualProperty are (int) [LU]
void PropControllable::setTargetProperty(int iTargetValue) {
    if (m_iTargetProperty != iTargetValue)  //only on change (even the smallest change)
    {
        m_iTargetProperty = iTargetValue;      //change property
        onTargetValueChanged();                //notify listener
    }
}

//-----
void PropControllable::setActualProperty(int iActualValue)     //[LU]
{
    int iChange = m_iActualProperty - iActualValue; //calculate delta

    if (iChange)                           //on changes only
    {
        m_iActualProperty = iActualValue;      //change property
        onActualValueChanged(iChange);       //notify listener
    }
}

//-------------------------------------------------------------
void PropControllable::setErrorNumber(int iErrorNumber) {
    if (m_iErrorNumber != iErrorNumber)    //only on change
    {
        m_iErrorNumber = iErrorNumber;         //change property
    }
}

//------------------------------------------------------------
void PropControllable::setStatusProperty(int iCtrlStatus) {
    if (m_iCtrlStatus != iCtrlStatus)         //on changes only
    {
        m_iCtrlStatus = iCtrlStatus;             //change property

        //split this into bits:
        bool fBit0_Enabled = (iCtrlStatus) & 0x01;  //bit0
        bool fBit1_Ready = (iCtrlStatus >> 1) & 0x01;  //bit1
        bool fBit2_Homed = (iCtrlStatus >> 2) & 0x01;  //bit2
        bool fBit3_Error = (iCtrlStatus >> 3) & 0x01;  //bit3

        setEnabledProperty(fBit0_Enabled);      //process bit0
        setReadyProperty(fBit1_Ready);      //process bit1
        setHomedProperty(fBit2_Homed);      //process bit2
        setErrorProperty(fBit3_Error);      //process bit3

        //process bit0 & bit1 interaction: (re)set m_pWaitReadyEvent
        if ((fBit0_Enabled) && (!fBit1_Ready)) m_pWaitReadyEvent.ResetEvent();  // controlled, but not ready
        else m_pWaitReadyEvent.SetEvent();    // not controlled

    }
}

//-------------------------------------------------------------
void PropControllable::setEnabledProperty(bool fTrueFalse) {
    if (m_fEnabled != fTrueFalse) {
        m_fEnabled = fTrueFalse;
    }
}

void PropControllable::setReadyProperty(bool fTrueFalse) {
    if (m_fReady != fTrueFalse) {
        m_fReady = fTrueFalse;
        onReadyChanged();
    }
}

void PropControllable::setHomedProperty(bool fTrueFalse) {
    if (m_fHomed != fTrueFalse)               //on changes only
    {
        m_fHomed = fTrueFalse;                  //change property
        onHomedChanged();                       //notify listener
    }
}

void PropControllable::setErrorProperty(bool fTrueFalse) {
    if (m_fError != fTrueFalse)               //on changes only
    {
        m_fError = fTrueFalse;                  //change property

        if (fTrueFalse) {
            //Throw ERROR here
            int iCtrlErrorNo = m_pCAN->getError(m_eObj);
            std::string strDesc =
                    "Ctrl error notification, Obj: " + std::to_string(m_eObj) + ", Error-code: " +
                    std::to_string(iCtrlErrorNo);
            m_pExceptionGenerator->onException(this, CONTROLLABLE_HW_ERROR_NOTIFICATION, eErrorUnknown, strDesc);
        }
    }
}


//interface properties from default
float PropControllable::getMinValue() {
    return m_pDefaultGenerator->getControllableSettings(m_eObj)->rMinProperty;
}

float PropControllable::getMaxValue() {
    return m_pDefaultGenerator->getControllableSettings(m_eObj)->rMaxProperty;
}


//END SECTION PROPERTIES

//START SECTION LISTENERS
//--Controllable listeners are added by the derived hardware classes--
//--implemented are listener-events on the following changes:
//  onReadyChanged
//  onTargetValueChanged
//  onActualValueChanged
//  pattern:
//  - 1 get Item from ListOfListeners
//  - 2 cast to ControllableListener*
//  - 3 call On"Property"Changed function
//  - 4 repeat for every item on list
void PropControllable::addControllableListener(ControllableListener *h) {
    addListenerToList(h);
}

void PropControllable::removeControllableListener(ControllableListener *h) {
    removeListenerFromList(h);
}

//---
void PropControllable::onReadyChanged() {

    std::lock_guard<std::recursive_mutex> guard(m_pcsListenerGuard);
    for (int index = 0; index < m_pListOfListeners.size(); index++) {
        auto *pBase = (EventHandler *) m_pListOfListeners[index];
        auto *pCtrl = dynamic_cast<ControllableListener *>(pBase);
        pCtrl->onReadyChanged(this);
    }

}

//---
void PropControllable::onHomedChanged() {

    std::lock_guard<std::recursive_mutex> guard(m_pcsListenerGuard);

    for (auto &m_pListOfListener : m_pListOfListeners) {
        auto *pCtrl = dynamic_cast<ControllableListener *>(m_pListOfListener);
        pCtrl->onHomedChanged(this);
    }

}

//--
void PropControllable::onTargetValueChanged() {

    std::lock_guard<std::recursive_mutex> guard(m_pcsListenerGuard);
    for (auto &m_pListOfListener : m_pListOfListeners) {
        auto *pCtrl = dynamic_cast<ControllableListener *>(m_pListOfListener);
        pCtrl->onTargetValueChanged(this);
    }

}

//--
void PropControllable::onActualValueChanged(int iChange) {

    std::lock_guard<std::recursive_mutex> guard(m_pcsListenerGuard);

    for (auto &m_pListOfListener : m_pListOfListeners) {
        //tolerance is the user defined controller-tolerance: the higest allowable difference from setpoint in [�C], [Barg], [mm], [�l]
        //a typical value for a heatable ctrl tolerance: � 0.5�C
        //the (listener)resolution is defined (hardcoded) as 1/4 tolerance, in this example: 0.5�C / 4 = 0.125 �C.
        //That means the listener triggers every 0.125�C
        //during run-time the actual comparison is done in (int) "LU" , so the (float) rResolution (e.g. 0.125�C) is converted to LU
        if (!fEnteredBefore) {
            assert (m_pDefaultGenerator);
            float rTolerance = (m_pDefaultGenerator->getControllableSettings(m_eObj))->rTolerance;
            float rResolution = rTolerance / 4;
            // user defined sensor-resolution in SI translated to LU
            m_iResolution = m_pDefaultGenerator->convertSItoLU(m_eObj,
                                                               rResolution);
            fEnteredBefore = true;
        }

        if (abs(iChange) > abs(m_iResolution)) {
            auto *pCtrl = dynamic_cast<ControllableListener *>(m_pListOfListener);
            pCtrl->onActualValueChanged(this);
        }
    }

}






