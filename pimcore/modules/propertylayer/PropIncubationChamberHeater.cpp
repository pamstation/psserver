//---------------------------------------------------------------------------


#pragma hdrstop
#include <cassert>
#include "PropIncubationChamberHeater.h"   //for header-file
#include "../pimcore/IncubationChamberHeaterListener.h"    //for listener
//---------------------------------------------------------------------------

#pragma package(smart_init)

int PropIncubationChamberHeater::iInstanceCounter=0;
//------------------
PropIncubationChamberHeater::PropIncubationChamberHeater(EIncubationChamberHeater eIncChmbrHtr)
{
  switch (eIncChmbrHtr){
  case eIncChmbrHtrCoverLeft   : m_eObj = eCtrlHtrCoverLeft    ; break;
  case eIncChmbrHtrCoverMiddle : m_eObj = eCtrlHtrCoverMiddle  ; break;
  case eIncChmbrHtrCoverRight  : m_eObj = eCtrlHtrCoverRight   ; break;

  case eIncChmbrHtrBottomLeft  : m_eObj = eCtrlHtrBottomLeft   ; break;
  case eIncChmbrHtrBottomMiddle: m_eObj = eCtrlHtrBottomMiddle ; break;
  case eIncChmbrHtrBottomRight : m_eObj = eCtrlHtrBottomRight  ; break;

  default          : assert(0);
  }

  assert (++iInstanceCounter <= 6);               //3 bottom + 3 cover heaters
}
PropIncubationChamberHeater::~PropIncubationChamberHeater()
{
  m_eObj = eCanObjMin;                            //reset identifier
  assert (--iInstanceCounter <= 5);               //3 bottom + 3 cover heaters
}
//---------
void PropIncubationChamberHeater::addIncubationChamberHeaterListener(IncubationChamberHeaterListener *h)
{
  addListenerToList(h);
}
void PropIncubationChamberHeater::removeIncubationChamberHeaterListener(IncubationChamberHeaterListener *h)
{
  removeListenerFromList(h);
}

