//---------------------------------------------------------------------------


#pragma hdrstop

//#include <iostream>
#include <cassert>
#include "PropWagonXYPositioner.h"    //header-file

#include "../systemlayer/CANController.h"         //for use of CANController
#include "../pimcore/PamStationFactory.h"              //for obtaining & using PamStation
#include "../pimcore/PamStation.h"                     //for obtaining & using PamStation
#include "../pimcore/AspirateHeadPositioner.h"         //for use of object
#include "../pimcore/WagonPositioner.h"                //for use of object
#include "../pimcore/WagonXYPositionerListener.h"      //for use of object
#include "../systemlayer/SysExceptionGenerator.h"      //for passing Exception to ExceptionGenerator
#include "../systemlayer/SysDefaultGenerator.h"        //for using DefaultGenerator
//#include <Math.hpp> //for SameValue
#include "../utils/util.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)

int PropWagonXYPositioner::iInstanceCounter = 0;

//------------------------
PropWagonXYPositioner::PropWagonXYPositioner() {
    initClassMembers();
    assert (++iInstanceCounter == 1);  //only one instance of this class allowed (singleton)
}

PropWagonXYPositioner::~PropWagonXYPositioner() {
    terminateClassMembers();
    assert (--iInstanceCounter == 0); //only one instance of this class allowed (singleton)
}

void PropWagonXYPositioner::initClassMembers() {
    //get a handle to the X&Y-transporters:
    PamStation *pPamStation = PamStationFactory::getCurrentPamStation();
    m_pX = pPamStation->getWagonPositioner(ePositionerX);
    m_pY = pPamStation->getWagonPositioner(ePositionerY);
    m_pAspHead = pPamStation->getAspirateHeadPositioner();

    m_eObjX = eCtrlTransporterX;
    m_eObjY = eCtrlTransporterY;
}

void PropWagonXYPositioner::terminateClassMembers() {
    m_pX = nullptr;   //actual destruct is done in PamStation
    m_pY = nullptr;   //actual destruct is done in PamStation
    m_pAspHead = nullptr;   //actual destruct is done in PamStation

}

//--------------------------
void PropWagonXYPositioner::addWagonXYPositionerListener(WagonXYPositionerListener *h) {
    addListenerToList(h);
}

void PropWagonXYPositioner::removeWagonXYPositionerListener(WagonXYPositionerListener *h) {
    removeListenerFromList(h);
}
//--------------------------

void PropWagonXYPositioner::goLoadPosition() {
    setTargetValue(getLoadPosition());
}

void PropWagonXYPositioner::goFrontPanelPosition() {
    setTargetValue(getFrontPanelPosition());
}

void PropWagonXYPositioner::goIncubationPosition() {
    setTargetValue(getIncubationPosition());
}

void PropWagonXYPositioner::goReadPosition(EWellNo eWell) {
    setTargetValue(getReadPosition(eWell));
}

void PropWagonXYPositioner::goAspiratePosition(EWellNo eWell) {
    setTargetValue(getAspiratePosition(eWell));
}

void PropWagonXYPositioner::goDispensePosition(EWellNo eWell, EDispenseHeadNo eHead) {
    setTargetValue(getDispensePosition(eWell, eHead));
}

void PropWagonXYPositioner::goPrimePosition(EDispenseHeadNo eHead) {
    setTargetValue(getPrimePosition(eHead));
}

void PropWagonXYPositioner::goFocusGlassPosition(EDisposableNo eDisposable) {
    setTargetValue(getFocusGlassPosition(eDisposable));
}

//------------------------------------------------
bool PropWagonXYPositioner::getInLoadPosition() {
    bool fXPosOk = Util::SameValue(m_pX->getActualValue(), getLoadPosition().X,
                             (m_pDefaultGenerator->getControllableSettings(m_eObjX))->rTolerance);
    bool fYPosOk = Util::SameValue(m_pY->getActualValue(), getLoadPosition().Y,
                             (m_pDefaultGenerator->getControllableSettings(m_eObjY))->rTolerance);

    return (fXPosOk && fYPosOk);
}

bool PropWagonXYPositioner::getInFrontPanelPosition() {
    bool fXPosOk = Util::SameValue(m_pX->getActualValue(), getFrontPanelPosition().X,
                             (m_pDefaultGenerator->getControllableSettings(m_eObjX))->rTolerance);
    bool fYPosOk = Util::SameValue(m_pY->getActualValue(), getFrontPanelPosition().Y,
                             (m_pDefaultGenerator->getControllableSettings(m_eObjY))->rTolerance);

    return (fXPosOk && fYPosOk);
}

bool PropWagonXYPositioner::getInIncubationPosition() {
    bool fXPosOk = Util::SameValue(m_pX->getActualValue(), getIncubationPosition().X,
                             (m_pDefaultGenerator->getControllableSettings(m_eObjX))->rTolerance);
    bool fYPosOk = Util::SameValue(m_pY->getActualValue(), getIncubationPosition().Y,
                             (m_pDefaultGenerator->getControllableSettings(m_eObjY))->rTolerance);

    return (fXPosOk && fYPosOk);
}

bool PropWagonXYPositioner::getInReadPosition(EWellNo eWell) {
    bool fXPosOk = Util::SameValue(m_pX->getActualValue(), getReadPosition(eWell).X,
                             (m_pDefaultGenerator->getControllableSettings(m_eObjX))->rTolerance);
    bool fYPosOk = Util::SameValue(m_pY->getActualValue(), getReadPosition(eWell).Y,
                             (m_pDefaultGenerator->getControllableSettings(m_eObjY))->rTolerance);

    return (fXPosOk && fYPosOk);
}

bool PropWagonXYPositioner::getInAspiratePosition(EWellNo eWell) {
    bool fXPosOk = Util::SameValue(m_pX->getActualValue(), getAspiratePosition(eWell).X,
                             (m_pDefaultGenerator->getControllableSettings(m_eObjX))->rTolerance);
    bool fYPosOk = Util::SameValue(m_pY->getActualValue(), getAspiratePosition(eWell).Y,
                             (m_pDefaultGenerator->getControllableSettings(m_eObjY))->rTolerance);

    return (fXPosOk && fYPosOk);
}

bool PropWagonXYPositioner::getInDispensePosition(EWellNo eWell, EDispenseHeadNo eHead) {
    bool fXPosOk = Util::SameValue(m_pX->getActualValue(), getDispensePosition(eWell, eHead).X,
                             (m_pDefaultGenerator->getControllableSettings(m_eObjX))->rTolerance);
    bool fYPosOk = Util::SameValue(m_pY->getActualValue(), getDispensePosition(eWell, eHead).Y,
                             (m_pDefaultGenerator->getControllableSettings(m_eObjY))->rTolerance);

    return (fXPosOk && fYPosOk);
}

bool PropWagonXYPositioner::getInPrimePosition(EDispenseHeadNo eHead) {
    bool fXPosOk = Util::SameValue(m_pX->getActualValue(), getPrimePosition(eHead).X,
                             (m_pDefaultGenerator->getControllableSettings(m_eObjX))->rTolerance);
    bool fYPosOk = Util::SameValue(m_pY->getActualValue(), getPrimePosition(eHead).Y,
                             (m_pDefaultGenerator->getControllableSettings(m_eObjY))->rTolerance);

    return (fXPosOk && fYPosOk);
}

bool PropWagonXYPositioner::getInFocusGlassPosition(EDisposableNo eDisposable) {
    bool fXPosOk = Util::SameValue(m_pX->getActualValue(), getFocusGlassPosition(eDisposable).X,
                             (m_pDefaultGenerator->getControllableSettings(m_eObjX))->rTolerance);
    bool fYPosOk = Util::SameValue(m_pY->getActualValue(), getFocusGlassPosition(eDisposable).Y,
                             (m_pDefaultGenerator->getControllableSettings(m_eObjY))->rTolerance);

    return (fXPosOk && fYPosOk);
}

//-------------------------------------------------------------------------
XYPos PropWagonXYPositioner::getLoadPosition() {
    return m_pDefaultGenerator->getWagonLogicalPositions()->xyLoadPosition;
}

XYPos PropWagonXYPositioner::getFrontPanelPosition() {
    return m_pDefaultGenerator->getWagonLogicalPositions()->xyFrontPanelPosition;
}

XYPos PropWagonXYPositioner::getIncubationPosition() {
    return m_pDefaultGenerator->getWagonLogicalPositions()->xyIncubationPosition;
}

XYPos PropWagonXYPositioner::getReadPosition(EWellNo eWell) {
    XYPos returnPosition;

    XYPos tReadPosition = m_pDefaultGenerator->getWagonLogicalPositions()->xyReadPosition;
    XYPos tOffset_Well = m_pDefaultGenerator->getWagonLogicalPositions()->xyOffset_Well;
    XYPos tOffset_Disposable = m_pDefaultGenerator->getWagonLogicalPositions()->xyOffset_Disposable;

    int iWellsOnDisposable = (eWellNoMax - 1) / (eDisposableNoMax - 1); // nr of wells per disposable
    int iCol = (eWell - 1) / iWellsOnDisposable;            // which disposable (0, 1, 2)
    int iRow = (eWell - 1) - (iWellsOnDisposable * iCol);            // which well-row   (0, 1, 2, 3)

    returnPosition.X = tReadPosition.X - (iRow * tOffset_Well.X);
    returnPosition.Y = tReadPosition.Y - (iCol * tOffset_Disposable.Y);

    return returnPosition;
}

XYPos PropWagonXYPositioner::getAspiratePosition(EWellNo eWell) {
    XYPos returnPosition;

    XYPos tAspiratePosition = m_pDefaultGenerator->getWagonLogicalPositions()->xyAspiratePosition;
    XYPos tOffset_Well = m_pDefaultGenerator->getWagonLogicalPositions()->xyOffset_Well;
    XYPos tOffset_Disposable = m_pDefaultGenerator->getWagonLogicalPositions()->xyOffset_Disposable;

    int iWellsOnDisposable = (eWellNoMax - 1) / (eDisposableNoMax - 1); // nr of wells per disposable
    int iCol = (eWell - 1) / iWellsOnDisposable;            // which disposable (0, 1, 2)
    int iRow = (eWell - 1) - (iWellsOnDisposable * iCol);            // which well-row   (0, 1, 2, 3)

    returnPosition.X = tAspiratePosition.X - (iRow * tOffset_Well.X);
    returnPosition.Y = tAspiratePosition.Y - (iCol * tOffset_Disposable.Y);

    return returnPosition;
}

XYPos PropWagonXYPositioner::getDispensePosition(EWellNo eWell, EDispenseHeadNo eHead) {
    XYPos returnPosition;

    XYPos tDispensePosition = m_pDefaultGenerator->getWagonLogicalPositions()->xyDispensePosition;
    XYPos tOffset_Well = m_pDefaultGenerator->getWagonLogicalPositions()->xyOffset_Well;
    XYPos tOffset_Disposable = m_pDefaultGenerator->getWagonLogicalPositions()->xyOffset_Disposable;

    int iWellsOnDisposable = (eWellNoMax - 1) / (eDisposableNoMax - 1); // nr of wells per disposable
    int iCol = (eWell - 1) / iWellsOnDisposable;            // which disposable (0, 1, 2)
    int iRow = (eWell - 1) - (iWellsOnDisposable * iCol);            // which well-row   (0, 1, 2, 3)

    float YOffset_DispenseHead;

    switch (eHead) {
        case eHead1:
            YOffset_DispenseHead = m_pDefaultGenerator->getWagonLogicalPositions()->xyOffset_DispenseHead1.Y;
            break;
        case eHead2:
            YOffset_DispenseHead = m_pDefaultGenerator->getWagonLogicalPositions()->xyOffset_DispenseHead2.Y;
            break;
        case eHead3:
            YOffset_DispenseHead = m_pDefaultGenerator->getWagonLogicalPositions()->xyOffset_DispenseHead3.Y;
            break;
        case eHead4:
            YOffset_DispenseHead = m_pDefaultGenerator->getWagonLogicalPositions()->xyOffset_DispenseHead4.Y;
            break;
        default    :
            assert(0);
    }

    returnPosition.X = tDispensePosition.X - (iRow * tOffset_Well.X);
    returnPosition.Y = tDispensePosition.Y - (iCol * tOffset_Disposable.Y) + YOffset_DispenseHead;

    return returnPosition;
}

XYPos PropWagonXYPositioner::getPrimePosition(EDispenseHeadNo eHead) {
    XYPos returnPosition;
    //start with m_tDispensePosition = Well1 under Head1
    //correct for offset Well1 to Prime1 or Prime2  (Head1&2 in Prime1, Head3&4 in Prime2)
    //correct for offset Head1 to Head2,3,4

    XYPos tDispensePosition = m_pDefaultGenerator->getWagonLogicalPositions()->xyDispensePosition;

    float YOffset_DispenseHead;
    float XOffset_Prime;
    float YOffset_Prime;

    switch (eHead) {
        case eHead1:
            YOffset_DispenseHead = m_pDefaultGenerator->getWagonLogicalPositions()->xyOffset_DispenseHead1.Y;
            XOffset_Prime = m_pDefaultGenerator->getWagonLogicalPositions()->xyOffset_Prime1.X; //dispenseHead1 primes in Prime1
            YOffset_Prime = m_pDefaultGenerator->getWagonLogicalPositions()->xyOffset_Prime1.Y; //dispenseHead1 primes in Prime1
            break;
        case eHead2:
            YOffset_DispenseHead = m_pDefaultGenerator->getWagonLogicalPositions()->xyOffset_DispenseHead2.Y;
            XOffset_Prime = m_pDefaultGenerator->getWagonLogicalPositions()->xyOffset_Prime1.X; //dispenseHead2 primes in Prime1
            YOffset_Prime = m_pDefaultGenerator->getWagonLogicalPositions()->xyOffset_Prime1.Y; //dispenseHead2 primes in Prime1
            break;
        case eHead3:
            YOffset_DispenseHead = m_pDefaultGenerator->getWagonLogicalPositions()->xyOffset_DispenseHead3.Y;
            XOffset_Prime = m_pDefaultGenerator->getWagonLogicalPositions()->xyOffset_Prime2.X; //dispenseHead3 primes in Prime2
            YOffset_Prime = m_pDefaultGenerator->getWagonLogicalPositions()->xyOffset_Prime2.Y; //dispenseHead3 primes in Prime2
            break;
        case eHead4:
            YOffset_DispenseHead = m_pDefaultGenerator->getWagonLogicalPositions()->xyOffset_DispenseHead4.Y;
            XOffset_Prime = m_pDefaultGenerator->getWagonLogicalPositions()->xyOffset_Prime2.X; //dispenseHead4 primes in Prime2
            YOffset_Prime = m_pDefaultGenerator->getWagonLogicalPositions()->xyOffset_Prime2.Y; //dispenseHead4 primes in Prime2
            break;
        default    :
            assert(0);
    }

    returnPosition.X = tDispensePosition.X - XOffset_Prime;
    returnPosition.Y = tDispensePosition.Y - YOffset_Prime + YOffset_DispenseHead;

    return returnPosition;
}


XYPos PropWagonXYPositioner::getFocusGlassPosition(EDisposableNo eDisposable) {
    XYPos returnPosition;

    XYPos tReadPosition = m_pDefaultGenerator->getWagonLogicalPositions()->xyReadPosition;
    XYPos tOffset_Disposable = m_pDefaultGenerator->getWagonLogicalPositions()->xyOffset_Disposable;
    XYPos tOffset_FocusGlass = m_pDefaultGenerator->getWagonLogicalPositions()->xyOffset_FocusGlass;

    returnPosition.X = tReadPosition.X - tOffset_FocusGlass.X;
    returnPosition.Y = tReadPosition.Y - ((eDisposable - 1) * tOffset_Disposable.Y);  //eDisposable is 1-based

    return returnPosition;
}


//--------------------------------------------------------------------
void PropWagonXYPositioner::checkCoverClosed(void) {
    if (!getCoverClosed())  //if cover is not closed
    {
        m_pExceptionGenerator->onException(this, WAGON_COVER_NOT_CLOSED, eErrorSafety, "WAGON_COVER_NOT_CLOSED");
    }
}

bool PropWagonXYPositioner::getCoverClosed(void) {
    return m_pX->getCoverClosed();//both objects have this method, use the X-positioner
}
//---------------------------------------

//HomablePositionable-like methods
void PropWagonXYPositioner::goHome() {
    //make sure aspirateHead is up
    m_pAspHead->setAspirateHeadPosition(eUp);
    m_pAspHead->waitReady();

    //check safe-to-move-conditions:
    m_pAspHead->checkAspirateHeadPositionerUp();
    this->checkCoverClosed();

    m_pX->goHome();
    m_pY->goHome();
}

bool PropWagonXYPositioner::getHomed() {
    return (m_pX->getHomed() && m_pY->getHomed());
}

//Controllable-like methods
void PropWagonXYPositioner::waitReady() {
    m_pX->waitReady();
    m_pY->waitReady();
    //m_pCAN->setTarget( ePowerOutSpare2 , 0 );
}

void PropWagonXYPositioner::setEnabled(bool fTrueFalse) {
    m_pX->setEnabled(fTrueFalse);
    m_pY->setEnabled(fTrueFalse);
}

void PropWagonXYPositioner::setTargetValue(XYPos xyPos) {
//    std::cout << "setTargetValue " << xyPos.X << "," << xyPos.Y << std::endl;

    //make sure aspirateHead is up
    m_pAspHead->setAspirateHeadPosition(eUp);
    m_pAspHead->waitReady();

    //check safe-to-move-conditions:
    m_pAspHead->checkAspirateHeadPositionerUp();
    this->checkCoverClosed();

    m_pX->setTargetValue(xyPos.X);              // [mm]
    m_pY->setTargetValue(xyPos.Y);              // [mm]
}

bool PropWagonXYPositioner::getEnabled() {
    return (m_pX->getEnabled() && m_pY->getEnabled());
}

bool PropWagonXYPositioner::getReady() {
    return (m_pX->getReady() && m_pY->getReady());
}

XYPos PropWagonXYPositioner::getTargetValue() {
    XYPos returnPosition;
    returnPosition.X = m_pX->getTargetValue();  // [mm]
    returnPosition.Y = m_pY->getTargetValue();  // [mm]
    return returnPosition;
}

XYPos PropWagonXYPositioner::getActualValue() {
    XYPos returnPosition;
    returnPosition.X = m_pX->getActualValue();  // [mm]
    returnPosition.Y = m_pY->getActualValue();  // [mm]
    return returnPosition;
}

XYPos PropWagonXYPositioner::getMinValue() {
    XYPos returnPosition;
    returnPosition.X = m_pX->getMinValue();     // [mm]
    returnPosition.Y = m_pY->getMinValue();     // [mm]
    return returnPosition;
}

XYPos PropWagonXYPositioner::getMaxValue() {
    XYPos returnPosition;
    returnPosition.X = m_pX->getMaxValue();     // [mm]
    returnPosition.Y = m_pY->getMaxValue();     // [mm]
    return returnPosition;
}

bool PropWagonXYPositioner::getErrorStatus() {
    return (m_pX->getErrorStatus() && m_pY->getErrorStatus());
}

XYPos PropWagonXYPositioner::getErrorNumber() {
    XYPos returnErrorNumbers;
    returnErrorNumbers.X = m_pX->getErrorNumber();  // a number from eError
    returnErrorNumbers.Y = m_pY->getErrorNumber();  // a number from eError
    return returnErrorNumbers;
}

//LISTENER_FUNCTIONALITY:
void PropWagonXYPositioner::updateProperties(void) {
    setTargetProperty(getTargetValue());  // in xyPos, [SI]
    setActualProperty(getActualValue());  // in xyPos, [SI]
    setReadyProperty(getReady());
    setHomedProperty(getHomed());
}

void PropWagonXYPositioner::setReadyProperty(bool fTrueFalse) {
    if (m_fReady != fTrueFalse)              //on changes only
    {
        m_fReady = fTrueFalse;               //set property
        onReadyChanged();                    //notify listener
    }
}

void PropWagonXYPositioner::setHomedProperty(bool fTrueFalse) {
    if (m_fHomed != fTrueFalse)               //on changes only
    {
        m_fHomed = fTrueFalse;                  //change property
        onHomedChanged();                       //notify listener
    }
}

void PropWagonXYPositioner::setTargetProperty(XYPos xyTargetValue) {
    if (m_xyTargetProperty.X != xyTargetValue.X ||
        m_xyTargetProperty.Y != xyTargetValue.Y)  //on changes only

    {
        m_xyTargetProperty.X = xyTargetValue.X;         //change property
        m_xyTargetProperty.Y = xyTargetValue.Y;

        onTargetValueChanged();                         //notify listener
    }
}

void PropWagonXYPositioner::setActualProperty(XYPos xyActualValue) {
    if (m_xyActualProperty.X != xyActualValue.X ||
        m_xyActualProperty.Y != xyActualValue.Y)  //on changes only

    {
        m_xyActualProperty.X = xyActualValue.X;         //change property
        m_xyActualProperty.Y = xyActualValue.Y;

        onActualValueChanged();                         //notify listener
    }

}

//-----------------------------------------
void PropWagonXYPositioner::onReadyChanged() {

    std::lock_guard<std::recursive_mutex> guard(m_pcsListenerGuard);

    for (int index = 0; index < m_pListOfListeners.size(); index++) {
        EventHandler *pBase = (EventHandler *) m_pListOfListeners[index];
        WagonXYPositionerListener *pSource = dynamic_cast<WagonXYPositionerListener *>(pBase);
        pSource->onReadyChanged(this);
    }

}

void PropWagonXYPositioner::onHomedChanged() {

    std::lock_guard<std::recursive_mutex> guard(m_pcsListenerGuard);

    for (int index = 0; index < m_pListOfListeners.size(); index++) {
        EventHandler *pBase = (EventHandler *) m_pListOfListeners[index];
        WagonXYPositionerListener *pSource = dynamic_cast<WagonXYPositionerListener *>(pBase);
        pSource->onHomedChanged(this);
    }

}

void PropWagonXYPositioner::onTargetValueChanged() {

    std::lock_guard<std::recursive_mutex> guard(m_pcsListenerGuard);

    for (int index = 0; index < m_pListOfListeners.size(); index++) {
        EventHandler *pBase = (EventHandler *) m_pListOfListeners[index];
        WagonXYPositionerListener *pSource = dynamic_cast<WagonXYPositionerListener *>(pBase);
        pSource->onTargetValueChanged(this);
    }

}

void PropWagonXYPositioner::onActualValueChanged() {

    std::lock_guard<std::recursive_mutex> guard(m_pcsListenerGuard);

    for (int index = 0; index < m_pListOfListeners.size(); index++) {
        EventHandler *pBase = (EventHandler *) m_pListOfListeners[index];
        WagonXYPositionerListener *pSource = dynamic_cast<WagonXYPositionerListener *>(pBase);
        pSource->onActualValueChanged(this);
    }

}


