//---------------------------------------------------------------------------


#pragma hdrstop
#include <cassert>
#include "PropUnderPressureSupplier.h"  //header-file
#include "../pimcore/UnderPressureSupplierListener.h"    //for listener

//---------------------------------------------------------------------------

#pragma package(smart_init)

int PropUnderPressureSupplier::iInstanceCounter=0;
//---------------
PropUnderPressureSupplier::PropUnderPressureSupplier()
{
  assert (++iInstanceCounter == 1);     //only one instance of this class allowed (singleton)
  m_eObj = eCtrlUnderPressureSupplier;  //init identifier
}
PropUnderPressureSupplier::~PropUnderPressureSupplier()
{
  m_eObj = eCanObjMin;                  //reset identifier
  assert (--iInstanceCounter == 0);     //only one instance of this class allowed (singleton)
}
//--------------------------
void PropUnderPressureSupplier::addUnderPressureSupplierListener(UnderPressureSupplierListener* h)
{
  addListenerToList(h);
}
void PropUnderPressureSupplier::removeUnderPressureSupplierListener(UnderPressureSupplierListener* h)
{
  removeListenerFromList(h);
}
