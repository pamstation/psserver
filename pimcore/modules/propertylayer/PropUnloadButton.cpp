//---------------------------------------------------------------------------


#pragma hdrstop

#include <cassert>
#include "PropUnloadButton.h"   //header-file
#include "../pimcore/UnloadButtonListener.h"     //for listener
//---------------------------------------------------------------------------

#pragma package(smart_init)

int PropUnloadButton::iInstanceCounter = 0;


PropUnloadButton::PropUnloadButton() {
    m_stateId = 0;
    m_eObj = eCtrlUnloadButton;                      //init identifier
    initUnloadButtonMemberValues(m_eObj);            //init member variables

    ++iInstanceCounter;
    assert (iInstanceCounter == 1);  //only one instance of this class allowed (singleton)

    m_fEnabled = false;
}


PropUnloadButton::~PropUnloadButton() {
    terminateUnloadButtonMemberValues();            //reset member variables
    m_eObj = eCanObjMin;                      //reset identifier
    --iInstanceCounter;
    assert (iInstanceCounter == 0); //only one instance of this class allowed (singleton)
}

int PropUnloadButton::getStateId() {
    return m_stateId;
}

void PropUnloadButton::initUnloadButtonMemberValues(ECanObj m_eObj) {
    //waitButtonPressed event
//    m_pWaitButtonPressedEvent = nullptr;
//    m_pWaitButtonPressedEvent = new TSimpleEvent();
//    assert (m_pWaitButtonPressedEvent);
    m_pWaitButtonPressedEvent.ResetEvent();  //block

}

void PropUnloadButton::terminateUnloadButtonMemberValues(void) {
//    assert (m_pWaitButtonPressedEvent);
    m_pWaitButtonPressedEvent.SetEvent();  //release
//    FREEPOINTER(m_pWaitButtonPressedEvent);
}

//----------------------------------------------------
void PropUnloadButton::addUnloadButtonListener(UnloadButtonListener *h) {
    addListenerToList(h);
}

void PropUnloadButton::removeUnloadButtonListener(UnloadButtonListener *h) {
    removeListenerFromList(h);
}

//--------------------------------------------------------------------
void PropUnloadButton::press(void) {
//    assert (m_pWaitButtonPressedEvent);

    if (m_fEnabled) m_pWaitButtonPressedEvent.SetEvent();  //release
    onPressed();                   //notify listener
}

void PropUnloadButton::onPressed(void) {

    std::lock_guard<std::recursive_mutex> guard(m_pcsListenerGuard);

    for (int index = 0; index < m_pListOfListeners.size(); index++) {
        EventHandler *pBase = (EventHandler *) m_pListOfListeners[index];
        UnloadButtonListener *pBut = dynamic_cast<UnloadButtonListener *>(pBase);
        pBut->onPressed(this);
    }

}

//-------------------------------------------------
void PropUnloadButton::waitPressedEvent(void) {
    //waitEvent blocks, but wakes up every second
    //waitEvent unblocks when button is pressed   = wrSignaled
    /* IN SIMULATION DON'T BLOCK*/
#ifdef _STUB_UNLOADBUTTON
    //don't block
#else
    bool tResult;  // wrSignaled, false, wrAbandoned, wrError
    int iWaitTimeout = 1000;
    do {
        tResult = m_pWaitButtonPressedEvent.WaitFor(iWaitTimeout);
    } while (!tResult);
#endif
}

void PropUnloadButton::setEnabled(bool fTrueFalse) {

    if (m_fEnabled != fTrueFalse)              //on changes only
    {
        m_fEnabled = fTrueFalse;                 //change property
        m_stateId++;

        if (m_fEnabled) m_pWaitButtonPressedEvent.ResetEvent();   //block
        else m_pWaitButtonPressedEvent.SetEvent();     //release

        if (fTrueFalse) onEnabled();             //notify listener , but only when enabled changes to true
    }
}

void PropUnloadButton::onEnabled(void) {

    std::lock_guard<std::recursive_mutex> guard(m_pcsListenerGuard);

    for (int index = 0; index < m_pListOfListeners.size(); index++) {
        EventHandler *pBase = (EventHandler *) m_pListOfListeners[index];
        UnloadButtonListener *pBut = dynamic_cast<UnloadButtonListener *>(pBase);
        pBut->onEnabled(this);
    }

}

bool PropUnloadButton::getEnabled(void) {
    return m_fEnabled;
}
