#ifndef WagonHeaterH
#define WagonHeaterH

#include "Heatable.h"        //for inheritance
class WagonHeaterListener;				      //for listener
/**
* @ingroup PimCoreHW  //see PamDefs_Defaults.h
*/
///use this class for synchronised controlling the disposable(s)-temperature using 3 TopHeaters and 3 Bottomheaters

/**
* This class is an interface between
* - the user-concept of one disposable Heater controlling the temperature of the disposable(s) present and
* - the hardware reality of 6 independent IncubationChamberHeater, grouped in a cover and a bottom heater for each of the 3 disposables
*
*
* Use this class for
* - controlling the disposable(s) temperature
* - controlling the disposable(s) temperature gain
* - waiting (CPU-friendly) untill all heaters are ready ( waitReady blocks if at least one heater is enabled & not ready)
* - checking if the Temperature is below a "safe-to-touch" preset value (UnloadTemperatureSafe)
*
* @throws PamStationSafetyException
* - if disposable temperature > human-safe temperature
* @throws PamStationTimeoutException
* - when trying to reach target temperature exceeds the default maximum time of one of the heaters
*
* Developpers note:
* - although the WagonHeater class is a wrapper around 6 independent heaters, it has the same interface for better usablility
* - to emulate the behaviour of the 6 independent heaters into one WagonHeater-behaviour, it's interface-values are calculated, based on the actual hardware values. See the comments for the member functions
* - requirement: the wagonHeater behaviour is independent of the number of loaded disposables, or the wagonHeater always controlls all incubateChamberHeaters
* - requirement: the top temperature must be higher than the bottom temperature to prevent condensation
* - requirement: because the top heaters don't have cooling, the bottom heaters must take care of targetTemperatures below "room-temperature"
* - requirement: one targetTemperature for the wagonHeater is split into setpoints for the 6 incubationChamberHeaters using an algoritm
* - requirement: when ready the six incubationChamberHeater-temperatures must be combined to one WagonHeater temperature. This value must be the same as the setpoint
* - requirement: although using heaters above and below the disposable, the wagonHEater must control the temperature of the disposable itself.
*/
class WagonHeater: virtual public Heatable
{
  public:
    virtual void     addWagonHeaterListener(WagonHeaterListener *h) = 0; ///<adds listener
    virtual void  removeWagonHeaterListener(WagonHeaterListener *h) = 0; ///<removes listener

    virtual void  checkUnloadTemperatureSafe(void)                  = 0;  ///<@throws PamStationSafetyException if disposable temperature > human-safe temperature  (Load_Unload_Settings_t::rSafeUnloadTemperature)

    //OVERRIDE FROM CONTROLLABLE
    virtual void  setEnabled(bool fTrueFalse) = 0;  ///<enables/disables all heaters
    virtual void  setTargetValue(float)       = 0;  ///<splits this parameter [�C] into a setpoint for the TopHeaters and a setpoint for the BottomHeaters [�C]
    virtual bool  getEnabled()                = 0;  ///<returns true if all heaters are enabled
    virtual bool  getReady()                  = 0;  ///<returns true if all heaters are ready
    virtual float getTargetValue()            = 0;  ///<checks if all TopHeaters and all BottomHeaters have the same setpoint, and calculates one targetValue using the targetProperty of the middle TopHeater and the middle BottomHeater
    virtual float getActualValue()            = 0;  ///<calculates one actualValue using the actualProperty of the middle TopHeater and the middle BottomHeater
    virtual void  waitReady()                 = 0;  ///<blocks if all heaters are (enabled & not Ready)
    virtual float getMinValue()               = 0;  ///<calculates one minValue using the minValueProperty of the middle TopHeater and the middle BottomHeater
    virtual float getMaxValue()               = 0;  ///<calculates one maxValue using the maxValueProperty of the middle TopHeater and the middle BottomHeater
    virtual bool  getErrorStatus()            = 0;  ///<returns true if at least one heater is in error
    virtual int   getErrorNumber()            = 0;  ///<returns controller error number

    virtual void updateProperties(void)       = 0;  //No Interface-method, override from controllable

    //OVERRIDE FROM HEATABLE
    virtual void  setTemperatureGain(float)   = 0;  ///<passes this parameter [�C] to all heaters
    virtual float getTemperatureGain()        = 0;  ///<checks if all heaters have the same temperatureGainProperty and returns the middle TopHeater's temperatureGainProperty [�C]
    virtual float getMinTemperatureGain()     = 0;  ///<checks if all heaters have the same preset minGainProperty and returns the middle TopHeater's minGainProperty [�C/min]. Take care when filling the ini-settings
    virtual float getMaxTemperatureGain()     = 0;  ///<checks if all heaters have the same preset maxGainProperty and returns the middle TopHeater's maxGainProperty [�C/min]. Take care when filling the ini-settings


};

#endif //WagonHeaterH

