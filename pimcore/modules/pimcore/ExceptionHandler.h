#ifndef ExceptionHandlerH
#define ExceptionHandlerH

#include "EventHandler.h"          //for inheritance
#include "PamStationException.h"   //for exception class
class ExceptionGenerator;						               //for listener

class ExceptionHandler : virtual public EventHandler
{
  public:
    virtual void onException(void * source, PamStationException * exception) = 0;
};

#endif //ExceptionHandlerH

