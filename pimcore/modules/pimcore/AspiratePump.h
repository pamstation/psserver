#ifndef AspiratePumpH
#define AspiratePumpH

#include "Switchable.h"    //for inheritance
class AspiratePumpListener;                //for listener


class AspiratePump : virtual public Switchable
{
public:

    //Adds/Removes the corresonding listener
    virtual void    addAspiratePumpListener(AspiratePumpListener * h) =0;  ///<adds listener
    virtual void removeAspiratePumpListener(AspiratePumpListener * h) =0;  ///<removes listener

    virtual void  checkAspiratePressureOk(void)      = 0; ///<@throws PamStationSafetyException when aspiratePressure not within preset range
    virtual float   getAspiratePressure  (void)      = 0; ///<returns AspiratePressure [barg]
};

#endif //AspiratePumpH
