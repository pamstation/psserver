#ifndef DisposableH
#define DisposableH

#include <future>
#include "ChangeSource.h" //for inheritance

class DisposableListener;                 //for listener
/**
* @ingroup PimCoreHW  //see PamDefs_Defaults.h
*/

///use this class for pumping up and down fluid in the wells

/**
* Class Disposable is used for controlling the droplet position in the wells.
* The OverPressureSupplier and UnderPressureSupplier guard the supply pressure levels,
* these Suppliers receive their setpoint during the LoadStep.
* The struct PumpSettings_t contains all necessary values for pumping.
*/

class Disposable : virtual public ChangeSource {
public:

    virtual void addDisposableListener(DisposableListener *h) = 0;  /**<adds listener   */
    virtual void removeDisposableListener(DisposableListener *h) = 0;  /**<removes listener */


//    virtual void waitPumpReady(
//            void) = 0;  /**<CPU-friendly way of waiting for PumpUp or PumpDown to finish. Throws PamStationTimeoutException when not finished within PumpSettings_t::uMaximumWaitingTime*/

    virtual EDropletPosition getDropletPosition() = 0;  /**<returns droplet position: Up, Down, Unknown*/

    virtual void getPumpUpSemaphore(void) = 0;  /**<acquires semaphore, for synchronisation with ReadNowStep*/
    virtual void releasePumpUpSemaphore(void) = 0;  /**<releases semaphore, for synchronisation with ReadNowStep*/

    virtual void setFluidDownCallBack(
            void ( *pFluidDownCallBack)(void)) = 0;  /**<adds CallBack, for synchronisation with SyncReadStep*/

    virtual void checkBrokenMembranes(
            unsigned uWellBitSet) = 0;  /**<throws PamStationSafetyException, when the well-pressure of the given wellbitset on moment of checking is below the preset value PumpSettings_t::rBrokenMembranePressure*/
    virtual unsigned getBrokenMembranes(
            unsigned uWellBitSet) = 0;  /**<returns the subset of the given wellbitset, where well-pressure of the given wellbitset on moment of checking is below the preset value PumpSettings_t::rBrokenMembranePressure*/

    virtual void checkDisposablesLoaded(
            unsigned uWellBitSet) = 0;  /**<throws PamStationException, on mismatch between detected disposables and adressed wells*/
    virtual unsigned getDisposablesLoaded(
            void) = 0;  /**<returns wellbitset representing the loaded disposables: range [0, 7] = [none, all 3 disposables] */

    /**
    * Pumps the droplets up (above the membrane) (Non-blocking call)
    * - Close All Valves,
    * - Open eValvePrsSelection,
    * - wait PumpSettings_t::uPressureSettlingDelay,
    * - OpenSelectedValves,
    * - wait PumpSettings_t::uValveOpenTime,
    * - CloseAllValves,
    * - wait PumpSettings_t::uFluidFlowDelay.
    */
    virtual void pumpUp(unsigned uWellBitSet, signed sPumpTimeMs = -1) = 0;

    virtual std::future<int> pumpUpAsync(unsigned uWellBitSet, signed sPumpTimeMs = -1) = 0;

    /**
    * Pumps the droplets up (above the membrane) (Non-blocking call)
    * - Close All Valves,
    * - Open eValveVacSelection,
    * - wait PumpSettings_t::uPressureSettlingDelay,
    * - OpenSelectedValves,
    * - wait PumpSettings_t::uValveOpenTime,
    * - CloseAllValves,
    * - wait PumpSettings_t::uFluidFlowDelay.
    */
    virtual void pumpDown(unsigned uWellBitSet, signed sPumpTimeMs = -1) = 0;
    virtual std::future<int> pumpDownAsync(unsigned uWellBitSet, signed sPumpTimeMs = -1) = 0;

};

#endif //DisposableH
