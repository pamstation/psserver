#ifndef ControllableListenerH
#define ControllableListenerH

#include "ChangeListener.h"  //for inheritance
class Controllable;                          //for listener-source

class ControllableListener : virtual public ChangeListener
{
  public:
    virtual void onReadyChanged      (Controllable * source) = 0;  ///<triggered by hardware-change. Ready = false when enabled, but not ready. All other cases: ready = true
    virtual void onTargetValueChanged(Controllable * source) = 0;  ///<triggered by hardware-change
    virtual void onActualValueChanged(Controllable * source) = 0;  ///<triggered by hardware-change, on each minimal (preset) change
    virtual void onHomedChanged      (Controllable * source) = 0;  ///<triggered by hardware-change
};


#endif //ControllableListenerH
