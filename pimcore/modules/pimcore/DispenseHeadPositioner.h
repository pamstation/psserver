#ifndef DispenseHeadPositionerH
#define DispenseHeadPositionerH

#include "Positionable.h" //for inheritance
class DispenseHeadPositionerListener;		  //for listener
/**
* @ingroup PimCoreHW  //see PamDefs_Defaults.h
*/
class DispenseHeadPositioner: virtual public Positionable
{
  public:
    virtual void    addDispenseHeadPositionerListener(DispenseHeadPositionerListener* h) = 0;  ///<adds listener
    virtual void removeDispenseHeadPositionerListener(DispenseHeadPositionerListener* h) = 0;  ///<removes listener

  //virtual void  checkAlmostEmpty()         = 0;  ///@throws PamStationSafetyException, if not enough fluid is present to finish the entire experiment
    virtual void  checkDispenseHeadPresent() = 0;  ///@throws PamStationException, if dispenseHead is not present when checking

  };

#endif //DispenseHeadPositionerH

