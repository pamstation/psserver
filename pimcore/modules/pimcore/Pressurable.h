#ifndef PRESSURABLE_H
#define PRESSURABLE_H


#include "Controllable.h" //for inheritance
class PressurableListener;                //for listener

class Pressurable : virtual public Controllable
{
  public:
    virtual void    addPressurableListener(PressurableListener* h) = 0; ///<adds listener (use only for test-reasons)
    virtual void removePressurableListener(PressurableListener* h) = 0; ///<removes listener (use only for test-reasons)
};

#endif //PRESSURABLE_H

