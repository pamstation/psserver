#ifndef PamDefs_DefaultsH
#define PamDefs_DefaultsH


  //definition of position vector, needed for logical wagon positions
  typedef struct {
  float X                       ;    /**< X-position in [mm] */
  float Y                       ;    /**< Y-position in [mm] */
  } XYPos;


/**
* \defgroup FactorySettings Factory Settings
*@{
*/

  /** 
  * Sensor_Calibration_Data_t contains factory settings for analog sensors, current sensors and encoders to convert:
  * - from SI ( [�C], [�C/min], [Barg], [mm], [�l], [A] ) to LU "Local Units" ( [V], [puls] and [-] ), see DefaultGenerator::convertSItoLU
  * - from LU to SI, see DefaultGenerator::convertLUtoSI
  *
  * This data is obtained from the relevant datasheets and possibly refined by calibration for the next sensors & encoders:
  *
  * TEMPERATURE SENSORS
  * -  eTempSensDispense
  * -  eTempSensCalibration
  * -  eTempSensBottomLeft
  * -  eTempSensBottomMiddle
  * -  eTempSensBottomRight
  * -  eTempSensCoverLeft
  * -  eTempSensCoverMiddle
  * -  eTempSensCoverRight
  *
  * UNDERPRESSURE SENSORS
  * -  ePressSensUnderPress
  * -  ePressSensAspirate
  *
  * OVERPRESSURE SENSORS
  * -  ePressSensOverPress
  *
  * OVER- and UNDERPRESSURE SENSORS
  * -  ePressSensWell1
  * -  ePressSensWell2
  * -  ePressSensWell3
  * -  ePressSensWell4
  * -  ePressSensWell5
  * -  ePressSensWell6
  * -  ePressSensWell7
  * -  ePressSensWell8
  * -  ePressSensWell9
  * -  ePressSensWell10
  * -  ePressSensWell11
  * -  ePressSensWell12
  *
  * ENCODERS
  * -  eEncDispenseHead1A
  * -  eEncDispenseHead2A
  * -  eEncDispenseHead3A
  * -  eEncDispenseHead4A
  * -  eEncFilterA
  * -  eEncFocusA
  * -  eEncTransXA
  * -  eEncTransYA
  *
  * CURRENT SENSORS
  * -  eCurrSensAspirateMotor
  * -  eCurrSensBottomLeft
  * -  eCurrSensBottomMiddle
  * -  eCurrSensBottomRight
  * -  eCurrSensCoverLeft
  * -  eCurrSensCoverMiddle
  * -  eCurrSensCoverRight
  * -  eCurrSensDispenseHeate
  * -  eCurrSensFilter
  * -  eCurrSensFocus
  * -  eCurrSensTransX
  * -  eCurrSensTransY
  *
  * Read this data using DefaultGenerator::getSensorCalibrationData\n
  * Write this data using DefaultGenerator::setSensorCalibrationData, during a flash-memory sequence
  *
  */
  typedef struct {
  ECanObj eSensorObj   ;    /**<Object id                                                                     */
  float   rSysOffset   ;    /**<calibration defined default value, (now) only used for heatables              */
  float   rSysGain     ;    /**<calibration defined default value, (now) only used for heatables              */
  float   rSysGain2    ;    /**<calibration defined default value, (now) only used for heatables              */
  float   rSensOffset  ;    /**<calibration (analog sensor) or hardware (encoder-type) defined default value  */
  float   rSensGain    ;    /**<calibration (analog sensor) or hardware (encoder-type) defined default value  */
  float   rSensGain2   ;    /**<calibration (analog sensor) or hardware (encoder-type) defined default value  */
  float   rADConversion;    /**<hardware (AD-concerter) defined value                                         */
  float   rTsample     ;    /**<ESW (controller sample rate) defined value                                    */
  } Sensor_Calibration_Data_t;

  /**
  * Controllable_Settings_t contains factory settings for the various controllers\n
  *
  * POSITIONABLE CONTROLLERS
  * - eCtrlAspirateHeadPositioner
  * - eCtrlDispenseHeadPositioner1
  * - eCtrlDispenseHeadPositioner2
  * - eCtrlDispenseHeadPositioner3
  * - eCtrlDispenseHeadPositioner4
  *
  * HOMABLE POSISITIONABLE CONTROLLERS
  * - eCtrlFilterPositioner
  * - eCtrlFocusPositioner
  * - eCtrlTransporterX
  * - eCtrlTransporterY
  *
  * PRESSURABLE CONTROLLERS
  * - eCtrlOverPressureSupplier
  * - eCtrlUnderPressureSupplier
  *
  * HEATABLE CONTROLLERS
  * - eCtrlDispenseHeadHeater
  * - eCtrlHtrCoverLeft
  * - eCtrlHtrCoverMiddle
  * - eCtrlHtrCoverRight
  * - eCtrlHtrBottomLeft
  * - eCtrlHtrBottomMiddle
  * - eCtrlHtrBottomRight
  *
  *
  * Read this data using DefaultGenerator::getControllableSettings\n
  * Write this data using DefaultGenerator::setControllableSettings, during a flash-memory sequence
  *
  */

  typedef struct {
  ECanObj   eCtrlObj             ;   /**<Controller id                                                                                                  */
  ECanObj   eSensObj             ;   /**<Sensor id where SI-to-LU-conversion factors (= calibrationdata) are to be stored, see DefaultGenerator::convertSItoLU */
  ECanObj   eCurrSensObj         ;   /**<Current Sensor id where Ampere-to-LU-conversion factors are to be stored, see DefaultGenerator::convertSItoLU */

  unsigned  uWaitReadyTimeout    ;   /**<in [ms]  ; ( see PamStationTimeoutException )                                                                  */
  int       iDefaultPwm          ;   /**<default PWM value (range [-200, 200]) for DispenseHeads, AspirateHead                                                                         */
  float     rMaxProperty         ;   /**<highest allowable setpoint in [�C], [Barg], [mm], [�l] ; ( see PamStationRangeException )                      */
  float     rMinProperty         ;   /**<lowest  allowable setpoint in [�C], [Barg], [mm], [�l] ; ( see PamStationRangeException )                      */
  float     rTrackingError       ;   /**highest allowable difference from setpoint in [�C] or [mm] during moving to targetValue for NIOS-positionables and heatables    */
  float     rTolerance           ;   /**<highest allowable difference from setpoint in [�C], [Barg], [mm], [�l], otherwise the controller should throw an Exception;
                                       * The ControllableListener::onActualValueChanged resolution is derived from this value                           */
  float     rMaxCurrent          ;   /**<highest allowable input in [A] for current-loop (if implemented)  */
  float     rMaxVelocity         ;   /**<highest allowable velocity (in [�C/ s] (heatables) or [mm/s] (NIOS-positionables) or not used ); ( see PamStationRangeException ) */
  float     rMinVelocity         ;   /**<lowest  allowable velocity (in [�C/ s] (heatables) or [mm/s] (NIOS-positionables) or not used ); ( see PamStationRangeException ) */
  float     rMaxAcceleration     ;   /**<highest allowable acceleration (in [�C/ s2] (heatables) or [mm/s2] (NIOS-positionables) or not used ); ( see PamStationRangeException ) */
  float     rMinAcceleration     ;   /**<lowest  allowable acceleration (in [�C/ s2] (heatables) or [mm/s2] (NIOS-positionables) or not used ); ( see PamStationRangeException ) */
  float     rDefaultVelocity     ;   /**<default           velocity (in [�C/ s] (heatables) or [mm/s] (NIOS-positionables) or not used );                                  */
  float     rDefaultAcceleration ;   /**<default           acceleration (in [�C/ s2] (heatables) or [mm/s2] (NIOS-positionables) or not used );                                  */
  unsigned  uSettleTime          ;   /**settle time in [ms] */


  int       sd_KPp               ;   /**P value of PID setpoint loop (if implemented for controller, else 0)*/
  int       sd_KIp               ;   /**I value of PID setpoint loop (if implemented for controller, else 0)*/
  int       sd_KDp               ;   /**D value of PID setpoint loop (if implemented for controller, else 0)*/
  int       sd_Integ_Lim_max     ;   /**Max I value of PID setpoint loop (if implemented for controller, else 0)*/
  int       sd_Integ_Lim_min     ;   /**Min I value of PID setpoint loop (if implemented for controller, else 0)*/
  int       KPi                  ;   /**P value of PI current loop (if implemented for controller, else 0)*/
  int       KIi                  ;   /**I value of PI current loop (if implemented for controller, else 0)*/
  float     DefaultVel           ;   /**default velocity in [LU]*/
  float     DefaultAcc           ;   /**default acceleration in [LU]*/

  int       sf_KPp               ;   /**scale factor for P value of PID setpoint loop (if implemented for controller, else 0)*/
  int       sf_KIp               ;   /**scale factor for I value of PID setpoint loop (if implemented for controller, else 0)*/
  int       sf_KDp               ;   /**scale factor for D value of PID setpoint loop (if implemented for controller, else 0)*/
  int       va_scale             ;   /**scale factor for velocity and acceleration*/
  int       T_Settletime         ;   /**settle time after setpoint generation is finished in [LU]*/

  unsigned  Allow_Homing         ;   /**homable or not*/
  int       Homing_tick_count    ;   /**number of interrupt tick to wait*/
  int       Homing_Speed_PWM     ;   /**home sensor seek speed [-200-200]*/
  int       Homing_Seeking_PWM   ;   /**index puls seek speed [-200-200]*/

  int       max_output           ;   /**max input value for current controller, [LU]*/
  int       min_output           ;   /**min input value for current controller, [LU]*/
  int       max_setpoint         ;   /**max for setpoint generator in [LU] */
  int       min_setpoint         ;   /**min for setpoint generator in [LU] */
  int       max_tracking_error   ;   /**max tracking error permitted when moving to setpoint, [LU]*/
  int       max_tolerance_error  ;   /**max tolerance error permitted in steady state, [LU]*/
  int       max_dc_i             ;   /**max dc value for current loop*/
  int       max_dc_pwm           ;   /**max current value for driver PWM*/
  float     du_max_vel           ;   /**max velocity in [LU], may never be zero */
  float     du_min_vel           ;   /**min velocity in [LU], may never be zero */
  float     du_max_acc           ;   /**max acceleration in [LU], may never be zero */
  float     du_min_acc           ;   /**min acceleration in [LU], may never be zero */
  } Controllable_Settings_t;


  /**
  * Wagon_LogicalXYPos_t contains logical positions (factory settings) for the WagonXYPositioner in [mm,mm]      \n
  * Read this data using DefaultGenerator::getWagonLogicalPositions   \n
  * Write this data using DefaultGenerator::setWagonLogicalPositions, during a flash-memory sequence
  *
  * These positions must be within the range set by the relevant Controllable_Settings_t::rMinProperty and Controllable_Settings_t::rMaxProperty for ECanObj::eCtrlTransporterX  and ECanObj::eCtrlTransporterY
  */                                                                                                                                                 
  typedef struct {
      XYPos xyLoadPosition        ;  /**<position for Well1 [mm,mm]                         */
      XYPos xyIncubationPosition  ;  /**<position for Well1 [mm,mm]                         */
      XYPos xyFrontPanelPosition  ;  /**<position for Well1 [mm,mm]                         */
      XYPos xyReadPosition        ;  /**<position for Well1 [mm,mm]                         */
      XYPos xyAspiratePosition    ;  /**<position for Well1 [mm,mm]                         */
      XYPos xyDispensePosition    ;  /**<position for Well1 and DispenseHead1  [mm,mm]      */
      XYPos xyOffset_Well         ;  /**<pitch between wells  [mm,0]                        */
      XYPos xyOffset_Disposable   ;  /**<pitch between disposables [0, mm]                  */
      XYPos xyOffset_FocusGlass   ;  /**<position of FocusGlass wrt top-well of disposable [mm,0] */
      XYPos xyOffset_DispenseHead1;  /**<position of Head1 wrt Head1 [mm,mm] = [0,0]        */
      XYPos xyOffset_DispenseHead2;  /**<position of Head2 wrt Head1 [0,mm]                 */
      XYPos xyOffset_DispenseHead3;  /**<position of Head3 wrt Head1 [0,mm]                 */
      XYPos xyOffset_DispenseHead4;  /**<position of Head4 wrt Head1 [0,mm]                 */
      XYPos xyOffset_Prime1       ;  /**<position of Prime1 wrt Well1  [mm,mm]              */
      XYPos xyOffset_Prime2       ;  /**<position of Prime2 wrt Well1  [mm,mm]              */
  } Wagon_LogicalXYPos_t;


  /** 
  * Filter_LogicalPos_t contains logical positions (factory settings) for the FilterPositioner in [mm]\n
  * Read this data using DefaultGenerator::getFilterLogicalPositions   \n
  * Write this data using DefaultGenerator::setFilterLogicalPositions, during a flash-memory sequence
  *
  * These positions must be within the range set by the Controllable_Settings_t::rMinProperty and Controllable_Settings_t::rMaxProperty for ECanObj::eCtrlFilterPositioner
  */
  typedef struct {
      float rPosFilter1;            /**<position of Filter1 [mm]  */
      float rPosFilter2;            /**<position of Filter2 [mm]  */
      float rPosFilter3;            /**<position of Filter3 [mm]  */
  } Filter_LogicalPos_t;


  /**
  * Focus_LogicalPos_t contains logical positions (factory settings) for the FocusPositioner\n
  * Read this data using DefaultGenerator::getFocusLogicalPositions\n
  * Write this data using DefaultGenerator::setFocusLogicalPositions, during a flash-memory sequence
  *
  * These positions must be within the range set by the Controllable_Settings_t::rMinProperty and Controllable_Settings_t::rMaxProperty for ECanObj::eCtrlFocusPositioner
  */
  typedef struct {
      float rPosForFilter1;         /**<focusposition for Filter1 [mm]             */
      float rPosForFilter2;         /**<focusposition for Filter2 [mm]             */
      float rPosForFilter3;         /**<focusposition for Filter3 [mm]             */
      float rOffsetForDisp1;        /**<offset for wells on disposable1 [mm]=[0]   */
      float rOffsetForDisp2;        /**<offset for wells on disposable1 [mm]       */
      float rOffsetForDisp3;        /**<offset for wells on disposable1 [mm]       */
  } Focus_LogicalPos_t;

typedef struct {
    float rPosForFilter1;         /**<focusposition for Filter1 [mm]             */
    float rPosForFilter2;         /**<focusposition for Filter2 [mm]             */
    float rPosForFilter3;         /**<focusposition for Filter3 [mm]             */
    float rOffsetForWell1;        /**<offset for well1 [mm]=[0]   */
    float rOffsetForWell2;
    float rOffsetForWell3;
    float rOffsetForWell4;
    float rOffsetForWell5;
    float rOffsetForWell6;
    float rOffsetForWell7;
    float rOffsetForWell8;
    float rOffsetForWell9;
    float rOffsetForWell10;
    float rOffsetForWell11;
    float rOffsetForWell12;
} Focus_LogicalWellPos_t;
  /**
  * LEDIntensity_t contains factory settings for LED PWM  \n
  * Read this data using DefaultGenerator::getLEDIntensity\n
  * Write this data using DefaultGenerator::setLEDIntensity, during a flash-memory sequence
  *
  * These PWM-values must be within the range [0-200%PWM]
  */
  typedef struct {
      unsigned uIntensity_LED1;    /**<PWM 0-200%, 0 is off, 200 is max intensity  */
      unsigned uIntensity_LED2;    /**<PWM 0-200%, 0 is off, 200 is max intensity  */
      unsigned uIntensity_LED3;    /**<PWM 0-200%, 0 is off, 200 is max intensity  */
  } LEDIntensity_t;

  /**
  * WagonHeater_Settings_t contains factory settings for the process calibration of the WagonHeater \n
  * The class WagonHeater is used to synchronise six IncubationChamberHeater s
  *
  * Tbottom is represented by the middle bottom heater  /n
  * Tcover  is represented by the middle cover  heater  /n
  * Tdisposable = a*Tbottom + b*Tcover + c*Tbottom^2 + d*Tcover^2 + e
  *
  * if  ( Tdisposable < rMinCoverTemp )  Tcover =  m_rMinCoverTemp
  * else                                 Tcover = Tbottom + rCoverBottomOffset;
  */
  typedef struct {
        float rBottomGain       ;  /**<a, 1st order gain for the BottomHeaters-group */
        float rCoverGain        ;  /**<b, 1st order gain for the CoverHeaters-group */
        float rBottomGain2      ;  /**<c, 2nd order gain for the BottomHeaters-group */
        float rCoverGain2       ;  /**<d, 2nd order gain for the CoverHeaters-group */
        float rOffset           ;  /**<e, offset */
        float rCoverBottomOffset;  /**<setpoint offset between Cover&Bottom to prevent evaporation */
        float rMinCoverTemp     ;  /**<to types of calculation are used depending on whether Disposable setpoint */
  } WagonHeater_Settings_t;

/*@} */  // end of defgroup FactorySettings Factory Settings

  /* NOT FOR DOXIGEN YET
  * DispenseHeadHeater_Settings_t contains factory settings for the process calibration of the WagonHeater
  * A process calibration will take the effects into account
  * of the difference in temperature of the fluid leaving the needle tip
  * and the DispenseHeadHeater temperature sensor
  *
  * Tneedle_tip_calc = rOffset + rGain* Tmeas + rGain2 * Tmeas^2
  */

  typedef struct {
        float rGain       ;  /*a, 1st order gain */
        float rGain2      ;  /*c, 2nd order */
        float rOffset     ;  /*e, offset */
  } DispenseHeadHeater_Settings_t;

//"PS12_NR_050501, SYCB_NR_FFFFFFFFFFF0, SYCB_FW_002, SYCB_ESW_003, SYCB_FLASH_20050523,
// PS12_NR_050501, ICCB_NR_FFFFFFFFFFF1, ICCB_FW_002, ICCB_ESW_003, ICCB_FLASH_20050523,
// PS12_NR_050501, WSCB_NR_FFFFFFFFFFF2, WSCB_FW_002, WSCB_ESW_003, WSCB_FLASH_20050523";

  /**
  * Each board contains serial numbers and software & firmware version numbers
  */
  typedef struct {
    int   iPS12_nr;       /**<Instrument serial number, set at manufacturing (0)YMMXX*/
    int   iBrd_nr_high;   /**<Board serial number, byte 5&4 from [5,4,3,2,1,0,]*/
    int   iBrd_nr_medium; /**<Board serial number, byte 3&2 from [5,4,3,2,1,0,]*/
    int   iBrd_nr_low;    /**<Board serial number, byte 1&0 from [5,4,3,2,1,0,]*/
    int   iFW_version;    /**<Firmware version number, hardcoded by programmer*/
    int   iESW_version;   /**<Embedded software version number, hardcoded by programmer*/
    float rFlashDate;     /**<Most recent FlashDate, set by pimcore, YYMMDD*/
  } BoardInfo_t;

  typedef struct {
    BoardInfo_t tICCBInfo;
    BoardInfo_t tSYCBInfo;
    BoardInfo_t tWSCBInfo;
  } PS12_Version_t;



/**
* \defgroup ProtocolRelatedData Protocol Related Data
*@{
*/

  /**
  * Load_Unload_Settings_t contains Protocol defaults to be used in the LoadStep and the UnloadStep \n
  * A set of instrument defaults is provide hardcoded (see SysDefaultGenerator::initialise_protocolSettings() ) and can be overwritten\n
  * Read this data using DefaultGenerator::getLoad_Unload_Defaults \n
  * Overwrite this data using DefaultGenerator::setLoad_Unload_Defaults, before each experiment
  *
  * These settings must be within the range set by the Controllable_Settings_t::rMinProperty and Controllable_Settings_t::rMaxProperty
  * for ECanObj::eCtrlOverPressureSupplier, ECanObj::eCtrlUnderPressureSupplier and IncubationChamberHeaters:
  * ECanObj::eCtrlHtrBottomLeft, ECanObj::eCtrlHtrBottomMiddle, ECanObj::eCtrlHtrBottomRight,
  * ECanObj::eCtrlHtrCoverLeft, ECanObj::eCtrlHtrCoverMiddle, ECanObj::eCtrlHtrCoverRight
  */
  typedef struct {
      float     rDefaultOverPressure  ;   /**<[barg], default setpoint for OverPressureSupplier   */
      float     rDefaultUnderPressure ;   /**<[barg], default setpoint for UnderPressureSupplier  */
      float     rInitWagonTemperature ;   /**<[�C], at the start of each experiment, the Wagon is first heated/cooled to this temperature */
      float     rSafeUnloadTemperature;   /**<[�C], wagonHeater::checkUnloadTemperatureSafe() throws a PamStationSafetyExceptionis if before unloading wagonHeater::getActualValue() > rSafeUnloadTemperature */
  } Load_Unload_Settings_t;

  /**
  * PumpSettings_t contains Protocol defaults used in the class Disposable for the PumpingStep, the (Manual)AspirateStep and the (Manual)DispenseStep\n
  * A set of instrument defaults is provide hardcoded (see SysDefaultGenerator::initialise_protocolSettings() ) and can be overwritten\n
  * Read this data using DefaultGenerator::getPumpingDefaults   \n
  * Overwrite this data using DefaultGenerator::setPumpingDefaults, before each experiment
  *
  * The setting for rBrokenMembranePressure must be between 0 and the Min( rDefaultOverPressure, abs(rDefaultUnderPressure) ), otherwise a BrokenMembraneException is thrown each Pump-action;
  */
  typedef struct {
      unsigned  uValveOpenTime         ;   /**<[ms], time valve should be open to allow pressure building up under the well   */
      unsigned  uPressureSettlingDelay ;   /**<[ms], time to allow selected pressure building up in the disposable system     */
      unsigned  uFluidFlowDelay        ;   /**<[ms], time to wait after closing the valves (before checking on broken membranes) */
      float     rBrokenMembranePressure;   /**<[barg], well (under- and over)pressures under this value are considered as broken membrane pressures\n
                                             * The setting for rBrokenMembranePressure must be between 0 and the Min( rDefaultOverPressure, abs(rDefaultUnderPressure) ), otherwise a BrokenMembraneException is thrown each Pump-action;*/
      unsigned  uMaximumWaitingTime    ;   /**<[ms], when Disposable::waitPumpReady() exceeds this time, an PamStationTimeoutException is thrown     */
  } PumpSettings_t;

  /**
  * AspirationSettings_t contains Protocol defaults to be used in the class AspiratePump for the AspirateStep\n
  * A set of instrument defaults is provide hardcoded (see SysDefaultGenerator::initialise_protocolSettings() ) and can be overwritten\n
  * Read this data using DefaultGenerator::getAspirationDefaults   \n
  * Overwrite this data using DefaultGenerator::setAspirationDefaults, before each experiment
  */
  typedef struct {
      float     rPressureUnderlimit    ;     /**<[barg], highest allowable underpressure-level. When encountering underpressures below this level, the apirate-tubing system is considered blocked (e.g.  -0.3 barg ) and a PamStationSafetyException is thrown */
      float     rPressureUpperLimit    ;     /**<[barg], lowest  allowable underpressure-level. When encountering underpressures above this level, the apirate-tubing system is considered leaking (e.g. - 0.03 barg) and a PamStationSafetyException is thrown */
      unsigned  uAspirateTubeCleanDelay;     /**<[ms], time aspiratePump should remain "on" after the actual aspiration of the well, to clean the apirate-tubing system */
      unsigned  uAspPressBuildupDelay  ;     /**<[ms], delay to wait after the pump is switched on, to allow the aspiratePressure to build up */
  } AspirationSettings_t;

  /**
  * DispenseSettings_t contains Protocol defaults to be used in the DispenseStep\n
  * A set of instrument defaults is provide hardcoded (see SysDefaultGenerator::initialise_protocolSettings() ) and can be overwritten\n
  * Read this data using DefaultGenerator::getDispenseDefaults   \n
  * Overwrite this data using DefaultGenerator::setDispenseDefaults, before each experiment
  *
  * These settings must be within the range set by the Controllable_Settings_t::rMinProperty and Controllable_Settings_t::rMaxProperty
  * for ECanObj::eCtrlDispenseHeadPositioner1, ECanObj::eCtrlDispenseHeadPositioner2, ECanObj::eCtrlDispenseHeadPositioner3, ECanObj::eCtrlDispenseHeadPositioner4
  */
  typedef struct {
      unsigned  uHeatableHeadBitSet;         /**<[-] , a bitset indicating which heads are heatable. (f.e. 0b0101 = 5 = Head1&3 are heatable) */
      float     rPrimeAmount       ;         /**<[�l], kind of default dispenseAmount, used in PrimeStep                                      */
      float     rRetractAmount     ;         /**<[�l], amount the syringe needs to retract to avoid dripping                                  */
      unsigned  uRetractDelay      ;         /**<[ms], delay between dispensing and retracting, to avoid dripping                             */
  } DispenseSettings_t;
/**@} */ //end of defgroup ProtocolRelatedData Protocol Related Data


/**
* \defgroup PimCoreHW PIMCore Hardware Classes
*/

#endif //PamDefs_DefaultsH
