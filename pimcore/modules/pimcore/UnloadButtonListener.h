#ifndef UnloadButtonListenerH
#define UnloadButtonListenerH

#include "ChangeListener.h" //for inheritance
class UnloadButton;					                //for listener

class UnloadButtonListener : virtual public ChangeListener
{
  public:
    virtual void onPressed(UnloadButton* source) = 0;  ///<triggered when user pushes button (uses press()-method)
    virtual void onEnabled(UnloadButton* source) = 0;  ///<triggered when button is enabled (and will react to the press()-method)
};

#endif //UnloadButtonListenerH

