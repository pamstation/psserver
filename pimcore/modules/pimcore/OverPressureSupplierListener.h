#ifndef OverPressureSupplierListenerH
#define OverPressureSupplierListenerH


#include "PressurableListener.h"   //for inheritance
class OverPressureSupplier;                        //for listener

class OverPressureSupplierListener : virtual public PressurableListener
{
};

#endif //OverPressureSupplierListenerH

