#ifndef WagonHeaterListenerH
#define WagonHeaterListenerH

#include "HeatableListener.h"     //for inheritance
class WagonHeater;						                   //for listener

class WagonHeaterListener : virtual public HeatableListener
{
};

#endif //WagonHeaterListenerH

