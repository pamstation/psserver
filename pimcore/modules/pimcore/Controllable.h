//-------------------------------------------------------------------------
#ifndef ControllableH
#define ControllableH


#include "ChangeSource.h"         //for inheritance
class ControllableListener;                       //for listener
//-------------------------------------------------------------------------
///base class for all hardware, except the CCD
/**
*  Base class for:
* - Heatable :
*   - DispenseHeadHeater [�C]
*   - WagonHeater [�C]
* - HomablePositionable:
*   - FilterPositioner [Filter1,2,3]
*   - FocusPositioner  [Filter1,2,3]
*   - WagonPositioner  [Logical Positions]
* - Positionable [logical positions]:
*   - AspirateHeadPositioner  [up,down]
*   - DispenseHeadPositioner  [�l]
* - Pressurable :
*   - OverPressureSupplier    [barg]
*   - UnderPressureSupplier   [barg]
*/
class Controllable : virtual public ChangeSource
{
  public:
    virtual void addControllableListener   (ControllableListener* h) = 0;  ///<adds listener (use only for test-reasons)
    virtual void removeControllableListener(ControllableListener* h) = 0;  ///<removes listener (use only for test-reasons)


    /**
    * CPU-friendly way of waiting, blocks if controller is enabled AND setpoint is not reached
    * @throws PamStationTimeoutException if wait time exceeds the preset maximum wait time
    */
    virtual void  waitReady()                         = 0;
    virtual void  stopWaitReady()                         = 0;

    /**
    * Property with listener-functionality
    * @retval ReadyProperty: false if setpoint is not reached
    */
    virtual bool  getReady()                          = 0;

    virtual void  setTargetValue(float rTargetValue)  = 0;  ///<sets setpoint. @throws PamStationRangeException if setpoint not in range
    virtual float getTargetValue()                    = 0;  ///<returns setpoint of controller. Property with listener-functionality

    virtual float getMinValue()                       = 0;  ///<returns setpoint's lower limit
    virtual float getMaxValue()                       = 0;  ///<returns setpoint's upper limit
    virtual float getActualValue()                    = 0;  ///<returns current value of controller. Property with listener-functionality

    virtual void setEnabled(bool fTrueFalse)          = 0;  ///<for enabling/initialising the servo or control-loop
    virtual bool getEnabled()                         = 0;  ///<returns current enabled-status of controller


    virtual bool  getErrorStatus()                    = 0;  ///<returns controller status
    virtual int   getErrorNumber()                    = 0;  ///<returns controller error number

};


#endif //ControllableH
