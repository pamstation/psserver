#ifndef HOMABLEPOSITIONABLE_H
#define HOMABLEPOSITIONABLE_H


#include "Controllable.h"  //for inheritance
class HomablePositionableListener;         //for listeners

class HomablePositionable : virtual public Controllable
{
  public:    
    virtual void    addHomablePositionableListener(HomablePositionableListener * h) = 0; ///<adds listener (use only for test-reasons)
    virtual void removeHomablePositionableListener(HomablePositionableListener * h) = 0; ///<removes listener (use only for test-reasons)

    virtual void goHome  (void)  = 0;  ///<moves device to its home-position and resets the encoder to nullptr
    virtual bool getHomed(void)  = 0;  ///<returns if the device is (already/still) homed. Property with listener-functionality
};

#endif //HOMABLEPOSITIONABLE_H
