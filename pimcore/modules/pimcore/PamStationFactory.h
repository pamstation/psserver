#ifndef PamStationFactoryH
#define PamStationFactoryH

class PamStation;
class PamStationFactory {

  public:
    
    static PamStation * getCurrentPamStation(); ///<creates a (singleton) instance of PamStation
    static void         shutdown();  ///<closes the PamStation instance and destructs it.

};


#endif //PamStationFactoryH

