#ifndef CCDH
#define CCDH

#include "ChangeSource.h" //for inheritance
class CCDListener;                        //for listener
class Image;
/**
* @ingroup PimCoreHW  //see PamDefs_Defaults.h
*/
/**
 * This class is representing the ccd-camera.
*/
class CCD : virtual public ChangeSource
{
public:

    //Adds/Removes the corresonding listener
    virtual void    addCCDListener(CCDListener * h) = 0;  ///<adds listener
    virtual void removeCCDListener(CCDListener * h) = 0;  ///<removes listener 

    virtual void setEnabled(bool fTrueFalse)        = 0;  ///<for enabling/initialising the CCD
    virtual bool getEnabled(void)                   = 0;  ///<returns current enabled-status of CCD

    virtual void        setExposureTime(unsigned rExpTime) = 0;  ///<sets exposureTime in [ms]. @throws PamStationRangeException, when setpoint exceeds (preset) maximum value
    virtual unsigned    getExposureTime(void)              = 0;  ///<returns current setpoint for exposureTime in [ms]. Property with listener-functionality
    virtual unsigned getMaxExposureTime(void)              = 0;  ///<returns (preset) upper limit for setpoint for exposureTime in [ms]

    virtual Image * acquireImage(void)               = 0; ///<command to acquire and process an Image. When ready the ImageGenerator-class raises an onImageAcquired event
//    virtual void waitAcquisitionDone(void)        = 0; ///<CPU-friendly way of waiting. @throws PamStationTimeoutException
    virtual void setImageInfo(EWellNo, EFilterNo) = 0; ///<passes information on current Well and Filter to add to Image


};

#endif //CCDH
