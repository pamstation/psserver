#ifndef DispenseHeadHeaterH
#define DispenseHeadHeaterH

#include "Heatable.h"     //for inheritance
class DispenseHeadHeaterListener;			//for listener
/**
* @ingroup PimCoreHW  //see PamDefs_Defaults.h
*/
class DispenseHeadHeater: virtual public Heatable
{
  public:
    virtual void    addDispenseHeadHeaterListener(DispenseHeadHeaterListener* h) = 0;  ///<adds listener
    virtual void removeDispenseHeadHeaterListener(DispenseHeadHeaterListener* h) = 0;  ///<removes listener

    //virtual bool isHeatable(EDispenseHeadNo eHeadNo)                              = 0; ///<returns if fluid in the selected dispenseHead is heatable
};

#endif //DispenseHeadHeaterH

