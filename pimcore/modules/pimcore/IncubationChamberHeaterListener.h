#ifndef IncubationChamberHeaterListenerH
#define IncubationChamberHeaterListenerH

#include "HeatableListener.h"     //for inheritance
class IncubationChamberHeater;						        //for source

class IncubationChamberHeaterListener : virtual public HeatableListener
{
};

#endif //IncubationChamberHeaterListenerH

