#ifndef PAMDEFS_H
#define PAMDEFS_H


//#include <vcl.h>
//#include <assert.h>

#include "PamDefs_Can.h"      //contains all CAN configuration data
#include "PamDefs_Defaults.h" //contains all Defaults configuration data

//macros
#define FREEPOINTER(x)   if (x)\
                         {\
                           delete x;\
                           x = nullptr;\
                         }
//HOMABLE POSITIONABLE SETTINGS (FILTER<FOCUS&WAGON)
#define HOMEPOS         0    //targetValue when homing



//START INSTRUMENT CONFIGURATION SETTINGS
    typedef enum
    {
      eHeadUnknown    = -1,
      eHeadMin        = 0,
      eHead1          = 1,
      eHead2             ,
      eHead3             ,
      eHead4             ,
      eHeadMax
    } EDispenseHeadNo;

    typedef enum
    {
      ePositionerUnknown    = -1,
      ePositionerMin        =  0,
      ePositionerX          =  1,
      ePositionerY              ,
      ePositionerMax
    } EWagonPositioner;


    typedef enum
    {
      eIncChmbrHtrUnknown     = -1,
      eIncChmbrHtrMin         =  0,
      eIncChmbrHtrCoverLeft   =  1,
      eIncChmbrHtrCoverMiddle     ,
      eIncChmbrHtrCoverRight      ,
      eIncChmbrHtrBottomLeft      ,
      eIncChmbrHtrBottomMiddle    ,
      eIncChmbrHtrBottomRight     ,
      eIncChmbrHtrMax
    } EIncubationChamberHeater;
    /*
    typedef struct {
    float X   ;
    float Y   ;
    } XYPos;
    */

    typedef enum
    {
      eAspHeadPosUnknown  = 0,
      eUp                 = 1,
      eDown                  ,
      eAspHeadPosMax
    } EAspirateHeadPosition;

    typedef enum
    {
      eFilterNoUnknown  = 0,
      eFilter1          = 1,
      eFilter2             ,
      eFilter3             ,
      eFilterNoMax
    } EFilterNo;

    typedef enum
    {
      eLEDNoUnknown  = -1,
      eLEDNoMin      =  0,
      eLED1          =  1,
      eLED2              ,
      eLED3              ,
      eLEDNoMax
    } ELEDNo;

    typedef enum
    {
      eWellNoUnknown  = 0,
      eWell1          = 1,
      eWell2             ,
      eWell3             ,
      eWell4             ,
      eWell5             ,
      eWell6             ,
      eWell7             ,
      eWell8             ,
      eWell9             ,
      eWell10            ,
      eWell11            ,
      eWell12            ,
      eWellNoMax
    } EWellNo;

    typedef enum
    {
      eDropletUnknown      = -1,
      eDropletMin          =  0,
      eDropletDown         =  1,
      eDropletUp              ,
      eDropletMax
    } EDropletPosition;

    typedef enum
    {
      eDisposableNoUnknown  = 0,
      eDisposable1          = 1,
      eDisposable2             ,
      eDisposable3             ,
      eDisposableNoMax
    } EDisposableNo;

//END INSTRUMENT CONFIGURATION SETTINGS


//-----ERROR HANDLING---------
    typedef enum
    {
      eErrorUnknown  = 0,
      eErrorRange    = 1,
      eErrorTimeOut  = 2,
      eErrorSafety      ,
      eErrorMax
    } EErrorType;


#define CONTROLLABLE_EXCEPTION_START            1000
#define CONTROLLABLE_TIMEOUT                    CONTROLLABLE_EXCEPTION_START+1
#define CONTROLLABLE_RANGE_ERROR                CONTROLLABLE_EXCEPTION_START+2
#define CONTROLLABLE_HW_ERROR_NOTIFICATION      CONTROLLABLE_EXCEPTION_START+3

//#define PRESSURABLE_EXCEPTION_START             1100

#define HEATABLE_EXCEPTION_START                1200
#define HEATABLE_RANGE_ERROR                    HEATABLE_EXCEPTION_START+1

//#define POSITIONABLE_EXCEPTION_START            1300
//#define HOMABLEPOSITIONABLE_EXCEPTION_START     1400
//#define SWITCHABLE_EXCEPTION_START              1500

#define ASPIRATEHEADPOSTIONER_EXCEPTION_START   2000
#define ASPIRATEHEADPOSTIONER_NOT_UP            ASPIRATEHEADPOSTIONER_EXCEPTION_START+1

#define ASPIRATEPUMP_EXCEPTION_START            2100
#define ASPIRATE_PRESSURE_NOT_OK                ASPIRATEPUMP_EXCEPTION_START+1

#define CCD_EXCEPTION_START                     2200
#define CCD_RANGE_ERROR                         CCD_EXCEPTION_START+1
#define CCD_TIMEOUT                             CCD_EXCEPTION_START+2
#define CCD_GET_DEVICE_LIST                     CCD_EXCEPTION_START+3
#define CCD_GET_DEVICE_COUNT                    CCD_EXCEPTION_START+4
#define CCD_OPEN_DEVICE                         CCD_EXCEPTION_START+5
#define CCD_SET_CAMERA_MODE                     CCD_EXCEPTION_START+6
#define CCD_SET_CALLBACK                        CCD_EXCEPTION_START+7
#define CCD_STOP                                CCD_EXCEPTION_START+8
#define CCD_SET_BUFFER_COUNT                    CCD_EXCEPTION_START+9
#define CCD_SET_INTEGRATION_TIME                CCD_EXCEPTION_START+10
#define CCD_RUN                                 CCD_EXCEPTION_START+11
#define CCD_GET_BUFFER                          CCD_EXCEPTION_START+12
#define CCD_RELEASE_BUFFER                      CCD_EXCEPTION_START+13
#define CCD_SET_BINNING                         CCD_EXCEPTION_START+14
#define CCD_SOFTTRIGGER                         CCD_EXCEPTION_START+15
#define CCD_CLOSEDEVICE                         CCD_EXCEPTION_START+16
#define CCD_STOP2                               CCD_EXCEPTION_START+17
#define CCD_DEVICELOST                          CCD_EXCEPTION_START+18
#define CCD_RESTART                             CCD_EXCEPTION_START+19
#define CCD_NULLIMAGE                           CCD_EXCEPTION_START+20
#define CCD_BUSRESET                            CCD_EXCEPTION_START+21
#define CCD_EXPOSURETIME                            CCD_EXCEPTION_START+22
#define CCD_SETTINGS                            CCD_EXCEPTION_START+23
#define CCD_IMAGE_FORMAT                            CCD_EXCEPTION_START+24

//#define CCD_COPY_BUFFER_DONE_TIMEOUT            CCD_EXCEPTION_START+13
//#define CCD_CALLBACK_DONE_TIMEOUT               CCD_EXCEPTION_START+14

//#define DISPENSEHEADHEATER_EXCEPTION_START      2300

#define DISPENSEHEADPOSITIONER_EXCEPTION_START  2400
#define DISPENSEHEAD_ALMOST_EMPTY               DISPENSEHEADPOSITIONER_EXCEPTION_START+1
#define DISPENSEHEAD_NOT_PRESENT                DISPENSEHEADPOSITIONER_EXCEPTION_START+2

#define DISPOSABLE_EXCEPTION_START              2500
#define DISPOSABLE_RANGE_ERROR                  DISPOSABLE_EXCEPTION_START+1
#define DISPOSABLE_TIMEOUT                      DISPOSABLE_EXCEPTION_START+2
#define DISPOSABLE_BROKEN_MEMBRANE_ERROR        DISPOSABLE_EXCEPTION_START+3
#define DISPOSABLE_NOT_LOADED                   DISPOSABLE_EXCEPTION_START+4

//#define FILTERPOSITIONER_EXCEPTION_START        2600
//#define FOCUSPOSITIONER_EXCEPTION_START         2700
//#define LEDUNIT_EXCEPTION_START                 2800
//#define OVERPRESSURESUPPLIER_EXCEPTION_START    2900
//#define UNDERPRESSURESUPPLIER_EXCEPTION_START   3000
//#define UNLOADBUTTON_EXCEPTION_START            3100

#define WAGONHEATER_EXCEPTION_START             3200
#define WAGONHEATER_UNLOAD_TEMPERATURE_NOT_SAFE WAGONHEATER_EXCEPTION_START+1

#define WAGONPOSITIONER_EXCEPTION_START         3300
#define WAGON_COVER_NOT_CLOSED                  WAGONPOSITIONER_EXCEPTION_START+1

#define CANINTERFACE_EXCEPTION_START            4000
#define CANINTERFACE_OPEN_CHANNEL               CANINTERFACE_EXCEPTION_START+1
#define CANINTERFACE_UNABLE_GET_POINTER         CANINTERFACE_EXCEPTION_START+2
#define CANINTERFACE_NOT_SJA1000                CANINTERFACE_EXCEPTION_START+3
#define CANINTERFACE_UNABLE_TO_INITIALISE       CANINTERFACE_EXCEPTION_START+4
#define CANINTERFACE_UNABLE_TO_CLOSE            CANINTERFACE_EXCEPTION_START+5
#define CANINTERFACE_UNABLE_TO_COUNT_MSG        CANINTERFACE_EXCEPTION_START+6
#define CANINTERFACE_UNABLE_TO_SEND             CANINTERFACE_EXCEPTION_START+7
#define CANINTERFACE_WAIT_TIMEOUT               CANINTERFACE_EXCEPTION_START+8
#define CANINTERFACE_NO_REPLY                   CANINTERFACE_EXCEPTION_START+9
#define CANINTERFACE_HANDLE_IS_NULL             CANINTERFACE_EXCEPTION_START+10
#define CANINTERFACE_REPLY_INCORRECT            CANINTERFACE_EXCEPTION_START+11

#define CANCONTROLLER_EXCEPTION_START           4100
#define CANCONTROLLER_ESW_ERROR_NOTIFICATION    CANCONTROLLER_EXCEPTION_START+1

#define PAMSTATION_EXCEPTION_START              4200
#define PAMSTATION_ABORT_CALLED                 PAMSTATION_EXCEPTION_START+1


//-------------------------------------------------
#endif //PAMDEFS_H

