#ifndef WagonPositionerH
#define WagonPositionerH

#include "HomablePositionable.h" //for inheritance
class WagonPositionerListener;		       //for listener

///use this class for controlling the X or the Y positioner  seperately

/**
* This class:
* - inherits from Controllable for controlling the X&Y positioners (seperately)
* - inherits from HomablePositionable for homing the X&Y positioners (seperately)
* - returns the cover status (open or closed)
*
* For moving the X&Y positioners synchronised, use WagonXYPositioner
*/

class WagonPositioner: virtual public HomablePositionable
{
  public:
    virtual void    addWagonPositionerListener(WagonPositionerListener* h) = 0;  ///<adds listener
    virtual void removeWagonPositionerListener(WagonPositionerListener* h) = 0;  ///<removes listener

  //virtual void checkCoverClosed(void)    = 0; ///<@throws PamStationSafetyException when cover not closed
    virtual bool   getCoverClosed(void)    = 0; ///<returns the cover status: Open(0) or Closed(1)









};

#endif //WagonPositionerH

