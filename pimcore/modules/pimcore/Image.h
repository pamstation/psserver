#ifndef ImageH
#define ImageH

#include <ctime>
#include <string>
#include "PamDefs.h"


/**
 * The image is acquired during the experiment. Contains the moment when it is acquired and other environment values.
 * Saves the image as a tiff-image. The tiff-file includes images dimensions, exposureTime, well, filter.
 */

class Image
{
  public:
    virtual ~Image() {};                           ///<needed for properly destructing Image-class

    /*POINTER TO ARRAY OF PIXELS*/
    virtual unsigned short* getArrayOfPixels()= 0;	///<returns a pointer to the raw image data. The Image-destructorfrees the array. Use duplicate() to obtain a copy of the complete Image

    /*INFO ON ARRAY OF PIXELS*/
    virtual int       getSizeX() 			        = 0;	///<returns the actual x-size of the image
    virtual int       getSizeY() 			        = 0;	///<returns the actual y-size of the image
    virtual int       getBitsPerPixel() 	    = 0;	///<returns the bits per pixel
  //virtual float     getGain() 			        = 0;	///<returns the photon gain of the CCD while acquiring this image.
  //virtual unsigned  getPixel(int x, int y)  = 0;  ///<returns the pixelvalue at position (x,y)

    /*MOMENT OF IMAGE ACQUISITION*/
    virtual std::time_t     getDate() 			        = 0;	///<returns the date at which the image was taken

    /*HARDWARE STATUS ON MOMENT OF IMAGE ACQUISITION (WELL&ILLUMINATION)*/
    virtual unsigned  getExposureTime() 	    = 0;  ///<returns the exposure time (in ms) used to acquire this image.
    virtual EFilterNo getFilter() 		        = 0;	///<returns the filter, with which the image was made
  //virtual float     getFocusPosition() 	    = 0;	///<returns the position (in [mm]) of the FocusMotor, when the image was made
    virtual EWellNo   getWell() 		          = 0;	///<returns the well-coordinate on which the image was made

    /*CALCULATE_STATISTICS_FROM_PIXEL_INFO*/
    virtual unsigned  getMinValue() 		      = 0;	///<returns the minimum value of the pixel values
    virtual unsigned  getMaxValue() 		      = 0;	///<returns the maximum value of the pixel values
    virtual unsigned  getAverageValue() 	    = 0;	///<returns the average light value of the pixels
    virtual unsigned  getSaturationLimit()    = 0;	///<returns the highest possible pixel value, that of saturated pixels*
    virtual unsigned  getNumSaturated() 	    = 0;	///<returns the number of saturated pixels
  //virtual float     getFocusValue() 		    = 0;	///<returns the sharpness of the pixel values
  virtual double getRMSContrast()=0;
  virtual double getSumDiff()=0;

    /*IMAGE HANDLING METHODS*/
    virtual Image*    duplicate()             = 0;  ///<must be used to get a copy of the acquired Image. The original is destructed by the PIMCore
    virtual void      save(std::string path) 	    = 0;  ///<saves (duplicated) image as a TIFF file
//    virtual TMemoryStream* createStreamOfPixels()  = 0;  ///<returns a copy of the ArrayOfPixels in MemoryStream-Format (byte-format). PIMCore will not free the memory
    //virtual void      calculateImageStatistics() = 0; //fills CALCULATE_STATISTICS_FROM_PIXEL_INFO

};
#endif //ImageH

    
     /**
     * Saves the image in a <b>standard tiff-file</b>. We use the following tags:
     *
     * Tag 254 - NewSubFileType
     * Tag 256 - ImageWitdth
     * Tag 257 - ImageLength
     * Tag 258 - BitsPerSample
     * Tag 259 - Compression
     * Tag 262 - PhotometricInterpertation
     * Tag 270 - ImageDescitption (328 byte long)
     * Tag 277 - SamplePerPixel
     * Tag 278 - RowsPerStrip
     * Tag 282 - XResolution
     * Tag 283 - YResolution
     * Tag 296 - ResolutionUnit
     * Tag 306 - DateTime
     *
     * @see <a href="http://www.libtiff.org">LibTiff Webside</a>
     *
     * @throws PamStationException
     * @param path Save path.
     */
