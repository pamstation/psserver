#ifndef PositionableH
#define PositionableH


#include "Controllable.h"  //for inheritance
class PositionableListener;                //for listeners

class Positionable : virtual public Controllable
{
  public:    
    virtual void    addPositionableListener(PositionableListener * h) = 0;  ///<adds listener (use only for test-reasons)
    virtual void removePositionableListener(PositionableListener * h) = 0;  ///<removes listener (use only for test-reasons)
};

#endif //PositionableH
