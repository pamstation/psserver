#ifndef SWITCHABLE_H
#define SWITCHABLE_H


#include "ChangeSource.h"       //for inheritance
class SwitchableListener;                       //for listener

class Switchable : virtual public ChangeSource
{

public:
    virtual void addSwitchableListener   (SwitchableListener* h) = 0; ///<adds listener  (use for test-reasons only)
    virtual void removeSwitchableListener(SwitchableListener* h) = 0; ///<removes listener (use for test-reasons only)

    virtual void setSwitchStatus(bool fTrueFalse)                = 0;  ///<switches device On or OFF
    virtual bool getSwitchStatus(void)                           = 0;  ///<returns device status (ON or OFF). Property with listener-functionality
};


#endif //SWITCHABLE_H
