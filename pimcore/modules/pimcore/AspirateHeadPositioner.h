#ifndef AspirateHeadPositionerH
#define AspirateHeadPositionerH

#include "Positionable.h"  //for inheritance
class AspirateHeadPositionerListener;      //for listener
/**
* @ingroup PimCoreHW  //see PamDefs_Defaults.h
*/
class AspirateHeadPositioner : virtual public Positionable
{
  public:
    virtual void    addAspirateHeadPositionerListener(AspirateHeadPositionerListener * h) =0;  ///<adds listener
    virtual void removeAspirateHeadPositionerListener(AspirateHeadPositionerListener * h) =0;  ///<removes listener

    virtual void setAspirateHeadPosition(EAspirateHeadPosition) = 0;  ///<goto a logical position (Up or Down)
    virtual EAspirateHeadPosition getAspirateHeadPosition(void) = 0;  ///<returns the logical position(Up, Down, Unknown).

    virtual void  checkAspirateHeadPositionerUp()               = 0; ///< @throws PamStationSafetyException if AspirateHead not Up (= Unknown or Down)

};

#endif //AspirateHeadPositionerH
