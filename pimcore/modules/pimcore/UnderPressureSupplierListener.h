#ifndef UnderPressureSupplierListenerH
#define UnderPressureSupplierListenerH


#include "PressurableListener.h"  //for inheritance
class UnderPressureSupplier;                      //for listener

class UnderPressureSupplierListener : virtual public PressurableListener
{
};


#endif //UnderPressureSupplierListenerH

