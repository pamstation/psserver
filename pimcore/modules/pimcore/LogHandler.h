#ifndef LogHandlerH
#define LogHandlerH

#include "EventHandler.h"     //for inheritance
class LogGenerator;						                //for listener

class LogHandler : virtual public EventHandler
{
public:
    virtual void logInfo(std::string aMsg) = 0;
    virtual void logError(std::string aMsg) = 0;
};

#endif //LogHandlerH

