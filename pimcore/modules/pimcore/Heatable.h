#ifndef HEATABLE_H
#define HEATABLE_H

#include "Controllable.h"        //for inheritance
class HeatableListener;					            //for listener

class Heatable : virtual public Controllable 
{
  public:
    virtual void    addHeatableListener(HeatableListener *h) = 0;  ///<adds listener (use only for test-reasons)
    virtual void removeHeatableListener(HeatableListener *h) = 0;  ///<removes listener (use only for test-reasons)

    virtual void  setTemperatureGain(float temperatureGain) = 0;   ///<sets temperature gain [�C/min]. throws PamStationRangeException if setpoint not in range
    virtual float getTemperatureGain()                      = 0;   ///<returns current setpoint [�C/min]. Property with listener-functionality
    virtual float getMinTemperatureGain()                   = 0;   ///<returns TemperatureGain setpoint lower limit [�C/min]
    virtual float getMaxTemperatureGain()                   = 0;   ///<returns TemperatureGain setpoint upper limit [�C/min]
};

#endif //HEATABLE_H

