#ifndef FOCUSPOSITIONER_H
#define FOCUSPOSITIONER_H

#include "HomablePositionable.h" //for inheritance
class FocusPositionerListener;		               //for listener
/**
* @ingroup PimCoreHW  //see PamDefs_Defaults.h
*/
class FocusPositioner: virtual public HomablePositionable
{
  public:
    virtual void    addFocusPositionerListener(FocusPositionerListener* h) = 0;         ///<adds listener
    virtual void removeFocusPositionerListener(FocusPositionerListener* h) = 0;         ///<removes listener

    virtual void     goPositionForFilterAndWell(EFilterNo eFilter, EWellNo eWell) = 0;  ///<goto this logical position
    virtual bool  getInPositionForFilterAndWell(EFilterNo eFilter, EWellNo eWell) = 0;  ///<checks if this logical position is reached
    virtual float   getPositionForFilterAndWell(EFilterNo eFilter, EWellNo eWell) = 0;  ///<returns the location of this logical position in [mm]
};

#endif //FOCUSPOSITIONER_H

