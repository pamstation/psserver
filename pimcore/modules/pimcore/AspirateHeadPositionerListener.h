#ifndef AspirateHeadPositionerListenerH
#define AspirateHeadPositionerListenerH

#include "PositionableListener.h" //for inheritance
class AspirateHeadPositioner;					            //for listener

class AspirateHeadPositionerListener : virtual public PositionableListener
{
public:
    //virtual void onAspirateHeadPositionChanged(AspirateHeadPositioner * source)   = 0; ///<typical sequence: Up=>Unknown=>Down=>Unknown=>Up
};

#endif //AspirateHeadPositionerListenerH

