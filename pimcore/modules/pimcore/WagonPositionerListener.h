#ifndef WagonPositionerListenerH
#define WagonPositionerListenerH

#include "HomablePositionableListener.h"  //for inheritance
class WagonPositioner;						    //for listener

class WagonPositionerListener : virtual public HomablePositionableListener
{
};

#endif //WagonPositionerListenerH

