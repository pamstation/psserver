#ifndef UnderPressureSupplierH
#define UnderPressureSupplierH

#include "Pressurable.h"   //for inheritance
class UnderPressureSupplierListener;       //for listener
/**
* @ingroup PimCoreHW  //see PamDefs_Defaults.h
*/

class UnderPressureSupplier : virtual public Pressurable
{
  public:
    //Adds & removes the UnderPressureSupplier listener
    virtual void    addUnderPressureSupplierListener(UnderPressureSupplierListener * h) = 0; ///<adds listener
    virtual void removeUnderPressureSupplierListener(UnderPressureSupplierListener * h) = 0; ///<removes listener
};

#endif //UnderPressureSupplierH
