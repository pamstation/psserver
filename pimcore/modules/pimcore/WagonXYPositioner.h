#ifndef WagonXYPositionerH
#define WagonXYPositionerH

#include "ChangeSource.h"

///use this class for synchronised controlling of the X&Y positioners to logical positions

/**
* This class is an interface between
* the user-concept of a WagonXYPositioner and
* the hardware reality of a seperate X and Y WagonPositioner
*
* Use this class for
* - synchronised controlling of the X&Y WagonPositioner to logical positions
* - synchronised homing of the X&Y WagonPositioner
* - waiting (CPU-friendly) untill both WagonPositioner are ready ( waitReady blocks if at least one controller is enabled & not ready)
* - getting the cover status (open or closed)
*
* @throws PamStationSafetyException
* - when cover is not closed before movement
* @throws PamStationTimeoutException
* - when trying to reach a logical position exceeds the default maximum time of either the X or the Y positioner (or both)
*/
class WagonXYPositionerListener;
/**
* @ingroup PimCoreHW  //see PamDefs_Defaults.h
*/
class WagonXYPositioner: virtual public ChangeSource
{
  public:
    virtual void    addWagonXYPositionerListener (WagonXYPositionerListener * h) = 0;
    virtual void removeWagonXYPositionerListener (WagonXYPositionerListener * h) = 0;

    virtual void  goLoadPosition          ()                              = 0;  ///<goto this logical position
    virtual bool  getInLoadPosition       ()                              = 0;  ///<checks if this logical position is reached
    virtual XYPos getLoadPosition         ()                              = 0;  ///<returns the location of this logical position in [mm]

    virtual void  goFrontPanelPosition    ()                              = 0;  ///<goto this logical position
    virtual bool  getInFrontPanelPosition ()                              = 0;  ///<checks if this logical position is reached
    virtual XYPos getFrontPanelPosition   ()                              = 0;  ///<returns the location of this logical position in [mm]

    virtual void  goIncubationPosition    ()                              = 0;  ///<goto this logical position
    virtual bool  getInIncubationPosition ()                              = 0;  ///<checks if this logical position is reached
    virtual XYPos getIncubationPosition   ()                              = 0;  ///<returns the location of this logical position in [mm]

    virtual void  goReadPosition          (EWellNo eWell)                 = 0;  ///<goto this logical position
    virtual bool  getInReadPosition       (EWellNo eWell)                 = 0;  ///<checks if this logical position is reached
    virtual XYPos getReadPosition         (EWellNo eWell)                 = 0;  ///<returns the location of this logical position in [mm]

    virtual void  goAspiratePosition      (EWellNo eWell)                 = 0;  ///<goto this logical position
    virtual bool  getInAspiratePosition   (EWellNo eWell)                 = 0;  ///<checks if this logical position is reached
    virtual XYPos getAspiratePosition     (EWellNo eWell)                 = 0;  ///<returns the location of this logical position in [mm]

    virtual void  goDispensePosition      (EWellNo eWell, EDispenseHeadNo eHead)  = 0; ///<goto this logical position
    virtual bool  getInDispensePosition   (EWellNo eWell, EDispenseHeadNo eHead)  = 0; ///<checks if this logical position is reached
    virtual XYPos getDispensePosition     (EWellNo eWell, EDispenseHeadNo eHead)  = 0; ///<returns the location of this logical position in [mm]

    virtual void  goPrimePosition         (EDispenseHeadNo eHead)         = 0;  ///<goto this logical position
    virtual bool  getInPrimePosition      (EDispenseHeadNo eHead)         = 0;  ///<checks if this logical position is reached
    virtual XYPos getPrimePosition        (EDispenseHeadNo eHead)         = 0;  ///<returns the location of this logical position in [mm]

    virtual void  goFocusGlassPosition    (EDisposableNo eDisposable)     = 0;  ///<goto this logical position
    virtual bool  getInFocusGlassPosition (EDisposableNo eDisposable)     = 0;  ///<checks if this logical position is reached
    virtual XYPos getFocusGlassPosition   (EDisposableNo eDisposable)     = 0;  ///<returns the location of this logical position in [mm]

    virtual void checkCoverClosed(void)    = 0; ///<@throws PamStationSafetyException when cover not closed
    virtual bool   getCoverClosed(void)    = 0; ///<returns the cover status: Open(0) or Closed(1)

    //HomablePositionable-like methods
    virtual void  goHome                  ()                              = 0;  ///<move X&Y transporters to their home and reset encoders (first X then Y)
    virtual bool  getHomed                ()                              = 0;  ///<checks if both transporters are homed

    //Controllable-like methods
    virtual void  waitReady               ()                              = 0;  ///<blocks when (both) controller enabled & not ready
    virtual void  setEnabled              (bool fTrueFalse)               = 0;  ///<enables/disabled both controllers
    virtual void  setTargetValue          (XYPos xyPos)                   = 0;  ///<splits target xyPos to seperate targets for both controllers
    virtual bool  getEnabled              ()                              = 0;  ///<returns true if both controllers are enabled, else false
    virtual bool  getReady                ()                              = 0;  ///<returns true if both controllers are ready  , else false
    virtual XYPos getTargetValue          ()                              = 0;  ///<returns target-XYPos
    virtual XYPos getActualValue          ()                              = 0;  ///<returns actual-XYPos
    virtual XYPos getMinValue             ()                              = 0;  ///<returns XYPos: (minX, minY), this could be an inaccessible position!!!
    virtual XYPos getMaxValue             ()                              = 0;  ///<returns XYPos: (maxX, maxY), this could be an inaccessible position!!!
    virtual bool  getErrorStatus          ()                              = 0;  ///<returns true if at least one of the WagonPositioner is in error
    virtual XYPos getErrorNumber          ()                              = 0;  ///<returns XYPos: (errornumber WagonPositioner X, errornumber WagonPositioner Y)
};

#endif //WagonXYPositionerH

