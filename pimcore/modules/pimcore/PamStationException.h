#ifndef PamStationException_H
#define PamStationException_H

#include <string>

#include "PamDefs.h"
// This class is the parent of the all exceptions stating PamStation related failures. 
class PamStationException
{
public:
    virtual ~PamStationException() {};
    virtual int getId()=0;         // Returns the unique identifier of the error.
    virtual void setId(int id)=0;  // Sets the unique error identifier
   
    virtual std::string getDescription()=0;                 // Returns the description of the failure. Should be Unicode compatible.
    virtual void setDescription(std::string description)=0; //Sets the description for the error

    virtual void * getSourceObject()=0;                  // Returns the stored object.
    virtual void setSourceObject(void * sourceObject)=0; // Stores the object, that caused the error

    virtual PamStationException* duplicate()=0;           // Returns a copy of this (should be deleted by the caller)
};
/*
class PamStationRangeException:   virtual public PamStationException
{ };
class PamStationTimeoutException: virtual public PamStationException
{};
class PamStationSafetyException:  virtual public PamStationException
{};

*/
#endif //PamStationException_H
