#ifndef DisposableListenerH
#define DisposableListenerH

#include "ChangeListener.h" //for inheritance
class Disposable;					                  //for listener

class DisposableListener : virtual public ChangeListener
{
  public:
    virtual void onDropletPositionChanged(Disposable* source) = 0;  ///<Typical sequence: UP=>Down=>Up... etc

};

#endif //DisposableListenerH

