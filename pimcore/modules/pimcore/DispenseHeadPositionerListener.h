#ifndef DispenseHeadPositionerListenerH
#define DispenseHeadPositionerListenerH

#include "PositionableListener.h"  //for inheritance
class DispenseHeadPositioner;						                 //for listener

class DispenseHeadPositionerListener : virtual public PositionableListener
{
};

#endif //DispenseHeadPositionerListenerH

