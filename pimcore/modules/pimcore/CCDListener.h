#ifndef CCDListenerH
#define CCDListenerH

#include "ChangeListener.h" //for inheritance
class CCD;					                        //for listener

class CCDListener : virtual public ChangeListener
{
  public:
    virtual void onExposureTimeChanged(CCD* source) = 0;  ///<triggered when user changes exposureTime

};

#endif //CCDListenerH

