#ifndef FILTERPOSITIONER_H
#define FILTERPOSITIONER_H

#include "HomablePositionable.h" //for inheritance
class FilterPositionerListener;				           //for listener
/**
* @ingroup PimCoreHW  //see PamDefs_Defaults.h
*/
class FilterPositioner: virtual public HomablePositionable
{
  public:
    virtual void    addFilterPositionerListener(FilterPositionerListener* h) = 0;  ///<adds listener
    virtual void removeFilterPositionerListener(FilterPositionerListener* h) = 0;  ///<removes listener

    virtual void     goPositionForFilter(EFilterNo eFilter) = 0;  ///<goto this logical position
    virtual bool  getInPositionForFilter(EFilterNo eFilter) = 0;  ///<checks if this logical position is reached
    virtual float   getPositionForFilter(EFilterNo eFilter) = 0;  ///<returns the location of this logical position in [mm]



};

#endif //FILTERPOSITIONER_H

