#ifndef POSITIONABLELISTENER_H
#define POSITIONABLELISTENER_H


#include "ControllableListener.h" //for inheritance
class Positionable;                               //for listener

class PositionableListener : virtual public ControllableListener
{
};


#endif //POSITIONABLELISTENER_H
