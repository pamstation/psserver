#ifndef DefaultGeneratorH
#define DefaultGeneratorH

#include <string>
#include "EventSource.h"   //for inheritance
//class DefaultHandler;                    //for listener

  /**
  * The struct Sensor_Calibration_Data_t contains all data to convert
  * - from a "SI"-unit [�C], [�C/min], [Barg], [mm]
  * - to a so called Local Unit "LU", [V], [puls] and back
  */
  /*
  typedef struct {
  ECanObj eObj       ;    ///<the obj id of the sensor concerned
  float rSysOffset   ;    ///<calibration defined default value, (now) only used for heatables
  float rSysGain     ;    ///<calibration defined default value, (now) only used for heatables
  float rSysGain2    ;    ///<calibration defined default value, (now) only used for heatables
  float rSensOffset  ;    ///<calibration (analog sensor) or hardware (encoder-type) defined default value
  float rSensGain    ;    ///<calibration (analog sensor) or hardware (encoder-type) defined default value
  float rSensGain2   ;    ///<calibration (analog sensor) or hardware (encoder-type) defined default value
  float rADConversion;    ///<hardware (AD-concerter) defined value
  float rTsample     ;    ///<ESW (controller sample rate) defined value
  } Sensor_Calibration_Data_t;
  */

  /**
  * The struct Controllable_Settings_t contains user defined settings for
  * - minimum value, maximum value: used in range checks, entering a value outside this limit will result in an RangeException
  * - default value: used in some occasions f.e PressureSupplier
  * - timeout value [msec]
  * - resolution: definable minimum sensor resolution in [�C], [Barg] or [mm]
  */
  /*
  typedef struct {
  ECanObj eObj                   ;    ///<the obj id of the controller concerned
  float     rMinProperty         ;   ///<lowest allowable setpoint in [�C], [Barg], [mm], [�l]
  float     rMaxProperty         ;   ///<higest allowable setpoint in [�C], [Barg], [mm], [�l]
  unsigned  uWaitReadyTimeout    ;   ///< in [ms]
  float     rResolution          ;   ///< ajustable sensor resolution in [�C], [Barg], [mm], [�l], only changes BIGGER THAN this value
  } Controllable_Settings_t;
  */

class DefaultGenerator: virtual public EventSource
{
 public:

  virtual int   convertSItoLU(ECanObj eCanObj, float rValueInSI)  =0;   ///<input "SI": �C, barg, �l, mm, returns "LU": V, puls
  virtual float convertLUtoSI(ECanObj eCanObj, int   iValueInLU)  =0;   ///<input "LU": V, puls, returns "SI": �C, barg, �l, mm
  virtual int   CalculateRootQuadraticEquation (double& rX1, double& rX2, double& rA, double& rB, double& rC) =0;  //<calculates (non-imaginair) roots of an quadratic equation

  //INSTRUMENT RELATED DATA:
  //instrument related data will be stored on the instrument
  //it is expected this data will change only at instrument creation and on scheduled "calibration maintanance"
  //to change this data, use the flash-memory procedure to upload the new data to the instrument

  //upload data to instrument
  virtual void setSensorCalibrationData     (Sensor_Calibration_Data_t    ) = 0;
  virtual void setControllableSettings      (Controllable_Settings_t      ) = 0;
  virtual void setWagonLogicalPositions     (Wagon_LogicalXYPos_t         ) = 0;
  virtual void setFilterLogicalPositions    (Filter_LogicalPos_t          ) = 0;
  virtual void setFocusLogicalPositions     (Focus_LogicalPos_t           ) = 0;
  virtual void setFocusLogicalWellPositions     (Focus_LogicalWellPos_t           ) = 0;
  virtual void setFocusLogicalWellPosition     (EWellNo well , float value         ) = 0;
  virtual void setLEDIntensity              (LEDIntensity_t               ) = 0;
  virtual void setWagonHeaterSettings       (WagonHeater_Settings_t       ) = 0;
  virtual void setDispenseHeadHeaterSettings(DispenseHeadHeater_Settings_t) = 0;

  //download data from instrument, for use during run-time
  virtual Sensor_Calibration_Data_t*     getSensorCalibrationData(ECanObj eCanObj)=0; ///<eCanObj is either a sensor or the controller using this sensor
  virtual Controllable_Settings_t*       getControllableSettings (ECanObj eCanObj)=0;
  virtual Wagon_LogicalXYPos_t*          getWagonLogicalPositions     (void)      =0;
  virtual Filter_LogicalPos_t*           getFilterLogicalPositions    (void)      =0;
  virtual Focus_LogicalPos_t*            getFocusLogicalPositions     (void)      =0;
  virtual Focus_LogicalWellPos_t*        getFocusLogicalWellPositions (void)      =0;
  virtual float getFocusLogicalWellPosition     (EWellNo well) = 0;
  virtual LEDIntensity_t*                getLEDIntensity              (void)      =0;
  virtual WagonHeater_Settings_t*        getWagonHeaterSettings       (void)      =0;
  virtual DispenseHeadHeater_Settings_t* getDispenseHeadHeaterSettings(void)      =0;

  //PROTOCOL RELATED DATA:
  //default settings for protocol related data are kept in memory (hard coded)
  //these settings can be overwritten ( setDefaults() )during runtime (f.e. at the start of an experiment)

  //upload data
  virtual void            setPumpingDefaults     (PumpSettings_t        ) = 0;
  virtual void            setAspirationDefaults  (AspirationSettings_t  ) = 0;
  virtual void            setDispenseDefaults    (DispenseSettings_t    ) = 0;
  virtual void            setLoad_Unload_Defaults(Load_Unload_Settings_t) = 0;

  //download data
  virtual PumpSettings_t*         getPumpingDefaults   (void)  = 0;
  virtual AspirationSettings_t*   getAspirationDefaults(void)  = 0;
  virtual DispenseSettings_t*     getDispenseDefaults(void)    = 0;
  virtual Load_Unload_Settings_t* getLoad_Unload_Settings(void)= 0;


  //version info data
  virtual std::string getVersionInfo     (void)         = 0; ///<returns all relevant version info of all boards as a string
  virtual void   setPS12SerialNumber(int iPS12_nr) = 0; ///<flashes PS12 serial number (YYMMXX) on each board
  virtual int    getPS12SerialNumber(void)         = 0; ///<reads PS12 serial number (YYMMXX) on each board, returns number if identical, else -1

  //flash related methods
  virtual void enableAllBoards(bool fEnable) = 0; ///<set all boards in enabled or disabled mode (when writing values to RAM, the board status should be disabled)
  virtual void flashAllBoards (void)         = 0; ///<flashes all values from the RAM-memory to the flash-prom

  //NB all structs are declared in file PamDefs_Defaults.h
};


#endif //DefaultGeneratorH
