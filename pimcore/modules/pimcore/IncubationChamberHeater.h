#ifndef IncubationChamberHeaterH
#define IncubationChamberHeaterH

#include "Heatable.h"        //for inheritance
class IncubationChamberHeaterListener;  //for listener

class IncubationChamberHeater: virtual public Heatable
{
  public:
    virtual void     addIncubationChamberHeaterListener(IncubationChamberHeaterListener *h) = 0; ///<adds listener
    virtual void  removeIncubationChamberHeaterListener(IncubationChamberHeaterListener *h) = 0; ///<removes listener
};

#endif //IncubationChamberHeaterH

