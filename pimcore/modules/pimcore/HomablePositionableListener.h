#ifndef HomablePositionableListenerH
#define HomablePositionableListenerH


#include "ControllableListener.h" //for inheritance
class HomablePositionable;                        //for listener

class HomablePositionableListener : virtual public ControllableListener
{
  public:
    //virtual void onHomedChanged(HomablePositionable* source) = 0;  ///<Triggered when the encoder is reset for the first time after a home-command
};


#endif //HomablePositionableListenerH
