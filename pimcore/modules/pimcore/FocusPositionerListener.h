#ifndef FOCUSPOSITIONERLISTENER_H
#define FOCUSPOSITIONERLISTENER_H

#include "HomablePositionableListener.h"  //for inheritance
class FocusPositioner;						    //for listener

class FocusPositionerListener : virtual public HomablePositionableListener
{
};

#endif //FOCUSPOSITIONERLISTENER_H

