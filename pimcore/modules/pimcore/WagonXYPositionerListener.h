#ifndef WagonXYPositionerListenerH
#define WagonXYPositionerListenerH

#include "ChangeListener.h"  //for inheritance
class WagonXYPositioner;				   //for source class

class WagonXYPositionerListener : virtual public ChangeListener
{
  public:
    virtual void onReadyChanged      (WagonXYPositioner* source) = 0;  ///<triggered by hardware-change. Ready = false when enabled, but not ready. All other cases: ready = true
    virtual void onTargetValueChanged(WagonXYPositioner* source) = 0;  ///<triggered by hardware-change
    virtual void onActualValueChanged(WagonXYPositioner* source) = 0;  ///<triggered by hardware-change, on each minimal (preset) change
    virtual void onHomedChanged      (WagonXYPositioner* source) = 0;  ///<triggered by hardware-change
};

#endif //WagonXYPositionerListenerH

