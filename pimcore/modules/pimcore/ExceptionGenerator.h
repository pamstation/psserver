#ifndef ExceptionGeneratorH
#define ExceptionGeneratorH


#include "EventSource.h"  //for inheritance
class ExceptionHandler;                   //for listener
class ExceptionGenerator : virtual public EventSource
{
  public:
    virtual void    addExceptionHandler(ExceptionHandler * h) = 0;   ///<adds listener, obsolete?
    virtual void removeExceptionHandler(ExceptionHandler * h) = 0;   ///<removes listener, obsolete?


};

#endif //ExceptionGeneratorH
