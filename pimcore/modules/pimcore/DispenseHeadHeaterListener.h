#ifndef DispenseHeadHeaterListenerH
#define DispenseHeadHeaterListenerH

#include "HeatableListener.h"     //for inheritance
class DispenseHeadHeater;			        //for listener

class DispenseHeadHeaterListener : virtual public HeatableListener
{
};

#endif //DispenseHeadHeaterListenerH

