#ifndef FILTERPOSITIONERLISTENER_H
#define FILTERPOSITIONERLISTENER_H

#include "HomablePositionableListener.h"  //for inheritance
class FilterPositioner;						    //for listener

class FilterPositionerListener : virtual public HomablePositionableListener
{
};

#endif //FILTERPOSITIONERLISTENER_H

