#ifndef LEDUNIT_H
#define LEDUNIT_H

#include "Switchable.h"  //for inheritance
class LEDUnitListener;                   //for listener
/**
* @ingroup PimCoreHW  //see PamDefs_Defaults.h
*/

class LEDUnit : virtual public Switchable
{
public:

    //Adds/Removes the corresonding listener
    virtual void    addLEDUnitListener(LEDUnitListener * h) =0; ///<adds listener
    virtual void removeLEDUnitListener(LEDUnitListener * h) =0; ///<removes listener
    virtual void  setLedWaitTime ( int   iLedWaitTime ) = 0;  // in [LU], has listener
    //getters
    virtual int   getLedWaitTime()  = 0 ;   //[LU]

private:
    int m_iLedWaitTime;
};

#endif //LEDUNIT_H
