#ifndef LogGeneratorH
#define LogGeneratorH


#include "EventSource.h"   //for inheritance
class LogHandler;                          //for listener
class LogGenerator : virtual public EventSource
{
  public:
    virtual void    addLogHandler(LogHandler * h) =0; ///<adds listener, (Not Implemented Yet, waiting for requirements)
    virtual void removeLogHandler(LogHandler * h) =0; ///<removes listener, (Not Implemented Yet, waiting for requirements)
};


#endif //LogGeneratorH
