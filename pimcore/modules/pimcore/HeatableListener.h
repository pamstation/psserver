#ifndef HeatableListenerH
#define HeatableListenerH

#include "ControllableListener.h" //for inheritance
class Heatable;						                        //for listener-source

class HeatableListener: virtual public ControllableListener  
{
  public:
    virtual void onTemperatureGainChanged(Heatable * source) = 0; ///<Triggered when user changes TemperatureGain
};

#endif //HeatableListenerH

