#ifndef PamDefs_CanH
#define PamDefs_CanH

///this file contains the CANbus related configuration data
typedef enum
{
  ERR_NO_ERROR                                = 0, // No Error
  ERR_CTR_NOT_HOMED,                               // Controller not homed
  ERR_CTR_NOT_HOMABLE,                             // Home requested, homing not posible
  ERR_CTR_BUSY,                                    // Controller busy
  ERR_CTR_VELOCITY_VALUE_OUT_OF_RANGE,             // Velocity value out of range or zero
  ERR_CTR_ACCELERATION_VALUE_OUT_OF_RANGE,         // Acceleration value out of range or zero
  ERR_CTR_TARGET_VALUE_OUT_OF_RANGE,               // Target value out of range or zero
  ERR_CTR_COMMAND_FAILED,                          // Controller command failed
  ERR_CTR_DISABLED,                                // Command failed due to controller being disabled
  ERR_CTR_ENABLED,
  ERR_UNKNOWN_CMD,                                 // Message command unknown
  ERR_UNKNOWN_OBJ,                                 // Message object unknown
  ERR_TRACKING_ERROR,                              // Controller tracking error.
  ERR_TOLERANCE_ERROR,
  ERR_OVER_CURRENT,                                // Firmware overcurrent detected.
  ERR_CAN_MSG_LENGTH_INVALID,                      // CAN message length > 8
  ERR_CTR_IN_ERROR,                                // Controller failure
  ERR_INITIALISE_CONTOLLERS_FAILED,                // Interrupt can not be registered, controllers not avaliable.
  ERR_KPI_OUT_OF_RANGE,
  ERR_KII_OUT_OF_RANGE,
  ERR_ABS_CURRENT_OUT_OF_RANGE,
  ERR_ABS_MAX_SETPOINT_OUT_OF_RANGE,
  ERR_ABS_MIN_SETPOINT_OUT_OF_RANGE,
  ERR_ABS_PWM_OUT_OF_RANGE,
  ERR_ABS_TOLERANCE_OUT_OF_RANGE,
  ERR_PWM_OUT_OF_RANGE,
  ERR_CTRL_DC_OUT_OF_RANGE,
  ERR_ADC_FAILURE,
  ERR_EEPROM_INCORRECT_VERSION,
  ERR_EEPROM_INCORRECT_LENGTH,
  ERR_EEPROM_FLASH_FAILED,
  ERR_EEPROM_UNKNOWN
}eError;

/**
* A CanMsg consists of 8 bytes:
* - Byte0  : msgID, a unique number for each sender-receiver-combination, see enum ECanID
* - Byte1  : msgLength
* - Byte2  : CanObj, a unique number for each input (AI, DI, OI), output (PO, PWM), HW-ctrl or CanCard-ctrl, see enum ECanObj
* - Byte3  : CanCmd, a list of commands, see enum ECanCmnd
* - Byte4-7: CanData
*/


  typedef enum
  {
    eCanIDMin               =  0  ,
    ePCtoBOARD1             = 16  ,     // 0000 1000  = 0x10 //bit3,4,5 indicates board nr
    ePCtoBOARD2             = 32  ,     // 0001 0000  = 0x20
    ePCtoBOARD3             = 64  ,     // 0010 0000  = 0x40
    eBOARD1toPC             = 24  ,     // 0000 1100  = 0x18 //bit2 indicates direction
    eBOARD2toPC             = 40  ,     // 0001 0100  = 0x28
    eBOARD3toPC             = 72  ,     // 0010 0100  = 0x48
    eCanIDMax
  } ECanID;

  typedef enum
  {
    eCanObjMin                  = 0  ,

    //SYCB, board-id 1
    ePressSensOverPress         = 1  ,    //SYCB, Analog In
    ePressSensUnderPress        = 2  ,    //SYCB, Analog In
    eCurrSensTransX             = 3  ,    //SYCB, Current In
    eCurrSensTransY             = 4  ,    //SYCB, Current In
    eCurrSensFocus              = 5  ,    //SYCB, Current In
    eCurrSensFilter             = 6  ,    //SYCB, Current In
    eEncTransXA                 = 7  ,    //SYCB, Digital In
    eEncTransXB                 = 8  ,    //SYCB, Digital In
    eEncTransXI                 = 9  ,    //SYCB, Digital In
    eEncTransYA                 = 10 ,    //SYCB, Digital In
    eEncTransYB                 = 11 ,    //SYCB, Digital In
    eEncTransYI                 = 12 ,    //SYCB, Digital In
    eEncFocusA                  = 13 ,    //SYCB, Digital In
    eEncFocusB                  = 14 ,    //SYCB, Digital In
    eEncFocusI                  = 15 ,    //SYCB, Digital In
    eEncFilterA                 = 16 ,    //SYCB, Digital In
    eEncFilterB                 = 17 ,    //SYCB, Digital In
    eEncFilterI                 = 18 ,    //SYCB, Digital In
    eSensTransXHome             = 19 ,    //SYCB, Optical In
    eSensTransYHome             = 20 ,    //SYCB, Optical In
    eSensFocusHome              = 21 ,    //SYCB, Optical In
    eSensFilterHome             = 22 ,    //SYCB, Optical In
    eSensOISpare1               = 23 ,    //SYCB, Optical In
    eSensSysPowerOK             = 24 ,    //SYCB, Optical In
    ePmpOverPress               = 25 ,    //SYCB, Power Out
    eValveOverPressRelease      = 26 ,    //SYCB, Power Out
    ePmpUnderPress              = 27 ,    //SYCB, Power Out
    eValveUnderPressRelease     = 28 ,    //SYCB, Power Out
    ePowerOutSpare0                  ,    //SYCB, Power Out
    ePowerOutSpare1                  ,    //SYCB, Power Out
    ePowerOutSpare2                  ,    //SYCB, Power Out
    ePowerOutSpare3                  ,    //SYCB, Power Out
    eLEDUnit1                        ,    //SYCB, LED Out
    eLEDUnit2                        ,    //SYCB, LED Out
    eLEDUnit3                        ,    //SYCB, LED Out
    ePWMSpare1                       ,    //SYCB, PWM Out
    eMotTransporterX                 ,    //SYCB, PWMH Out
    eMotTransporterY                 ,    //SYCB, PWMH Out
    eMotFocus                        ,    //SYCB, PWMH Out
    eMotFilter                       ,    //SYCB, PWMH Out
    eDiagSYCBPOError                 ,    //SYCB, DIAGNOSE obj
    eCtrlOverPressureSupplier        ,    //SYCB, HW CTRL
    eCtrlUnderPressureSupplier       ,    //SYCB, HW CTRL
    eCtrlTransporterX                ,    //SYCB, HW CTRL
    eCtrlTransporterY                ,    //SYCB, HW CTRL
    eCtrlFocusPositioner             ,    //SYCB, HW CTRL
    eCtrlFilterPositioner            ,    //SYCB, HW CTRL
    eBoard1Ctrl                      ,    //SYCB, BOARD CTRL //board1

    //ICCB, board-id 2
    eTempSensCoverLeft               ,    //ICCB, Analog In
    eTempSensBottomLeft              ,    //ICCB, Analog In
    eTempSensCoverMiddle             ,    //ICCB, Analog In
    eTempSensBottomMiddle            ,    //ICCB, Analog In
    eTempSensCoverRight              ,    //ICCB, Analog In
    eTempSensBottomRight             ,    //ICCB, Analog In
    eTempSensCalibration             ,    //ICCB, Analog In
    ePressSensWell1                  ,    //ICCB, Analog In
    ePressSensWell2                  ,    //ICCB, Analog In
    ePressSensWell3                  ,    //ICCB, Analog In
    ePressSensWell4                  ,    //ICCB, Analog In
    ePressSensWell5                  ,    //ICCB, Analog In
    ePressSensWell6                  ,    //ICCB, Analog In
    ePressSensWell7                  ,    //ICCB, Analog In
    ePressSensWell8                  ,    //ICCB, Analog In
    ePressSensWell9                  ,    //ICCB, Analog In
    ePressSensWell10                 ,    //ICCB, Analog In
    ePressSensWell11                 ,    //ICCB, Analog In
    ePressSensWell12                 ,    //ICCB, Analog In
    eCurrSensBottomLeft              ,    //ICCB, Current In
    eCurrSensBottomMiddle            ,    //ICCB, Current In
    eCurrSensBottomRight             ,    //ICCB, Current In
    eCurrSensCoverLeft               ,    //ICCB, Current In
    eCurrSensCoverMiddle             ,    //ICCB, Current In
    eCurrSensCoverRight              ,    //ICCB, Current In
    eSensCoverClosed                 ,    //ICCB, Optical In
    eSensDispPresentLeft             ,    //ICCB, Optical In
    eSensDispPresentMiddle           ,    //ICCB, Optical In
    eSensDispPresentRight            ,    //ICCB, Optical In
    eFanCover                        ,    //ICCB, Power Out
    eFanBottom                       ,    //ICCB, Power Out
    eValveWell1                      ,    //ICCB, Power Out
    eValveWell2                      ,    //ICCB, Power Out
    eValveWell3                      ,    //ICCB, Power Out
    eValveWell4                      ,    //ICCB, Power Out
    eValveWell5                      ,    //ICCB, Power Out
    eValveWell6                      ,    //ICCB, Power Out
    eValveWell7                      ,    //ICCB, Power Out
    eValveWell8                      ,    //ICCB, Power Out
    eValveWell9                      ,    //ICCB, Power Out
    eValveWell10                     ,    //ICCB, Power Out
    eValveWell11                     ,    //ICCB, Power Out
    eValveWell12                     ,    //ICCB, Power Out
    eValvePrsSelection               ,    //ICCB, Power Out
    eValveVacSelection               ,    //ICCB, Power Out
    eHtrCoverLeft                    ,    //ICCB, PWM Out
    eHtrCoverMiddle                  ,    //ICCB, PWM Out
    eHtrCoverRight                   ,    //ICCB, PWM Out
    eHtrBottomLeft                   ,    //ICCB, PWMH Out
    eHtrBottomMiddle                 ,    //ICCB, PWMH Out
    eHtrBottomRight                  ,    //ICCB, PWMH Out
    eDiagICCBPOError                 ,    //ICCB, DIAGNOSE obj
    eCtrlHtrCoverLeft                ,    //ICCB, HW CTRL
    eCtrlHtrBottomLeft               ,    //ICCB, HW CTRL
    eCtrlHtrCoverMiddle              ,    //ICCB, HW CTRL
    eCtrlHtrBottomMiddle             ,    //ICCB, HW CTRL
    eCtrlHtrCoverRight               ,    //ICCB, HW CTRL
    eCtrlHtrBottomRight              ,    //ICCB, HW CTRL
    eBoard2Ctrl                      ,    //ICCB, BOARD CTRL   //board2


    //WSCB, board-id 3
    eTempSensDispense                ,    //WSCB, Analog In
    ePressSensAspirate               ,    //WSCB, Analog In
    eCurrSensDispenseHeater          ,    //WSCB, Current In
    eCurrSensAspirateMotor           ,    //WSCB, Current In
    eEncDispenseHead1A               ,    //WSCB, Digital In
    eEncDispenseHead1B               ,    //WSCB, Digital In
    eEncDispenseHead2A               ,    //WSCB, Digital In
    eEncDispenseHead2B               ,    //WSCB, Digital In
    eEncDispenseHead3A               ,    //WSCB, Digital In
    eEncDispenseHead3B               ,    //WSCB, Digital In
    eEncDispenseHead4A               ,    //WSCB, Digital In
    eEncDispenseHead4B               ,    //WSCB, Digital In
    eDigitalInSpare1                 ,    //WSCB, Digital In
    eDigitalInSpare2                 ,    //WSCB, Digital In
    eDigitalInSpare3                 ,    //WSCB, Digital In
    eDigitalInSpare4                 ,    //WSCB, Digital In
    eSensDispHead1Empty              ,    //WSCB, Optical In
    eSensDispHead2Empty              ,    //WSCB, Optical In
    eSensDispHead3Empty              ,    //WSCB, Optical In
    eSensDispHead4Empty              ,    //WSCB, Optical In
    eSensAspirateHeadUp              ,    //WSCB, Optical In
    eSensAspirateHeadDown            ,    //WSCB, Optical In
    ePmpAspirate                     ,    //WSCB, Power Out
    eHtrDispense                     ,    //WSCB, PWM Out
    eMotAspirateHead                 ,    //WSCB, PWM Out
    eMotDispenseHead1                ,    //WSCB, LCPWM Out
    eMotDispenseHead2                ,    //WSCB, LCPWM Out
    eMotDispenseHead3                ,    //WSCB, LCPWM Out
    eMotDispenseHead4                ,    //WSCB, LCPWM Out
    eCtrlAspirateHeadPositioner      ,    //WSCB, HW CTRL
    eCtrlDispenseHeadHeater          ,    //WSCB, HW CTRL
    eCtrlDispenseHeadPositioner1     ,    //WSCB, HW CTRL
    eCtrlDispenseHeadPositioner2     ,    //WSCB, HW CTRL
    eCtrlDispenseHeadPositioner3     ,    //WSCB, HW CTRL
    eCtrlDispenseHeadPositioner4     ,    //WSCB, HW CTRL
    eBoard3Ctrl                      ,    //WSCB, BOARD CTRL

    //MISC                                //this enum is used also to give some non-CAN-hardware an unique ID
    eCtrlCCD                         ,
    eCtrlDisposable                  ,
    eCtrlUnloadButton                ,

    eCanObjMax
  } ECanObj;

   //CANCONTROLLER COMMAND TYPES
   typedef enum
   {
    eCanCmndUnknown  = 0,     // 0 = 0x00
    eCmdSetEnabled   = 1,     // 1 = 0x01
    eCmdSetTarget    = 2,     // 2 = 0x02
    eCmdGoHome          ,     // 3 = 0x03
    eCmdSetSpeed        ,     // 4 = 0x04
    eCmdGetStatus       ,     // 5 = 0x05
    eCmdGetTarget       ,     // 6 = 0x06
    eCmdGetActual       ,     // 7 = 0x07
    eCmdGetError        ,     // 8 = 0x08

    eCmdSetBoardStatus  ,     // 9 = 0x09
    eCmdGetBoardStatus  ,     //10 = 0x0A
    eCmdFlashBoard      ,     //11 = 0x0B

    eCmdSetValue1       ,
    eCmdSetValue2       ,
    eCmdSetValue3       ,
    eCmdSetValue4       ,
    eCmdSetValue5       ,
    eCmdSetValue6       ,
    eCmdSetValue7       ,
    eCmdSetValue8       ,
    eCmdSetValue9       ,
    eCmdSetValue10      ,
    eCmdSetValue11      ,
    eCmdSetValue12      ,
    eCmdSetValue13      ,
    eCmdSetValue14      ,
    eCmdSetValue15      ,
    eCmdSetValue16      ,
    eCmdSetValue17      ,
    eCmdSetValue18      ,
    eCmdSetValue19      ,
    eCmdSetValue20      ,
    eCmdSetValue21      ,
    eCmdSetValue22      ,
    eCmdSetValue23      ,
    eCmdSetValue24      ,
    eCmdSetValue25      ,
    eCmdSetValue26      ,
    eCmdSetValue27      ,
    eCmdSetValue28      ,
    eCmdSetValue29      ,
    eCmdSetValue30      ,
    eCmdSetValue31      ,
    eCmdSetValue32      ,
    eCmdSetValue33      ,
    eCmdSetValue34      ,
    eCmdSetValue35      ,
    eCmdSetValue36      ,
    eCmdSetValue37      ,
    eCmdSetValue38      ,
    eCmdSetValue39      ,
    eCmdSetValue40      ,
    eCmdSetValue41      ,
    eCmdSetValue42      ,
    eCmdSetValue43      ,
    eCmdSetValue44      ,
    eCmdSetValue45      ,
    eCmdSetValue46      ,
    eCmdSetValue47      ,
    eCmdSetValue48      ,
    eCmdSetValue49      ,
    eCmdSetValue50      ,

    eCmdGetValue1       ,
    eCmdGetValue2       ,
    eCmdGetValue3       ,
    eCmdGetValue4       ,
    eCmdGetValue5       ,
    eCmdGetValue6       ,
    eCmdGetValue7       ,
    eCmdGetValue8       ,
    eCmdGetValue9       ,
    eCmdGetValue10      ,
    eCmdGetValue11      ,
    eCmdGetValue12      ,
    eCmdGetValue13      ,
    eCmdGetValue14      ,
    eCmdGetValue15      ,
    eCmdGetValue16      ,
    eCmdGetValue17      ,
    eCmdGetValue18      ,
    eCmdGetValue19      ,
    eCmdGetValue20      ,
    eCmdGetValue21      ,
    eCmdGetValue22      ,
    eCmdGetValue23      ,
    eCmdGetValue24      ,
    eCmdGetValue25      ,
    eCmdGetValue26      ,
    eCmdGetValue27      ,
    eCmdGetValue28      ,
    eCmdGetValue29      ,
    eCmdGetValue30      ,
    eCmdGetValue31      ,
    eCmdGetValue32      ,
    eCmdGetValue33      ,
    eCmdGetValue34      ,
    eCmdGetValue35      ,
    eCmdGetValue36      ,
    eCmdGetValue37      ,
    eCmdGetValue38      ,
    eCmdGetValue39      ,
    eCmdGetValue40      ,
    eCmdGetValue41      ,
    eCmdGetValue42      ,
    eCmdGetValue43      ,
    eCmdGetValue44      ,
    eCmdGetValue45      ,
    eCmdGetValue46      ,
    eCmdGetValue47      ,
    eCmdGetValue48      ,
    eCmdGetValue49      ,
    eCmdGetValue50      ,

    eCmdError           , //this cmd-nr is returned by ESW in error situations

    eCanCmndMax
   } ECanCmnd;

#endif //PamDefs_CanH
