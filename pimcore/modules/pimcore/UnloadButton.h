#ifndef UnloadButtonH
#define UnloadButtonH

#include "ChangeSource.h" //for inheritance
class UnloadButtonListener;                        //for listener


class UnloadButton : virtual public ChangeSource
{
public:
    virtual void    addUnloadButtonListener(UnloadButtonListener * h) =0; ///<adds listener
    virtual void removeUnloadButtonListener(UnloadButtonListener * h) =0; ///<removes listener

    virtual void press(void)                  = 0; ///<unblocks the waitPressedEvent()
    virtual void waitPressedEvent(void)       = 0; ///<CPU-friendly way of waiting for "press()"

    virtual void setEnabled(bool)             = 0; ///<enables the button (blocks the waitPressedEvent() ), only when enabled the button responds to "press"
    virtual bool getEnabled(void)             = 0; ///<disables the button (unblocks the waitPressedEvent()), only when enabled the button responds to "press"
    virtual int getStateId() = 0; // usefull to asynchroniously track state change
};

#endif //UnloadButtonH
