#ifndef OVERPRESSURESUPPLIER_H
#define OVERPRESSURESUPPLIER_H

#include "Pressurable.h"   //for inheritance
class OverPressureSupplierListener;        //for listener
/**
* @ingroup PimCoreHW  //see PamDefs_Defaults.h
*/
class OverPressureSupplier : virtual public Pressurable
{
  public:
    //Adds & remove the OverPressureSupplier listener
    virtual void    addOverPressureSupplierListener(OverPressureSupplierListener * h) = 0; ///<adds listener
    virtual void removeOverPressureSupplierListener(OverPressureSupplierListener * h) = 0; ///<removes listener
};

#endif //OVERPRESSURESUPPLIER_H
