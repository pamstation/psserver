#ifndef PressurableListenerH
#define PressurableListenerH


#include "ControllableListener.h" //for inheritance
class Pressurable;                                //listener

class PressurableListener : virtual public ControllableListener 
{
};


#endif //PressurableListenerH
