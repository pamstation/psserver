#ifndef PamStationH
#define PamStationH


#include "EventSource.h"    //for inheritance

class DefaultGenerator;
class ExceptionGenerator;
//class ImageGenerator;
class LogGenerator;

class AspirateHeadPositioner;
class AspiratePump;
class CCD;
class DispenseHeadHeater;
class DispenseHeadPositioner;
class Disposable;
class FilterPositioner;
class FocusPositioner;
class LEDUnit;
class OverPressureSupplier;
class UnderPressureSupplier;
class UnloadButton;
class WagonHeater;
class IncubationChamberHeater;
class WagonPositioner;
class WagonXYPositioner;
class PropLeds;

class PamStation : virtual public EventSource
{
  public:
    //get (singleton) instance of generator-class
    virtual DefaultGenerator      * getDefaultGenerator()      = 0;    ///<returns (singleton) handle to exception throwing object
    virtual ExceptionGenerator    * getExceptionGenerator()    = 0;    ///<returns (singleton) handle to exception throwing object
//    virtual ImageGenerator        * getImageGenerator()        = 0;    ///<returns (singleton) handle to image generating object
    virtual LogGenerator          * getLogGenerator()          = 0;    ///<returns (singleton) handle to logging generating object

    //get (singleton) instance of hardware-class
    virtual AspirateHeadPositioner* getAspirateHeadPositioner()= 0;  ///<returns (singleton) handle
    virtual AspiratePump          * getAspiratePump()          = 0;  ///<returns (singleton) handle
    virtual CCD                   * getCCD()                   = 0;  ///<returns (singleton) handle
    virtual DispenseHeadHeater    * getDispenseHeadHeater()    = 0;  ///<returns (singleton) handle
    virtual DispenseHeadPositioner* getDispenseHeadPositioner(EDispenseHeadNo)= 0;  //returns (singleton) handle to one of the four DispenseHeadPositioner
    virtual Disposable            * getDisposable()            = 0;  ///<returns (singleton) handle
    virtual FilterPositioner      * getFilterPositioner()      = 0;  ///<returns (singleton) handle
    virtual FocusPositioner       * getFocusPositioner()       = 0;  ///<returns (singleton) handle
    virtual LEDUnit               * getLEDUnit(ELEDNo)         = 0;  ///<returns (singleton) handle to one of the three LED's
    virtual OverPressureSupplier  * getOverPressureSupplier()  = 0;  ///<returns (singleton) handle
    virtual UnderPressureSupplier * getUnderPressureSupplier() = 0;  ///<returns (singleton) handle
    virtual UnloadButton          * getUnloadButton()          = 0;  ///<returns (singleton) handle
    virtual WagonHeater           * getWagonHeater()           = 0;  ///<returns (singleton) handle  WagonHeater contains 6 heaters
    virtual IncubationChamberHeater* getIncubationChamberHeater(EIncubationChamberHeater) = 0; //returns (singleton) handle to one of the six IncubationChamberHeaters
    virtual WagonPositioner       * getWagonPositioner(EWagonPositioner)       = 0;  ///<returns (singleton) handle. WagonPositioner contains a X- and Y-transporter
    virtual WagonXYPositioner     * getWagonXYPositioner()     = 0;  ///<returns (singleton) handle. WagonPositioner contains a X- and Y-transporter

    //extra functionality
    virtual void abort(void) = 0;    ///<use to stop protocolsteps: stops&disables all hardware and throws an exception

    virtual PropLeds              * getLeds()     = 0;  ///<returns (singleton) handle.

};


#endif //PamStationH

