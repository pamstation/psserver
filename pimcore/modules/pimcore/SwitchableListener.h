#ifndef SwitchableListenerH
#define SwitchableListenerH

#include "ChangeListener.h" //for inheritance
class Switchable;                           //for listener-source

class SwitchableListener : virtual public ChangeListener
{
  public:
      virtual void onSwitchedChanged (Switchable * source) = 0;   ///<Typical sequence: ON=>OFF=>ON...
};


#endif //SwitchableListenerH
