#ifndef PamDefs_InitialValuesH
#define PamDefs_InitialValuesH


//PumpSettings_t, used in SysDefaultGenerator.cpp:
//to be overwritten by DefaultGenerator::setPumpingDefaults()
//CCM-set
#define  VALVE_OPEN_TIME   500      //[ms]    PumpSettings_t::uValveOpenTime
#define  PRESS_SETTL_TIME  500      //[ms]    PumpSettings_t::uPressureSettlingDelay
#define  FLUID_FLOW_TIME   2000      //[ms]    PumpSettings_t::uFluidFlowDelay
#define  BROKEN_MEMB_PRESS 0.09   //[barg]  PumpSettings_t::rBrokenMembranePressure
#define  MAX_PMP_TIME      70000  //[ms]    PumpSettings_t::uMaximumWaitingTime

/*
//Pamgene-set
#define  VALVE_OPEN_TIME   500    //[ms]    PumpSettings_t::uValveOpenTime
#define  PRESS_SETTL_TIME  0      //[ms]    PumpSettings_t::uPressureSettlingDelay
#define  FLUID_FLOW_TIME   0      //[ms]    PumpSettings_t:uFluidFlowDelay:
#define  BROKEN_MEMB_PRESS 0.12   //[barg]  PumpSettings_t::rBrokenMembranePressure
#define  MAX_PMP_TIME      10000  //[ms]    PumpSettings_t::uMaximumWaitingTime
*/

//AspirationSettings_t, used in SysDefaultGenerator.cpp:
//to be overwritten by DefaultGenerator::setAspirationDefaults()
#define  ASP_MIN_PRS    -0.5  //[barg] AspirationSettings_t::rPressureUnderlimit
#define  ASP_MAX_PRS    -0.01  //[barg] AspirationSettings_t::rPressureUpperLimit
#define  ASP_DRY_TIME    1000     //[ms]   AspirationSettings_t::uAspirateTubeCleanDelay
#define  ASP_BUILD_TIME  5000     //[ms]   AspirationSettings_t::uAspPressBuildupDelay

//DispenseSettings_t, used in SysDefaultGenerator.cpp:
//to be overwritten by DefaultGenerator::setDispenseDefaults()
#define HTBL_BITSET       12  //[-]  DispenseSettings_t::uHeatableHeadBitSet (12 = 0b1100 = 12head3+4)
#define PRIME_AMOUNT      35  //[�l] DispenseSettings_t::rPrimeAmount
#define RETRACT_AMOUNT    10  //[�l] DispenseSettings_t::rPrimeAmount
#define RETRACT_DELAY     500  //[ms] DispenseSettings_t::uRetractDelay

/*
//DispenseSettings_t, used in SysDefaultGenerator.cpp:
//Pamgene-set
#define HTBL_BITSET       12  //[-]  DispenseSettings_t::uHeatableHeadBitSet (12 = 0b1100 = 12head3+4)
#define PRIME_AMOUNT      35  //[�l] DispenseSettings_t::rPrimeAmount
#define RETRACT_AMOUNT    10  //[�l] DispenseSettings_t::rPrimeAmount
#define RETRACT_DELAY   1000  //[ms] DispenseSettings_t::uRetractDelay
*/


//Load_Unload_Settings_t, used in SysDefaultGenerator.cpp:
//to be overwritten by DefaultGenerator::setLoad_Unload_Defaults()
#define SUPPLY_PRESS    0.2  //[barg] Load_Unload_Settings_t::rDefaultOverPressure
#define SUPPLY_VAC     -0.2  //[barg] Load_Unload_Settings_t::rDefaultUnderPressure
#define INIT_TEMP       30   //[�C]   Load_Unload_Settings_t::rInitWagonTemperature
#define SAFE_UNLD_TEMP  70   //[�C]   Load_Unload_Settings_t::rSafeUnloadTemperature


//wagonHeater, used in PropWagonHeater.cpp
#define   BOTTOM_OFFSET 0 //�C
#define   COVER_OFFSET  1 //�C

//dispenseHeadHeater, used in PropDispenseHeadHeater.cpp
#define DISPHTR_OFFSET -3 //�C



//Refresh_loop delay properties, see StubPamStation.cpp
#ifdef _STUB
        #define  REFRESH_DELAY 100   //[ms] 100 means update all relevant controller properties, then sleep 100 ms
        #define  WAIT_DELAY    2   //[ms] delay between class-updates, to prvent continuous burst
#else
        #define  REFRESH_DELAY 750 //[ms] 100 means update all relevant controller properties, then sleep 100 ms
        #define  WAIT_DELAY    2   //[ms] delay between class-updates, to prvent continuous burst
#endif



#endif //PamDefs_InitialValuesH
