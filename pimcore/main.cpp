//
// Created by alex on 14/03/19.
//
#include <iostream>
#include "modules/pimcore/WagonXYPositioner.h"
#include "modules/systemlayer/StubPamStationFactory.h"
#include "modules/systemlayer/StubPamStation.h"

#include "modules/systemlayer/SysPamStationException.h"

#include "modules/pimcore/CCD.h"
#include "modules/pimcore/Image.h"
#include "modules/pimcore/LEDUnit.h"

int main(int argc, char *argv[]) {
    std::cout << "pimcore started\n";
    try {
        StubPamStation *pam = StubPamStationFactory::getCurrentPamStation();

        std::cout << "wagon goHome\n";
        pam->getWagonXYPositioner()->goHome();
        pam->getWagonXYPositioner()->waitReady();

        std::cout << "led 1 -- setSwitchStatus on\n";

        LEDUnit *pLED1 = pam->getLEDUnit((ELEDNo) 1);
        pLED1->setSwitchStatus(true);

        std::cout << "wagon done\n";
        std::cout << "wagon goReadPosition\n";
        pam->getWagonXYPositioner()->goReadPosition(EWellNo::eWell1);
        pam->getWagonXYPositioner()->waitReady();

        std::cout << "ccd setExposureTime 300\n";

        pam->getCCD()->setExposureTime(200);

        std::cout << "ccd acquireImage\n";

        Image *image = pam->getCCD()->acquireImage();

        std::cout << "led 1 -- setSwitchStatus off\n";
        pLED1->setSwitchStatus(false);

        image->save("pimcoretest.tiff");

        std::cout << "image saved to pimcoretest.tiff\n";

        delete image;

        std::cout << "ccd done\n";

        std::cout << "wagon goHome\n";
        pam->getWagonXYPositioner()->goHome();
        pam->getWagonXYPositioner()->waitReady();
        std::cout << "wagon done\n";

    } catch (SysPamStationException e) {
        std::cout << e.getDescription() << "\n";
    } catch (...) {
        std::cout << "error !!!" << "\n";
    }


    std::cout << "pimcore done\n";

//    pam->getLeds();

}