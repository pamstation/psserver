//
// Created by alex on 21/03/19.
//

// PYLON_CAMEMU environment variable
// export PYLON_CAMEMU=2

//#include <pylon/PylonIncludes.h>
#include <pylon/PylonIncludes.h>
#include <pylon/BaslerUniversalInstantCamera.h>

int main(int argc, char* argv[])
{
    try
    {
        Pylon::PylonInitialize();

        Pylon::CBaslerUniversalInstantCamera * camera = new Pylon::CBaslerUniversalInstantCamera(Pylon::CTlFactory::GetInstance().CreateFirstDevice());

        // Print the model name of the camera.
        std::cout << "Using device " << camera->GetDeviceInfo().GetModelName() << std::endl;
        std::cout << "Using device " << camera->GetDeviceInfo().GetModelName() << " class "
                  << camera->GetDeviceInfo().GetDeviceClass() << std::endl;
        camera->Open();

        const char Filename[] = "config/current-camera-settings-before-reset.pfs";
        Pylon::CFeaturePersistence::Save(Filename, &camera->GetNodeMap());

        camera->DeviceReset.Execute();

        std::cout << std::endl << "DeviceReset." << std::endl;

    } catch (const Pylon::GenericException &e) {
        std::cout << "error " << e.GetDescription();
//        m_pExceptionGenerator->onException(this, CCD_OPEN_DEVICE, eErrorUnknown, e.GetDescription());
    }

    Pylon::PylonTerminate();

    return 0;
}