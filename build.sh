#!/usr/bin/env bash

set -e

# see build-arm.sh
#TARGET=${ARCH:="aarch64-unknown-linux"}
TARGET=${ARCH:="x86_64-unknown-linux-gnu"}
BUILD_DIR=_build/${TARGET}

echo "Building tiff"

TIFF_LIB_DIR=pimcore/drivers/tiff


if [ -d ${TIFF_LIB_DIR}/${BUILD_DIR} ]; then
  rm -r ${TIFF_LIB_DIR:?}/${BUILD_DIR}
fi

mkdir -p ${TIFF_LIB_DIR}/${BUILD_DIR}
cd ${TIFF_LIB_DIR}/${BUILD_DIR}
../../configure $CONFIGURE_FLAGS --disable-jpeg --disable-zlib --build="x86_64-unknown-linux-gnu" --host="${TARGET}" --target="${TARGET}"
make

cd -
echo "Building tiff done"

echo "Building tinyxml2"
TINYXML2_DIR=third/tinyxml2

if [ -d ${TINYXML2_DIR}/${BUILD_DIR} ]; then
  rm -r ${TINYXML2_DIR:?}/${BUILD_DIR}
fi

mkdir -p ${TINYXML2_DIR}/${BUILD_DIR}
cd ${TINYXML2_DIR}/${BUILD_DIR}
cmake ../../ -B. -DBUILD_SHARED_LIBS:BOOL=OFF -DBUILD_STATIC_LIBS:BOOL=ON -DCMAKE_BUILD_TYPE=Release -G "CodeBlocks - Unix Makefiles"
make

cd -
echo "Building tinyxml2 done"

echo "Building gsl"
GSL_DIR=third/gsl

if [ -d ${GSL_DIR}/${BUILD_DIR} ]; then
  rm -rf ${GSL_DIR:?}/${BUILD_DIR}
fi
#
mkdir -p ${GSL_DIR}/${BUILD_DIR}
cd ${GSL_DIR}/${BUILD_DIR}
../../configure $CONFIGURE_FLAGS --build="x86_64-unknown-linux-gnu" --host="${TARGET}" --target="${TARGET}"

make -j8

echo "Building gsl done"
cd -

echo "Building psserver"

if [ -d ${BUILD_DIR} ]; then
  rm -r ${BUILD_DIR:?}
fi

mkdir -p ${BUILD_DIR}
cd ${BUILD_DIR}

cmake ../../ -B. -DCMAKE_BUILD_TYPE=Release -DTARGET=${TARGET} -G "CodeBlocks - Unix Makefiles"
make -j8


echo "Building psserver done"

# readelf -d psserver
# 0x00000001 (NEEDED)                     Shared library: [libpylonbase-5.2.0.so]
# 0x00000001 (NEEDED)                     Shared library: [libpylonutility-5.2.0.so]
# 0x00000001 (NEEDED)                     Shared library: [libGenApi_gcc_v3_1_Basler_pylon.so]
# 0x00000001 (NEEDED)                     Shared library: [libGCBase_gcc_v3_1_Basler_pylon.so]
# 0x00000001 (NEEDED)                     Shared library: [libboost_system.so.1.64.0]
# 0x00000001 (NEEDED)                     Shared library: [libboost_filesystem.so.1.64.0]
# 0x00000001 (NEEDED)                     Shared library: [libstdc++.so.6]
# 0x00000001 (NEEDED)                     Shared library: [libm.so.6]
# 0x00000001 (NEEDED)                     Shared library: [libgcc_s.so.1]
# 0x00000001 (NEEDED)                     Shared library: [libpthread.so.0]
# 0x00000001 (NEEDED)                     Shared library: [libc.so.6]

# scp ./psserver root@10.110.253.49:~/psserver
# ssh root@10.110.253.47