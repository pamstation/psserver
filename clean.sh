#!/usr/bin/env bash

  rm -r pimcore/drivers/tiff/_build
  rm -r third/tinyxml2/_build
  rm -r third/gsl/_build
  rm -r _build

