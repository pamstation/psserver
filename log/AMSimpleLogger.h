#ifndef AMSimpleLoggerH
#define AMSimpleLoggerH

#include "AMLogger.h"

#include <string>
#include <iostream>
#include <fstream>

class AMSimpleLogger : virtual public AMLogger
{
public:
    AMSimpleLogger(std::string aPath, std::string aName);
    ~AMSimpleLogger();
    virtual void logInfo(std::string aMsg);
    virtual void logError(std::string aMsg);
private:
        std::string path;
        std::ofstream stream;
};

#endif //AMSimpleLoggerH

 