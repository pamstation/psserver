//---------------------------------------------------------------------------
#pragma hdrstop

#include <ctime>
#include "AMSimpleLogger.h"

#pragma package(smart_init)


// systemd-journal-upload --url https://127.0.0.1:4040/
// sudo nano /etc/systemd/journal-upload.conf
// sudo systemctl enable systemd-journal-upload.service
// sudo systemctl restart systemd-journal-upload.service
// sudo systemctl stop systemd-journal-upload.service
// sudo systemctl disable systemd-journal-upload.service
//---------------------------------------------------------------------------

AMSimpleLogger::AMSimpleLogger(std::string aPath, std::string aName) {

//    path = aPath;
//    path.append("\\");
//
//
////    path.append(Date().FormatString("yyyy-MM").c_str());
////    path.append(" ");
//    path.append(aName);
//    path.append(".log");
//    stream.open(path.c_str(), ios::out | ios_base::app);
//    if (!stream.is_open()) throw "Failed to open log file." ;
}

AMSimpleLogger::~AMSimpleLogger() {
//    stream.close();
}

void AMSimpleLogger::logInfo(std::string aMsg) {
    std::time_t t = std::time(nullptr);
    char mbstr[100];
    if (std::strftime(mbstr, sizeof(mbstr), "%Y-%m-%dT%H:%M:%SZ", std::localtime(&t))) {
        std::cout << mbstr << "--INFO--" << aMsg << "\n";
    }
}

void AMSimpleLogger::logError(std::string aMsg) {

    std::time_t t = std::time(nullptr);
    char mbstr[100];
    if (std::strftime(mbstr, sizeof(mbstr), "%Y-%m-%dT%H:%M:%SZ", std::localtime(&t))) {
        std::cout << mbstr << "--ERROR-- " << aMsg << "\n";
    }
}




