# IP

```bash
# dev
PSSERVER_IP=10.110.253.49
# prod
PSSERVER_IP=10.110.253.47
```
# Format sda

```bash
ssh root@${PSSERVER_IP}

# wipefs -f /dev/sda
mkfs.ext4 /dev/sda
mkdir -p /var/lib/psserver

mount /dev/sda /var/lib/psserver
umount /var/lib/psserver
```
## Install Pylon5

https://www.baslerweb.com/en/sales-support/downloads/software-downloads/#type=pylonsoftware;language=all;version=5.2.0;os=linuxarm
pylon 5.2.0 Camera Software Suite Linux ARM 32 bit hardfloat
  
```bash

scp ./docker/pylon-5.2.0.13457-armhf.tar.gz root@${PSSERVER_IP}:~/pylon-5.2.0.13457-armhf.tar.gz

ssh root@${PSSERVER_IP}

tar xvzf pylon-5.2.0.13457-armhf.tar.gz 
cd pylon-5.2.0.13457-armhf  
mkdir /opt
tar -C /opt -xzf pylonSDK*.tar.gz  
cd ..  
rm pylon-5.2.0.13457-armhf.tar.gz  
cd pylon-5.2.0.13457-armhf
chmod +x setup-usb.sh
./setup-usb.sh
cd ..
rm -r pylon-5.2.0.13457-armhf
    
```

## Install boost

```bash
opkg update
opkg install boost

```

# Install
```

# sda mount
scp systemd/var-lib-psserver.mount root@${PSSERVER_IP}:/etc/systemd/system/var-lib-psserver.mount
ssh root@${PSSERVER_IP}
chmod 664 /etc/systemd/system/var-lib-psserver.mount
systemctl daemon-reload
systemctl enable var-lib-psserver.mount
systemctl status var-lib-psserver.mount
systemctl start var-lib-psserver.mount

# can1
scp systemd/can1.service root@${PSSERVER_IP}:/etc/systemd/system/can1.service
ssh root@${PSSERVER_IP}
chmod 664 /etc/systemd/system/can1.service
systemctl daemon-reload
systemctl enable can1.service
systemctl status can1.service
systemctl start can1.service
 
# psserver

# dev
PSSERVER_IP=10.110.253.49
# prod
PSSERVER_IP=10.110.253.47
 
scp ./_build/arm/psserver root@${PSSERVER_IP}:/var/lib/psserver/psserver
scp -r ./web root@${PSSERVER_IP}:/var/lib/psserver/web 

scp systemd/psserver.service root@${PSSERVER_IP}:/etc/systemd/system/psserver.service
ssh root@${PSSERVER_IP}
chmod 664 /etc/systemd/system/psserver.service
systemctl daemon-reload
systemctl start psserver
systemctl stop psserver
systemctl restart psserver
systemctl status psserver
systemctl enable psserver
systemctl disable psserver

journalctl -f -u psserver
```

# camera reset

```bash
scp ./_build/arm/pimcore/camerareset root@${PSSERVER_IP}:/var/lib/psserver/camerareset
```

