#include <boost/thread/thread.hpp>
#include <iostream>

#include "Simple-Web-Server/server_http.hpp"

// Added for the default_resource example
#include <algorithm>
#include <boost/filesystem.hpp>
#include <fstream>
#include <vector>

#include "server/AMRequestDispatcher.h"
#include "server/AMRequest.h"
#include "server/AMResponse.h"
#include "server/AMRequestImpl.h"
#include "server/AMResponseImpl.h"

using namespace std;

using HttpServer = SimpleWeb::Server<SimpleWeb::HTTP>;

string urlDecode(string str){
    string ret;
    char ch;
    int i, ii, len = str.length();

    for (i=0; i < len; i++){
        if(str[i] != '%'){
            if(str[i] == '+')
                ret += ' ';
            else
                ret += str[i];
        }else{
            sscanf(str.substr(i + 1, 2).c_str(), "%x", &ii);
            ch = static_cast<char>(ii);
            ret += ch;
            i = i + 2;
        }
    }
    return ret;
}

int main(int argc, char *argv[]) {
//    std::cout << "Using Boost "
//              << BOOST_VERSION / 100000 << "."  // major version
//              << BOOST_VERSION / 100 % 1000 << "."  // minor version
//              << BOOST_VERSION % 100                // patch level
//              << std::endl;

    HttpServer server;
    server.config.port = 8080;

    // GET-example for the path /info
    // Responds with server-information
    server.resource["^/info$"]["GET"] = [](shared_ptr<HttpServer::Response> response,
                                           shared_ptr<HttpServer::Request> request) {
        stringstream stream;
        stream << "<h1>Request from " << request->remote_endpoint_address() << ":" << request->remote_endpoint_port()
               << "</h1>";

        stream << request->method << " " << request->path << " HTTP/" << request->http_version;

        stream << "<h2>Query Fields</h2>";
        auto query_fields = request->parse_query_string();
        for (auto &field : query_fields)
            stream << field.first << ": " << field.second << "<br>";

        stream << "<h2>Header Fields</h2>";
        for (auto &field : request->header)
            stream << field.first << ": " << field.second << "<br>";

        response->write(stream);
    };

    server.resource["^/api/.*"]["GET"] = [](shared_ptr<HttpServer::Response> response,
                                          shared_ptr<HttpServer::Request> request) {
        AMRequestImpl req = AMRequestImpl(request);
        AMResponseImpl resp = AMResponseImpl(response);
        AMRequestDispatcher::getCurrent()->dispatch(&req, &resp);
        resp.write();
    };

    server.resource["^/api/.*"]["POST"] = [](shared_ptr<HttpServer::Response> response,
                                            shared_ptr<HttpServer::Request> request) {
        AMRequestImpl req = AMRequestImpl(request);
        AMResponseImpl resp = AMResponseImpl(response);
        AMRequestDispatcher::getCurrent()->dispatch(&req, &resp);
        resp.write();
    };

    server.resource["^/api/.*"]["PUT"] = [](shared_ptr<HttpServer::Response> response,
                                             shared_ptr<HttpServer::Request> request) {
        AMRequestImpl req = AMRequestImpl(request);
        AMResponseImpl resp = AMResponseImpl(response);
        AMRequestDispatcher::getCurrent()->dispatch(&req, &resp);
        resp.write();
    };

    // Default GET-example. If no other matches, this anonymous function will be called.
    // Will respond with content in the web/-directory, and its subdirectories.
    // Default file: index.html
    // Can for instance be used to retrieve an HTML 5 client that uses REST-resources on this server
    server.default_resource["GET"] = [](shared_ptr<HttpServer::Response> response,
                                        shared_ptr<HttpServer::Request> request) {
        try {
            auto web_root_path = boost::filesystem::canonical("web");
            auto path = boost::filesystem::canonical(web_root_path / urlDecode(request->path));
            // Check if path is within web_root_path
            if (distance(web_root_path.begin(), web_root_path.end()) > distance(path.begin(), path.end()) ||
                !equal(web_root_path.begin(), web_root_path.end(), path.begin()))
                throw invalid_argument("path must be within root path");
            if (boost::filesystem::is_directory(path))
                path /= "index.html";

            SimpleWeb::CaseInsensitiveMultimap header;

            // Uncomment the following line to enable Cache-Control
            // header.emplace("Cache-Control", "max-age=86400");

#ifdef HAVE_OPENSSL
            //    Uncomment the following lines to enable ETag
            //    {
            //      ifstream ifs(path.string(), ifstream::in | ios::binary);
            //      if(ifs) {
            //        auto hash = SimpleWeb::Crypto::to_hex_string(SimpleWeb::Crypto::md5(ifs));
            //        header.emplace("ETag", "\"" + hash + "\"");
            //        auto it = server->header.find("If-None-Match");
            //        if(it != server->header.end()) {
            //          if(!it->second.empty() && it->second.compare(1, hash.size(), hash) == 0) {
            //            response->write(SimpleWeb::StatusCode::redirection_not_modified, header);
            //            return;
            //          }
            //        }
            //      }
            //      else
            //        throw invalid_argument("could not read file");
            //    }
#endif

            auto ifs = make_shared<ifstream>();
            ifs->open(path.string(), ifstream::in | ios::binary | ios::ate);

            if (*ifs) {
                auto length = ifs->tellg();
                ifs->seekg(0, ios::beg);

                header.emplace("Content-Length", to_string(length));
                response->write(header);

                // Trick to define a recursive function within this scope (for example purposes)
                class FileServer {
                public:
                    static void
                    read_and_send(const shared_ptr<HttpServer::Response> &response, const shared_ptr<ifstream> &ifs) {
                        // Read and send 128 KB at a time
                        static vector<char> buffer(131072); // Safe when server is running on one thread
                        streamsize read_length;
                        if ((read_length = ifs->read(&buffer[0], static_cast<streamsize>(buffer.size())).gcount()) >
                            0) {
                            response->write(&buffer[0], read_length);
                            if (read_length == static_cast<streamsize>(buffer.size())) {
                                response->send([response, ifs](const SimpleWeb::error_code &ec) {
                                    if (!ec)
                                        read_and_send(response, ifs);
                                    else
                                        cerr << "Connection interrupted" << endl;
                                });
                            }
                        }
                    }
                };
                FileServer::read_and_send(response, ifs);
            } else
                throw invalid_argument("could not read file");
        }
        catch (const exception &e) {
            response->write(SimpleWeb::StatusCode::client_error_bad_request,
                            "Could not open path " + request->path + ": " + e.what());
        }
    };

    server.on_error = [](shared_ptr<HttpServer::Request> request, const SimpleWeb::error_code &ec) {
        // Handle errors here
        // Note that connection timeouts will also call this handle with ec set to SimpleWeb::errc::operation_canceled

        if (SimpleWeb::errc::operation_canceled != ec && boost::asio::error::eof != ec){
            AMRequestDispatcher::getCurrent()->logger->logError("server.on_error : " + ec.message());
        }
    };

    thread server_thread([&server]() {
        server.start();
    });

    server_thread.join();

    return 0;
}

