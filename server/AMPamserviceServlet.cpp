//---------------------------------------------------------------------------
#pragma hdrstop

#include <string>
#include <sstream>

#include "AMPamserviceServlet.h"
#include "AMRequest.h"
#include "AMResponse.h"

#include "modules/systemlayer/StubPamStationFactory.h"
#include "modules/systemlayer/StubPamStation.h"

#include "modules/utils/AMConfiguration.h"

#pragma package(smart_init)
//---------------------------------------------------------------------------
void AMPamserviceServlet::doGet(AMRequest * request, AMResponse * response )
{
    auto config = StubPamStationFactory::getCurrentPamStation()->getConfig();

    auto cmd = request->getParameter("cmd");

    if (cmd == "mode"){
        std::string content = config->getMode().dump();
        response->setContent(&content);
    } else if (cmd == "settings"){
        std::string content = config->getSettings().dump();
        response->setContent(&content);
    } else {
        std::string content = "bad cmd";
        response->setContent(&content);
        response->setResponseStatus(400);
    }

}

void AMPamserviceServlet::doPost(AMRequest * request, AMResponse * response )
{
    doPut(request,response);
}

void AMPamserviceServlet::doPut(AMRequest * request, AMResponse * response )
{
    auto cmd = request->getParameter("cmd");

    if (cmd == "mode"){
        // we don't need the singleton current pamstation because we need to restart anyway
        auto config = AMConfiguration();
        auto aConfig = json::parse(request->getContent());
        config.setMode(aConfig);
    } else if (cmd == "settings") {
        auto config = StubPamStationFactory::getCurrentPamStation()->getConfig();
        auto aConfig = json::parse(request->getContent());
        config->setSettings(aConfig);
    }  else if (cmd == "flash"){
        auto config = StubPamStationFactory::getCurrentPamStation()->getConfig();
        config->flash();
    } else {
        std::string content = "bad cmd";
        response->setContent(&content);
        response->setResponseStatus(400);
    }
}



