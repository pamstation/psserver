//
// Created by alex on 26/07/19.
//

#ifndef PSSERVER_AMPAMSERVICEASPIRATESERVLET_H
#define PSSERVER_AMPAMSERVICEASPIRATESERVLET_H


#include "AMServletBase.h"
#include "AMRequest.h"
#include "AMResponse.h"


class AMPamserviceAspirateServlet : virtual public AMServletBase
{
public:
    virtual void doGet(AMRequest * request, AMResponse * response );
    virtual void doPut(AMRequest * request, AMResponse * response );
    virtual void doPost(AMRequest * request, AMResponse * response );
    unsigned run(unsigned wellBitSet, unsigned aspirateTime,  unsigned pumpUpTime);
};


#endif //PSSERVER_AMPAMSERVICEASPIRATESERVLET_H
