//
// Created by alex on 11/07/19.
//
#include "modules/pimcore/PamStationFactory.h"
#include "modules/pimcore/PamStation.h"
#include "modules/pimcore/WagonHeater.h"
#include "modules/pimcore/IncubationChamberHeater.h"


#include "AMPamserviceTempServlet.h"
#include "json.hpp"

using json = nlohmann::json;

void AMPamserviceTempServlet::doGet(AMRequest *request, AMResponse *response) {

    auto temperature = request->getParameter("temperature");

    if (temperature != "none") {
        PamStationFactory::getCurrentPamStation()->getWagonHeater()->setTargetValue(std::stof(temperature));
    }

    json temp = {{"kind",         "Temperatures"},
                 {"coverLeft",    PamStationFactory::getCurrentPamStation()->getIncubationChamberHeater(
                         eIncChmbrHtrCoverLeft)->getActualValue()},
                 {"coverMiddle",  PamStationFactory::getCurrentPamStation()->getIncubationChamberHeater(
                         eIncChmbrHtrCoverMiddle)->getActualValue()},
                 {"coverRight",   PamStationFactory::getCurrentPamStation()->getIncubationChamberHeater(
                         eIncChmbrHtrCoverRight)->getActualValue()},
                 {"bottomLeft",   PamStationFactory::getCurrentPamStation()->getIncubationChamberHeater(
                         eIncChmbrHtrBottomLeft)->getActualValue()},
                 {"bottomMiddle", PamStationFactory::getCurrentPamStation()->getIncubationChamberHeater(
                         eIncChmbrHtrBottomMiddle)->getActualValue()},
                 {"bottomRight",  PamStationFactory::getCurrentPamStation()->getIncubationChamberHeater(
                         eIncChmbrHtrBottomRight)->getActualValue()}};

    std::string content = temp.dump();
    response->setContent(&content);
}

void AMPamserviceTempServlet::doPost(AMRequest *request, AMResponse *response) {
    doPut(request, response);
}

void AMPamserviceTempServlet::doPut(AMRequest *request, AMResponse *response) {

}