//
// Created by alex on 12/07/19.
//

#ifndef PSSERVER_AMPAMSERVICECAMERASERVLET_H
#define PSSERVER_AMPAMSERVICECAMERASERVLET_H


#include "AMServletBase.h"
#include "AMRequest.h"
#include "AMResponse.h"

#include "modules/pimcore/PamDefs.h"
#include "modules/pimcore/Image.h"

class AMPamserviceCameraServlet : virtual public AMServletBase
{
public:
    virtual void doGet(AMRequest * request, AMResponse * response );
    virtual void doPut(AMRequest * request, AMResponse * response );
    virtual void doPost(AMRequest * request, AMResponse * response );
    Image * runRead(EWellNo currentWell, EFilterNo currentFilter, unsigned exposureTime);
    Image * snapshot(unsigned exposureTime);
    void runAutoFocus(unsigned wellBitSet, EFilterNo currentFilter, unsigned exposureTime, float focusRange);
};


#endif //PSSERVER_AMPAMSERVICECAMERASERVLET_H
