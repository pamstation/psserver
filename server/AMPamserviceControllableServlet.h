 
#ifndef PSSERVER_AMPAMSERVICECCONTROLLABLESERVLET_H
#define PSSERVER_AMPAMSERVICECCONTROLLABLESERVLET_H



#include "AMServletBase.h"
#include "AMRequest.h"
#include "AMResponse.h"

class AMPamserviceControllableServlet : virtual public AMServletBase
{
public:
    virtual void doGet(AMRequest * request, AMResponse * response );
    virtual void doPut(AMRequest * request, AMResponse * response );
    virtual void doPost(AMRequest * request, AMResponse * response );
};

#endif //PSSERVER_AMPAMSERVICECCONTROLLABLESERVLET_H
