//
// Created by alex on 17/07/19.
//

#include "AMPamserviceValvesServlet.h"

#include "modules/systemlayer/StubPamStationFactory.h"
#include "modules/systemlayer/StubPamStation.h"
#include "modules/propertylayer/PropDisposable.h"
#include "modules/pimcore/OverPressureSupplier.h"    //for obtaining & using object
#include "modules/pimcore/UnderPressureSupplier.h"


void AMPamserviceValvesServlet::doGet(AMRequest *request, AMResponse *response) {
    auto disposable = StubPamStationFactory::getCurrentPamStation()->getPropDisposable();

    auto cmd = request->getParameter("cmd");

    if (cmd == "close") {
        disposable->closeAllValves();
    } else if (cmd == "open.well"){
        auto wellBitSet = std::stoi(request->getParameter("wellBitSet"));
        disposable->openSelectedWellValves((unsigned)wellBitSet);
    } else if (cmd == "close.well"){
        auto wellBitSet = std::stoi(request->getParameter("wellBitSet"));
        disposable->closeSelectedWellValves((unsigned)wellBitSet);
    } else if (cmd == "open.pressure"){
        disposable->openSelectedPressureValves(EDropletPosition::eDropletUp);
    } else if (cmd == "open.vacuum"){
        disposable->openSelectedPressureValves(EDropletPosition::eDropletDown);
    } else if (cmd == "close.pressure"){
        disposable->closeSelectedPressureValves(EDropletPosition::eDropletUp);
    } else if (cmd == "close.vacuum"){
        disposable->closeSelectedPressureValves(EDropletPosition::eDropletDown);
    } else if (cmd == "set.pressure"){
        OverPressureSupplier * pOverPressureSupplier  = StubPamStationFactory::getCurrentPamStation()->getOverPressureSupplier();
        auto value = std::stof(request->getParameter("value"));
        pOverPressureSupplier ->setTargetValue(value );   //throws if protocol setting doesn't match controller setting for range
        pOverPressureSupplier ->waitReady(); //throws CONTROLLABLE_HW_ERROR_NOTIFICATION or CONTROLLABLE_TIMEOUT
    } else if (cmd == "set.vacuum"){
        UnderPressureSupplier * pUnderPressureSupplier  = StubPamStationFactory::getCurrentPamStation()->getUnderPressureSupplier();
        auto value = std::stof(request->getParameter("value"));
        pUnderPressureSupplier ->setTargetValue(value );   //throws if protocol setting doesn't match controller setting for range
        pUnderPressureSupplier ->waitReady(); //throws CONTROLLABLE_HW_ERROR_NOTIFICATION or CONTROLLABLE_TIMEOUT
    }  else if (cmd == "get.pressure"){
        OverPressureSupplier * pOverPressureSupplier  = StubPamStationFactory::getCurrentPamStation()->getOverPressureSupplier();
        json temp = {{"kind",  "OverPressureSupplierValue"},
                     {"value", pOverPressureSupplier->getActualValue() }};
        std::string content = temp.dump();
        response->setContent(&content);
    } else if (cmd == "get.vacuum"){
        UnderPressureSupplier * pUnderPressureSupplier  = StubPamStationFactory::getCurrentPamStation()->getUnderPressureSupplier();
        json temp = {{"kind",  "UnderPressureSupplierValue"},
                     {"value", pUnderPressureSupplier->getActualValue() }};
        std::string content = temp.dump();
        response->setContent(&content);
    } else {
        response->setResponseStatus(400);
    }
}

void AMPamserviceValvesServlet::doPost(AMRequest *request, AMResponse *response) {
    doPut(request, response);
}

void AMPamserviceValvesServlet::doPut(AMRequest *request, AMResponse *response) {

}