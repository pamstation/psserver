//
// Created by alex on 17/07/19.
//

#ifndef PSSERVER_AMPAMSERVICEVALVESSERVLET_H
#define PSSERVER_AMPAMSERVICEVALVESSERVLET_H


#include "AMServletBase.h"
#include "AMRequest.h"
#include "AMResponse.h"

class AMPamserviceValvesServlet : virtual public AMServletBase
{
public:
    virtual void doGet(AMRequest * request, AMResponse * response );
    virtual void doPut(AMRequest * request, AMResponse * response );
    virtual void doPost(AMRequest * request, AMResponse * response );
};

#endif //PSSERVER_AMPAMSERVICEVALVESSERVLET_H
