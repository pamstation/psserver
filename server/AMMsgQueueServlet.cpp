//---------------------------------------------------------------------------
#pragma hdrstop

#include <string>


#include "AMMsgQueueServlet.h"
#include "AMRequest.h"
#include "AMResponse.h"

#include "Protocol.h"
#include "AMRequestDispatcher.h"

#pragma package(smart_init)
//---------------------------------------------------------------------------
 


void AMMsgQueueServlet::doGet(AMRequest * request, AMResponse * response )
{
    std::string content = "";
    AMRequestDispatcher::getCurrent()->getMsgs(&content);
    std::string contentType = "text/xml";
    response->setContentType(&contentType);
    response->setContent(&content);
}

void AMMsgQueueServlet::doPost(AMRequest * request, AMResponse * response )
{
    std::string c = request->getContent();
    int index = std::stoi(c);

    if ( index < 0 ) {
        response->setResponseStatus(400 );   //Bad Request
    } else {
        std::string msg = "";
         AMRequestDispatcher::getCurrent()->getMsgs(index, &msg);
        std::string contentType = "text/xml";
        response->setContentType(&contentType);
        response->setContent(&msg);
    }
}
 
