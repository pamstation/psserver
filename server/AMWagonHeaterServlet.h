#ifndef AMWagonHeaterServletH
#define AMWagonHeaterServletH

#include "AMServletBase.h"
#include "AMRequest.h"
#include "AMResponse.h"

class AMWagonHeaterServlet : virtual public AMServletBase
{
public:
    virtual void doGet(AMRequest * request, AMResponse * response );
};

#endif //AMWagonHeaterServletH



