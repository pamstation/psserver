//
// Created by alex on 11/07/19.
//

#ifndef PSSERVER_AMPAMSERVICETEMPSERVLET_H
#define PSSERVER_AMPAMSERVICETEMPSERVLET_H

#include "AMServletBase.h"
#include "AMRequest.h"
#include "AMResponse.h"

class AMPamserviceTempServlet : virtual public AMServletBase
{
public:
    virtual void doGet(AMRequest * request, AMResponse * response );
    virtual void doPut(AMRequest * request, AMResponse * response );
    virtual void doPost(AMRequest * request, AMResponse * response );
};

#endif //PSSERVER_AMPAMSERVICETEMPSERVLET_H
