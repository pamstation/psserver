//
// Created by alex on 06/03/19.
//

#include <string>

#include "AMPingServlet.h"
#include "AMRequest.h"
#include "AMResponse.h"

void AMPingServlet::doGet(AMRequest * request, AMResponse * response )
{
    std::string c = "OK";
    response->setContent(&c);
}