#ifndef AMAbortProtocolServletH
#define AMAbortProtocolServletH

#include "AMServletBase.h"
#include "AMRequest.h"
#include "AMResponse.h"

class AMAbortProtocolServlet : virtual public AMServletBase
{
public:
    virtual void doGet(AMRequest * request, AMResponse * response );
};

#endif //AMAbortProtocolServletH



