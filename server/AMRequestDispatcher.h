//
// Created by alex on 06/03/19.
//

#ifndef PSSERVER_AMREQUESTDISPATCHER_H
#define PSSERVER_AMREQUESTDISPATCHER_H

#include <map>
#include <vector>
#include <mutex>
#include <thread>

#include "modules/pimcore/ExceptionHandler.h"
#include "modules/pimcore/PamStationException.h"
#include "AMSimpleLogger.h"

#include "Protocol.h"

//class Protocol;
class AMRequest;
class AMResponse;
class AMServlet;
class AMProtocolMsg;
class AMQueue;

typedef enum {
    pTurnOff = 0,
    pTurnOn = 1,
    pLoaded = 2
} PSState;

class AMRequestDispatcher  : virtual public ExceptionHandler {
public:
    static AMRequestDispatcher *getCurrent();

    static void deleteCurrent();

    AMRequestDispatcher();

    ~AMRequestDispatcher();

    //ExceptionHandler
    virtual void onException(void *source, PamStationException *exception);

    virtual void addMsg(AMProtocolMsg *msg);

    virtual bool internalDispatch(AMRequest *request, AMResponse *response);

    virtual void dispatch(AMRequest *request, AMResponse *response);

    virtual void addServlet(std::string path, AMServlet *servlet);

    virtual bool loadProtocol(Protocol *p);

    virtual bool unloadProtocol();

    virtual int runProtocol();

    virtual int abortProtocol();

    virtual void getProtocolMsgs(std::string * out);

    virtual void getProtocolMsgs(int i, std::string * out);

    virtual void getMsgs(std::string * out);

    virtual void getMsgs(int i, std::string * out);

    virtual int turnOn();

    virtual int shutdown();

    virtual void setState(PSState aState);

    virtual bool isTurnOn();

    Protocol *protocol;
    AMSimpleLogger *logger;

private:
    static AMRequestDispatcher *current;

    PSState state;
    std::mutex mutex;
//    TCriticalSection *criticalSection;
    std::map<std::string, AMServlet *> *servletMap;

    AMQueue *msgQueue_;

    std::thread *protocolThread;
};


#endif //PSSERVER_AMREQUESTDISPATCHER_H
