//
// Created by alex on 12/07/19.
//

#ifndef PSSERVER_AMPAMSERVICEWAGONSERVLET_H
#define PSSERVER_AMPAMSERVICEWAGONSERVLET_H



#include "AMServletBase.h"
#include "AMRequest.h"
#include "AMResponse.h"

class AMPamserviceWagonServlet : virtual public AMServletBase
{
public:
    virtual void doGet(AMRequest * request, AMResponse * response );
    virtual void doPut(AMRequest * request, AMResponse * response );
    virtual void doPost(AMRequest * request, AMResponse * response );
};

#endif //PSSERVER_AMPAMSERVICEWAGONSERVLET_H
