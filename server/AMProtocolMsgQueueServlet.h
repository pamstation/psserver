#ifndef AMProtocolMsgQueueServletH
#define AMProtocolMsgQueueServletH

#include "AMServletBase.h"
#include "AMRequest.h"
#include "AMResponse.h"



class AMProtocolMsgQueueServlet : virtual public AMServletBase
{
public:
    virtual void doGet(AMRequest * request, AMResponse * response );
    virtual void doPost(AMRequest * request, AMResponse * response );
};

#endif //AMProtocolMsgQueueServletH
