//
// Created by alex on 06/03/19.
//

#ifndef PSSERVER_AMRESPONSEIMPL_H
#define PSSERVER_AMRESPONSEIMPL_H


#include "Simple-Web-Server/server_http.hpp"
#include "Simple-Web-Server/status_code.hpp"

#include "AMResponse.h"

using HttpServer = SimpleWeb::Server<SimpleWeb::HTTP>;

class AMResponseImpl : virtual public AMResponse {
public:
    AMResponseImpl(std::shared_ptr<HttpServer::Response> response);

    ~AMResponseImpl();

    virtual void setContent(std::string *content);
    virtual void setStreamContent(std::istream * stream);

    virtual void setContentType(std::string *content);

    virtual void setResponseStatus(int status);

    virtual void write();


    std::shared_ptr<HttpServer::Response> response;
    SimpleWeb::StatusCode status;
    std::string content;
    std::istream * stream;
    SimpleWeb::CaseInsensitiveMultimap header;

};


#endif //PSSERVER_AMRESPONSEIMPL_H
