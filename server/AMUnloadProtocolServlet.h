#ifndef AMUnloadProtocolServletH
#define AMUnloadProtocolServletH

#include "AMServletBase.h"
#include "AMRequest.h"
#include "AMResponse.h"

class AMUnloadProtocolServlet : virtual public AMServletBase
{
public:
    virtual void doGet(AMRequest * request, AMResponse * response );
};

#endif //AMUnloadProtocolServlet


