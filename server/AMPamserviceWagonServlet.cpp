//
// Created by alex on 12/07/19.
//

#include "AMPamserviceWagonServlet.h"


#include "modules/pimcore/PamStationFactory.h"
#include "modules/pimcore/PamStation.h"
#include "modules/pimcore/WagonXYPositioner.h"
#include "modules/pimcore/DispenseHeadPositioner.h"

#include "json.hpp"

using json = nlohmann::json;

void AMPamserviceWagonServlet::doGet(AMRequest *request, AMResponse *response) {

    try {
        auto position = request->getParameter("position");

        if (position == "home") {
            if (PamStationFactory::getCurrentPamStation()->getWagonXYPositioner()->getHomed()){
                // if we are in home position !!!
                auto actualPosition = PamStationFactory::getCurrentPamStation()->getWagonXYPositioner()->getActualValue();
                actualPosition.X -= 2;
                actualPosition.Y += 2;

                PamStationFactory::getCurrentPamStation()->getWagonXYPositioner()->setTargetValue(actualPosition);
                PamStationFactory::getCurrentPamStation()->getWagonXYPositioner()->waitReady();
            }
            PamStationFactory::getCurrentPamStation()->getWagonXYPositioner()->goHome();
            PamStationFactory::getCurrentPamStation()->getWagonXYPositioner()->waitReady();
        } else if (position == "load") {
            PamStationFactory::getCurrentPamStation()->getWagonXYPositioner()->goLoadPosition();
            PamStationFactory::getCurrentPamStation()->getWagonXYPositioner()->waitReady();
        } else if (position == "loadSafe") {

            if ( ! PamStationFactory::getCurrentPamStation()->getWagonXYPositioner()->getHomed() )
            {
                PamStationFactory::getCurrentPamStation()->getWagonXYPositioner()->goHome();
                PamStationFactory::getCurrentPamStation()->getWagonXYPositioner()->waitReady();
             }

            PamStationFactory::getCurrentPamStation()->getWagonXYPositioner()->goFrontPanelPosition();
            PamStationFactory::getCurrentPamStation()->getWagonXYPositioner()->waitReady();

            PamStationFactory::getCurrentPamStation()->getWagonXYPositioner()->goLoadPosition();
            PamStationFactory::getCurrentPamStation()->getWagonXYPositioner()->waitReady();

        } else if (position == "front") {
            PamStationFactory::getCurrentPamStation()->getWagonXYPositioner()->goFrontPanelPosition();
            PamStationFactory::getCurrentPamStation()->getWagonXYPositioner()->waitReady();
        } else if (position == "incubation") {
            PamStationFactory::getCurrentPamStation()->getWagonXYPositioner()->goIncubationPosition();
            PamStationFactory::getCurrentPamStation()->getWagonXYPositioner()->waitReady();
        } else if (position == "read") {
            auto well = std::stoi(request->getParameter("well"));
            if (well < 1 || well > 12) {
                response->setResponseStatus(401);
                return ;
            }
            auto w = (EWellNo) well;
            PamStationFactory::getCurrentPamStation()->getWagonXYPositioner()->goReadPosition(w);
            PamStationFactory::getCurrentPamStation()->getWagonXYPositioner()->waitReady();
        }  else if (position == "aspirate") {
            auto well = std::stoi(request->getParameter("well"));
            if (well < 1 || well > 12) {
                response->setResponseStatus(401);
                return ;
            }
            auto w = (EWellNo) well;
            PamStationFactory::getCurrentPamStation()->getWagonXYPositioner()->goAspiratePosition(w);
            PamStationFactory::getCurrentPamStation()->getWagonXYPositioner()->waitReady();
        }  else if (position == "prime") {
            auto dispenseHead = std::stoi(request->getParameter("dispenseHead"));
            if (dispenseHead < 1 || dispenseHead > 4) {
                response->setResponseStatus(401);
                return;
            }
            auto dh = (EDispenseHeadNo) dispenseHead;
            PamStationFactory::getCurrentPamStation()->getWagonXYPositioner()->goPrimePosition(dh);
            PamStationFactory::getCurrentPamStation()->getWagonXYPositioner()->waitReady();
        } else if (position == "dispense") {
            auto well = std::stoi(request->getParameter("well"));
            if (well < 1 || well > 12) {
                response->setResponseStatus(401);
                return ;
            }
            auto w = (EWellNo) well;
            auto dispenseHead = std::stoi(request->getParameter("dispenseHead"));
            if (dispenseHead < 1 || dispenseHead > 4) {
                response->setResponseStatus(401);
                return;
            }
            auto dh = (EDispenseHeadNo) dispenseHead;
            PamStationFactory::getCurrentPamStation()->getWagonXYPositioner()->goDispensePosition(w,dh);
            PamStationFactory::getCurrentPamStation()->getWagonXYPositioner()->waitReady();
        } else if (position == "move") {

            auto actualPosition = PamStationFactory::getCurrentPamStation()->getWagonXYPositioner()->getActualValue();
            if (!request->getParameter("x").empty())            actualPosition.X = std::stof(request->getParameter("x"));
            if (!request->getParameter("y").empty())            actualPosition.Y = std::stof(request->getParameter("y"));

            PamStationFactory::getCurrentPamStation()->getWagonXYPositioner()->setTargetValue(actualPosition);
            PamStationFactory::getCurrentPamStation()->getWagonXYPositioner()->waitReady();

            json temp = {{"kind",  "XYPos"},
                         {"x", actualPosition.X },
                         {"y", actualPosition.Y }};

            std::string content = temp.dump();
            response->setContent(&content);
        }  else {
            response->setResponseStatus(401);
        }
    } catch (...){
        PamStationFactory::getCurrentPamStation()->getWagonXYPositioner()->setEnabled(false);
        throw ;
    }
    PamStationFactory::getCurrentPamStation()->getWagonXYPositioner()->setEnabled(false);

}

void AMPamserviceWagonServlet::doPost(AMRequest *request, AMResponse *response) {
    doPut(request, response);
}

void AMPamserviceWagonServlet::doPut(AMRequest *request, AMResponse *response) {

}