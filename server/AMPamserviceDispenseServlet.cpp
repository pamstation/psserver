//
// Created by alex on 26/07/19.
//

#include "AMPamserviceDispenseServlet.h"

#include "modules/systemlayer/SysExceptionGenerator.h"      //for passing Exception to ExceptionGenerator
#include "modules/pimcore/ExceptionGenerator.h"
#include "modules/systemlayer/SysExceptionGenerator.h"

#include "modules/pimcore/PamStationFactory.h"      //for obtaining & using PamStation
#include "modules/pimcore/PamStation.h"             //for obtaining & using PamStation
#include "modules/pimcore/DefaultGenerator.h"       //for obtaining DispenseDefaults
#include "modules/pimcore/DispenseHeadHeater.h"     //for obtaining & using object
#include "modules/pimcore/DispenseHeadPositioner.h" //for obtaining & using object
#include "modules/pimcore/Disposable.h"             //for obtaining & using object
#include "modules/pimcore/WagonXYPositioner.h"      //for obtaining & using object

#include "modules/pimcore/WagonHeater.h"
#include "modules/pimcore/OverPressureSupplier.h"    //for obtaining & using object
#include "modules/pimcore/UnderPressureSupplier.h"

#include "modules/utils/util.h"


#include "json.hpp"

using json = nlohmann::json;

void AMPamserviceDispenseServlet::doGet(AMRequest *request, AMResponse *response) {

    auto wellBitSet = (unsigned)std::stoi(request->getParameter("wellBitSet"));
    auto dispenseHead = (unsigned)std::stoi(request->getParameter("dispenseHead"));
    auto amount = std::stof(request->getParameter("amount"));
    auto temperature = std::stof(request->getParameter("temperature"));
    auto waitTimeAfterLastWellDispensed = (unsigned)std::stoi(request->getParameter("waitTimeAfterLastWellDispensed"));

    if (dispenseHead >= EDispenseHeadNo::eHeadMax){
        std::string content = "bad dispense head";
        response->setContent(&content);
        response->setResponseStatus(401);
    } else {
        unsigned brokenWellBitSet = run(wellBitSet, dispenseHead, amount, temperature, waitTimeAfterLastWellDispensed);

        if (brokenWellBitSet > 0) {
            json temp = {{"kind",  "BrokenWellBitSet"},
                         {"value", brokenWellBitSet}};

            std::string content = temp.dump();
            response->setContent(&content);
            response->setResponseStatus(400);
        }
    }


}

void AMPamserviceDispenseServlet::doPost(AMRequest *request, AMResponse *response) {
    auto dispenseHead = (unsigned)std::stoi(request->getParameter("dispenseHead"));
    auto amount = std::stof(request->getParameter("amount"));

    PamStation *pPamStation = PamStationFactory::getCurrentPamStation();
    DispenseHeadPositioner *pDispenseHeadPositioner = pPamStation->getDispenseHeadPositioner((EDispenseHeadNo)dispenseHead);
    DefaultGenerator *pDefaultGenerator = pPamStation->getDefaultGenerator();

    float rRetractAmount = pDefaultGenerator->getDispenseDefaults()->rRetractAmount;
    unsigned uRetractDelay = pDefaultGenerator->getDispenseDefaults()->uRetractDelay;

    //dispense fluid (+ extra retract-amount)
    pDispenseHeadPositioner->setTargetValue(amount + rRetractAmount);
    pDispenseHeadPositioner->waitReady();

    //wait some time to prevent droplet-forming
    Util::Sleep(uRetractDelay);

    //retract syringe to prevent droplet forming
    pDispenseHeadPositioner->setTargetValue(-rRetractAmount);
    pDispenseHeadPositioner->waitReady();

    response->setResponseStatus(200);
}

void AMPamserviceDispenseServlet::doPut(AMRequest *request, AMResponse *response) {

}

unsigned
AMPamserviceDispenseServlet::run(unsigned wellBitSet,  unsigned dispenseHead, float amount, float temperature,
                                 unsigned waitTimeAfterLastWellDispensed) {
    PamStation *pPamStation = PamStationFactory::getCurrentPamStation();
    DefaultGenerator *pDefaultGenerator = pPamStation->getDefaultGenerator();
    DispenseHeadHeater *pDispenseHeadHeater = pPamStation->getDispenseHeadHeater();
    DispenseHeadPositioner *pDispenseHeadPositioner = pPamStation->getDispenseHeadPositioner((EDispenseHeadNo)dispenseHead);
    Disposable *pDisposable = pPamStation->getDisposable();
    WagonXYPositioner *pWagonXYPositioner = pPamStation->getWagonXYPositioner();

    WagonHeater          * pWagonHeater           = pPamStation->getWagonHeater();
    OverPressureSupplier * pOverPressureSupplier  = pPamStation->getOverPressureSupplier();
    UnderPressureSupplier* pUnderPressureSupplier = pPamStation->getUnderPressureSupplier();

    pDispenseHeadPositioner->checkDispenseHeadPresent();  //thows DISPENSEHEAD_NOT_PRESENT

    try   //tryI:make sure that finally DispenseHead-Position & -Temperature control are stopped
    {

        /// build pressure

        float rDefaultOverPressure  =  pDefaultGenerator->getLoad_Unload_Settings()->rDefaultOverPressure ;  //a protocol setting
        float rDefaultUnderPressure =  pDefaultGenerator->getLoad_Unload_Settings()->rDefaultUnderPressure;  //a protocol setting
        float rInitWagonTemperature =  pDefaultGenerator->getLoad_Unload_Settings()->rInitWagonTemperature;  //a protocol setting
        float rMaxTemperatureGain   =  pWagonHeater->getMaxTemperatureGain();                                //a controller setting

        //START TEMPERATURE CONTROL & PRESSURE CONTROL & GO INCUBATIONPOSITION
        pWagonHeater          ->setTemperatureGain( rMaxTemperatureGain );
        pWagonHeater          ->setTargetValue(rInitWagonTemperature);   //throws if protocol setting doesn't match controller setting for range
        pOverPressureSupplier ->setTargetValue(rDefaultOverPressure );   //throws if protocol setting doesn't match controller setting for range
        pUnderPressureSupplier->setTargetValue(rDefaultUnderPressure);   //throws if protocol setting doesn't match controller setting for range
        pWagonXYPositioner    ->goIncubationPosition();


        pOverPressureSupplier ->waitReady(); //throws CONTROLLABLE_HW_ERROR_NOTIFICATION or CONTROLLABLE_TIMEOUT
        pUnderPressureSupplier->waitReady(); //throws CONTROLLABLE_HW_ERROR_NOTIFICATION or CONTROLLABLE_TIMEOUT
        pWagonXYPositioner    ->waitReady(); //throws CONTROLLABLE_HW_ERROR_NOTIFICATION or CONTROLLABLE_TIMEOUT
        pWagonHeater          ->waitReady(); //throws CONTROLLABLE_HW_ERROR_NOTIFICATION or CONTROLLABLE_TIMEOUT

        pWagonXYPositioner->setEnabled(false);

        //////

        //get default values :
        float rRetractAmount = pDefaultGenerator->getDispenseDefaults()->rRetractAmount;
        unsigned uRetractDelay = pDefaultGenerator->getDispenseDefaults()->uRetractDelay;
        unsigned uHeatableHeadBitSet = pDefaultGenerator->getDispenseDefaults()->uHeatableHeadBitSet;

        //make sure sign  rRetractAmount is positive (see use of this rRetractAmount)
        if (rRetractAmount < 0) rRetractAmount = -rRetractAmount;

        //HEATING OF DISPENSEFLUID
        if (1 << (dispenseHead - 1) & uHeatableHeadBitSet) //check if Head is selected, 1<<(m_eHead-1): 1,2,4,8
        {
            if (temperature > 0) {
                //move to incubate position to prevent sample-evaporation during fluid heating
                pWagonXYPositioner->goIncubationPosition();
                pWagonXYPositioner->waitReady();
                //set fluid temperature
                pDispenseHeadHeater->setTargetValue(temperature);
                pDispenseHeadHeater->getTargetValue();
                pDispenseHeadHeater->waitReady();
            }
        }


        //DISPENSING OF FLUID
        //for each selected well

        unsigned currentWell = eWell1;

        for (; currentWell < eWellNoMax  ; currentWell++)   //loop for all wells, i: 1,2,3,4,5,etc
        {
            auto currentWellBitSet = (unsigned) (1 << (currentWell - 1));

            if (currentWellBitSet & wellBitSet)         //check if well is selected, 1<<(i-1): 1,2,4,8,16,etc
            {
                //place selected well under selected DispenseHead
                pWagonXYPositioner->goDispensePosition((EWellNo) currentWell, (EDispenseHeadNo)dispenseHead);
                pWagonXYPositioner->waitReady();

                //dispense fluid (+ extra retract-amount)
                pDispenseHeadPositioner->setTargetValue(amount + rRetractAmount);
                pDispenseHeadPositioner->waitReady();

                //wait some time to prevent droplet-forming
                Util::Sleep(uRetractDelay);

                //retract syringe to prevent droplet forming
                pDispenseHeadPositioner->setTargetValue(-rRetractAmount);
                pDispenseHeadPositioner->waitReady();

            }//end if
        }  //end for

        Util::Sleep(waitTimeAfterLastWellDispensed);

        pDisposable->pumpDown(wellBitSet);
//        pDisposable->waitPumpReady();

//        pDisposable->pumpDownAsync(wellBitSet).get();


        unsigned brokenWellBitSet = pDisposable->getBrokenMembranes(wellBitSet);

        if (brokenWellBitSet > 0) {
            pDispenseHeadHeater->setEnabled(false);
            pDispenseHeadPositioner->setEnabled(false);

            //stop wagonPositioner control
            pWagonXYPositioner->setEnabled(false);
            return brokenWellBitSet;
        }


        pDispenseHeadHeater->setEnabled(false);
        pDispenseHeadPositioner->setEnabled(false);

        //stop wagonPositioner control
        pWagonXYPositioner->setEnabled(false);

    }
    catch (...) {

        pDispenseHeadHeater->setEnabled(false);
        pDispenseHeadPositioner->setEnabled(false);

        //stop wagonPositioner control
        pWagonXYPositioner->setEnabled(false);

        throw;
    }
    return 0;
}
