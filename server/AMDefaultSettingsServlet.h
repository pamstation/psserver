#ifndef AMDefaultSettingsServletH
#define AMDefaultSettingsServletH

#include "AMServletBase.h"
#include "AMRequest.h"
#include "AMResponse.h"

class AMDefaultSettingsServlet : virtual public AMServletBase
{
public:
    virtual void doGet(AMRequest * request, AMResponse * response );
    virtual void doPut(AMRequest * request, AMResponse * response );
};

#endif //AMDefaultSettingsServletH

 