
//---------------------------------------------------------------------------
#pragma hdrstop

#include <string>
#include <sstream>

#include "AMProtocolServlet.h"
#include "Protocol.h"

#include "AMRequestDispatcher.h"

#pragma package(smart_init)
//---------------------------------------------------------------------------


void AMProtocolServlet::doGet(AMRequest * request, AMResponse * response )
{
    Protocol * aP = AMRequestDispatcher::getCurrent()->protocol;
    if (aP == NULL)
    {
        std::string contentType = "text/xml";
        response->setContentType(&contentType);
        response->setResponseStatus(400 );   //Bad Request
        std::string msg = "<?xml version=\"1.0\"?>"
                          "<error code=\"protocol.null\">"
                          "</error>";
        response->setContent(&msg);
        return;
    }
    std::string contentType = "text/xml";
    response->setContentType(&contentType);
    std::string content = aP->getXML();
    response->setContent(&content);
    return;
}
void AMProtocolServlet::doPost(AMRequest * request, AMResponse * response )
{
    Protocol * p = new Protocol(AMRequestDispatcher::getCurrent()->logger);
    if (!p->initializeFromXML(request->getParameter("protocol").c_str()))
    {
        delete p;
        std::string contentType = "text/xml";
        response->setContentType(&contentType);
        response->setResponseStatus(400 );   //Bad Request
        std::string msg = "<?xml version=\"1.0\"?>"
                          "<error code=\"protocol.init\"/>";
        response->setContent(&msg);

        return;
    }

    if (!AMRequestDispatcher::getCurrent()->loadProtocol(p))
    {
        delete p;
        std::string contentType = "text/xml";
        response->setContentType(&contentType);
        response->setResponseStatus(400 );   //Bad Request
        std::string msg = "<?xml version=\"1.0\"?>"
                          "<error code=\"protocol.load\"/>";
        response->setContent(&msg);
        return;
    }
}

void AMProtocolServlet::doPut(AMRequest * request, AMResponse * response )
{
    std::string c = request->getContent();
    Protocol * p = new Protocol(AMRequestDispatcher::getCurrent()->logger);
    if (!p->initializeFromXML(c.c_str()))
    {
        delete p;
        std::string contentType = "text/xml";
        response->setContentType(&contentType);
        response->setResponseStatus(400 );   //Bad Request
        std::string msg = "<?xml version=\"1.0\"?>"
                          "<error code=\"protocol.init\"/>";
        response->setContent(&msg);

        return;
    }

    if (!AMRequestDispatcher::getCurrent()->loadProtocol(p))
    {
        delete p;
        std::string contentType = "text/xml";
        response->setContentType(&contentType);
        response->setResponseStatus(400 );   //Bad Request
        std::string msg = "<?xml version=\"1.0\"?>"
                          "<error code=\"protocol.load\"/>";
        response->setContent(&msg);
        return;
    }
}






