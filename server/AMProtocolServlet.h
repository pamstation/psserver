#ifndef AMProtocolServletH
#define AMProtocolServletH

#include "AMServletBase.h"
#include "AMRequest.h"
#include "AMResponse.h"

class AMProtocolServlet : virtual public AMServletBase
{
public:
    virtual void doGet(AMRequest * request, AMResponse * response );
    virtual void doPut(AMRequest * request, AMResponse * response );
    virtual void doPost(AMRequest * request, AMResponse * response );
};

#endif //AMProtocolServletH




