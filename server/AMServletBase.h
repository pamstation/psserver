//
// Created by alex on 06/03/19.
//

#ifndef PSSERVER_AMSERVLETBASE_H
#define PSSERVER_AMSERVLETBASE_H


#include "AMServlet.h"
#include "AMRequest.h"
#include "AMResponse.h"

class AMServletBase : virtual public AMServlet
{
public:
    AMServletBase();
    virtual void service(AMRequest * request, AMResponse * response );
    virtual void doGet(AMRequest * request, AMResponse * response );
    virtual void doPost(AMRequest * request, AMResponse * response );
    virtual void doPut(AMRequest * request, AMResponse * response );
};


#endif //PSSERVER_AMSERVLETBASE_H
