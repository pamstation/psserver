//---------------------------------------------------------------------------
#pragma hdrstop

#include <string>
#include <sstream>

#include "AMButtonServlet.h"
#include "AMRequest.h"
#include "AMResponse.h"

#include "modules/pimcore/PamStationFactory.h"      //for obtaining & using PamStation
#include "modules/pimcore/PamStation.h"             //for obtaining & using PamStation
#include "modules/pimcore/UnloadButton.h"

#pragma package(smart_init)
//---------------------------------------------------------------------------
void AMButtonServlet::doGet(AMRequest * request, AMResponse * response )
{
    PamStation * pPamStation = PamStationFactory::getCurrentPamStation();
    UnloadButton * button = pPamStation->getUnloadButton();
    std::string contentType = "text/xml";
    response->setContentType(&contentType);
    std::stringstream st;
    st << "<?xml version=\"1.0\"?>";
    st << "<button status=\"";
    st << button->getEnabled();
    st << "\"";
    st << " stateId=\"";
    st << button->getStateId();
    st << "\"";
    st << "/>";

    std::string content = st.str();

    response->setContent(&content);
}

void AMButtonServlet::doPost(AMRequest * request, AMResponse * response )
{
    doPut(request,response);
}

void AMButtonServlet::doPut(AMRequest * request, AMResponse * response )
{
    PamStation * pPamStation = PamStationFactory::getCurrentPamStation();
    UnloadButton * button = pPamStation->getUnloadButton();
    button->press();
}



