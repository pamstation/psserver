
//---------------------------------------------------------------------------
#pragma hdrstop

#include <string>


#include "AMUnloadProtocolServlet.h"
#include "AMRequest.h"
#include "AMResponse.h"

#include "Protocol.h"
#include "AMRequestDispatcher.h"

#pragma package(smart_init)
//---------------------------------------------------------------------------

void AMUnloadProtocolServlet::doGet(AMRequest *request, AMResponse *response) {
    if (!AMRequestDispatcher::getCurrent()->unloadProtocol()) {
        std::string contentType = "text/xml";
        response->setContentType(&contentType);
        response->setResponseStatus(400);   //Bad Request
        std::string msg = "<?xml version=\"1.0\"?>"
                          "<error code=\"protocol.unload\">"
                          "</error>";
        response->setContent(&msg);
        return;
    }
}




