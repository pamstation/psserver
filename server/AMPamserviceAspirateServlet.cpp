//
// Created by alex on 26/07/19.
//

#include "AMPamserviceAspirateServlet.h"

//#include "modules/pimcore/PamStationFactory.h"
//#include "modules/pimcore/PamStation.h"
//
//#include "modules/pimcore/Disposable.h"             //for obtaining & using object
//#include "modules/pimcore/FilterPositioner.h"       //for obtaining & using object
//#include "modules/pimcore/FocusPositioner.h"        //for obtaining & using object
//#include "modules/pimcore/LEDUnit.h"                //for obtaining & using object
//#include "modules/pimcore/WagonXYPositioner.h"      //for obtaining & using object
//#include "modules/pimcore/WagonHeater.h"             //for obtaining & using object
//
//#include "modules/utils/util.h"
//


#include "modules/pimcore/PamStationFactory.h"      //for obtaining & using PamStation
#include "modules/pimcore/PamStation.h"             //for obtaining & using PamStation
#include "modules/pimcore/AspirateHeadPositioner.h" //for obtaining & using object
#include "modules/pimcore/AspiratePump.h"           //for obtaining & using object
#include "modules/pimcore/DefaultGenerator.h"       //for obtaining & using object
#include "modules/pimcore/Disposable.h"             //for obtaining & using object
#include "modules/pimcore/WagonXYPositioner.h"      //for obtaining & using object
#include "modules/pimcore/WagonHeater.h"
#include "modules/pimcore/OverPressureSupplier.h"    //for obtaining & using object
#include "modules/pimcore/UnderPressureSupplier.h"

#include "modules/utils/util.h"


#include "json.hpp"

using json = nlohmann::json;

void AMPamserviceAspirateServlet::doGet(AMRequest *request, AMResponse *response) {

    auto wellBitSet = (unsigned) std::stoi(request->getParameter("wellBitSet"));
    auto aspirateTime = (unsigned) std::stoi(request->getParameter("aspirateTime"));
    auto pumpUpTime = (unsigned) std::stoi(request->getParameter("pumpUpTime"));

    unsigned brokenWellBitSet = run(wellBitSet, aspirateTime, pumpUpTime);

    if (brokenWellBitSet > 0) {
        json temp = {{"kind",  "BrokenWellBitSet"},
                     {"value", brokenWellBitSet}};

        std::string content = temp.dump();
        response->setContent(&content);
        response->setResponseStatus(400);
    }
}

void AMPamserviceAspirateServlet::doPost(AMRequest *request, AMResponse *response) {
    auto cmd = request->getParameter("cmd");
    if (cmd == "up"){
        PamStation *pPamStation = PamStationFactory::getCurrentPamStation();
        AspirateHeadPositioner *pAspirateHeadPositioner = pPamStation->getAspirateHeadPositioner();
        pAspirateHeadPositioner->setAspirateHeadPosition(eUp );
        pAspirateHeadPositioner->waitReady();
        response->setResponseStatus(200);
    } else if (cmd == "down") {
        PamStation *pPamStation = PamStationFactory::getCurrentPamStation();
        AspirateHeadPositioner *pAspirateHeadPositioner = pPamStation->getAspirateHeadPositioner();
        pAspirateHeadPositioner->setAspirateHeadPosition(eDown);
        pAspirateHeadPositioner->waitReady();
        response->setResponseStatus(200);
    } else {
        std::string content = "bad cmd";
        response->setContent(&content);
        response->setResponseStatus(400);
    }
}

void AMPamserviceAspirateServlet::doPut(AMRequest *request, AMResponse *response) {

}

unsigned AMPamserviceAspirateServlet::run(unsigned wellBitSet, unsigned aspirateTime, unsigned pumpUpTime) {
    PamStation *pPamStation = PamStationFactory::getCurrentPamStation();
    AspirateHeadPositioner *pAspirateHeadPositioner = pPamStation->getAspirateHeadPositioner();
    AspiratePump *pAspiratePump = pPamStation->getAspiratePump();
    DefaultGenerator *pDefaultGenerator = pPamStation->getDefaultGenerator();
    Disposable *pDisposable = pPamStation->getDisposable();
    WagonXYPositioner *pWagonXYPositioner = pPamStation->getWagonXYPositioner();
    WagonHeater          * pWagonHeater           = pPamStation->getWagonHeater();
    OverPressureSupplier * pOverPressureSupplier  = pPamStation->getOverPressureSupplier();
    UnderPressureSupplier* pUnderPressureSupplier = pPamStation->getUnderPressureSupplier();

    unsigned uAspPressBuildupDelay = pDefaultGenerator->getAspirationDefaults()->uAspPressBuildupDelay;
    unsigned uAspirateTubeCleanDelay = pDefaultGenerator->getAspirationDefaults()->uAspirateTubeCleanDelay;



    try
    {
        /// build pressure

        float rDefaultOverPressure  =  pDefaultGenerator->getLoad_Unload_Settings()->rDefaultOverPressure ;  //a protocol setting
        float rDefaultUnderPressure =  pDefaultGenerator->getLoad_Unload_Settings()->rDefaultUnderPressure;  //a protocol setting
        float rInitWagonTemperature =  pDefaultGenerator->getLoad_Unload_Settings()->rInitWagonTemperature;  //a protocol setting
        float rMaxTemperatureGain   =  pWagonHeater->getMaxTemperatureGain();                                //a controller setting

        //START TEMPERATURE CONTROL & PRESSURE CONTROL & GO INCUBATIONPOSITION
        pWagonHeater          ->setTemperatureGain( rMaxTemperatureGain );
        pWagonHeater          ->setTargetValue(rInitWagonTemperature);   //throws if protocol setting doesn't match controller setting for range
        pOverPressureSupplier ->setTargetValue(rDefaultOverPressure );   //throws if protocol setting doesn't match controller setting for range
        pUnderPressureSupplier->setTargetValue(rDefaultUnderPressure);   //throws if protocol setting doesn't match controller setting for range
        pWagonXYPositioner    ->goIncubationPosition();


        pOverPressureSupplier ->waitReady(); //throws CONTROLLABLE_HW_ERROR_NOTIFICATION or CONTROLLABLE_TIMEOUT
        pUnderPressureSupplier->waitReady(); //throws CONTROLLABLE_HW_ERROR_NOTIFICATION or CONTROLLABLE_TIMEOUT
        pWagonXYPositioner    ->waitReady(); //throws CONTROLLABLE_HW_ERROR_NOTIFICATION or CONTROLLABLE_TIMEOUT
        pWagonHeater          ->waitReady(); //throws CONTROLLABLE_HW_ERROR_NOTIFICATION or CONTROLLABLE_TIMEOUT

        pWagonXYPositioner->setEnabled(false);

        //////

        //switch AspiratePump on
        pAspiratePump->setSwitchStatus(true);
        Util::Sleep(uAspPressBuildupDelay);                           //allow for some time to build up pressure

        //pump fluid-to-aspirate up in all selected wells
        pDisposable->pumpUp(wellBitSet, (signed) pumpUpTime);                // All selected wells
//        pDisposable->waitPumpReady();

//        pDisposable->pumpUpAsync(wellBitSet).get();

//        protocol->checkBrokenMembranes();  // All selected wells

        unsigned brokenWellBitSet = pDisposable->getBrokenMembranes(wellBitSet);

        if (brokenWellBitSet > 0) {
            return brokenWellBitSet;
        }

        unsigned currentWell = eWell1;

        //for each selected well
        for (; currentWell < eWellNoMax; currentWell++)               //loop for all wells, i: 1,2,3,4,5,etc
        {
            if ((1 << (currentWell - 1)) & wellBitSet)                     //check if well is selected, 1<<(i-1): 1,2,4,8,16,etc
            {
                //place selected well under AspirateHead
                pWagonXYPositioner->goAspiratePosition((EWellNo) currentWell);
                pWagonXYPositioner->waitReady();

                //make sure AspiratePressure is still in range
                pAspiratePump->checkAspiratePressureOk();

                try  //tryII: make sure that finally AspirateHead goes back Up
                {
                    //place AspirateHead in selected Well
                    pAspirateHeadPositioner->setAspirateHeadPosition(eDown);
                    pAspirateHeadPositioner->waitReady();

                    //allow for some time to aspirate the fluid
                    Util::Sleep(aspirateTime);

                    //bring AspirateHead -always- up
                    pAspirateHeadPositioner->setAspirateHeadPosition(eUp);
                    pAspirateHeadPositioner->waitReady();
                }

                catch (...) {
                    try {
                        //bring AspirateHead -always- up
                        pAspirateHeadPositioner->setAspirateHeadPosition(eUp);
                        pAspirateHeadPositioner->waitReady();
                    } catch (...) {}

                    throw;
                }
            } // end if
        }   // end for

        //allow for some time to empty the aspirate tubes
        Util::Sleep(uAspirateTubeCleanDelay);

        //stop -always- AspiratePump
        pAspiratePump->setSwitchStatus(false);

        //stop -always- control
        pAspirateHeadPositioner->setEnabled(false);

        //stop wagonPositioner control
        pWagonXYPositioner->setEnabled(false);

    }

    catch (...) {

        try {
            //stop -always- AspiratePump
            pAspiratePump->setSwitchStatus(false);

            //stop -always- control
            pAspirateHeadPositioner->setEnabled(false);

            //stop wagonPositioner control
            pWagonXYPositioner->setEnabled(false);
        } catch (...) {

        }


        throw;
    }
    return 0;
}


//unsigned AMPamserviceAspirateServlet::run(unsigned wellBitSet, unsigned aspirateTime) {
//    PamStation *pPamStation = PamStationFactory::getCurrentPamStation();
//    AspirateHeadPositioner *pAspirateHeadPositioner = pPamStation->getAspirateHeadPositioner();
//    AspiratePump *pAspiratePump = pPamStation->getAspiratePump();
//    DefaultGenerator *pDefaultGenerator = pPamStation->getDefaultGenerator();
//    Disposable *pDisposable = pPamStation->getDisposable();
//    WagonXYPositioner *pWagonXYPositioner = pPamStation->getWagonXYPositioner();
//
//
//    unsigned uAspPressBuildupDelay = pDefaultGenerator->getAspirationDefaults()->uAspPressBuildupDelay;
//    unsigned uAspirateTubeCleanDelay = pDefaultGenerator->getAspirationDefaults()->uAspirateTubeCleanDelay;
//
//    try  //tryI: make sure that finally AspiratePump is switched off, and AspirateHeadPositioner control is stopped
//    {
//        //switch AspiratePump on
//        pAspiratePump->setSwitchStatus(true);
//        Util::Sleep(uAspPressBuildupDelay);                           //allow for some time to build up pressure
//
//        unsigned currentWell = eWell1;
//
//        //for each selected well
//        for (; currentWell < eWellNoMax; currentWell++)               //loop for all wells, i: 1,2,3,4,5,etc
//        {
//            auto currentWellBitSet = (unsigned) (1 << (currentWell - 1));
//            if (currentWellBitSet & wellBitSet) {
//                //place selected well under AspirateHead
//                pWagonXYPositioner->goAspiratePosition((EWellNo) currentWell);
//                pWagonXYPositioner->waitReady();
//
//                //make sure the fluid in the selected well is (still) down
//                pDisposable->pumpDown(currentWellBitSet);              // (1 << (i-1)) because a WellBitSet is expected
//                pDisposable->waitPumpReady();
//
//                unsigned brokenWellBitSet = pDisposable->getBrokenMembranes(currentWellBitSet);
//
//                if (brokenWellBitSet > 0) {
//                    //stop -always- AspiratePump
//                    pAspiratePump->setSwitchStatus(false);
//
//                    //stop -always- control
//                    pAspirateHeadPositioner->setEnabled(false);
//
//                    //stop wagonPositioner control
//                    pWagonXYPositioner->setEnabled(false);
//                    return brokenWellBitSet;
//                }
//
//                //make sure AspiratePressure is still in range
//                pAspiratePump->checkAspiratePressureOk();
//
//                try  //tryII: make sure that finally AspirateHead goes back Up
//                {
//                    //place AspirateHead in selected Well
//                    pAspirateHeadPositioner->setAspirateHeadPosition(eDown);
//                    pAspirateHeadPositioner->waitReady();
//
//                    //pump fluid-to-aspirate up in selected well
//                    // (1 << (i-1)) because a WellBitSet is expected
//                    pDisposable->pumpUp(currentWellBitSet);
//                    pDisposable->waitPumpReady();
//                    if (0 == pDisposable->getBrokenMembranes(currentWellBitSet)) {
//                        //allow for some time to aspirate the fluid
//                        Util::Sleep(aspirateTime);
//                    }
//
//                    //bring AspirateHead -always- up
//                    pAspirateHeadPositioner->setAspirateHeadPosition(eUp);
//                    pAspirateHeadPositioner->waitReady();
//
//                    //allow for some time to empty the aspirate tubes
//                    Util::Sleep(uAspirateTubeCleanDelay);
//
//                }
//
//                catch (...) {
//                    //bring AspirateHead -always- up
//                    pAspirateHeadPositioner->setAspirateHeadPosition(eUp);
//                    pAspirateHeadPositioner->waitReady();
//
//                    //allow for some time to empty the aspirate tubes
//                    Util::Sleep(uAspirateTubeCleanDelay);
//
//                    throw;
//                }
//
//
//            } // end if
//        }   // end for
//
//        //stop -always- AspiratePump
//        pAspiratePump->setSwitchStatus(false);
//
//        //stop -always- control
//        pAspirateHeadPositioner->setEnabled(false);
//
//        //stop wagonPositioner control
//        pWagonXYPositioner->setEnabled(false);
//
//    }
//
//    catch (...) {
//
//        //stop -always- AspiratePump
//        pAspiratePump->setSwitchStatus(false);
//
//        //stop -always- control
//        pAspirateHeadPositioner->setEnabled(false);
//
//        //stop wagonPositioner control
//        pWagonXYPositioner->setEnabled(false);
//
//        throw;
//    }
//    return 0;
//}
