//
// Created by alex on 06/03/19.
//

#ifndef PSSERVER_AMREQUEST_H
#define PSSERVER_AMREQUEST_H



#include <string>

class AMRequest
{
public:
    virtual std::string getURL() = 0;
    virtual std::string getContent() = 0;
    virtual std::string getVerb() = 0;
    virtual std::string getParameter(std::string paramName) = 0;
};

#endif //PSSERVER_AMREQUEST_H
