//---------------------------------------------------------------------------
#pragma hdrstop

#include <string>
#include <sstream>


#include "AMProtocolMsgQueueServlet.h"
#include "AMRequest.h"
#include "AMResponse.h"


#include "Protocol.h"
#include "AMRequestDispatcher.h"

#pragma package(smart_init)

//---------------------------------------------------------------------------
void AMProtocolMsgQueueServlet::doGet(AMRequest *request, AMResponse *response) {
    std::string msg = "";
    AMRequestDispatcher::getCurrent()->getProtocolMsgs(&msg);
    if (msg.size() == 0) {
        std::string contentType = "text/xml";
        response->setContentType(&contentType);
        response->setResponseStatus(400);   //Bad Request
        std::string msg = "<?xml version=\"1.0\"?>"
                          "<error code=\"protocol.null\"/>";
        response->setContent(&msg);

        return;
    }
    std::string contentType = "text/xml";
    response->setContentType(&contentType);
    response->setContent(&msg);
}

void AMProtocolMsgQueueServlet::doPost(AMRequest *request, AMResponse *response) {
    std::string c = request->getContent();
    int index = std::stoi(c);

    if (index < 0) {
        response->setResponseStatus(400);   //Bad Request
    } else {
        std::string msg = "";
        AMRequestDispatcher::getCurrent()->getProtocolMsgs(index, &msg);
        if (msg.empty()) {
            std::string contentType = "text/xml";
            response->setContentType(&contentType);
            response->setResponseStatus(400);   //Bad Request
            std::string errorMsg = "<?xml version=\"1.0\"?>"
                                   "<error code=\"protocol.null\"/>";
            response->setContent(&errorMsg);

            return;
        }
        std::string contentType = "text/xml";
        response->setContentType(&contentType);
        response->setContent(&msg);
    }
}





