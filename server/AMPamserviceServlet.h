#ifndef AMPamserviceServletH
#define AMPamserviceServletH

#include "AMServletBase.h"
#include "AMRequest.h"
#include "AMResponse.h"

class AMPamserviceServlet : virtual public AMServletBase
{
public:
    virtual void doGet(AMRequest * request, AMResponse * response );
    virtual void doPut(AMRequest * request, AMResponse * response );
    virtual void doPost(AMRequest * request, AMResponse * response );
};

#endif //AMPamserviceServletH

