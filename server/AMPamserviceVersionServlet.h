//
// Created by alex on 11/07/19.
//

#ifndef PSSERVER_AMPAMSERVICEVERRSIONERVLET_H
#define PSSERVER_AMPAMSERVICEVERRSIONERVLET_H

#include "AMServletBase.h"
#include "AMRequest.h"
#include "AMResponse.h"

class AMPamserviceVersionServlet : virtual public AMServletBase
{
public:
    virtual void doGet(AMRequest * request, AMResponse * response );
    virtual void doPut(AMRequest * request, AMResponse * response );
    virtual void doPost(AMRequest * request, AMResponse * response );
};

#endif //PSSERVER_AMPAMSERVICEVERRSIONERVLET_H
