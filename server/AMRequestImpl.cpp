//
// Created by alex on 06/03/19.
//

#include "AMRequestImpl.h"
#include "parser/Parser.h"

AMRequestImpl::AMRequestImpl(std::shared_ptr<HttpServer::Request> req) {
    request = req;
}

AMRequestImpl::~AMRequestImpl() {

}

std::string AMRequestImpl::getURL() {
    return request->path;
}

std::string AMRequestImpl::getContent() {
    return request->content.string();
}

std::string AMRequestImpl::getVerb() {
    return request->method;
}

std::string AMRequestImpl::getParameter(std::string paramName) {
    auto query_fields = request->parse_query_string();
    for (auto &field : query_fields) {
        if (field.first == paramName) {
            return field.second;
        }
    }

    auto contentType = request->header.find("content-type");
    if (contentType != request->header.end() && contentType->second.find("multipart/form-data;") == 0) {
        MPFD::Parser parser = MPFD::Parser();

        parser.SetContentType(contentType->second);
        parser.SetMaxCollectedDataLength(2048);
        auto body = request->content.string();
        parser.AcceptSomeData(body.c_str(), body.length());

        auto field = parser.GetField(paramName);

        if (field != nullptr) {
            return field->GetTextTypeContent();
        }

    }


    return "";
}
