//
// Created by alex on 06/03/19.
//

#ifndef PSSERVER_AMREQUESTIMPL_H
#define PSSERVER_AMREQUESTIMPL_H

#include "Simple-Web-Server/server_http.hpp"

#include "AMRequest.h"

using HttpServer = SimpleWeb::Server<SimpleWeb::HTTP>;

class AMRequestImpl  : virtual public AMRequest {
public:
    AMRequestImpl(std::shared_ptr<HttpServer::Request> request);
    ~AMRequestImpl();
    virtual std::string getURL();
    virtual std::string getContent();
    virtual std::string getVerb();
    virtual std::string getParameter(std::string paramName);

    std::shared_ptr<HttpServer::Request> request;

};


#endif //PSSERVER_AMREQUESTIMPL_H
