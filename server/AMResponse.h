//
// Created by alex on 06/03/19.
//

#ifndef PSSERVER_AMRESPONSE_H
#define PSSERVER_AMRESPONSE_H


#include <string>

class AMResponse
{
public:
    virtual void setContent(std::string * content) = 0;
    virtual void setStreamContent(std::istream * stream) = 0;
    virtual void setContentType(std::string * content) = 0;
    virtual void setResponseStatus(int status) = 0;

    virtual void write() = 0;
};

#endif //PSSERVER_AMRESPONSE_H
