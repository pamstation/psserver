//
// Created by alex on 11/07/19.
//

#ifndef PSSERVER_AMPAMSERVICEPRESSURESERVLET_H
#define PSSERVER_AMPAMSERVICEPRESSURESERVLET_H


#include "AMServletBase.h"
#include "AMRequest.h"
#include "AMResponse.h"

class AMPamservicePressureServlet : virtual public AMServletBase
{
public:
    virtual void doGet(AMRequest * request, AMResponse * response );
    virtual void doPut(AMRequest * request, AMResponse * response );
    virtual void doPost(AMRequest * request, AMResponse * response );
};

#endif //PSSERVER_AMPAMSERVICEPRESSURESERVLET_H
