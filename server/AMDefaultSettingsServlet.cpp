//---------------------------------------------------------------------------
#pragma hdrstop

#include <string>
#include <sstream>

#include "AMDefaultSettingsServlet.h"
#include "AMRequest.h"
#include "AMResponse.h"

#include "tinyxml2.h"

#include "modules/pimcore/PamStationFactory.h"      //for obtaining & using PamStation
#include "modules/pimcore/PamStation.h"             //for obtaining & using PamStation
#include "modules/pimcore/DefaultGenerator.h"
#include "modules/pimcore/PamDefs_Defaults.h"


#pragma package(smart_init)
//---------------------------------------------------------------------------

void AMDefaultSettingsServlet::doGet(AMRequest * request, AMResponse * response )
{

    std::stringstream st;
    st << "<?xml version=\"1.0\"?>";
    st << "<settings>";

    st << "<AspirationSettings";

    st << " rPressureUnderlimit=\"";
    st << PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getAspirationDefaults()->rPressureUnderlimit;
    st << "\"";
    st << " rPressureUpperLimit=\"";
    st << PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getAspirationDefaults()->rPressureUpperLimit;
    st << "\"";
    st << " uAspirateTubeCleanDelay=\"";
    st << PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getAspirationDefaults()->uAspirateTubeCleanDelay;
    st << "\"";
    st << " uAspPressBuildupDelay=\"";
    st << PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getAspirationDefaults()->uAspPressBuildupDelay;
    st << "\"";

    st << "/>";

    st << "<DispenseSettings";

    st << " rPrimeAmount=\"";
    st << PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getDispenseDefaults()->rPrimeAmount;
    st << "\"";
    st << " rRetractAmount=\"";
    st << PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getDispenseDefaults()->rRetractAmount;
    st << "\"";
    st << " uRetractDelay=\"";
    st << PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getDispenseDefaults()->uRetractDelay;
    st << "\"";

    st << "/>";

    st << "<Load_Unload_Settings";

    st << " rDefaultOverPressure=\"";
    st << PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getLoad_Unload_Settings()->rDefaultOverPressure;
    st << "\"";
    st << " rDefaultUnderPressure=\"";
    st << PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getLoad_Unload_Settings()->rDefaultUnderPressure;
    st << "\"";
    st << " rInitWagonTemperature=\"";
    st << PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getLoad_Unload_Settings()->rInitWagonTemperature;
    st << "\"";
    st << " rSafeUnloadTemperature=\"";
    st << PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getLoad_Unload_Settings()->rSafeUnloadTemperature;
    st << "\"";

    st << "/>";

    st << "<PumpSettings";

    st << " uValveOpenTime=\"";
    st << PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getPumpingDefaults()->uValveOpenTime;
    st << "\"";
    st << " uPressureSettlingDelay=\"";
    st << PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getPumpingDefaults()->uPressureSettlingDelay;
    st << "\"";
    st << " uFluidFlowDelay=\"";
    st << PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getPumpingDefaults()->uFluidFlowDelay;
    st << "\"";
    st << " rBrokenMembranePressure=\"";
    st << PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getPumpingDefaults()->rBrokenMembranePressure;
    st << "\"";
    st << " uMaximumWaitingTime=\"";
    st << PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getPumpingDefaults()->uMaximumWaitingTime;
    st << "\"";

    st << "/>";

    st << "</settings>";

    std::string contentType = "text/xml";
    response->setContentType(&contentType);
    std::string content = st.str();
    response->setContent(&content);

}

void AMDefaultSettingsServlet::doPut(AMRequest * request, AMResponse * response )
{
    std::string xml = request->getContent();
    tinyxml2::XMLDocument doc;
    if (doc.Parse( xml.c_str() ) > 0)
    {
        response->setResponseStatus(400 );   //Bad Request
    }
    tinyxml2::XMLElement* settingsElement = doc.FirstChildElement( "settings" );
    if (settingsElement == NULL)
    {
        response->setResponseStatus(400 );   //Bad Request
    }
    tinyxml2::XMLElement* settingElement = settingsElement->FirstChildElement( "AspirationSettings" );
    float f;
    unsigned u;
    if (settingElement)
    {
        AspirationSettings_t * s = PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getAspirationDefaults();
        settingElement->QueryFloatAttribute("rPressureUnderlimit", &(s->rPressureUnderlimit)) ;
        settingElement->QueryFloatAttribute("rPressureUpperLimit", &(s->rPressureUpperLimit)) ;
        settingElement->QueryUnsignedAttribute("uAspirateTubeCleanDelay", &(s->uAspirateTubeCleanDelay)) ;
        settingElement->QueryUnsignedAttribute("uAspPressBuildupDelay", &(s->uAspPressBuildupDelay));
    }
    settingElement = settingsElement->FirstChildElement( "DispenseSettings" );
    if (settingElement)
    {
        DispenseSettings_t * s = PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getDispenseDefaults();
        settingElement->QueryFloatAttribute("rPrimeAmount", &(s->rPrimeAmount)) ;
        settingElement->QueryFloatAttribute("rRetractAmount", &(s->rRetractAmount)) ;
        settingElement->QueryUnsignedAttribute("uRetractDelay", &(s->uRetractDelay)) ;
    }
    settingElement = settingsElement->FirstChildElement( "Load_Unload_Settings" );
    if (settingElement)
    {
        Load_Unload_Settings_t * s = PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getLoad_Unload_Settings();
        settingElement->QueryFloatAttribute("rDefaultOverPressure", &(s->rDefaultOverPressure)) ;
        settingElement->QueryFloatAttribute("rDefaultUnderPressure", &(s->rDefaultUnderPressure)) ;
        settingElement->QueryFloatAttribute("rInitWagonTemperature", &(s->rInitWagonTemperature)) ;
        settingElement->QueryFloatAttribute("rSafeUnloadTemperature", &(s->rSafeUnloadTemperature)) ;
    }
    settingElement = settingsElement->FirstChildElement( "PumpSettings" );
    if (settingElement)
    {
        PumpSettings_t * s = PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getPumpingDefaults();
        settingElement->QueryUnsignedAttribute("uValveOpenTime", &(s->uValveOpenTime)) ;
        settingElement->QueryUnsignedAttribute("uPressureSettlingDelay", &(s->uPressureSettlingDelay)) ;
        settingElement->QueryUnsignedAttribute("uFluidFlowDelay", &(s->uFluidFlowDelay)) ;
        settingElement->QueryFloatAttribute("rBrokenMembranePressure", &(s->rBrokenMembranePressure)) ;
        settingElement->QueryUnsignedAttribute("uMaximumWaitingTime", &(s->uMaximumWaitingTime)) ;
    }
}



