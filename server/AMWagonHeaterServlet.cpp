
//---------------------------------------------------------------------------
#pragma hdrstop

#include <string>
#include <sstream>

#include "AMWagonHeaterServlet.h"

#include "modules/pimcore/PamStationFactory.h"       //for obtaining & using PamStation
#include "modules/pimcore/PamStation.h"              //for obtaining & using PamStation
#include "modules/pimcore/WagonHeater.h"             //for obtaining & using object

#pragma package(smart_init)
//---------------------------------------------------------------------------
void AMWagonHeaterServlet::doGet(AMRequest * request, AMResponse * response )
{
    std::string contentType = "text/xml";
    response->setContentType(&contentType);
    std::stringstream st;
    st << "<?xml version=\"1.0\"?>";
    st << "<wagon temperature=\"";
    st << PamStationFactory::getCurrentPamStation()->getWagonHeater()->getActualValue();
    st << "\"/>";
    std::string content  = st.str();
    response->setContent(&content);
}






