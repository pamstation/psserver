#ifndef AMButtonServletH
#define AMButtonServletH

#include "AMServletBase.h"
#include "AMRequest.h"
#include "AMResponse.h"

class AMButtonServlet : virtual public AMServletBase
{
public:
    virtual void doGet(AMRequest * request, AMResponse * response );
    virtual void doPut(AMRequest * request, AMResponse * response );
    virtual void doPost(AMRequest * request, AMResponse * response );
};

#endif //AMButtonServletH

