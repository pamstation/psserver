
//---------------------------------------------------------------------------
#pragma hdrstop

#include <string>
#include <sstream>



#include "AMRunProtocolServlet.h"
#include "AMRequest.h"
#include "AMResponse.h"

#include "Protocol.h"
#include "AMRequestDispatcher.h"

#pragma package(smart_init)
//---------------------------------------------------------------------------

void AMRunProtocolServlet::doGet(AMRequest * request, AMResponse * response )
{
    int code = AMRequestDispatcher::getCurrent()->runProtocol();
    if (code == 0) return;
    std::string contentType = "text/xml";
    response->setContentType(&contentType);
    response->setResponseStatus(400);   //Bad Request
    std::string msg = "<?xml version=\"1.0\"?>";
    msg.append("<error code=\"");
    std::stringstream st;
    st << code;
    msg.append(st.str());
    msg.append("\"/>");
    response->setContent(&msg);
}





