//
// Created by alex on 12/07/19.
//

#include "AMPamserviceLedServlet.h"
#include "modules/pimcore/PamStationFactory.h"
#include "modules/pimcore/PamStation.h"
#include "modules/pimcore/LEDUnit.h"

#include "AMPamserviceTempServlet.h"
#include "json.hpp"

using json = nlohmann::json;

void AMPamserviceLedServlet::doGet(AMRequest *request, AMResponse *response) {
    auto status = request->getParameter("status") == "on";
    if (request->getParameter("led") == "all") {
        PamStationFactory::getCurrentPamStation()->getLEDUnit((ELEDNo) 1)->setSwitchStatus(status);
        PamStationFactory::getCurrentPamStation()->getLEDUnit((ELEDNo) 2)->setSwitchStatus(status);
        PamStationFactory::getCurrentPamStation()->getLEDUnit((ELEDNo) 3)->setSwitchStatus(status);
    } else {
        auto led = std::stoi(request->getParameter("led"));

        if (led < 1 || led > 3) {
            response->setResponseStatus(401);
            return;
        }

        PamStationFactory::getCurrentPamStation()->getLEDUnit((ELEDNo) led)->setSwitchStatus(status);

    }

}

void AMPamserviceLedServlet::doPost(AMRequest *request, AMResponse *response) {
    doPut(request, response);
}

void AMPamserviceLedServlet::doPut(AMRequest *request, AMResponse *response) {

}