//
// Created by alex on 06/03/19.
//

#include <map>
#include <boost/filesystem.hpp>
#include <boost/exception/diagnostic_information.hpp>

#include "AMRequestDispatcher.h"
#include "AMRequest.h"
#include "AMResponse.h"
#include "AMPingServlet.h"

#include "AMQueue.h"
#include "AMStateException.h"

#include "AMSimpleLogger.h"

#include "AMButtonServlet.h"
#include "AMUnloadProtocolServlet.h"
#include "AMRunProtocolServlet.h"
#include "AMProtocolMsgQueueServlet.h"
#include "AMWagonHeaterServlet.h"
#include "AMProtocolServlet.h"
#include "AMMsgQueueServlet.h"
#include "AMAbortProtocolServlet.h"
#include "AMDefaultSettingsServlet.h"
#include "AMPamserviceServlet.h"
#include "AMPamserviceTempServlet.h"
#include "AMPamservicePressureServlet.h"
#include "AMPamserviceWagonServlet.h"
#include "AMPamserviceCameraServlet.h"
#include "AMPamserviceLedServlet.h"
#include "AMPamservicePumpServlet.h"
#include "AMPamserviceValvesServlet.h"
#include "AMPamserviceAspirateServlet.h"
#include "AMPamserviceDispenseServlet.h"
#include "AMPamserviceVersionServlet.h"
#include "AMPamserviceControllableServlet.h"



#include "modules/pimcore/PamStationFactory.h"      //for obtaining & using PamStation
#include "modules/pimcore/PamStation.h"
#include "modules/pimcore/ExceptionGenerator.h"
#include "modules/propertylayer/PropLeds.h"      //for obtaining & using object
#include "modules/pimcore/LogGenerator.h"
#include "modules/pimcore/PamStationException.h" //for using object

class AMSimpleLogger;


AMRequestDispatcher *AMRequestDispatcher::current = nullptr;

AMRequestDispatcher *AMRequestDispatcher::getCurrent() {
    if (!current) {
        current = new AMRequestDispatcher();
        current->addServlet("/api/ping", new AMPingServlet());
        current->addServlet("/api/version", new AMPamserviceVersionServlet());


        current->addServlet("/api/settings", new AMDefaultSettingsServlet());
        current->addServlet("/api/msgqueue", new AMMsgQueueServlet());
        current->addServlet("/api/button", new AMButtonServlet());
        current->addServlet("/api/wagon/temperature", new AMWagonHeaterServlet());

        current->addServlet("/api/protocol", new AMProtocolServlet());
        current->addServlet("/api/protocol/unload", new AMUnloadProtocolServlet());
        current->addServlet("/api/protocol/run", new AMRunProtocolServlet());
        current->addServlet("/api/protocol/msgqueue", new AMProtocolMsgQueueServlet());
        current->addServlet("/api/protocol/abort", new AMAbortProtocolServlet());

        current->addServlet("/api/pamservice/settings", new AMPamserviceServlet());
        current->addServlet("/api/pamservice/mode", new AMPamserviceServlet());

        current->addServlet("/api/pamservice/temperature", new AMPamserviceTempServlet());
        current->addServlet("/api/pamservice/pressure", new AMPamservicePressureServlet());
        current->addServlet("/api/pamservice/wagon", new AMPamserviceWagonServlet());
        current->addServlet("/api/pamservice/camera", new AMPamserviceCameraServlet());
        current->addServlet("/api/pamservice/led", new AMPamserviceLedServlet());
        current->addServlet("/api/pamservice/pump", new AMPamservicePumpServlet());
        current->addServlet("/api/pamservice/valves", new AMPamserviceValvesServlet());
        current->addServlet("/api/pamservice/aspirate", new AMPamserviceAspirateServlet());
        current->addServlet("/api/pamservice/dispense", new AMPamserviceDispenseServlet());
        current->addServlet("/api/pamservice/ctrl", new AMPamserviceControllableServlet());


    }
    return current;
}

void AMRequestDispatcher::deleteCurrent() {
    if (current) {

        AMRequestDispatcher *old = current;
        current = nullptr;
        delete old;
    }
}

AMRequestDispatcher::AMRequestDispatcher() {
    state = pTurnOff;
    servletMap = new std::map<std::string, AMServlet *>;

    protocol = nullptr;
    protocolThread = nullptr;

    msgQueue_ = new AMQueue;
    logger = new AMSimpleLogger("logs", "PSServer");
    logger->logInfo("AMRequestDispatcher constructor");
}

AMRequestDispatcher::~AMRequestDispatcher() {
    if (protocolThread) {
        if (protocolThread->joinable()) protocolThread->join();
        delete protocolThread;
        protocolThread = nullptr;
    }

    if (protocol) {
        delete protocol;
        protocol = nullptr;
    }

    if (state == pTurnOn || state == pLoaded) PamStationFactory::shutdown();

    delete msgQueue_;
//    delete criticalSection;

    std::map<std::string, AMServlet *>::const_iterator it;
    for (it = servletMap->begin(); it != servletMap->end(); ++it) {
        AMServlet *servlet = it->second;
        delete servlet;
    }
    delete servletMap;
    delete logger;
}

bool AMRequestDispatcher::isTurnOn() {
    return (state == pTurnOn || state == pLoaded);
}

void AMRequestDispatcher::setState(PSState aState) {
    switch (state) {
        case pTurnOn: {
            switch (aState) {
                case pTurnOn: {
                    throw AMStateException();
                }

                case pTurnOff: {
                    state = aState;
                }
                    break;
                case pLoaded: {
                    state = aState;
                }
                    break;
            }
        }
            break;
        case pTurnOff: {
            switch (aState) {
                case pTurnOn: {
                    state = aState;
                }
                    break;
                case pTurnOff: {
                    throw AMStateException();
                }

                case pLoaded: {
                    throw AMStateException();
                }
            }
        }
            break;
        case pLoaded: {
            switch (aState) {
                case pTurnOn: {
                    state = aState;
                }
                    break;
                case pTurnOff: {
                    throw AMStateException();
                }

                case pLoaded: {
                    throw AMStateException();
                }
            }
        }
            break;
    }
}

bool AMRequestDispatcher::loadProtocol(Protocol *p) {

    if (state == pTurnOn) {
        protocol = p;
        setState(pLoaded);
        logger->logInfo("AMRequestDispatcher::loadProtcol");
        logger->logInfo(protocol->getXML());

    } else {
        return false;
    }
    return true;
}

bool AMRequestDispatcher::unloadProtocol() {
    if (state == pLoaded) {
        if (protocol->isRunning())return false;
        delete protocol;
        protocol = nullptr;
        setState(pTurnOn);
        logger->logInfo("AMRequestDispatcher::unloadProtcol");
    } else {
        return false;
    }
    return true;
}


int AMRequestDispatcher::runProtocol() {
    if (state != pLoaded) return 1;
    if (protocol->isRunning())return 2;
    if (protocol->isCompleted())return 3;
    if (protocol->isAborting())return 4;

    std::stringstream st;
    st << "web/ImageResults/";

    if (boost::filesystem::exists(st.str())) {
        boost::filesystem::remove_all(st.str());
    }

    boost::filesystem::path dir(st.str());
    if (!boost::filesystem::create_directories(dir)) {
        //TODO throw
        logger->logInfo("AMRequestDispatcher::runProtocol failed to create ImageResults directory");
    }

    std::thread *old = nullptr;

    if (protocolThread) {
        old = protocolThread;
    }

    protocolThread = new std::thread([=]() {
        protocol->run();
    });

    if (old) {
        if (old->joinable()) old->join();
        delete old;
    }

    return 0;
}

int AMRequestDispatcher::abortProtocol() {
    if (state != pLoaded) return 1;
    try {
        protocol->abort();
        logger->logInfo("AMRequestDispatcher::abortProtcol");

    }
    catch (AMStateException &e) {
        return 2;
    }
    return 0;
}

void AMRequestDispatcher::getProtocolMsgs(std::string *out) {
    if (protocol == nullptr) return;
    return protocol->getProtocolMsgs(out);
}

void AMRequestDispatcher::getProtocolMsgs(int i, std::string *out) {
    if (protocol == nullptr) return;
    protocol->getProtocolMsgs(i, out);
}


void AMRequestDispatcher::addServlet(std::string path, AMServlet *servlet) {
    (*servletMap)[path] = servlet;
}

bool AMRequestDispatcher::internalDispatch(AMRequest *request, AMResponse *response) {

    std::string aUrl = request->getURL();

    if (aUrl == "/api/restart") {
        exit(0);
    }

    if (aUrl == "/api/status") {
        std::string contentType = "text/xml";
        response->setContentType(&contentType);
        std::stringstream st;
        st << "<?xml version=\"1.0\"?>";
        st << "<server state=\"";
        st << state;
        st << "\"/>";
        std::string content = st.str();
        response->setContent(&content);
        return true;
    }

    if (aUrl == "/api/version") {
        auto it = servletMap->find(aUrl);
        if (it != servletMap->end()) {
             it->second->service(request, response);
        } else {
            response->setResponseStatus(404);     // Not Found
        }

        return true;
    }

    if (aUrl == "/api/pamservice/mode") {
        auto it = servletMap->find(aUrl);
        if (it != servletMap->end()) {
            it->second->service(request, response);
        } else {
            response->setResponseStatus(404);     // Not Found
        }

        return true;
    }

    if (aUrl == "/api/turnOn") {
        int code = turnOn();
        if (code == 0) return true;
        std::string contentType = "text/xml";
        response->setContentType(&contentType);
        response->setResponseStatus(400);   //Bad Request
        std::string msg = "<?xml version=\"1.0\"?>";
        msg.append("<error code=\"");
        std::stringstream st;
        st << code;
        msg.append(st.str());
        msg.append("\"/>");
        response->setContent(&msg);
        return true;
    }

    if (state == pTurnOff) {
        std::string contentType = "text/xml";
        response->setContentType(&contentType);
        response->setResponseStatus(400);   //Bad Request
        std::string msg = "<?xml version=\"1.0\"?><error code=\"-42\"/>";
        response->setContent(&msg);
        return true;
    }

    if (aUrl == "/api/shutdown") {
        int code = shutdown();
        if (code == 0) return true;
        std::string contentType = "text/xml";
        response->setContentType(&contentType);
        response->setResponseStatus(400);   //Bad Request
        std::string msg = "<?xml version=\"1.0\"?>";
        msg.append("<error code=\"");
        std::stringstream st;
        st << code;
        msg.append(st.str());
        msg.append("\"/>");
        response->setContent(&msg);
        return true;
    }


    return false;
}

void AMRequestDispatcher::dispatch(AMRequest *request, AMResponse *response) {

    std::lock_guard<std::mutex> guard(mutex);

    try {
        if (internalDispatch(request, response)) return;

        auto it = servletMap->find(request->getURL());

        if (it != servletMap->end()) {
            AMServlet *servlet = it->second;
            servlet->service(request, response);
        } else {
            response->setResponseStatus(404);     // Not Found
        }
    } catch (PamStationException &e) {
        response->setResponseStatus(500);
        auto description = e.getDescription();
        response->setContent(&description);
    } catch (...) {
        std::cout << "AMRequestDispatcher -- dispatch -- unknown error -- " << std::endl <<
                  boost::current_exception_diagnostic_information();

        response->setResponseStatus(500);
        std::string description = "dispatch unknown error -- " + boost::current_exception_diagnostic_information();
        response->setContent(&description);
    }
}


int AMRequestDispatcher::shutdown() {
    if (state == pTurnOff) return 1;
    if (state == pLoaded) return 2;

    PamStation *pPamStation = PamStationFactory::getCurrentPamStation();
    PropLeds *pLeds = pPamStation->getLeds();
    pLeds->SetRedLed(false);
    pLeds->SetGreenLed(false);

    pPamStation->getLogGenerator()->removeLogHandler(logger);

    PamStationFactory::shutdown();
    setState(pTurnOff);
    logger->logInfo("AMRequestDispatcher::shutdown");
    return 0;
}

int AMRequestDispatcher::turnOn() {
    if (state == pTurnOn) return 1;
    if (state == pLoaded) return 2;

    try {
        PamStation *pPamStation = PamStationFactory::getCurrentPamStation();
        pPamStation->getExceptionGenerator()->addExceptionHandler(this);
        pPamStation->getLogGenerator()->addLogHandler(logger);
        setState(pTurnOn);

        PropLeds *pLeds = pPamStation->getLeds();
        pLeds->SetRedLed(false);
        pLeds->SetGreenLed(true);

        logger->logInfo("AMRequestDispatcher::turnOn");
    }
    catch (PamStationException &e) {

        std::stringstream st;
        st << "AMRequestDispatcher::turnOn failed with exception, id : ";
        st << e.getId();
        st << " description : ";
        st << e.getDescription().c_str();
        std::string aMsg = st.str();

        logger->logInfo(aMsg);

//        exit(42);

        return -42;
    }
    return 0;
}


void AMRequestDispatcher::onException(void *source, PamStationException *exception) {
    std::stringstream st;
    st << "<error description=\"";
    st << exception->getDescription().c_str();
    st << "\" id=\"";
    st << exception->getId();
    st << "\"/>";
    std::string aMsg = st.str();

    AMProtocolMsg *pm = new AMProtocolMsg(1, aMsg);
    addMsg(pm);
}

void AMRequestDispatcher::addMsg(AMProtocolMsg *msg) {
    logger->logError(msg->msg);
    msgQueue_->add(msg);
}

void AMRequestDispatcher::getMsgs(std::string *out) {
    msgQueue_->getMsgs(out);
}

void AMRequestDispatcher::getMsgs(int i, std::string *out) {
    msgQueue_->getMsgs(i, out);
}


