//
// Created by alex on 12/07/19.
//

#include "AMPamserviceCameraServlet.h"
#include <gsl/gsl_multimin.h>

//#include "modules/pimcore/PamStationFactory.h"
//#include "modules/pimcore/PamStation.h"
//#include "modules/pimcore/WagonXYPositioner.h"
//#include "modules/pimcore/DispenseHeadPositioner.h"

#include "json.hpp"

#include "modules/pimcore/PamStationFactory.h"      //for obtaining & using PamStation
#include "modules/pimcore/PamStation.h"             //for obtaining & using PamStation

#include "modules/systemlayer/StubPamStationFactory.h"
#include "modules/systemlayer/StubPamStation.h"

#include "modules/pimcore/CCD.h"                    //for obtaining & using object
#include "modules/pimcore/Disposable.h"             //for obtaining & using object
#include "modules/pimcore/FilterPositioner.h"       //for obtaining & using object
#include "modules/pimcore/FocusPositioner.h"        //for obtaining & using object
#include "modules/pimcore/LEDUnit.h"                //for obtaining & using object
#include "modules/pimcore/WagonXYPositioner.h"      //for obtaining & using object
#include "modules/pimcore/WagonHeater.h"             //for obtaining & using object
#include "modules/pimcore/DefaultGenerator.h"

#include "modules/pimcore/Image.h"
#include "modules/systemlayer/CCMImage.h"

#include "modules/utils/util.h"
#include "modules/utils/AMConfiguration.h"
#include <iostream>
#include <fstream>

#include <boost/lexical_cast.hpp>

using boost::lexical_cast;

using json = nlohmann::json;

void AMPamserviceCameraServlet::doGet(AMRequest *request, AMResponse *response) {

    auto cmd = request->getParameter("cmd");

    if (cmd == "auto.focus") {
        auto exposureTime = std::stoi(request->getParameter("exposureTime"));
        if (exposureTime < 0) {
            response->setResponseStatus(401);
            return;
        }
        auto filter = std::stoi(request->getParameter("filter"));
        if (filter < 1 || filter > 3) {
            response->setResponseStatus(401);
            return;
        }
        auto wellBitSet = std::stoi(request->getParameter("wellBitSet"));
        if (wellBitSet < 1 || wellBitSet >= 4096) {
            response->setResponseStatus(401);
            return;
        }

        auto focusRange = std::stof(request->getParameter("focusRange"));
        runAutoFocus(wellBitSet, (EFilterNo) filter, (unsigned) exposureTime, focusRange);

        auto config = StubPamStationFactory::getCurrentPamStation()->getConfig();

        auto settings = config->getSettings();

        auto focus_logicalWellPos_t = settings["focus_logicalWellPos_t"];
        std::string content = focus_logicalWellPos_t.dump();
        response->setContent(&content);
    }  else if (cmd =="move.filter") {
        auto position = std::stof(request->getParameter("position"));

        PamStation *pPamStation = PamStationFactory::getCurrentPamStation();
        FilterPositioner *pPositioner = pPamStation->getFilterPositioner();

        try {
            pPositioner->setEnabled(true);
            if (!pPositioner->getHomed()) {
                pPositioner->goHome();
                pPositioner->waitReady();
            }
            pPositioner->setTargetValue(position);
            pPositioner->waitReady();
        } catch (...) {
            pPositioner->setEnabled(false);
            throw ;
        }
        pPositioner->setEnabled(false);
    } else if (cmd =="snapshot") {
        auto exposureTime = std::stoi(request->getParameter("exposureTime"));
        if (exposureTime < 0) {
            response->setResponseStatus(401);
            return;
        }
        auto image = snapshot((unsigned) exposureTime);

        std::string path = "/tmp/image.tiff";
        image->save(path);

        std::istream *stream = new std::ifstream(path, std::ios::binary);

        std::string contentType = "application/octet-stream";
        response->setContentType(&contentType);
        response->setStreamContent(stream);

        delete image;
    } else {
        auto exposureTime = std::stoi(request->getParameter("exposureTime"));
        if (exposureTime < 0) {
            response->setResponseStatus(401);
            return;
        }
        auto filter = std::stoi(request->getParameter("filter"));
        if (filter < 1 || filter > 3) {
            response->setResponseStatus(401);
            return;
        }
        auto well = std::stoi(request->getParameter("well"));
        if (well < 1 || well > 12) {
            response->setResponseStatus(401);
            return;
        }
        auto image = runRead((EWellNo) well, (EFilterNo) filter, (unsigned) exposureTime);

        std::string path = "/tmp/image.tiff";
        image->save(path);

        std::istream *stream = new std::ifstream(path, std::ios::binary);

        std::string contentType = "application/octet-stream";
        response->setContentType(&contentType);
        response->setStreamContent(stream);

        delete image;
    }
}

void AMPamserviceCameraServlet::doPost(AMRequest *request, AMResponse *response) {
    doPut(request, response);
}

void AMPamserviceCameraServlet::doPut(AMRequest *request, AMResponse *response) {

}

Image *AMPamserviceCameraServlet::snapshot(unsigned int exposureTime) {
    PamStation *pPamStation = PamStationFactory::getCurrentPamStation();
    CCD *pCCD = pPamStation->getCCD();
    LEDUnit *pLED1 = pPamStation->getLEDUnit((ELEDNo) 1);

    Image *pImage;
    int iLedDelayTime;

    try   // tryI
    {

        if (!pCCD->getEnabled()) {
            pCCD->setEnabled(true);
        }

        pLED1->setSwitchStatus(false);

        pLED1->setSwitchStatus(true); //switch LED on

        iLedDelayTime = pLED1->getLedWaitTime();

        if (iLedDelayTime > 1000)
            iLedDelayTime = 10;

        Util::Sleep(iLedDelayTime);

        pCCD->setExposureTime(exposureTime);
        pCCD->setImageInfo((EWellNo) 1, (EFilterNo) 1);

        pImage = pCCD->acquireImage();

        pLED1->setSwitchStatus(false);
    }    //end tryI
    catch (...) {
        pLED1->setSwitchStatus(false);
        throw;
    }

    return pImage;
}

Image *AMPamserviceCameraServlet::runRead(EWellNo currentWell, EFilterNo currentFilter, unsigned exposureTime) {
    PamStation *pPamStation = PamStationFactory::getCurrentPamStation();
    CCD *pCCD = pPamStation->getCCD();
    Disposable *pDisposable = pPamStation->getDisposable();
    FilterPositioner *pFilterPositioner = pPamStation->getFilterPositioner();
    FocusPositioner *pFocusPositioner = pPamStation->getFocusPositioner();
    LEDUnit *pLED1 = pPamStation->getLEDUnit((ELEDNo) 1);
    LEDUnit *pLED2 = pPamStation->getLEDUnit((ELEDNo) 2);
    LEDUnit *pLED3 = pPamStation->getLEDUnit((ELEDNo) 3);
    WagonXYPositioner *pWagonXYPositioner = pPamStation->getWagonXYPositioner();

    int iLedDelayTime;

    Image *pImage;

    try   // tryI
    {
//if FilterPositioner or FocusPositioner is not homed, home both
        if (!(pFilterPositioner->getHomed() && pFocusPositioner->getHomed())) {
            pFilterPositioner->goHome();
            pFocusPositioner->goHome();
            pFilterPositioner->waitReady();
            pFocusPositioner->waitReady();
        }

//        pDisposable->getPumpUpSemaphore();

//place selected well under camera
        pWagonXYPositioner->goReadPosition((EWellNo) currentWell);
        pWagonXYPositioner->waitReady();

//for (  ; currentFilter < eFilterNoMax && !(protocol->isAborting()); currentFilter ++)   //loop for all filters, j: 1,2,3,etc

//positions selected filter
        pFilterPositioner->goPositionForFilter((EFilterNo) currentFilter);
        pFilterPositioner->waitReady();  //throws PamStationTimeoutException

//set in focus for selected Filter & Well
        pFocusPositioner->goPositionForFilterAndWell((EFilterNo) currentFilter, (EWellNo) currentWell);
        pFocusPositioner->waitReady();

// make sure the camera is enabled.
        if (!pCCD->getEnabled()) {
            pCCD->setEnabled(true);
        }
//make sure all LED's are off
        pLED1->setSwitchStatus(false);
        pLED2->setSwitchStatus(false);
        pLED3->setSwitchStatus(false);

//switch led for selected filter
        LEDUnit *pSelectedLED = pPamStation->getLEDUnit((ELEDNo) currentFilter);
        pSelectedLED->setSwitchStatus(true); //switch LED on
//- JPau added 20090403 for getting the waittime for the ledunit
        iLedDelayTime = pSelectedLED->getLedWaitTime();

        if (iLedDelayTime > 1000)
            iLedDelayTime = 10;

//- JPau added 20090403 for wait time after setting led on
        Util::Sleep(iLedDelayTime);

        pCCD->setExposureTime(exposureTime);
        pCCD->setImageInfo((EWellNo) currentWell, (EFilterNo) currentFilter);
        pImage = pCCD->acquireImage();
        pSelectedLED->setSwitchStatus(false);
//        pDisposable->releasePumpUpSemaphore();

//disable controllers
        pFilterPositioner->setEnabled(false);
        pFocusPositioner->setEnabled(false);

// Alex we keep the camera enabled
//pCCD              ->setEnabled(false);

        pWagonXYPositioner->setEnabled(false);

//make sure all LED's are off
        pLED1->setSwitchStatus(false);
        pLED2->setSwitchStatus(false);
        pLED3->setSwitchStatus(false);


    }    //end tryI
    catch (...) {
//        pDisposable->releasePumpUpSemaphore();

//disable controllers
        pFilterPositioner->setEnabled(false);
        pFocusPositioner->setEnabled(false);

// Alex we keep the camera enabled
//pCCD              ->setEnabled(false);

        pWagonXYPositioner->setEnabled(false);

//make sure all LED's are off
        pLED1->setSwitchStatus(false);
        pLED2->setSwitchStatus(false);
        pLED3->setSwitchStatus(false);

        throw;
    }

    return pImage;
}

struct gslparams
{
    EWellNo currentWell;
    EFilterNo currentFilter;
    unsigned exposureTime;
};

double my_f (const gsl_vector *v, void *params)
{
    double pos;
    gslparams * imageParams = (gslparams *)params;
    pos = gsl_vector_get(v, 0);

    PamStation *pPamStation = PamStationFactory::getCurrentPamStation();
    CCD *pCCD = pPamStation->getCCD();
    FocusPositioner *pFocusPositioner = pPamStation->getFocusPositioner();

    std::cout << "set pos "  <<  lexical_cast<std::string>(pos) << " for well " << imageParams->currentWell << std::endl;

    PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->setFocusLogicalWellPosition(imageParams->currentWell, pos);

    //set in focus for selected Filter & Well
    pFocusPositioner->goPositionForFilterAndWell(imageParams->currentFilter,imageParams->currentWell);
    pFocusPositioner->waitReady();

    Image *pImage = pCCD->acquireImage();

    auto contrast = pImage->getSumDiff();

    delete pImage;

    std::cout << "contrast "  << lexical_cast<std::string>(contrast) <<  std::endl;

    return 1.0/contrast;
}

//double my_f1 (double pos, void *params)
//{
//     gslparams * imageParams = (gslparams *)params;
//
//    PamStation *pPamStation = PamStationFactory::getCurrentPamStation();
//    CCD *pCCD = pPamStation->getCCD();
//    FocusPositioner *pFocusPositioner = pPamStation->getFocusPositioner();
//
//    std::cout << "set pos "  << lexical_cast<std::string>(pos)  << " for well " << imageParams->currentWell << std::endl;
//
//    PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->setFocusLogicalWellPosition(imageParams->currentWell, pos);
//
//    //set in focus for selected Filter & Well
//    pFocusPositioner->goPositionForFilterAndWell(imageParams->currentFilter,imageParams->currentWell);
//    pFocusPositioner->waitReady();
//
//    Image *pImage = pCCD->acquireImage();
//
//    auto contrast = pImage->getSumDiff();
//
//    delete pImage;
//
//    std::cout << "contrast "  << lexical_cast<std::string>(contrast) <<  std::endl;
//
//    return 1/contrast;
//}


void AMPamserviceCameraServlet::runAutoFocus(unsigned wellBitSet, EFilterNo currentFilter, unsigned exposureTime,
                                             float focusRange) {



    std::cout.precision(16);

    PamStation *pPamStation = PamStationFactory::getCurrentPamStation();
    CCD *pCCD = pPamStation->getCCD();
    Disposable *pDisposable = pPamStation->getDisposable();
    FilterPositioner *pFilterPositioner = pPamStation->getFilterPositioner();
    FocusPositioner *pFocusPositioner = pPamStation->getFocusPositioner();
    LEDUnit *pLED1 = pPamStation->getLEDUnit((ELEDNo) 1);
    LEDUnit *pLED2 = pPamStation->getLEDUnit((ELEDNo) 2);
    LEDUnit *pLED3 = pPamStation->getLEDUnit((ELEDNo) 3);
    WagonXYPositioner *pWagonXYPositioner = pPamStation->getWagonXYPositioner();

    int iLedDelayTime;

    try   // tryI
    {
//if FilterPositioner or FocusPositioner is not homed, home both
        if (!(pFilterPositioner->getHomed() && pFocusPositioner->getHomed())) {
            pFilterPositioner->goHome();
            pFocusPositioner->goHome();
            pFilterPositioner->waitReady();
            pFocusPositioner->waitReady();
        }

//        pDisposable->getPumpUpSemaphore();

        pFilterPositioner->goPositionForFilter((EFilterNo) currentFilter);
        pFilterPositioner->waitReady();

        if (!pCCD->getEnabled()) {
            pCCD->setEnabled(true);
        }
        pLED1->setSwitchStatus(false);
        pLED2->setSwitchStatus(false);
        pLED3->setSwitchStatus(false);

        LEDUnit *pSelectedLED = pPamStation->getLEDUnit((ELEDNo) currentFilter);
        pSelectedLED->setSwitchStatus(true);
        iLedDelayTime = pSelectedLED->getLedWaitTime();

        if (iLedDelayTime > 1000)
            iLedDelayTime = 10;

        Util::Sleep(iLedDelayTime);

        pCCD->setExposureTime(exposureTime);

        unsigned currentWell = eWell1;

        //for each selected well
        for (; currentWell < eWellNoMax; currentWell++)               //loop for all wells, i: 1,2,3,4,5,etc
        {
            if ((1 << (currentWell - 1)) & wellBitSet)
            {
                 auto initialPos0 = PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getFocusLogicalWellPosition((EWellNo)currentWell);
                std::cout << "initialPos0 "  << lexical_cast<std::string>(initialPos0)  << " for well " << currentWell << std::endl;

                pWagonXYPositioner->goReadPosition((EWellNo) currentWell);
                pWagonXYPositioner->waitReady();

                pCCD->setImageInfo((EWellNo) currentWell, (EFilterNo) currentFilter);

                float bestPosition = initialPos0;

                /////////////////////////////////////////////////////////

                size_t np = 1; //dimension of the problem

                gslparams par = {(EWellNo) currentWell, (EFilterNo) currentFilter, exposureTime};

                const gsl_multimin_fminimizer_type *T = gsl_multimin_fminimizer_nmsimplex; //nedler-mead simplex algorithm
                gsl_multimin_fminimizer *s = nullptr;
                gsl_vector *ss, *x;
                gsl_multimin_function minex_func;
                size_t iter = 0, i;
                int status;
                double size;

                /* Initial vertex size vector */
                ss = gsl_vector_alloc (np);

                /* Set all step sizes to 1 */
                gsl_vector_set_all (ss, focusRange/10);

                /* Starting point */
                x = gsl_vector_alloc (np);
                gsl_vector_set (x, 0, initialPos0);

                /* Initialize method and iterate */
                minex_func.f = &my_f;
                minex_func.n = np;
                minex_func.params = (void *)&par;
                s = gsl_multimin_fminimizer_alloc (T, np);
                gsl_multimin_fminimizer_set (s, &minex_func, x, ss);
                do
                {
                    iter++;
                    status = gsl_multimin_fminimizer_iterate(s);
                    if (status)
                        break;
                    size = gsl_multimin_fminimizer_size (s);
                    status = gsl_multimin_test_size (size, 1e-3);

                    if (status == GSL_SUCCESS)
                    {
                        printf ("converged to minimum at\n");
                    }

                    printf ("%5d ", iter);

                    for (i = 0; i < np; i++)
                    {
                        printf ("%10.5e ", gsl_vector_get (s->x, i));
                    }

                    printf ("f() = %7.5f size = %.5f\n", s->fval, size);


                    bestPosition = gsl_vector_get (s->x, 0);
                }
                while (status == GSL_CONTINUE && iter < 100);

                //clean up
                gsl_vector_free(x);
                gsl_vector_free(ss);
                gsl_multimin_fminimizer_free (s);

                ////////////////////////////////////////////////////////

                std::cout << "set bestPosition to "  << lexical_cast<std::string>(bestPosition)  << " for well " << currentWell << std::endl;
                PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->setFocusLogicalWellPosition((EWellNo)currentWell, bestPosition);
            }
        }
        pSelectedLED->setSwitchStatus(false);
//        pDisposable->releasePumpUpSemaphore();

        pFilterPositioner->setEnabled(false);
        pFocusPositioner->setEnabled(false);
        pWagonXYPositioner->setEnabled(false);

        pLED1->setSwitchStatus(false);
        pLED2->setSwitchStatus(false);
        pLED3->setSwitchStatus(false);

    }    //end tryI
    catch (...) {
//        pDisposable->releasePumpUpSemaphore();
        pFilterPositioner->setEnabled(false);
        pFocusPositioner->setEnabled(false);
        pWagonXYPositioner->setEnabled(false);
        pLED1->setSwitchStatus(false);
        pLED2->setSwitchStatus(false);
        pLED3->setSwitchStatus(false);
        throw;
    }
}