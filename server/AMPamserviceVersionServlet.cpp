
#include "modules/pimcore/PamStationFactory.h"
#include "modules/pimcore/PamStation.h"
#include "modules/pimcore/WagonHeater.h"
#include "modules/pimcore/IncubationChamberHeater.h"


#include "AMPamserviceVersionServlet.h"
#include "version.h"
#include "json.hpp"
#include <sys/utsname.h>

using json = nlohmann::json;

void AMPamserviceVersionServlet::doGet(AMRequest *request, AMResponse *response) {

    utsname uname_buf;

    uname(&uname_buf);

    auto version = json::parse(PSSERVER_VERSION);


    json temp = {{"kind", "Versions"},
                 {"psserver", version},
                 {"linux", {
                                   {"kind", "LinuxVersion"},
                                   {"sysname", uname_buf.sysname},
                                   {"nodename", uname_buf.nodename},
                                   {"release", uname_buf.release},
                                   {"version", uname_buf.version},
                                   {"machine", uname_buf.machine},
                           }
                  },
                 };

    std::string content = temp.dump();
    response->setContent(&content);
}

void AMPamserviceVersionServlet::doPost(AMRequest *request, AMResponse *response) {

}

void AMPamserviceVersionServlet::doPut(AMRequest *request, AMResponse *response) {

}