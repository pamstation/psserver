//
// Created by alex on 06/03/19.
//

#ifndef PSSERVER_AMSERVLET_H
#define PSSERVER_AMSERVLET_H

#include "AMRequest.h"
#include "AMResponse.h"

class AMServlet {
public:
    virtual ~AMServlet() = 0;
    virtual void service(AMRequest * request, AMResponse * response ) = 0;
};


#endif //PSSERVER_AMSERVLET_H
