//
// Created by alex on 12/07/19.
//

#include "AMPamservicePumpServlet.h"

#include "modules/pimcore/PamStationFactory.h"
#include "modules/pimcore/PamStation.h"

#include "modules/pimcore/Disposable.h"             //for obtaining & using object
#include "modules/pimcore/FilterPositioner.h"       //for obtaining & using object
#include "modules/pimcore/FocusPositioner.h"        //for obtaining & using object
#include "modules/pimcore/LEDUnit.h"                //for obtaining & using object
#include "modules/pimcore/WagonXYPositioner.h"      //for obtaining & using object
#include "modules/pimcore/WagonHeater.h"             //for obtaining & using object

#include "modules/utils/util.h"

#include "json.hpp"

using json = nlohmann::json;

void AMPamservicePumpServlet::doGet(AMRequest *request, AMResponse *response) {

    unsigned wellBitSet = std::stoi(request->getParameter("wellBitSet"));
    unsigned pumpCycles = std::stoi(request->getParameter("pumpCycles"));
    unsigned extraUpTime = std::stoi(request->getParameter("extraUpTime"));
    unsigned extraDownTime = std::stoi(request->getParameter("extraDownTime"));

    unsigned brokenWellBitSet = run(wellBitSet, pumpCycles, extraUpTime, extraDownTime);

    if (brokenWellBitSet > 0) {
        json temp = {{"kind",  "BrokenWellBitSet"},
                     {"value", brokenWellBitSet}};

        std::string content = temp.dump();
        response->setContent(&content);
        response->setResponseStatus(400);
    }
}

void AMPamservicePumpServlet::doPost(AMRequest *request, AMResponse *response) {
    doPut(request, response);
}

void AMPamservicePumpServlet::doPut(AMRequest *request, AMResponse *response) {

}

unsigned
AMPamservicePumpServlet::run(unsigned wellBitSet, unsigned pumpCycles, unsigned extraUpTime, unsigned extraDownTime) {
    PamStation *pPamStation = PamStationFactory::getCurrentPamStation();
    Disposable *pDisposable = pPamStation->getDisposable();
//    WagonXYPositioner *pWagonXYPositioner = pPamStation->getWagonXYPositioner();

    //pump fluid down in the selected wells
    pDisposable->pumpDown(wellBitSet);
//    pDisposable->waitPumpReady();

//    pDisposable->pumpDownAsync(wellBitSet).get();


    unsigned currentPumpCycles = 0;


    for (; currentPumpCycles < pumpCycles; currentPumpCycles++) {
        try //tryI, use try/finally to make sure semaphore is unlocked
        {
            //lock semaphore to synchonize with ReadNowStep
//            pDisposable->getPumpUpSemaphore();

            //make sure the wagon is (still) in incubation position
//            pWagonXYPositioner->goIncubationPosition();
//            pWagonXYPositioner->waitReady();

            //pump fluid up in the selected wells
            pDisposable->pumpUp(wellBitSet);
//            pDisposable->waitPumpReady();

//            pDisposable->pumpUpAsync(wellBitSet).get();


//            protocol->checkBrokenMembranes();

            unsigned brokenWellBitSet = pDisposable->getBrokenMembranes(wellBitSet);

            if (brokenWellBitSet > 0) {
//                pDisposable->releasePumpUpSemaphore();
                return brokenWellBitSet;
            }

            Util::Sleep(extraUpTime);

            //pump fluid down in the selected wells
            pDisposable->pumpDown(wellBitSet);
//            pDisposable->waitPumpReady();

//            pDisposable->pumpDownAsync(wellBitSet).get();


            if (brokenWellBitSet > 0) {
//                pDisposable->releasePumpUpSemaphore();
                return brokenWellBitSet;
            }


            Util::Sleep(extraDownTime);
            //unlock semaphore to synchonize with ReadNowStep
//            pDisposable->releasePumpUpSemaphore();
        } //end tryI
        catch (...) {
//            try {//unlock semaphore to synchonize with ReadNowStep
//                pDisposable->releasePumpUpSemaphore();
//            } catch (...) {}
            throw;
        }

    }//end for-loop
    return 0;
}
