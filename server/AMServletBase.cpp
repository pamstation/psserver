//
// Created by alex on 06/03/19.
//
#include <string>
#include "AMServletBase.h"



AMServletBase::AMServletBase() {}


void AMServletBase::service(AMRequest * request, AMResponse * response )
{
    std::string verb = request->getVerb();
    if (verb.compare("GET") == 0)
    {
        doGet(request,response);
    }
    else if (verb.compare("POST") == 0)
    {
        doPost(request,response);
    }
    else if (verb.compare("PUT") == 0)
    {
        doPut(request,response);
    }
    else
    {
        response->setResponseStatus(400 );   //Bad Request
    }
}

void AMServletBase::doGet(AMRequest * request, AMResponse * response )  {
    response->setResponseStatus(400 );   //Bad Request
}

void AMServletBase::doPost(AMRequest * request, AMResponse * response )  {
    response->setResponseStatus(400 );   //Bad Request
}

void AMServletBase::doPut(AMRequest * request, AMResponse * response )  {
    response->setResponseStatus(400 );   //Bad Request
}

