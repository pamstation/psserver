
#include "AMPamserviceControllableServlet.h"

#include "modules/systemlayer/SysExceptionGenerator.h"      //for passing Exception to ExceptionGenerator
#include "modules/pimcore/ExceptionGenerator.h"
#include "modules/systemlayer/SysExceptionGenerator.h"

#include "modules/pimcore/PamStationFactory.h"      //for obtaining & using PamStation
#include "modules/pimcore/PamStation.h"             //for obtaining & using PamStation
#include "modules/pimcore/DefaultGenerator.h"       //for obtaining DispenseDefaults
#include "modules/pimcore/DispenseHeadHeater.h"     //for obtaining & using object
#include "modules/pimcore/DispenseHeadPositioner.h" //for obtaining & using object
#include "modules/pimcore/Disposable.h"             //for obtaining & using object
#include "modules/pimcore/WagonXYPositioner.h"      //for obtaining & using object
#include "modules/pimcore/WagonPositioner.h"
#include "modules/pimcore/WagonHeater.h"
#include "modules/pimcore/OverPressureSupplier.h"    //for obtaining & using object
#include "modules/pimcore/UnderPressureSupplier.h"

#include "modules/utils/util.h"

#include "modules/pimcore/PamDefs.h"
#include "modules/pimcore/Controllable.h"


#include "json.hpp"

using json = nlohmann::json;

void AMPamserviceControllableServlet::doGet(AMRequest *request, AMResponse *response) {

    PamStation *pPamStation = PamStationFactory::getCurrentPamStation();

    auto ctrl_name = request->getParameter("ctrl_name");

    Controllable *ctrl = nullptr;

    if (ctrl_name == "wagon.x"){
        ctrl = pPamStation->getWagonPositioner(EWagonPositioner::ePositionerX);
    } else if (ctrl_name == "wagon.y"){
        ctrl = pPamStation->getWagonPositioner(EWagonPositioner::ePositionerY);
    } else {
        response->setResponseStatus(404);
    }

    if (ctrl == nullptr){
        response->setResponseStatus(404);
    } else {
        json temp = {{"kind",  "Controllable"},
                     {"name", ctrl_name},
                     {"enabled", ctrl->getEnabled()},
                     {"error_status", ctrl->getErrorStatus()},
                     {"error_number", ctrl->getErrorNumber()},
                     {"target_value", ctrl->getTargetValue()},
                     {"actual_value", ctrl->getActualValue()},
                     {"min_value", ctrl->getMinValue()},
                     {"max_value", ctrl->getMaxValue()}};
        std::string content = temp.dump();
        response->setContent(&content);
        response->setResponseStatus(200);
    }


}

void AMPamserviceControllableServlet::doPost(AMRequest *request, AMResponse *response) {
    doPut(request, response);
}

void AMPamserviceControllableServlet::doPut(AMRequest *request, AMResponse *response) {

}
