//
// Created by alex on 26/07/19.
//

#ifndef PSSERVER_AMPAMSERVICEDISPENSESERVLET_H
#define PSSERVER_AMPAMSERVICEDISPENSESERVLET_H


#include "AMServletBase.h"
#include "AMRequest.h"
#include "AMResponse.h"


class AMPamserviceDispenseServlet : virtual public AMServletBase
{
public:
    virtual void doGet(AMRequest * request, AMResponse * response );
    virtual void doPut(AMRequest * request, AMResponse * response );
    virtual void doPost(AMRequest * request, AMResponse * response );
    unsigned run(unsigned wellBitSet,  unsigned dispenseHead, float amount, float temperature,
                 unsigned waitTimeAfterLastWellDispensed);
};


#endif //PSSERVER_AMPAMSERVICEDISPENSESERVLET_H
