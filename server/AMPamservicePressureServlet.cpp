//
// Created by alex on 11/07/19.
//

#include "AMPamservicePressureServlet.h"

#include "modules/systemlayer/StubPamStationFactory.h"
#include "modules/systemlayer/StubPamStation.h"
#include "modules/propertylayer/PropDisposable.h"
#include "modules/pimcore/AspiratePump.h"


#include "json.hpp"

using json = nlohmann::json;

void AMPamservicePressureServlet::doGet(AMRequest *request, AMResponse *response) {

    auto disposable = StubPamStationFactory::getCurrentPamStation()->getPropDisposable();
    auto wellBitSet = (unsigned) std::stoi(request->getParameter("wellBitSet"));
    std::vector<int> wellIndex;
    std::vector<float> wellPressure;

    for (int i = eWell1; i < eWellNoMax; i++)  //loop for all wells, i: 1,2,3,4,5,etc
    {
        if ((1 << (i - 1)) & wellBitSet)           //check if well is selected, 1<<(i-1): 1,2,4,8,16,etc
        {
            wellIndex.push_back(i);
            wellPressure.push_back(disposable->getWellPressure(i));
        }
    }

    auto aspiratePumpStatus = StubPamStationFactory::getCurrentPamStation()->getAspiratePump()->getSwitchStatus();
    auto aspiratePumpPressure = StubPamStationFactory::getCurrentPamStation()->getAspiratePump()->getAspiratePressure();

    json temp = {{"kind",  "WellPressures"},
                 {"wells", wellIndex},
                 {"pressures", wellPressure},
                 {"aspiratePumpStatus", aspiratePumpStatus},
                 {"aspiratePressure", aspiratePumpPressure}};

    std::string content = temp.dump();
    response->setContent(&content);
}

// 96155777

void AMPamservicePressureServlet::doPost(AMRequest *request, AMResponse *response) {
    doPut(request, response);
}

void AMPamservicePressureServlet::doPut(AMRequest *request, AMResponse *response) {
    auto aspiratePumpStatus = (bool) std::stoi(request->getParameter("aspiratePumpStatus"));
    StubPamStationFactory::getCurrentPamStation()->getAspiratePump()->setSwitchStatus(aspiratePumpStatus);
}