//
// Created by alex on 12/07/19.
//

#ifndef PSSERVER_AMPAMSERVICEPUMPSERVLET_H
#define PSSERVER_AMPAMSERVICEPUMPSERVLET_H


#include "AMServletBase.h"
#include "AMRequest.h"
#include "AMResponse.h"


class AMPamservicePumpServlet : virtual public AMServletBase
{
public:
    virtual void doGet(AMRequest * request, AMResponse * response );
    virtual void doPut(AMRequest * request, AMResponse * response );
    virtual void doPost(AMRequest * request, AMResponse * response );
    unsigned run(unsigned wellBitSet, unsigned pumpCycles, unsigned extraUpTime, unsigned extraDownTime);
 };


#endif //PSSERVER_AMPAMSERVICEPUMPSERVLET_H
