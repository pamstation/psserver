//
// Created by alex on 06/03/19.
//

#ifndef PSSERVER_AMPINGSERVLET_H
#define PSSERVER_AMPINGSERVLET_H


#include "AMServletBase.h"
#include "AMRequest.h"
#include "AMResponse.h"

class AMPingServlet : virtual public AMServletBase
{
public:
    virtual void doGet(AMRequest * request, AMResponse * response );
};

#endif //PSSERVER_AMPINGSERVLET_H
