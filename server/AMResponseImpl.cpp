//
// Created by alex on 06/03/19.
//

#include "AMResponseImpl.h"


AMResponseImpl::AMResponseImpl(std::shared_ptr<HttpServer::Response> resp) {
    response = resp;
    status = SimpleWeb::StatusCode::success_ok;
    stream = nullptr;
}

AMResponseImpl::~AMResponseImpl() {
    if (stream){
        delete(stream);
        stream = nullptr;
    }
}

void AMResponseImpl::setResponseStatus(int s) {
    switch (s) {
        case 200 :
            status = SimpleWeb::StatusCode::success_ok;
            break;
        case 400 :
            status = SimpleWeb::StatusCode::client_error_bad_request;
            break;
        case 404 :
            status = SimpleWeb::StatusCode::client_error_not_found;
            break;
        default:
            status = SimpleWeb::StatusCode::server_error_not_implemented;
    }
}

void AMResponseImpl::setContent(std::string *c) {
    content.clear();
    content.append(c->c_str());
}

void AMResponseImpl::setStreamContent(std::istream * instream) {
    if (stream){
        delete(stream);
        stream = nullptr;
    }
    stream = instream;
}

void AMResponseImpl::setContentType(std::string *c) {
    header.emplace("content-type", c->c_str());
}


void AMResponseImpl::write() {
    if (stream){
        response->write(status, *stream, header);
        delete(stream);
        stream = nullptr;
    } else {
        response->write(status, content, header);
    }

}