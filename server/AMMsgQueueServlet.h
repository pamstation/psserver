#ifndef AMMsgQueueServletH
#define AMMsgQueueServletH

#include "AMServletBase.h"
#include "AMRequest.h"
#include "AMResponse.h"

class AMMsgQueueServlet : virtual public AMServletBase
{
public:
    virtual void doGet(AMRequest * request, AMResponse * response );
    virtual void doPost(AMRequest * request, AMResponse * response );
};

#endif //AMMsgQueueServletH

