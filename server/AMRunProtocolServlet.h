#ifndef AMRunProtocolServletH
#define AMRunProtocolServletH

#include "AMServletBase.h"
#include "AMRequest.h"
#include "AMResponse.h"

class AMRunProtocolServlet : virtual public AMServletBase
{
public:
    virtual void doGet(AMRequest * request, AMResponse * response );
};

#endif //AMRunProtocolServletH


