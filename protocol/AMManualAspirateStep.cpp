//---------------------------------------------------------------------------
#pragma hdrstop

#include "AMManualAspirateStep.h"                      //for header-file

#include "modules/pimcore/PamStationFactory.h"   //for obtaining & using PamStation
#include "modules/pimcore/PamStation.h"          //for obtaining & using PamStation
#include "modules/pimcore/Disposable.h"          //for obtaining & using object
#include "modules/pimcore/WagonXYPositioner.h"   //for obtaining & using object

//#include "protocols/EjectSubStep.h"              //for using ProtocolStep
#include "modules/pimcore/WagonHeater.h"  
#include "modules/pimcore/UnloadButton.h"
//#include "protocols/HomeTransporterSubStep.h"    //for use of this sub-step
#include "tinyxml2.h"

#pragma package(smart_init)


//---------------------------------------------------------------------------

AMManualAspirateStep::AMManualAspirateStep(Protocol * aP)
{
    protocol = aP;
    name_ = "manualAspirateStep";
    hasPumpUp = false;
}
AMManualAspirateStep::~AMManualAspirateStep() {}

std::string AMManualAspirateStep::name()
{
    return name_;
}

void AMManualAspirateStep::appendXML(std::stringstream * st)
{
    *st << "<step";

    *st << " name=\"";
    *st <<  name_;
    *st << "\"";

    *st << "/>";
}

bool AMManualAspirateStep::initializeFromXMLElement(tinyxml2::XMLElement* aStepElement)
{

    return true;
}

void AMManualAspirateStep::run()
{
    PamStation       * pPamStation        = PamStationFactory::getCurrentPamStation();
    Disposable       * pDisposable        = pPamStation->getDisposable();
    WagonXYPositioner* pWagonXYPositioner = pPamStation->getWagonXYPositioner();

    //EjectSubStep aEjectSubStep = EjectSubStep();

    try
    {
        if (!hasPumpUp)
        {
            //move incubation chamber to incubate position
            pWagonXYPositioner->goIncubationPosition();
            pWagonXYPositioner->waitReady();

            //pump fluid to manually aspirate up in the selected wells
            pDisposable->pumpUp(protocol->wellBitSet()); //extra waitTime = 0 ms;
//            pDisposable->waitPumpReady();

//            pDisposable->pumpUpAsync(protocol->wellBitSet()).get();


            protocol->checkBrokenMembranes();
            hasPumpUp = true;
        }

        //eject.run() =
        //1 on button-press: goFrontPanelPosition, then goLoad()
        //2 allow user to manual aspirate
        //2 on button press: goFrontPanelPosition() again
        //aEjectSubStep.run();
        runEject();
        pDisposable->checkDisposablesLoaded(protocol->wellBitSet()); //throws exception "DISPOSABLE_NOT_LOADED"

    }

    catch (...)
    {
        pWagonXYPositioner->setEnabled(false);
        throw ;
    }
}


void AMManualAspirateStep::runEject()
{
    PamStation            * pPamStation             = PamStationFactory::getCurrentPamStation();
    WagonHeater           * pWagonHeater            = pPamStation->getWagonHeater();
    WagonXYPositioner     * pWagonXYPositioner      = pPamStation->getWagonXYPositioner();
    UnloadButton     * pUnloadButton      = pPamStation->getUnloadButton();

//    HomeTransporterSubStep  aHomeTransporterSubStep = HomeTransporterSubStep();

    //home the wagonPositioner first, use sub step "HomeTransporterSubStep"
    //button will be enabled, and is blocking untill pressed
    pUnloadButton->setEnabled(true);   //button is pressable
    pUnloadButton->waitPressedEvent(); //now waiting for press-command, does not timeout!
    //on this point the button is pressed
    pUnloadButton->setEnabled(false);  //disable button

    if ( ! pWagonXYPositioner->getHomed() )  //only when necessary
    {
        pWagonXYPositioner->goHome();
        pWagonXYPositioner->waitReady();
        //at this point wagon reached home, and encoders are resetted
    }

    //FrontPanelPosition is nearly home
    //going to this position gives smoother moves without chance of touching the casing
    pWagonXYPositioner->goFrontPanelPosition();
    pWagonXYPositioner->waitReady();

    //WagonPositioner is now homed & in FrontPanelPosition

    //check if unloadtemperature safe
    pWagonHeater->checkUnloadTemperatureSafe();

    //goto (un)load position
    pWagonXYPositioner->goLoadPosition();
    pWagonXYPositioner->waitReady();

    //wagon is now in (un)load position

    /* if (coverNotClosedException)
     {
         coverNotClosedException = false;
         SysExceptionGenerator * excptionGenerator = dynamic_cast<SysExceptionGenerator*>( pPamStation->getExceptionGenerator());
         excptionGenerator->onException(this, WAGON_COVER_NOT_CLOSED, eErrorSafety, "WAGON_COVER_NOT_CLOSED");
     }
     */
    //button will be enabled, and is blocking untill pressed
    //in other words: the wagon will go to home (inside the instrument) on pressing the button
//    aHomeTransporterSubStep.run();  //run again, now from load position

    pUnloadButton->setEnabled(true);   //button is pressable
    pUnloadButton->waitPressedEvent(); //now waiting for press-command, does not timeout!
    //on this point the button is pressed
    pUnloadButton->setEnabled(false);  //disable button

    if ( ! pWagonXYPositioner->getHomed() )  //only when necessary
    {
        pWagonXYPositioner->goHome();
        pWagonXYPositioner->waitReady();
        //at this point wagon reached home, and encoders are resetted
    }

    //FrontPanelPosition is nearly home
    //going to this position gives smoother moves without chance of touching the casing
    pWagonXYPositioner->goFrontPanelPosition();
    pWagonXYPositioner->waitReady();

    //the wagon is back in FrontPanelPosition
}



