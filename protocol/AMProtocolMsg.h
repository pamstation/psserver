
#ifndef AMProtocolMsgH
#define AMProtocolMsgH

#include <string>

class AMProtocolMsg
{
public:
    AMProtocolMsg(int aType, std::string aMsg);
    ~AMProtocolMsg();
    int type;
    std::string msg;
};

#endif //AMProtocolMsgH
