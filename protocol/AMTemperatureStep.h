
#ifndef AMTemperatureStepH
#define AMTemperatureStepH

#include "Protocol.h"
#include "AMStep.h"

#include <string>
#include "tinyxml2.h"


class Protocol;

class AMTemperatureStep : virtual public AMStep
{
public:

    AMTemperatureStep(Protocol * aP);
    ~AMTemperatureStep();
    virtual std::string name();
    virtual bool initializeFromXMLElement(tinyxml2::XMLElement* aStepElement);
    virtual void appendXML(std::stringstream * st);
    virtual void run();


private:
    Protocol * protocol;
    std::string name_;
    float temperature;
    float gain;
};

#endif //AMTemperatureStepH





