#ifndef AMReadByCycleExStepH
#define AMReadByCycleExStepH

#include "Protocol.h"

#include <string>
#include <vector>
#include "tinyxml2.h"

#include "AMReadExStep.h"

class Protocol;
class Image;
class AMReadExStep;

class AMReadByCycleExStep : virtual public AMReadExStep
{
public:

    AMReadByCycleExStep(Protocol * aP);

    virtual bool initializeFromXMLElement(tinyxml2::XMLElement* aStepElement);
    virtual void appendXML(std::stringstream * st);
    virtual void run();

    virtual void setIncrCycle(bool b);



protected:

    unsigned pumpCycles;
    unsigned everyNCycles;
    unsigned extraUpTime;
    unsigned extraDownTime;
    bool aborted;
    bool incrCycle;

    unsigned currentPumpCycles;
    bool generateBrokenMembraneTest;


};

#endif //AMReadByCycleExStepH


