#ifndef AMDispenseStepH
#define AMDispenseStepH

#include "Protocol.h"
#include "AMStep.h"
#include "modules/pimcore/PamDefs.h"

#include <string>
#include "tinyxml2.h"


class Protocol;

class AMDispenseStep : virtual public AMStep
{
public:

    AMDispenseStep(Protocol * aP);
    ~AMDispenseStep();
    virtual std::string name();
    virtual bool initializeFromXMLElement(tinyxml2::XMLElement* aStepElement);
    virtual void appendXML(std::stringstream * st);
    virtual void run();


private:
    Protocol * protocol;
    std::string name_;
    unsigned currentWell;
    EDispenseHeadNo dispenseHead;
    float amount;
    float temperature;
    bool temperatureControl;
    float waitTimeAfterLastWellDispensed;
    bool syringeNotPresentException;

};

#endif //AMDispenseStepH

