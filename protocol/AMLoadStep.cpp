//---------------------------------------------------------------------------
#pragma hdrstop

#include "AMLoadStep.h"                      //for header-file
#include "Protocol.h"
#include "modules/pimcore/PamStationFactory.h"       //for obtaining & using PamStation
#include "modules/pimcore/PamStation.h"              //for obtaining & using PamStation

#include "modules/systemlayer/SysExceptionGenerator.h"      //for passing Exception to ExceptionGenerator
#include "modules/pimcore/ExceptionGenerator.h"
#include "modules/systemlayer/SysExceptionGenerator.h"
#include "modules/pimcore/DefaultGenerator.h"        //for obtaining & using object
#include "modules/pimcore/Disposable.h"              //for obtaining & using object
#include "modules/pimcore/OverPressureSupplier.h"    //for obtaining & using object
#include "modules/pimcore/UnderPressureSupplier.h"   //for obtaining & using object
#include "modules/pimcore/WagonHeater.h"             //for obtaining & using object
#include "modules/pimcore/WagonXYPositioner.h"       //for obtaining & using object
#include "modules/pimcore/UnloadButton.h"
//#include "protocols/HomeTransporterSubStep.h"    //for use of this sub-step
#include "tinyxml2.h"

#pragma package(smart_init)


//---------------------------------------------------------------------------

AMLoadStep::AMLoadStep(Protocol * aP)
{
    protocol = aP;
    name_ = "loadStep";
    coverNotClosedException = false;
    disposableNotLoadedException = false;
    overPressure_ = PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getLoad_Unload_Settings()->rDefaultOverPressure;
    underPressure_ = PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getLoad_Unload_Settings()->rDefaultUnderPressure;
    temperature_ = PamStationFactory::getCurrentPamStation()->getDefaultGenerator()->getLoad_Unload_Settings()->rInitWagonTemperature;
}
AMLoadStep::~AMLoadStep() {}

std::string AMLoadStep::name()
{
    return name_;
}

void AMLoadStep::appendXML(std::stringstream * st)
{
    *st << "<step";

    *st << " name=\"";
    *st <<  name_;
    *st << "\"";

    *st << " overPressure=\"";
    *st <<  overPressure_;
    *st << "\"";

    *st << " underPressure=\"";
    *st <<  underPressure_;
    *st << "\"";

    *st << " temperature=\"";
    *st <<  temperature_;
    *st << "\"";

    *st << " coverNotClosedException=\"";
    *st <<  coverNotClosedException;
    *st << "\"";

    *st << " disposableNotLoadedException=\"";
    *st <<  disposableNotLoadedException;
    *st << "\"";



    *st << "/>";
}

bool AMLoadStep::initializeFromXMLElement(tinyxml2::XMLElement* aStepElement)
{
    aStepElement->QueryFloatAttribute( "overPressure", &overPressure_ );
    aStepElement->QueryFloatAttribute( "underPressure", &underPressure_ );
    aStepElement->QueryFloatAttribute( "temperature", &temperature_ );
    aStepElement->QueryBoolAttribute( "coverNotClosedException", &coverNotClosedException );
    aStepElement->QueryBoolAttribute( "disposableNotLoadedException", &disposableNotLoadedException );
    return true;
}


 void AMLoadStep::run()
{
    PamStation           * pPamStation            = PamStationFactory::getCurrentPamStation();
    DefaultGenerator     * pDefaultGenerator      = pPamStation->getDefaultGenerator();
    Disposable           * pDisposable            = pPamStation->getDisposable();
    OverPressureSupplier * pOverPressureSupplier  = pPamStation->getOverPressureSupplier();
    UnderPressureSupplier* pUnderPressureSupplier = pPamStation->getUnderPressureSupplier();
    WagonHeater          * pWagonHeater           = pPamStation->getWagonHeater();
    WagonXYPositioner    * pWagonXYPositioner     = pPamStation->getWagonXYPositioner();

    pDefaultGenerator->getLoad_Unload_Settings()->rDefaultOverPressure = overPressure_;
    pDefaultGenerator->getLoad_Unload_Settings()->rDefaultUnderPressure = underPressure_;
    pDefaultGenerator->getLoad_Unload_Settings()->rInitWagonTemperature = temperature_;

    
    try
    {

        //eject.run() =
        //1 on button-press: if needed home(), then goFrontPanel, then goLoad()
        //2 on button press: if needed home(), then goFrontPanel
        runEject();

        //CHECK if the selected wells match the loaded disposables

        if (disposableNotLoadedException)
        {
            disposableNotLoadedException = false;
            SysExceptionGenerator * excptionGenerator = dynamic_cast<SysExceptionGenerator*>( pPamStation->getExceptionGenerator());
            excptionGenerator->onException(this, DISPOSABLE_NOT_LOADED, eErrorUnknown, "DISPOSABLE_NOT_LOADED");
        }
        pDisposable->checkDisposablesLoaded(protocol->wellBitSet()); //throws exception "DISPOSABLE_NOT_LOADED"


        //get setpoints
        float rDefaultOverPressure  =  pDefaultGenerator->getLoad_Unload_Settings()->rDefaultOverPressure ;  //a protocol setting
        float rDefaultUnderPressure =  pDefaultGenerator->getLoad_Unload_Settings()->rDefaultUnderPressure;  //a protocol setting
        float rInitWagonTemperature =  pDefaultGenerator->getLoad_Unload_Settings()->rInitWagonTemperature;  //a protocol setting
        float rMaxTemperatureGain   =  pWagonHeater->getMaxTemperatureGain();                                //a controller setting

        //START TEMPERATURE CONTROL & PRESSURE CONTROL & GO INCUBATIONPOSITION
        pWagonHeater          ->setTemperatureGain( rMaxTemperatureGain );
        pWagonHeater          ->setTargetValue(rInitWagonTemperature);   //throws if protocol setting doesn't match controller setting for range
        pOverPressureSupplier ->setTargetValue(rDefaultOverPressure );   //throws if protocol setting doesn't match controller setting for range
        pUnderPressureSupplier->setTargetValue(rDefaultUnderPressure);   //throws if protocol setting doesn't match controller setting for range
        pWagonXYPositioner    ->goIncubationPosition();


        pOverPressureSupplier ->waitReady(); //throws CONTROLLABLE_HW_ERROR_NOTIFICATION or CONTROLLABLE_TIMEOUT
        pUnderPressureSupplier->waitReady(); //throws CONTROLLABLE_HW_ERROR_NOTIFICATION or CONTROLLABLE_TIMEOUT
        pWagonXYPositioner    ->waitReady(); //throws CONTROLLABLE_HW_ERROR_NOTIFICATION or CONTROLLABLE_TIMEOUT
        pWagonHeater          ->waitReady(); //throws CONTROLLABLE_HW_ERROR_NOTIFICATION or CONTROLLABLE_TIMEOUT

        pWagonXYPositioner->setEnabled(false);
    }

    catch (...)
    {

        //stop wagonPositioner control (makes error-recovery easier)
        pWagonXYPositioner->setEnabled(false);
        throw ;
    }

}

void AMLoadStep::runEject()
{
    PamStation            * pPamStation             = PamStationFactory::getCurrentPamStation();
    WagonHeater           * pWagonHeater            = pPamStation->getWagonHeater();
    WagonXYPositioner     * pWagonXYPositioner      = pPamStation->getWagonXYPositioner();
    UnloadButton     * pUnloadButton      = pPamStation->getUnloadButton();

//    HomeTransporterSubStep  aHomeTransporterSubStep = HomeTransporterSubStep();

    //home the wagonPositioner first, use sub step "HomeTransporterSubStep"
    //button will be enabled, and is blocking untill pressed
//    aHomeTransporterSubStep.run();

    pUnloadButton->setEnabled(true);   //button is pressable
    pUnloadButton->waitPressedEvent(); //now waiting for press-command, does not timeout!
    //on this point the button is pressed
    pUnloadButton->setEnabled(false);  //disable button

    if ( ! pWagonXYPositioner->getHomed() )  //only when necessary
    {
        pWagonXYPositioner->goHome();
        pWagonXYPositioner->waitReady();
        //at this point wagon reached home, and encoders are resetted
    }

    //FrontPanelPosition is nearly home
    //going to this position gives smoother moves without chance of touching the casing
    pWagonXYPositioner->goFrontPanelPosition();
    pWagonXYPositioner->waitReady();

    //WagonPositioner is now homed & in FrontPanelPosition

    //check if unloadtemperature safe
    pWagonHeater->checkUnloadTemperatureSafe();

    //goto (un)load position
    pWagonXYPositioner->goLoadPosition();
    pWagonXYPositioner->waitReady();

    //wagon is now in (un)load position

    if (coverNotClosedException)
    {
        coverNotClosedException = false;
        SysExceptionGenerator * excptionGenerator = dynamic_cast<SysExceptionGenerator*>( pPamStation->getExceptionGenerator());
        excptionGenerator->onException(this, WAGON_COVER_NOT_CLOSED, eErrorSafety, "WAGON_COVER_NOT_CLOSED");
    }
    //button will be enabled, and is blocking untill pressed
    //in other words: the wagon will go to home (inside the instrument) on pressing the button
//    aHomeTransporterSubStep.run();  //run again, now from load position

    pUnloadButton->setEnabled(true);   //button is pressable
    pUnloadButton->waitPressedEvent(); //now waiting for press-command, does not timeout!
    //on this point the button is pressed
    pUnloadButton->setEnabled(false);  //disable button

    if ( ! pWagonXYPositioner->getHomed() )  //only when necessary
    {
        pWagonXYPositioner->goHome();
        pWagonXYPositioner->waitReady();
        //at this point wagon reached home, and encoders are resetted
    }

    //FrontPanelPosition is nearly home
    //going to this position gives smoother moves without chance of touching the casing
    pWagonXYPositioner->goFrontPanelPosition();
    pWagonXYPositioner->waitReady();
    //the wagon is back in FrontPanelPosition
}
