

//---------------------------------------------------------------------------
#pragma hdrstop

#include "AMWaitStep.h"
#include "modules/utils/util.h"
#pragma package(smart_init)


//---------------------------------------------------------------------------

AMWaitStep::AMWaitStep(Protocol * aP)
{
    protocol = aP;
    name_ = "waitStep";
    duration = 0;
}
AMWaitStep::~AMWaitStep() {}

string AMWaitStep::name()
{
    return name_;
}

void AMWaitStep::appendXML(std::stringstream * st)
{
    *st << "<step";

    *st << " name=\"";
    *st <<  name_;
    *st << "\"";

    *st << " duration=\"";
    *st <<  duration;
    *st << "\"";

    *st << "/>";
}

bool AMWaitStep::initializeFromXMLElement(tinyxml2::XMLElement* aStepElement)
{
    aStepElement->QueryUnsignedAttribute("duration", &duration );
    return true;
}


void AMWaitStep::run()
{
    Util::Sleep(duration);
}
