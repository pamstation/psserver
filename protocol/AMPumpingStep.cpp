//---------------------------------------------------------------------------
#pragma hdrstop

#include "AMPumpingStep.h"                      //for header-file

#pragma package(smart_init)

AMPumpingStep::AMPumpingStep(Protocol * aP) : AMReadExStep(aP) , AMReadByCycleExStep(aP)
{
    name_ = "pumpingStep";
}

 



void AMPumpingStep::appendXML(std::stringstream * st)
{
    *st << "<step";

    *st << " name=\"";
    *st <<  name_;
    *st << "\"";

    *st << " pumpCycles=\"";
    *st <<  pumpCycles;
    *st << "\"";

    *st << " extraUpTime=\"";
    *st <<  extraUpTime;
    *st << "\"";

    *st << " extraDownTime=\"";
    *st <<  extraDownTime;
    *st << "\"";

    *st << " generateBrokenMembraneTest=\"";
    *st <<  generateBrokenMembraneTest;
    *st << "\"";

    *st << "/>";
}

bool AMPumpingStep::initializeFromXMLElement(tinyxml2::XMLElement* aStepElement)
{
    aStepElement->QueryUnsignedAttribute( "pumpCycles", &pumpCycles );
    aStepElement->QueryUnsignedAttribute( "extraUpTime", &extraUpTime );
    aStepElement->QueryUnsignedAttribute( "extraDownTime", &extraDownTime );
    
    aStepElement->QueryBoolAttribute( "generateBrokenMembraneTest", &generateBrokenMembraneTest );
    return true;
}

void AMPumpingStep::runRead() {}
