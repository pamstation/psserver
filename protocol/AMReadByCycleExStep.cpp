//---------------------------------------------------------------------------

#pragma hdrstop

#include <iostream>
#include <future>

#include "AMReadByCycleExStep.h"                      //for header-file

#include "modules/pimcore/PamStationFactory.h"      //for obtaining & using PamStation
#include "modules/pimcore/PamStation.h"             //for obtaining & using PamStation

#include "modules/pimcore/CCD.h"                    //for obtaining & using object
#include "modules/pimcore/Disposable.h"             //for obtaining & using object
#include "modules/pimcore/FilterPositioner.h"       //for obtaining & using object
#include "modules/pimcore/FocusPositioner.h"        //for obtaining & using object
#include "modules/pimcore/LEDUnit.h"                //for obtaining & using object
#include "modules/pimcore/WagonXYPositioner.h"      //for obtaining & using object
#include "modules/pimcore/WagonHeater.h"             //for obtaining & using object

#include "modules/pimcore/Image.h"
#include "modules/systemlayer/CCMImage.h"

#include "modules/utils/util.h"

#pragma package(smart_init)


//---------------------------------------------------------------------------

AMReadByCycleExStep::AMReadByCycleExStep(Protocol *aP) : AMReadExStep(aP) {
    name_ = "readByCycleExStep";
    pumpCycles = 0;
    extraUpTime = 100;
    extraDownTime = 100;
    currentPumpCycles = 0;
    everyNCycles = 1;
    generateBrokenMembraneTest = false;
    aborted = false;
    incrCycle = true;
}

void AMReadByCycleExStep::setIncrCycle(bool b) {
    incrCycle = b;
}

void AMReadByCycleExStep::appendXML(std::stringstream *st) {
    *st << "<step";

    *st << " name=\"";
    *st << name_;
    *st << "\"";

    *st << " pumpCycles=\"";
    *st << pumpCycles;
    *st << "\"";

    *st << " everyNCycles=\"";
    *st << everyNCycles;
    *st << "\"";

    *st << " extraUpTime=\"";
    *st << extraUpTime;
    *st << "\"";

    *st << " extraDownTime=\"";
    *st << extraDownTime;
    *st << "\"";

    *st << " generateBrokenMembraneTest=\"";
    *st << generateBrokenMembraneTest;
    *st << "\"";

    *st << ">";

    *st << "<filters>";
    for (unsigned i = 0; i < exposureTimes_.size(); i++) {
        *st << "<filter";

        *st << " id=\"";
        *st << filterIds_[i];;
        *st << "\"";

        *st << " exposureTime=\"";
        *st << exposureTimes_[i];
        *st << "\"";

        *st << "/>";
    }
    *st << "</filters>";

    *st << "</step>";
}

bool AMReadByCycleExStep::initializeFromXMLElement(tinyxml2::XMLElement *aStepElement) {
    aStepElement->QueryUnsignedAttribute("pumpCycles", &pumpCycles);
    aStepElement->QueryUnsignedAttribute("everyNCycles", &everyNCycles);

    aStepElement->QueryUnsignedAttribute("extraUpTime", &extraUpTime);
    aStepElement->QueryUnsignedAttribute("extraDownTime", &extraDownTime);

    aStepElement->QueryBoolAttribute("generateBrokenMembraneTest", &generateBrokenMembraneTest);

    tinyxml2::XMLElement *filtersElement = aStepElement->FirstChildElement("filters");
    if (filtersElement == NULL) return false;
    tinyxml2::XMLElement *filterElement = filtersElement->FirstChildElement("filter");
    if (filterElement == NULL) return false;
    while (filterElement) {
        unsigned filterID;
        tinyxml2::XMLError err = filterElement->QueryUnsignedAttribute("id", &filterID);
        if (err != 0) return false;
        float exp = 0.0f;
        err = filterElement->QueryFloatAttribute("exposureTime", &exp);
        if (err != 0) return false;
        exposureTimes_.push_back(exp);
        filterIds_.push_back(filterID);
        filterElement = filterElement->NextSiblingElement("filter");
    }
    return true;
}

void AMReadByCycleExStep::run() {
    PamStation *pPamStation = PamStationFactory::getCurrentPamStation();
    Disposable *pDisposable = pPamStation->getDisposable();
    WagonXYPositioner *pWagonXYPositioner = pPamStation->getWagonXYPositioner();

    //pump fluid down in the selected wells
//    pDisposable->pumpDown(protocol->wellBitSet());
//    pDisposable->waitPumpReady();

    std::cout << "AMReadByCycleExStep::run -- pre -- pumpDown" << std::endl;
    pDisposable->pumpDown(protocol->wellBitSet());


    if (generateBrokenMembraneTest) protocol->generateBrokenMembranesTest();

    std::cout << "AMReadByCycleExStep::run -- pre -- checkBrokenMembranes" << std::endl;

    protocol->checkBrokenMembranes();

    if (aborted) {
        std::cout << "AMReadByCycleExStep::run -- pre -- aborted -- runRead" << std::endl;
        runRead();
        aborted = false;
    }

    bool doIncrCycle = currentPumpCycles == 0;      //on retry do not increment cycle on first pump down

    for (; currentPumpCycles < pumpCycles && !(protocol->isAborting()); currentPumpCycles++) {
        try //tryI, use try/finally to make sure semaphore is unlocked
        {
            //lock semaphore to synchonize with ReadNowStep
//            pDisposable->getPumpUpSemaphore();

            //make sure the wagon is (still) in incubation position
            std::cout << "AMReadByCycleExStep::run -- goIncubationPosition" << std::endl;
            pWagonXYPositioner->goIncubationPosition();
            std::cout << "AMReadByCycleExStep::run -- waitReady" << std::endl;
            pWagonXYPositioner->waitReady();

            //pump fluid up in the selected wells
            std::cout << "AMReadByCycleExStep::run -- pumpUp" << std::endl;
            pDisposable->pumpUp(protocol->wellBitSet());

//            std::cout << "AMReadByCycleExStep::run -- waitPumpReady" << std::endl;
//            pDisposable->waitPumpReady();

            std::cout << "AMReadByCycleExStep::run -- checkBrokenMembranes" << std::endl;
            protocol->checkBrokenMembranes();

            std::cout << "AMReadByCycleExStep::run -- Sleep" << std::endl;
            Util::Sleep(extraUpTime);

            //pump fluid down in the selected wells
            std::cout << "AMReadByCycleExStep::run -- pumpDownAsync" << std::endl;
            pDisposable->pumpDown(protocol->wellBitSet());
//            std::cout << "AMReadByCycleExStep::run -- waitPumpReady" << std::endl;
//            pDisposable->waitPumpReady();

            std::cout << "AMReadByCycleExStep::run -- checkBrokenMembranes" << std::endl;
            protocol->checkBrokenMembranes();

            if (incrCycle && doIncrCycle){
                std::cout << "AMReadByCycleExStep::run -- incrementCycle" << std::endl;
                protocol->incrementCycle();
            }
            doIncrCycle = true;

            std::cout << "AMReadByCycleExStep::run -- Sleep" << std::endl;
            Util::Sleep(extraDownTime);

            std::cout << "AMReadByCycleExStep::run -- done" << std::endl;
            //unlock semaphore to synchonize with ReadNowStep
//            pDisposable->releasePumpUpSemaphore();
        } //end tryI

        catch (...) {
//            try {//unlock semaphore to synchonize with ReadNowStep
//                pDisposable->releasePumpUpSemaphore();
//            } catch (...) {}
            throw ;

        }
        if (((currentPumpCycles + 1) % everyNCycles) == 0) {
            try {
                runRead();
            } catch (...) {
                aborted = true;
                throw;
            }
            if (!(protocol->isAborting())) {
                currentWell = eWell1;
                currentFilterExposureTime = 0;
            } else {
                aborted = true;
            }
        }
    }//end for-loop

}

