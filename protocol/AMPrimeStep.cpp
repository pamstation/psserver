

//---------------------------------------------------------------------------
#pragma hdrstop

#include "AMPrimeStep.h"

#include "modules/pimcore/PamDefs.h"                    //for header-file

#include "modules/pimcore/PamStationFactory.h"      //for obtaining & using PamStation
#include "modules/pimcore/PamStation.h"             //for obtaining & using PamStation
#include "modules/pimcore/DefaultGenerator.h"       //for obtaining DispenseDefaults
#include "modules/pimcore/DispenseHeadPositioner.h" //for obtaining & using object
#include "modules/pimcore/WagonXYPositioner.h"      //for obtaining & using object
#include "modules/utils/util.h"


#pragma package(smart_init)


//---------------------------------------------------------------------------

AMPrimeStep::AMPrimeStep(Protocol * aP)
{
    protocol = aP;
    name_ = "primeStep";
    headBitSet = 0;
    currentHeadBitSet = eHead1;
}
AMPrimeStep::~AMPrimeStep() {}

string AMPrimeStep::name()
{
    return name_;
}

void AMPrimeStep::appendXML(std::stringstream * st)
{
    *st << "<step";

    *st << " name=\"";
    *st <<  name_;
    *st << "\"";

    *st << " headBitSet=\"";
    *st <<  headBitSet;
    *st << "\"";

    *st << "/>";
}

bool AMPrimeStep::initializeFromXMLElement(tinyxml2::XMLElement* aStepElement)
{

    aStepElement->QueryUnsignedAttribute("headBitSet", &headBitSet );

    return true;
}


void AMPrimeStep::run()
{
    PamStation            * pPamStation             = PamStationFactory::getCurrentPamStation();
    DefaultGenerator      * pDefaultGenerator       = pPamStation->getDefaultGenerator();
    WagonXYPositioner     * pWagonXYPositioner      = pPamStation->getWagonXYPositioner();



    try //tryI
    {
        //thow exception when a selected DispenseHeadPositioner is not present
        for (; currentHeadBitSet < eHeadMax; currentHeadBitSet ++)   //loop for all Heads, i: 1,2,3,4
        {
            if ((1 << (currentHeadBitSet - 1)) & headBitSet)       //check if Head is selected, 1<<(i-1): 1,2,4,8
            {
                DispenseHeadPositioner* pDispenseHeadPositioner = pPamStation->getDispenseHeadPositioner((EDispenseHeadNo)currentHeadBitSet);
                pDispenseHeadPositioner->checkDispenseHeadPresent();  //thows DISPENSEHEAD_NOT_PRESENT
            }//end if
        } //end for

        currentHeadBitSet = eHead1;

        for (  ; currentHeadBitSet < eHeadMax && !(protocol->isAborting()); currentHeadBitSet ++)   //loop for all Heads, i: 1,2,3,4
        {
            if ((1 << (currentHeadBitSet - 1)) & headBitSet)       //check if Head is selected, 1<<(i-1): 1,2,4,8
            {
                //place selected well under selected DispenseHead
                pWagonXYPositioner->goPrimePosition((EDispenseHeadNo)currentHeadBitSet);
                pWagonXYPositioner->waitReady();

                //get handle to the selected DispenseHead
                DispenseHeadPositioner* pDispenseHeadPositioner = pPamStation->getDispenseHeadPositioner((EDispenseHeadNo)currentHeadBitSet);

                //get default values:
                float    rPrimeAmount   = pDefaultGenerator->getDispenseDefaults()->rPrimeAmount  ;
                float    rRetractAmount = pDefaultGenerator->getDispenseDefaults()->rRetractAmount;
                unsigned uRetractDelay  = pDefaultGenerator->getDispenseDefaults()->uRetractDelay ;

                //make sure sign RetractAmount is positive
                if (rRetractAmount < 0) rRetractAmount = -rRetractAmount;

                try  //tryII
                {
                    //dispense fluid (including extra retract-amount)
                    pDispenseHeadPositioner->setTargetValue(rPrimeAmount + rRetractAmount);
                    pDispenseHeadPositioner->waitReady();

                    //wait some time to prevent droplet-forming
                    Util::Sleep( uRetractDelay );

                    //retract syringe over preset retractAmount to prevent droplet foming
                    pDispenseHeadPositioner->setTargetValue( -rRetractAmount );
                    pDispenseHeadPositioner->waitReady();
                    pDispenseHeadPositioner->setEnabled(false);                //always disable controller
                }
                catch (...)
                {
                    pDispenseHeadPositioner->setEnabled(false);                //always disable controller
                    throw ;
                }

            } //end if
        }   //end for
    }     //end tryI
    catch (...)
    {

        //stop wagonPositioner control (makes error-recovery easier)
        pWagonXYPositioner->setEnabled(false);
        throw ;
    }
}
