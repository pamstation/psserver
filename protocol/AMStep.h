#ifndef AMStepH
#define AMStepH

#include <string>
#include <sstream>

#include "tinyxml2.h"

using namespace std;
using namespace tinyxml2;

class AMStep
{
public:
    virtual ~AMStep() = 0;
    virtual std::string name() = 0;
    virtual void run() = 0;
    virtual bool initializeFromXMLElement(tinyxml2::XMLElement* aStepElement) = 0;
    virtual void appendXML(std::stringstream * st) = 0;

};

#endif //AMStepH
