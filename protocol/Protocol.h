
#ifndef ProtocolH
#define ProtocolH

#include <mutex>
#include <vector>
#include <string>
#include <sstream>

#include "AMProtocolMsg.h"
#include "AMQueue.h"
#include "tinyxml2.h"
#include "AMLogger.h"

class AMStep;


typedef enum
{
    pInit     = 0,
    pRunning  = 1,
    pAborting  = 2,
    pAborted  = 3,
    pCompleted = 4
} PState;


class Protocol
{
public:

    Protocol(AMLogger * aLogger);
    ~Protocol();

    virtual std::string getUnit();

    virtual bool initializeFromXML(const char* xml);

    virtual std::vector<AMStep*> * steps();

    virtual std::string barcode1();
    virtual std::string barcode2();
    virtual std::string barcode3();

    virtual std::string id();
    virtual std::string path();

    virtual int imageCount();

    virtual unsigned wellBitSet();
    virtual unsigned cycle();
    virtual bool isRunning();
    virtual bool isAbort();
    virtual bool isAborting();
    virtual bool isCompleted();

    virtual void run();
    virtual void basicRun();
    virtual void abort();

    virtual AMStep * createStep(tinyxml2::XMLElement * aStepElement);

    virtual void addMsg(AMProtocolMsg* msg);
    virtual void getProtocolMsgs(std::string * out);
    virtual void getProtocolMsgs(int i, std::string * out);
    virtual std::string getXML();
    virtual void appendXML(std::stringstream * st);
    virtual std::string getStateStr();
    virtual void setState(PState aState);


    virtual void incrementImageCount();
    virtual void incrementCycle();

    virtual void checkBrokenMembranes();
    virtual void generateBrokenMembranesTest();
    virtual unsigned checkBrokenMembranes(unsigned aWellBitSet);



private:

    PState state_;

    std::recursive_mutex mutex;

    std::string unit_;

    unsigned currentStepIndex;
    std::string path_;
    std::string id_;
    std::string barcode1_;
    std::string barcode2_;
    std::string barcode3_;
    unsigned wellBitSet_;
    int cycle_;
    int imageCount_;
    std::vector<AMStep*> * steps_;
    AMQueue * msgQueue_;
    AMLogger * logger;

    virtual void addStateMsg();

};


#endif //ProtocolH
