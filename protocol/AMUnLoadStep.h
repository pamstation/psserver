#ifndef AMUnLoadStepH
#define AMUnLoadStepH

#include "Protocol.h"
#include "AMStep.h"

#include <string>
#include "tinyxml2.h"

class Protocol;

class AMUnLoadStep : virtual public AMStep
{
public:

    AMUnLoadStep(Protocol * aP);
    ~AMUnLoadStep();
    virtual std::string name();
    virtual bool initializeFromXMLElement(tinyxml2::XMLElement* aStepElement);
    virtual void appendXML(std::stringstream * st);
    virtual void run();

private:
    Protocol * protocol;
    std::string name_;

};

#endif //AMUnLoadStepH
