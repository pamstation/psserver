//---------------------------------------------------------------------------

#pragma hdrstop

//#include<windows.h>
#include <boost/filesystem.hpp>

#include "AMReadExStep.h"                      //for header-file

#include "modules/pimcore/PamStationFactory.h"      //for obtaining & using PamStation
#include "modules/pimcore/PamStation.h"             //for obtaining & using PamStation
#include "modules/systemlayer/StubPamStation.h"


#include "modules/pimcore/CCD.h"                    //for obtaining & using object
#include "modules/pimcore/Disposable.h"             //for obtaining & using object
#include "modules/pimcore/FilterPositioner.h"       //for obtaining & using object
#include "modules/pimcore/FocusPositioner.h"        //for obtaining & using object
#include "modules/pimcore/LEDUnit.h"                //for obtaining & using object
#include "modules/pimcore/WagonXYPositioner.h"      //for obtaining & using object
#include "modules/pimcore/WagonHeater.h"             //for obtaining & using object

#include "modules/pimcore/Image.h"
#include "modules/systemlayer/CCMImage.h"

#include "modules/utils/util.h"

#pragma package(smart_init)


//---------------------------------------------------------------------------

AMReadExStep::AMReadExStep(Protocol *aP) {
    protocol = aP;
    name_ = "readExStep";

    currentWell = eWell1;
    currentFilterExposureTime = 0;
}

AMReadExStep::~AMReadExStep() {

}

string AMReadExStep::name() {
    return name_;
}

void AMReadExStep::saveImage(Image *pImage) {

    protocol->incrementImageCount();

    std::string imagePath = protocol->path();

    EWellNo well = pImage->getWell();

    std::string barcode = "";
    int aWNo = 1;
    if (well < 5) {
        aWNo = well;
        barcode.append(protocol->barcode1());
    } else if (well < 9) {
        aWNo = well - 4;
        barcode.append(protocol->barcode2());
    } else {
        aWNo = well - 8;
        barcode.append(protocol->barcode3());
    }

    std::stringstream cycle;
    cycle << protocol->cycle();
    std::stringstream exposuretime;
    exposuretime << (int) exposureTimes_[currentFilterExposureTime];
    std::stringstream filter;
    filter << pImage->getFilter();
    std::stringstream row;
    row << aWNo;
    std::stringstream temperature;
    temperature << (int) PamStationFactory::getCurrentPamStation()->getWagonHeater()->getActualValue();

    std::map<int, std::string> props;

    props[PG_TAG_BARCODE] = barcode;

    props[PG_TAG_COL] = "1";
    props[PG_TAG_CYCLE] = cycle.str();
    props[PG_TAG_EXPOSURETIME] = exposuretime.str();
    props[PG_TAG_FILTER] = filter.str();
    props[PG_TAG_INSTRUMENTTYPE] = "PS12";
    props[PG_TAG_ROW] = row.str();
    props[PG_TAG_TEMPERATURE] = temperature.str();
    props[PG_TAG_TIMESTAMP] = "";

    PamStation *pPam = PamStationFactory::getCurrentPamStation();
    auto *pStubPam = dynamic_cast<StubPamStation *>(pPam);

    props[PG_TAG_UNIT] = pStubPam->getConfig()->getUnitName();

            //protocol->getUnit();
    props[PG_TAG_PROTOCOLID] = protocol->id();

    std::stringstream st;
    st << "web/ImageResults/";
//    st << "web/" << protocol->path() << "/ImageResults/";

    boost::filesystem::path dir(st.str());

    if (!boost::filesystem::create_directories(dir)) {
        //TODO throw
    }

    std::stringstream image_name_st;

    image_name_st << barcode;
    image_name_st << "_";
    image_name_st << "W";
    image_name_st << row.str();
    image_name_st << "_";
    image_name_st << "F";
    image_name_st << filter.str();
    image_name_st << "_";
    image_name_st << "T";
    image_name_st << exposuretime.str();
    image_name_st << "_";
    image_name_st << "P";
    image_name_st << cycle.str();
    image_name_st << "_";
    image_name_st << "I";
    image_name_st << protocol->imageCount();
    image_name_st << "_";
    image_name_st << "A";
    image_name_st << temperature.str();
    image_name_st << ".tif";

    st << image_name_st.str();

    //710059218_W1_F1_T10_P124_I253_A30.tif

    auto *cimage = dynamic_cast<CCMImage *>(pImage);

    cimage->save(st.str(), &props);
    props.clear();

    std::stringstream path_st;
//    path_st << protocol->path() << "/ImageResults/" << image_name_st.str();
    path_st << "ImageResults/" << image_name_st.str();

    std::stringstream stmsg;
    stmsg << "<image path=\"";
    stmsg << path_st.str();
    stmsg << "\"/>";
    std::string aMsg = stmsg.str();
    auto *pm = new AMProtocolMsg(1, aMsg);
    protocol->addMsg(pm);
}

void AMReadExStep::appendXML(std::stringstream *st) {
    *st << "<step";

    *st << " name=\"";
    *st << name_;
    *st << "\"";


    *st << ">";

    *st << "<filters>";
    for (unsigned i = 0; i < exposureTimes_.size(); i++) {
        *st << "<filter";

        *st << " id=\"";
        *st << filterIds_[i];;
        *st << "\"";

        *st << " exposureTime=\"";
        *st << exposureTimes_[i];
        *st << "\"";

        *st << "/>";
    }
    *st << "</filters>";

    *st << "</step>";
}

bool AMReadExStep::initializeFromXMLElement(tinyxml2::XMLElement *aStepElement) {
    tinyxml2::XMLElement *filtersElement = aStepElement->FirstChildElement("filters");
    if (filtersElement == NULL) return false;
    tinyxml2::XMLElement *filterElement = filtersElement->FirstChildElement("filter");
    if (filterElement == NULL) return false;
    while (filterElement) {
        unsigned filterID;
        tinyxml2::XMLError err = filterElement->QueryUnsignedAttribute("id", &filterID);
        if (err != 0) return false;
        float exp = 0.0f;
        err = filterElement->QueryFloatAttribute("exposureTime", &exp);
        if (err != 0) return false;
        exposureTimes_.push_back(exp);
        filterIds_.push_back(filterID);
        filterElement = filterElement->NextSiblingElement("filter");
    }
    return true;
}

void AMReadExStep::run() {
    runRead();
}


void AMReadExStep::runRead() {
    PamStation *pPamStation = PamStationFactory::getCurrentPamStation();
    CCD *pCCD = pPamStation->getCCD();
    Disposable *pDisposable = pPamStation->getDisposable();
    FilterPositioner *pFilterPositioner = pPamStation->getFilterPositioner();
    FocusPositioner *pFocusPositioner = pPamStation->getFocusPositioner();
    LEDUnit *pLED1 = pPamStation->getLEDUnit((ELEDNo) 1);
    LEDUnit *pLED2 = pPamStation->getLEDUnit((ELEDNo) 2);
    LEDUnit *pLED3 = pPamStation->getLEDUnit((ELEDNo) 3);
    WagonXYPositioner *pWagonXYPositioner = pPamStation->getWagonXYPositioner();

    int iLedDelayTime;


    try   // tryI
    {
        //if FilterPositioner or FocusPositioner is not homed, home both
        if (!(pFilterPositioner->getHomed() && pFocusPositioner->getHomed())) {
            pFilterPositioner->goHome();
            pFocusPositioner->goHome();

            pFilterPositioner->waitReady();
            pFocusPositioner->waitReady();
        }

//        pDisposable->getPumpUpSemaphore();

        //for each selected well
        for (; currentWell < eWellNoMax &&
               !(protocol->isAborting()); currentWell++)   //loop for all wells, i: 1,2,3,4,5,etc
        {
            if ((1 << (currentWell - 1)) &
                protocol->wellBitSet())         //check if well is selected, 1<<(i-1): 1,2,4,8,16,etc
            {
                //place selected well under camera
                pWagonXYPositioner->goReadPosition((EWellNo) currentWell);
                pWagonXYPositioner->waitReady();

                //for (  ; currentFilter < eFilterNoMax && !(protocol->isAborting()); currentFilter ++)   //loop for all filters, j: 1,2,3,etc
                for (; currentFilterExposureTime < filterIds_.size() &&
                       !(protocol->isAborting()); currentFilterExposureTime++) {
                    unsigned currentFilter = filterIds_[currentFilterExposureTime];
                    //positions selected filter
                    pFilterPositioner->goPositionForFilter((EFilterNo) currentFilter);
                    pFilterPositioner->waitReady();  //throws PamStationTimeoutException

                    //set in focus for selected Filter & Well
                    pFocusPositioner->goPositionForFilterAndWell((EFilterNo) currentFilter, (EWellNo) currentWell);
                    pFocusPositioner->waitReady();

                    // make sure the camera is enabled.
                    if (!pCCD->getEnabled()) {
                        pCCD->setEnabled(true);
                    }
                    //make sure all LED's are off
                    pLED1->setSwitchStatus(false);
                    pLED2->setSwitchStatus(false);
                    pLED3->setSwitchStatus(false);

                    //switch led for selected filter
                    //get LEDUnit belonging to selected filter (in fact pLEd1, pLEd2 or pLed3)
                    LEDUnit *pSelectedLED = pPamStation->getLEDUnit((ELEDNo) currentFilter);
                    pSelectedLED->setSwitchStatus(true); //switch LED on
                    //- JPau added 20090403 for getting the waittime for the ledunit
                    iLedDelayTime = pSelectedLED->getLedWaitTime();

                    if (iLedDelayTime > 1000) iLedDelayTime = 10;

                    //- JPau added 20090403 for wait time after setting led on
                    Util::Sleep(iLedDelayTime);

                    pCCD->setExposureTime(exposureTimes_[currentFilterExposureTime]);
                    pCCD->setImageInfo((EWellNo) currentWell, (EFilterNo) currentFilter);
                    Image *pImage = pCCD->acquireImage();
                    saveImage(pImage);
                    delete pImage;
                    pSelectedLED->setSwitchStatus(false);
                } // end for-loop filter
                currentFilterExposureTime = 0;
            } // end if       well
        } // end for-loop well

//        pDisposable->releasePumpUpSemaphore();

        //disable controllers
        pFilterPositioner->setEnabled(false);
        pFocusPositioner->setEnabled(false);

        // Alex we keep the camera enabled
        //pCCD              ->setEnabled(false);

        pWagonXYPositioner->setEnabled(false);

        //make sure all LED's are off
        pLED1->setSwitchStatus(false);
        pLED2->setSwitchStatus(false);
        pLED3->setSwitchStatus(false);

    }    //end tryI
    catch (...) {
//        pDisposable->releasePumpUpSemaphore();

        //disable controllers
        pFilterPositioner->setEnabled(false);
        pFocusPositioner->setEnabled(false);

        // Alex we keep the camera enabled
        //pCCD              ->setEnabled(false);

        pWagonXYPositioner->setEnabled(false);

        //make sure all LED's are off
        pLED1->setSwitchStatus(false);
        pLED2->setSwitchStatus(false);
        pLED3->setSwitchStatus(false);

        throw;
    }
}
