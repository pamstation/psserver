#ifndef AMManualDispenseStepH
#define AMManualDispenseStepH

#include "Protocol.h"
#include "AMStep.h"
#include "modules/pimcore/PamDefs.h"

#include <string>
#include "tinyxml2.h"


class Protocol;

class AMManualDispenseStep : virtual public AMStep
{
public:

    AMManualDispenseStep(Protocol * aP);
    ~AMManualDispenseStep();
    virtual std::string name();
    virtual bool initializeFromXMLElement(tinyxml2::XMLElement* aStepElement);
    virtual void appendXML(std::stringstream * st);
    virtual void run();
     virtual void runEject();


private:
    Protocol * protocol;
    std::string name_;

};

#endif //AMManualDispenseStepH


