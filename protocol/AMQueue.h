#ifndef AMQueueH
#define AMQueueH

#include <mutex>
#include <vector>
#include "AMProtocolMsg.h"

//using namespace std;

//class AMProtocolMsg;

class AMQueue
{

public:
    AMQueue();
    ~AMQueue();

    virtual void add(AMProtocolMsg * msg);
    virtual void getMsgs(std::string * out);
    virtual void getMsgs(int i, std::string * out);
    virtual void removeAll();


private:
//    TCriticalSection * criticalSection;
    std::recursive_mutex mutex;
    std::vector<AMProtocolMsg*> * msgQueue_;

};

#endif //AMQueueH
