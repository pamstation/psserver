
#ifndef AMAspirateStepH
#define AMAspirateStepH

#include "Protocol.h"
#include "AMStep.h"
#include "modules/pimcore/PamDefs.h"

#include <string>
#include "tinyxml2.h"


class Protocol;

class AMAspirateStep : virtual public AMStep
{
public:

    AMAspirateStep(Protocol * aP);
    ~AMAspirateStep();
    virtual std::string name();
    virtual bool initializeFromXMLElement(tinyxml2::XMLElement* aStepElement);
    virtual void appendXML(std::stringstream * st);
    virtual void run();


private:
    Protocol * protocol;
    std::string name_;
    unsigned currentWell;
    unsigned aspirateTime;

};

#endif //AMAspirateStepH

