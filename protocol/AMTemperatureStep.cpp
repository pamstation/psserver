

//---------------------------------------------------------------------------
#pragma hdrstop

#include "AMTemperatureStep.h"

#include "modules/pimcore/PamStationFactory.h"   //for obtaining & using PamStation
#include "modules/pimcore/PamStation.h"          //for obtaining & using PamStation
#include "modules/pimcore/WagonHeater.h"         //for obtaining & using object
#include "modules/pimcore/WagonXYPositioner.h"   //for obtaining & using object

#pragma package(smart_init)


//---------------------------------------------------------------------------

AMTemperatureStep::AMTemperatureStep(Protocol * aP)
{
    protocol = aP;
    name_ = "temperatureStep";
    temperature = 30;
    //gain = PamStationFactory::getCurrentPamStation()->getWagonHeater()->getMaxTemperatureGain();
    //from Evolve default
    gain = 8.0;
}
AMTemperatureStep::~AMTemperatureStep() {}

string AMTemperatureStep::name()
{
    return name_;
}

void AMTemperatureStep::appendXML(std::stringstream * st)
{
    *st << "<step";

    *st << " name=\"";
    *st <<  name_;
    *st << "\"";

    *st << " temperature=\"";
    *st <<  temperature;
    *st << "\"";

    *st << " gain=\"";
    *st <<  gain;
    *st << "\"";

    *st << "/>";
}

bool AMTemperatureStep::initializeFromXMLElement(tinyxml2::XMLElement* aStepElement)
{
    aStepElement->QueryFloatAttribute("temperature", &temperature );
    aStepElement->QueryFloatAttribute("gain", &gain );
    return true;
}


void AMTemperatureStep::run()
{
    PamStation       * pPamStation        = PamStationFactory::getCurrentPamStation();
    WagonHeater      * pWagonHeater       = pPamStation->getWagonHeater();
    WagonXYPositioner* pWagonXYPositioner = pPamStation->getWagonXYPositioner();



    //move Wagon to IncubationPosition (= only position for proper cooling)
    pWagonXYPositioner->goIncubationPosition();

    //TempGain is no parameter any longer, but given a fixed value in Evolve
    //To avoid unnecessary errors, but still allow for the parameter, top the parameter to the maximum = default tempgain
    if ( gain > pWagonHeater->getMaxTemperatureGain() )  gain = pWagonHeater->getMaxTemperatureGain();

    pWagonHeater->setTemperatureGain(gain       );
    pWagonHeater->setTargetValue    (temperature);

    //stop wagonPositioner control when wagon is in position
    pWagonXYPositioner->waitReady();
    pWagonXYPositioner->setEnabled(false);

    pWagonHeater->waitReady();
}

