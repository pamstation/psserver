//---------------------------------------------------------------------------

#pragma hdrstop

#include "AMQueue.h"
#include <sstream>

#pragma package(smart_init)


//---------------------------------------------------------------------------

AMQueue::AMQueue() {
    msgQueue_ = new std::vector<AMProtocolMsg *>;
}

AMQueue::~AMQueue() {
    removeAll();
    delete msgQueue_;
}

void AMQueue::removeAll() {

    std::lock_guard<std::recursive_mutex> guard(mutex);
    for (unsigned i = 0; i < msgQueue_->size(); i++) {
        AMProtocolMsg *msg = (*msgQueue_)[i];
        delete msg;
    }
    msgQueue_->clear();

}

void AMQueue::add(AMProtocolMsg *msg) {
    std::lock_guard<std::recursive_mutex> guard(mutex);
    msgQueue_->push_back(msg);

}

void AMQueue::getMsgs(std::string *out) {
    std::lock_guard<std::recursive_mutex> guard(mutex);

    out->append("<?xml version=\"1.0\"?>");
    out->append("<list>");

    for (unsigned i = 0; i < msgQueue_->size(); i++) {
        out->append((*msgQueue_)[i]->msg);
    }
    out->append("</list>");
}

void AMQueue::getMsgs(int j, std::string *out) {
    std::lock_guard<std::recursive_mutex> guard(mutex);

    out->append("<?xml version=\"1.0\"?>");
    out->append("<list>");

    for (unsigned i = j; i < msgQueue_->size(); i++) {
        out->append((*msgQueue_)[i]->msg);
    }
    out->append("</list>");
}


