#ifndef AMExceptionStepH
#define AMExceptionStepH

#include "Protocol.h"
#include "AMStep.h"

#include <string>
#include "tinyxml2.h"


class Protocol;

class AMExceptionStep : virtual public AMStep
{
public:

    AMExceptionStep(Protocol * aP);
    ~AMExceptionStep();
    virtual std::string name();
    virtual bool initializeFromXMLElement(tinyxml2::XMLElement* aStepElement);
    virtual void appendXML(std::stringstream * st);
    virtual void run();


private:
    int numTry;
    Protocol * protocol;
    std::string name_;

};

#endif //AMExceptionStepH

