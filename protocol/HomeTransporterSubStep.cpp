//---------------------------------------------------------------------------
#pragma hdrstop

#include "HomeTransporterSubStep.h"    //for header-file

#include "modules/pimcore/PamStationFactory.h"   //for obtaining & using PamStation
#include "modules/pimcore/PamStation.h"          //for obtaining & using PamStation
#include "modules/pimcore/UnloadButton.h"        //for obtaining & using needed device
#include "modules/pimcore/WagonXYPositioner.h"   //for obtaining & using needed device

#pragma package(smart_init)
//---------------------------------------------------------------------------
HomeTransporterSubStep:: HomeTransporterSubStep() {}
HomeTransporterSubStep::~HomeTransporterSubStep() {}

void HomeTransporterSubStep::run()
{
    PamStation       * pPamStation        = PamStationFactory::getCurrentPamStation();
    UnloadButton     * pUnloadButton      = pPamStation->getUnloadButton();
    WagonXYPositioner* pWagonXYPositioner = pPamStation->getWagonXYPositioner();


    pUnloadButton->setEnabled(true);   //button is pressable
    pUnloadButton->waitPressedEvent(); //now waiting for press-command, does not timeout!
    //on this point the button is pressed
    pUnloadButton->setEnabled(false);  //disable button

    if ( ! pWagonXYPositioner->getHomed() )  //only when necessary
    {
        pWagonXYPositioner->goHome();
        pWagonXYPositioner->waitReady();
        //at this point wagon reached home, and encoders are resetted
    }

    //FrontPanelPosition is nearly home
    //going to this position gives smoother moves without chance of touching the casing
    pWagonXYPositioner->goFrontPanelPosition();
    pWagonXYPositioner->waitReady();

}
