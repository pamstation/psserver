//---------------------------------------------------------------------------
#pragma hdrstop

#include "AMExceptionStep.h"
#include "modules/systemlayer/SysPamStationException.h"

#pragma package(smart_init)


//---------------------------------------------------------------------------

AMExceptionStep::AMExceptionStep(Protocol * aP)
{
    numTry = 1;
    protocol = aP;
    name_ = "exceptionStep";
}
AMExceptionStep::~AMExceptionStep() {}

string AMExceptionStep::name()
{
    return name_;
}

void AMExceptionStep::appendXML(std::stringstream * st)
{
    *st << "<step name=\"";
    *st <<  name_;
    *st << "\"/>";
}

bool AMExceptionStep::initializeFromXMLElement(tinyxml2::XMLElement* aStepElement)
{

    return true;
}


void AMExceptionStep::run()
{
    protocol->addMsg(new AMProtocolMsg(0, "<runExceptionStep/>"));
    if (numTry > 0)
    {
        numTry--;
        SysPamStationException * e = new SysPamStationException(0, "exceptionStep", 0) ;
        throw e;
    }

}

