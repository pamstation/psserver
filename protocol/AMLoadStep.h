#ifndef AMLoadStepH
#define AMLoadStepH

#include "Protocol.h"
#include "AMStep.h"

#include <string>
#include "tinyxml2.h"


class Protocol;

class AMLoadStep : virtual public AMStep
{
public:

    AMLoadStep(Protocol * aP);
    ~AMLoadStep();
    virtual std::string name();
    virtual bool initializeFromXMLElement(tinyxml2::XMLElement* aStepElement);
    virtual void appendXML(std::stringstream * st);
    virtual void run();
    virtual void runEject();



private:
    Protocol * protocol;
    std::string name_;
    float overPressure_;
    float underPressure_;
    float temperature_;
    bool coverNotClosedException;
    bool disposableNotLoadedException;

};

#endif //AMLoadStepH
