#ifndef AMPumpingStepH
#define AMPumpingStepH

#include "Protocol.h"
#include "AMReadExStep.h"
#include "AMReadByCycleExStep.h"

#include <string>
#include <vector>
#include "tinyxml2.h"

class Protocol;
class Image;
class AMReadByCycleExStep;

class AMPumpingStep : virtual public AMReadByCycleExStep
{
public:

    AMPumpingStep(Protocol * aP);

    virtual bool initializeFromXMLElement(tinyxml2::XMLElement* aStepElement);
    virtual void appendXML(std::stringstream * st);
    virtual void runRead();

};

#endif //AMPumpingStepH



