#ifndef AMQuickWashStepH
#define AMQuickWashStepH

#include "Protocol.h"
#include "AMStep.h"
#include "AMDispenseStep.h"
#include "AMPumpingStep.h"
#include "AMAspirateQuickStep.h"

#include "modules/pimcore/PamDefs.h"

#include <string>
#include "tinyxml2.h"



class Protocol;
class AMPumpStep;
class AMAspirateQuickStep;

#include "AMManualDispenseStep.h"

class AMQuickWashStep : virtual public AMStep
{
public:

    AMQuickWashStep(Protocol * aP);
    ~AMQuickWashStep();
    virtual std::string name();
    virtual bool initializeFromXMLElement(tinyxml2::XMLElement* aStepElement);
    virtual void appendXML(std::stringstream * st);
    virtual void run();


private:
    Protocol * protocol;
    std::string name_;
    unsigned currentStepIndex;
    AMStep * dispenseStep;
    AMPumpingStep * pumpStep;
    AMAspirateQuickStep * aspirateStep;


};

#endif //AMQuickWashStepH



