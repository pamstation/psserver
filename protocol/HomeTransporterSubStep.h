//
// Created by alex on 20/03/19.
//

#ifndef PSSERVER_HOMETRANSPORTERSUBSTEP_H
#define PSSERVER_HOMETRANSPORTERSUBSTEP_H


class HomeTransporterSubStep {
public:
    HomeTransporterSubStep(); ///<no parameters
    ~HomeTransporterSubStep();

    virtual void run();
};


#endif //PSSERVER_HOMETRANSPORTERSUBSTEP_H
