

//---------------------------------------------------------------------------
#pragma hdrstop

#include "AMAspirateStep.h"                      //for header-file

#include "modules/pimcore/PamStationFactory.h"      //for obtaining & using PamStation
#include "modules/pimcore/PamStation.h"             //for obtaining & using PamStation
#include "modules/pimcore/AspirateHeadPositioner.h" //for obtaining & using object
#include "modules/pimcore/AspiratePump.h"           //for obtaining & using object
#include "modules/pimcore/DefaultGenerator.h"       //for obtaining & using object
#include "modules/pimcore/Disposable.h"             //for obtaining & using object
#include "modules/pimcore/WagonXYPositioner.h"      //for obtaining & using object

#include "modules/utils/util.h"

#pragma package(smart_init)


//---------------------------------------------------------------------------

AMAspirateStep::AMAspirateStep(Protocol *aP) {
    protocol = aP;
    name_ = "aspirateStep";
    currentWell = eWell1;
    aspirateTime = 10000;
}

AMAspirateStep::~AMAspirateStep() {}

string AMAspirateStep::name() {
    return name_;
}

void AMAspirateStep::appendXML(std::stringstream *st) {
    *st << "<step";

    *st << " name=\"";
    *st << name_;
    *st << "\"";

    *st << " aspirateTime=\"";
    *st << aspirateTime;
    *st << "\"";

    *st << "/>";
}

bool AMAspirateStep::initializeFromXMLElement(tinyxml2::XMLElement *aStepElement) {
    aStepElement->QueryUnsignedAttribute("aspirateTime", &aspirateTime);
    return true;
}


void AMAspirateStep::run() {
    PamStation *pPamStation = PamStationFactory::getCurrentPamStation();
    AspirateHeadPositioner *pAspirateHeadPositioner = pPamStation->getAspirateHeadPositioner();
    AspiratePump *pAspiratePump = pPamStation->getAspiratePump();
    DefaultGenerator *pDefaultGenerator = pPamStation->getDefaultGenerator();
    Disposable *pDisposable = pPamStation->getDisposable();
    WagonXYPositioner *pWagonXYPositioner = pPamStation->getWagonXYPositioner();


    unsigned uAspPressBuildupDelay = pDefaultGenerator->getAspirationDefaults()->uAspPressBuildupDelay;
    unsigned uAspirateTubeCleanDelay = pDefaultGenerator->getAspirationDefaults()->uAspirateTubeCleanDelay;

    try  //tryI: make sure that finally AspiratePump is switched off, and AspirateHeadPositioner control is stopped
    {
        //switch AspiratePump on
        pAspiratePump->setSwitchStatus(true);
        Util::Sleep(uAspPressBuildupDelay);                           //allow for some time to build up pressure

        //for each selected well
        for (; currentWell < eWellNoMax &&
               !(protocol->isAborting()); currentWell++)               //loop for all wells, i: 1,2,3,4,5,etc
        {
            if ((1 << (currentWell - 1)) &
                protocol->wellBitSet())                     //check if well is selected, 1<<(i-1): 1,2,4,8,16,etc
            {
                //place selected well under AspirateHead
                pWagonXYPositioner->goAspiratePosition((EWellNo) currentWell);
                pWagonXYPositioner->waitReady();

                //make sure the fluid in the selected well is (still) down
//                pDisposable->pumpDown(
//                        (1 << (currentWell - 1)));              // (1 << (i-1)) because a WellBitSet is expected
//                pDisposable->waitPumpReady();

                pDisposable->pumpDown((1 << (currentWell - 1)));


                if (0 == protocol->checkBrokenMembranes((1 << (currentWell - 1)))) {
                    //make sure AspiratePressure is still in range
                    pAspiratePump->checkAspiratePressureOk();

                    try  //tryII: make sure that finally AspirateHead goes back Up
                    {
                        //place AspirateHead in selected Well
                        pAspirateHeadPositioner->setAspirateHeadPosition(eDown);
                        pAspirateHeadPositioner->waitReady();

                        //pump fluid-to-aspirate up in selected well
                        pDisposable->pumpUp(
                                (1 << (currentWell - 1)));              // (1 << (i-1)) because a WellBitSet is expected
//                        pDisposable->waitPumpReady();

//                        pDisposable->pumpUpAsync((1 << (currentWell - 1))).get();


                        if (0 == protocol->checkBrokenMembranes((1 << (currentWell - 1)))) {
                            //allow for some time to aspirate the fluid
                            Util::Sleep(aspirateTime);
                        }

                        //bring AspirateHead -always- up
                        pAspirateHeadPositioner->setAspirateHeadPosition(eUp);
                        pAspirateHeadPositioner->waitReady();

                        //allow for some time to empty the aspirate tubes
                        Util::Sleep(uAspirateTubeCleanDelay);

                    }

                    catch (...) {
                        //bring AspirateHead -always- up
                        pAspirateHeadPositioner->setAspirateHeadPosition(eUp);
                        pAspirateHeadPositioner->waitReady();

                        //allow for some time to empty the aspirate tubes
                        Util::Sleep(uAspirateTubeCleanDelay);

                        throw;
                    }
                }

            } // end if
        }   // end for

        //stop -always- AspiratePump
        pAspiratePump->setSwitchStatus(false);

        //stop -always- control
        pAspirateHeadPositioner->setEnabled(false);

        //stop wagonPositioner control
        pWagonXYPositioner->setEnabled(false);

    }

    catch (...) {

        //stop -always- AspiratePump
        pAspiratePump->setSwitchStatus(false);

        //stop -always- control
        pAspirateHeadPositioner->setEnabled(false);

        //stop wagonPositioner control
        pWagonXYPositioner->setEnabled(false);

        throw;
    }


}