#ifndef AMWashStepH
#define AMWashStepH

#include "Protocol.h"
#include "AMStep.h"
#include "AMDispenseStep.h"
#include "AMPumpingStep.h"
#include "AMAspirateStep.h"

#include "modules/pimcore/PamDefs.h"

#include <string>
#include "tinyxml2.h"


class Protocol;
 
class AMPumpStep;
class AMAspirateStep;

class AMWashStep : virtual public AMStep
{
public:

    AMWashStep(Protocol * aP);
    ~AMWashStep();
    virtual std::string name();
    virtual bool initializeFromXMLElement(tinyxml2::XMLElement* aStepElement);
    virtual void appendXML(std::stringstream * st);
    virtual void run();


private:
    Protocol * protocol;
    std::string name_;
    unsigned currentStepIndex;
    AMStep * dispenseStep;
    AMPumpingStep  * pumpStep;
    AMAspirateStep * aspirateStep;


};

#endif //AMWashStepH


