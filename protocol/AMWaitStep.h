
#ifndef AMWaitStepH
#define AMWaitStepH

#include "Protocol.h"
#include "AMStep.h"

#include <string>
#include "tinyxml2.h"


class Protocol;

class AMWaitStep : virtual public AMStep
{
public:

    AMWaitStep(Protocol * aP);
    ~AMWaitStep();
    virtual std::string name();
    virtual bool initializeFromXMLElement(tinyxml2::XMLElement* aStepElement);
    virtual void appendXML(std::stringstream * st);
    virtual void run();


private:
    Protocol * protocol;
    std::string name_;
    unsigned duration   ;
};

#endif //AMWaitStepH




