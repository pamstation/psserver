

//---------------------------------------------------------------------------
#pragma hdrstop

#include "AMAspirateQuickStep.h"                      //for header-file

#include "modules/pimcore/PamStationFactory.h"      //for obtaining & using PamStation
#include "modules/pimcore/PamStation.h"             //for obtaining & using PamStation
#include "modules/pimcore/AspirateHeadPositioner.h" //for obtaining & using object
#include "modules/pimcore/AspiratePump.h"           //for obtaining & using object
#include "modules/pimcore/DefaultGenerator.h"       //for obtaining & using object
#include "modules/pimcore/Disposable.h"             //for obtaining & using object
#include "modules/pimcore/WagonXYPositioner.h"      //for obtaining & using object

#include "modules/utils/util.h"


#pragma package(smart_init)


//---------------------------------------------------------------------------

AMAspirateQuickStep::AMAspirateQuickStep(Protocol *aP) {
    protocol = aP;
    name_ = "aspirateQuickStep";
    currentWell = eWell1;
    aspirateTime = 5000;
    pumpUpTime = 1000;
}

AMAspirateQuickStep::~AMAspirateQuickStep() {}

string AMAspirateQuickStep::name() {
    return name_;
}

void AMAspirateQuickStep::appendXML(std::stringstream *st) {
    *st << "<step";

    *st << " name=\"";
    *st << name_;
    *st << "\"";

    *st << " aspirateTime=\"";
    *st << aspirateTime;
    *st << "\"";

    *st << " pumpUpTime=\"";
    *st << pumpUpTime;
    *st << "\"";

    *st << "/>";
}

bool AMAspirateQuickStep::initializeFromXMLElement(tinyxml2::XMLElement *aStepElement) {
    aStepElement->QueryUnsignedAttribute("aspirateTime", &aspirateTime);
    aStepElement->QueryUnsignedAttribute("pumpUpTime", &pumpUpTime);
    return true;
}


void AMAspirateQuickStep::run() {
    PamStation *pPamStation = PamStationFactory::getCurrentPamStation();
    AspirateHeadPositioner *pAspirateHeadPositioner = pPamStation->getAspirateHeadPositioner();
    AspiratePump *pAspiratePump = pPamStation->getAspiratePump();
    DefaultGenerator *pDefaultGenerator = pPamStation->getDefaultGenerator();
    Disposable *pDisposable = pPamStation->getDisposable();
    WagonXYPositioner *pWagonXYPositioner = pPamStation->getWagonXYPositioner();


    unsigned uAspPressBuildupDelay = pDefaultGenerator->getAspirationDefaults()->uAspPressBuildupDelay;
    unsigned uAspirateTubeCleanDelay = pDefaultGenerator->getAspirationDefaults()->uAspirateTubeCleanDelay;

    try  //tryI: make sure that finally AspiratePump is switched off, and AspirateHeadPositioner control is stopped
    {
        //switch AspiratePump on
        pAspiratePump->setSwitchStatus(true);
        Util::Sleep(uAspPressBuildupDelay);                           //allow for some time to build up pressure

        //pump fluid-to-aspirate up in all selected wells
        pDisposable->pumpUp(protocol->wellBitSet(), (signed) pumpUpTime);                // All selected wells
//        pDisposable->waitPumpReady();

//        pDisposable->pumpUpAsync(protocol->wellBitSet()).get();

        protocol->checkBrokenMembranes();  // All selected wells


        //for each selected well
        for (; currentWell < eWellNoMax &&
               !(protocol->isAborting()); currentWell++)               //loop for all wells, i: 1,2,3,4,5,etc
        {
            if ((1 << (currentWell - 1)) &
                protocol->wellBitSet())                     //check if well is selected, 1<<(i-1): 1,2,4,8,16,etc
            {
                //place selected well under AspirateHead
                pWagonXYPositioner->goAspiratePosition((EWellNo) currentWell);
                pWagonXYPositioner->waitReady();

                //make sure AspiratePressure is still in range
                pAspiratePump->checkAspiratePressureOk();

                try  //tryII: make sure that finally AspirateHead goes back Up
                {
                    //place AspirateHead in selected Well
                    pAspirateHeadPositioner->setAspirateHeadPosition(eDown);
                    pAspirateHeadPositioner->waitReady();

                    //allow for some time to aspirate the fluid
                    Util::Sleep(aspirateTime);

                    //bring AspirateHead -always- up
                    pAspirateHeadPositioner->setAspirateHeadPosition(eUp);
                    pAspirateHeadPositioner->waitReady();
                }

                catch (...) {
                    try {
                        //bring AspirateHead -always- up
                        pAspirateHeadPositioner->setAspirateHeadPosition(eUp);
                        pAspirateHeadPositioner->waitReady();
                    } catch (...) {}

                    throw;
                }
            } // end if
        }   // end for

        //allow for some time to empty the aspirate tubes
        Util::Sleep(uAspirateTubeCleanDelay);

        //stop -always- AspiratePump
        pAspiratePump->setSwitchStatus(false);

        //stop -always- control
        pAspirateHeadPositioner->setEnabled(false);

        //stop wagonPositioner control
        pWagonXYPositioner->setEnabled(false);

    }

    catch (...) {

        try {
            //stop -always- AspiratePump
            pAspiratePump->setSwitchStatus(false);

            //stop -always- control
            pAspirateHeadPositioner->setEnabled(false);

            //stop wagonPositioner control
            pWagonXYPositioner->setEnabled(false);
        } catch (...) {

        }


        throw;
    }


}
