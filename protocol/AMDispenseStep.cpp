
//---------------------------------------------------------------------------
#pragma hdrstop

#include "AMDispenseStep.h"                      //for header-file

#include "modules/systemlayer/SysExceptionGenerator.h"      //for passing Exception to ExceptionGenerator
#include "modules/pimcore/ExceptionGenerator.h"
#include "modules/systemlayer/SysExceptionGenerator.h"

#include "modules/pimcore/PamStationFactory.h"      //for obtaining & using PamStation
#include "modules/pimcore/PamStation.h"             //for obtaining & using PamStation
#include "modules/pimcore/DefaultGenerator.h"       //for obtaining DispenseDefaults
#include "modules/pimcore/DispenseHeadHeater.h"     //for obtaining & using object
#include "modules/pimcore/DispenseHeadPositioner.h" //for obtaining & using object
#include "modules/pimcore/Disposable.h"             //for obtaining & using object
#include "modules/pimcore/WagonXYPositioner.h"      //for obtaining & using object
#include "modules/utils/util.h"


#pragma package(smart_init)


//---------------------------------------------------------------------------

AMDispenseStep::AMDispenseStep(Protocol *aP) {
    protocol = aP;
    name_ = "dispenseStep";
    currentWell = eWell1;
    dispenseHead = eHead1;
    amount = 0;
    temperature = 30;
    temperatureControl = true;
    waitTimeAfterLastWellDispensed = 1000;
    syringeNotPresentException = false;
}

AMDispenseStep::~AMDispenseStep() {}

string AMDispenseStep::name() {
    return name_;
}

void AMDispenseStep::appendXML(std::stringstream *st) {
    *st << "<step";

    *st << " name=\"";
    *st << name_;
    *st << "\"";

    *st << " dispenseHead=\"";
    *st << dispenseHead;
    *st << "\"";

    *st << " amount=\"";
    *st << amount;
    *st << "\"";

    *st << " temperature=\"";
    *st << temperature;
    *st << "\"";

    *st << " temperatureControl=\"";
    *st << temperatureControl;
    *st << "\"";

    *st << " waitTimeAfterLastWellDispensed=\"";
    *st << waitTimeAfterLastWellDispensed;
    *st << "\"";

    *st << " syringeNotPresentException=\"";
    *st << syringeNotPresentException;
    *st << "\"";

    *st << "/>";
}

bool AMDispenseStep::initializeFromXMLElement(tinyxml2::XMLElement *aStepElement) {
    unsigned aDHead = 0;
    aStepElement->QueryUnsignedAttribute("dispenseHead", &aDHead);
    dispenseHead = (EDispenseHeadNo) (aDHead);
    aStepElement->QueryFloatAttribute("amount", &amount);
    aStepElement->QueryFloatAttribute("temperature", &temperature);
    aStepElement->QueryBoolAttribute("temperatureControl", &temperatureControl);
    aStepElement->QueryFloatAttribute("waitTimeAfterLastWellDispensed", &waitTimeAfterLastWellDispensed);
    aStepElement->QueryBoolAttribute("syringeNotPresentException", &syringeNotPresentException);
    return true;
}


void AMDispenseStep::run() {
    PamStation *pPamStation = PamStationFactory::getCurrentPamStation();
    DefaultGenerator *pDefaultGenerator = pPamStation->getDefaultGenerator();
    DispenseHeadHeater *pDispenseHeadHeater = pPamStation->getDispenseHeadHeater();
    DispenseHeadPositioner *pDispenseHeadPositioner = pPamStation->getDispenseHeadPositioner(dispenseHead);
    Disposable *pDisposable = pPamStation->getDisposable();
    WagonXYPositioner *pWagonXYPositioner = pPamStation->getWagonXYPositioner();

    pDispenseHeadPositioner->checkDispenseHeadPresent();  //thows DISPENSEHEAD_NOT_PRESENT

    try   //tryI:make sure that finally DispenseHead-Position & -Temperature control are stopped
    {
        //get default values :
        float rRetractAmount = pDefaultGenerator->getDispenseDefaults()->rRetractAmount;
        unsigned uRetractDelay = pDefaultGenerator->getDispenseDefaults()->uRetractDelay;
        unsigned uHeatableHeadBitSet = pDefaultGenerator->getDispenseDefaults()->uHeatableHeadBitSet;

        if (syringeNotPresentException) {
            syringeNotPresentException = false;
            SysExceptionGenerator *excptionGenerator = dynamic_cast<SysExceptionGenerator *>( pPamStation->getExceptionGenerator());
            excptionGenerator->onException(this, DISPENSEHEAD_NOT_PRESENT, eErrorUnknown, "SYRINGE NOT PRESENT");
        }

        //make sure sign  rRetractAmount is positive (see use of this rRetractAmount)
        if (rRetractAmount < 0) rRetractAmount = -rRetractAmount;

        //HEATING OF DISPENSEFLUID
        if (1 << (dispenseHead - 1) & uHeatableHeadBitSet) //check if Head is selected, 1<<(m_eHead-1): 1,2,4,8
        {
            //move to incubate position to prevent sample-evaporation during fluid heating
            pWagonXYPositioner->goIncubationPosition();
            pWagonXYPositioner->waitReady();

            if (temperatureControl) {
                //set fluid temperature
                pDispenseHeadHeater->setTargetValue(temperature);
                pDispenseHeadHeater->getTargetValue();
                pDispenseHeadHeater->waitReady();
            }
        }


        //DISPENSING OF FLUID
        //for each selected well

        for (; currentWell < eWellNoMax &&
               !(protocol->isAborting()); currentWell++)   //loop for all wells, i: 1,2,3,4,5,etc
        {
            if ((1 << (currentWell - 1)) &
                protocol->wellBitSet())         //check if well is selected, 1<<(i-1): 1,2,4,8,16,etc
            {
                //place selected well under selected DispenseHead
                pWagonXYPositioner->goDispensePosition((EWellNo) currentWell, dispenseHead);
                pWagonXYPositioner->waitReady();

                //dispense fluid (+ extra retract-amount)
                pDispenseHeadPositioner->setTargetValue(amount + rRetractAmount);
                pDispenseHeadPositioner->waitReady();

                //wait some time to prevent droplet-forming
                Util::Sleep(uRetractDelay);

                //retract syringe to prevent droplet forming
                pDispenseHeadPositioner->setTargetValue(-rRetractAmount);
                pDispenseHeadPositioner->waitReady();

            }//end if
        }  //end for

        Util::Sleep(waitTimeAfterLastWellDispensed);
        pDisposable->pumpDown(protocol->wellBitSet());
//        pDisposable->waitPumpReady();

//        pDisposable->pumpDownAsync(protocol->wellBitSet()).get();

        protocol->checkBrokenMembranes();

        pDispenseHeadHeater->setEnabled(false);
        pDispenseHeadPositioner->setEnabled(false);

        //stop wagonPositioner control
        pWagonXYPositioner->setEnabled(false);

    }
    catch (...) {

        pDispenseHeadHeater->setEnabled(false);
        pDispenseHeadPositioner->setEnabled(false);

        //stop wagonPositioner control
        pWagonXYPositioner->setEnabled(false);

        throw;
    }

}
