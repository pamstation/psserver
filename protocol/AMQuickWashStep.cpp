//---------------------------------------------------------------------------
#pragma hdrstop

#include "AMQuickWashStep.h"                      //for header-file
#include "Protocol.h"
#include "AMManualDispenseStep.h"
#include "AMDispenseStep.h"

#include "tinyxml2.h"

#pragma package(smart_init)


//---------------------------------------------------------------------------

AMQuickWashStep::AMQuickWashStep(Protocol * aP)
{
    protocol = aP;
    name_ = "washQuickStep";
    currentStepIndex = 0;
    dispenseStep = NULL;
    pumpStep = new AMPumpingStep(aP);
    pumpStep->setIncrCycle(false);
    aspirateStep = new AMAspirateQuickStep(aP);
}
AMQuickWashStep::~AMQuickWashStep() {
    delete pumpStep;
    delete aspirateStep;
    if (dispenseStep) delete dispenseStep;
}

std::string AMQuickWashStep::name()
{
    return name_;
}

void AMQuickWashStep::appendXML(std::stringstream * st)
{
    *st << "<step";

    *st << " name=\"";
    *st <<  name_;
    *st << "\">";

    *st << "<steps>";
    dispenseStep->appendXML(st);
    pumpStep->appendXML(st);
    aspirateStep->appendXML(st);

    *st << "</steps>";

    *st << "</step>";
}

bool AMQuickWashStep::initializeFromXMLElement(tinyxml2::XMLElement* aStepElement)
{
    tinyxml2::XMLElement* stepsElement = aStepElement->FirstChildElement( "steps" );
    if (stepsElement == NULL) return false;
    tinyxml2::XMLElement* stepElement = stepsElement->FirstChildElement("step");
    while (stepElement)
    {
        std::string aStepName = stepElement->Attribute("name");
        if (aStepName.compare("dispenseStep") == 0)
        {
            dispenseStep = new AMDispenseStep(protocol);
            if (!dispenseStep->initializeFromXMLElement(stepElement))return false;
        }
        else if (aStepName.compare("manualDispenseStep") == 0)
        {
            dispenseStep = new AMManualDispenseStep(protocol);
            if (!dispenseStep->initializeFromXMLElement(stepElement))return false;
        }
        else if (aStepName.compare("pumpingStep") == 0)
        {
            if (!pumpStep->initializeFromXMLElement(stepElement))return false;
        }
        else if (aStepName.compare("aspirateQuickStep") == 0)
        {
            if (!aspirateStep->initializeFromXMLElement(stepElement))return false;
        }
        else return false;
        stepElement = stepElement->NextSiblingElement("step");
    }
    if (dispenseStep == NULL) return false;
    return true;
}


void AMQuickWashStep::run()
{
    if (currentStepIndex == 0)
    {
        dispenseStep->run();
        if (!(protocol->isAborting())) currentStepIndex++;
    }
    if (currentStepIndex == 1)
    {
        pumpStep->run();
        if (!(protocol->isAborting())) currentStepIndex++;
    }
    if (currentStepIndex == 2)
    {
        aspirateStep->run();
        if (!(protocol->isAborting())) currentStepIndex++;
    }
}


