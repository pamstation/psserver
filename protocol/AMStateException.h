#ifndef AMStateExceptionH
#define AMStateExceptionH
#include <exception>

class AMStateException: public std::exception
{
    virtual const char* what() const throw()
    {
        return "state error";
    }
};

#endif //AMStateExceptionH
