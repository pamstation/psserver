//---------------------------------------------------------------------------
#pragma hdrstop

#include "AMUnLoadStep.h"                      //for header-file
#include "Protocol.h"
#include "modules/pimcore/PamStationFactory.h"       //for obtaining & using PamStation
#include "modules/pimcore/PamStation.h"              //for obtaining & using PamStation
#include "modules/pimcore/OverPressureSupplier.h"    //for obtaining & using object
#include "modules/pimcore/UnderPressureSupplier.h"   //for obtaining & using object
#include "modules/pimcore/WagonHeater.h"             //for obtaining & using object
#include "modules/pimcore/WagonXYPositioner.h"       //for obtaining & using object
#include "modules/pimcore/UnloadButton.h"
//#include "protocols/EjectSubStep.h"                  //for using EjectSubStep

#include "tinyxml2.h"

#pragma package(smart_init)


//---------------------------------------------------------------------------

AMUnLoadStep::AMUnLoadStep(Protocol *aP) {
    protocol = aP;
    name_ = "unLoadStep";
}

AMUnLoadStep::~AMUnLoadStep() {}

std::string AMUnLoadStep::name() {
    return name_;
}

bool AMUnLoadStep::initializeFromXMLElement(tinyxml2::XMLElement *aStepElement) {
    return true;
}

void AMUnLoadStep::appendXML(std::stringstream *st) {
    *st << "<step name=\"";
    *st << name_;
    *st << "\"/>";
}


void AMUnLoadStep::run() {
    PamStation *pPamStation = PamStationFactory::getCurrentPamStation();
    OverPressureSupplier *pOverPressureSupplier = pPamStation->getOverPressureSupplier();
    UnderPressureSupplier *pUnderPressureSupplier = pPamStation->getUnderPressureSupplier();
    WagonHeater *pWagonHeater = pPamStation->getWagonHeater();
    WagonXYPositioner *pWagonXYPositioner = pPamStation->getWagonXYPositioner();
    UnloadButton *pUnloadButton = pPamStation->getUnloadButton();
//    EjectSubStep           aEjectSubStep          = EjectSubStep();

    //STOP PRESSURE&TEMPERATURE CONTROL INCUBATIONCHAMBER
    //STOP pressure control:
    pOverPressureSupplier->setEnabled(false);
    pUnderPressureSupplier->setEnabled(false);
    //STOP temperature control:
    pWagonHeater->setEnabled(false);


    //eject.run() =
    //1 on button-press: if needed home(), then goFrontPanel, then goLoad()
    //2 on button press: if needed home(), then goFrontPanel
//    aEjectSubStep.run();

    pUnloadButton->setEnabled(true);   //button is pressable
    pUnloadButton->waitPressedEvent(); //now waiting for press-command, does not timeout!
    //on this point the button is pressed
    pUnloadButton->setEnabled(false);  //disable button

    if (!pWagonXYPositioner->getHomed())  //only when necessary
    {
        pWagonXYPositioner->goHome();
        pWagonXYPositioner->waitReady();
        //at this point wagon reached home, and encoders are resetted
    }

    //FrontPanelPosition is nearly home
    //going to this position gives smoother moves without chance of touching the casing
    pWagonXYPositioner->goFrontPanelPosition();
    pWagonXYPositioner->waitReady();

/////////////////////////
//WagonPositioner is now homed & in FrontPanelPosition

    //check if unloadtemperature safe
    pWagonHeater->checkUnloadTemperatureSafe();

    //goto (un)load position
    pWagonXYPositioner->goLoadPosition();
    pWagonXYPositioner->waitReady();

    //wagon is now in (un)load position

    //button will be enabled, and is blocking untill pressed
    //in other words: the wagon will go to home (inside the instrument) on pressing the button
//    aHomeTransporterSubStep.run();  //run again, now from load position

    pUnloadButton->setEnabled(true);   //button is pressable
    pUnloadButton->waitPressedEvent(); //now waiting for press-command, does not timeout!
    //on this point the button is pressed
    pUnloadButton->setEnabled(false);  //disable button

    if (!pWagonXYPositioner->getHomed())  //only when necessary
    {
        pWagonXYPositioner->goHome();
        pWagonXYPositioner->waitReady();
        //at this point wagon reached home, and encoders are resetted
    }

    //FrontPanelPosition is nearly home
    //going to this position gives smoother moves without chance of touching the casing
    pWagonXYPositioner->goFrontPanelPosition();
    pWagonXYPositioner->waitReady();


    pWagonXYPositioner->setEnabled(false);
}

