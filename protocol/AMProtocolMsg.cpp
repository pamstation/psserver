//---------------------------------------------------------------------------
#pragma hdrstop

#include "AMProtocolMsg.h"

#pragma package(smart_init)

AMProtocolMsg::AMProtocolMsg(int aType, std::string aMsg)
{
    type = aType;
    msg = aMsg;
}

AMProtocolMsg::~AMProtocolMsg()
{
}
