#ifndef AMReadExStepH
#define AMReadExStepH

#include "Protocol.h"
#include "AMStep.h"

#include <string>
#include <vector>
#include "tinyxml2.h"


class Protocol;
class Image;

class AMReadExStep : virtual public AMStep
{
public:

    AMReadExStep(Protocol * aP);
    virtual ~AMReadExStep();
    virtual std::string name();
    virtual bool initializeFromXMLElement(tinyxml2::XMLElement* aStepElement);
    virtual void appendXML(std::stringstream * st);
    virtual void run();
    virtual void runRead();

    virtual void saveImage(  Image* pImage);


protected:
    Protocol * protocol;
    std::string name_;
    //unsigned filterBitSet_;
    std::vector<float> exposureTimes_;
    std::vector<float> filterIds_;

    unsigned currentWell;
    unsigned currentFilterExposureTime;

};

#endif //AMReadExStepH

