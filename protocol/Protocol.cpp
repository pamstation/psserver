//---------------------------------------------------------------------------

#pragma hdrstop

#include <vector>
#include <string>
#include <sstream>
#include <random>
//#include windows.h
#include <boost/exception/diagnostic_information.hpp>

#include "Protocol.h"
#include "AMStateException.h"

#include "AMLoadStep.h"
#include "AMUnLoadStep.h"
#include "AMExceptionStep.h"
#include "AMReadExStep.h"
#include "AMReadByCycleExStep.h"
#include "AMPumpingStep.h"
#include "AMDispenseStep.h"
#include "AMAspirateStep.h"
#include "AMAspirateQuickStep.h"
#include "AMPrimeStep.h"
#include "AMWaitStep.h"
#include "AMTemperatureStep.h"
#include "AMWashStep.h"
#include "AMQuickWashStep.h"
#include "AMManualDispenseStep.h"
#include "AMManualAspirateStep.h"


#include "modules/pimcore/PamStationFactory.h"      //for obtaining & using PamStation
#include "modules/pimcore/PamStation.h"             //for obtaining & using PamStation
#include "modules/pimcore/Disposable.h"
#include "modules/propertylayer/PropLeds.h"      //for obtaining & using object
#include "modules/propertylayer/PropCCD.h"

#include "modules/pimcore/PamStationException.h"


#pragma package(smart_init)

unsigned int random_char() {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(0, 255);
    return dis(gen);
}

std::string generate_runId(const unsigned int len) {
    std::stringstream ss;
    for (auto i = 0; i < len; i++) {
        const auto rc = random_char();
        std::stringstream hexstream;
        hexstream << std::hex << rc;
        auto hex = hexstream.str();
        ss << (hex.length() < 2 ? '0' + hex : hex);
    }
    return ss.str();
}

//---------------------------------------------------------------------------
Protocol::Protocol(AMLogger *aLogger) {
    currentStepIndex = 0;
    state_ = pInit;

    barcode1_ = "";
    barcode2_ = "";
    barcode3_ = "";
    id_ = generate_runId(12);
    path_ = "";
    wellBitSet_ = 0;
    cycle_ = 0;
    imageCount_ = 0;

    steps_ = new std::vector<AMStep *>;
    msgQueue_ = new AMQueue();
    logger = aLogger;

//    logger->logInfo("Cannot find registry key for unit name : HKEY_LOCAL_MACHINE\\SOFTWARE\\PamGene\\Evolve");
    unit_ = "";

    addStateMsg();

//    TRegistry *Reg = new TRegistry;
//    Reg->RootKey = HKEY_LOCAL_MACHINE;
//    if (Reg->KeyExists("SOFTWARE\\PamGene\\Evolve")) {
//        try {
//            if (Reg->OpenKey("SOFTWARE\\PamGene\\Evolve", FALSE)) {
//                String value = Reg->ReadString("Unit");
//                unit_ = value.c_str();
//                Reg->CloseKey();
//                //ShowMessage("Found Unit in registry " + value);
//
//            } else {
//                logger->logInfo(
//                        "Cannot find registry key for unit name : HKEY_LOCAL_MACHINE\\SOFTWARE\\PamGene\\Evolve:Unit");
//            }
//            Reg->CloseKey();
//        }
//        catch (ERegistryException &E) {
//            logger->logInfo(
//                    "Failed to open registry key for unit name : HKEY_LOCAL_MACHINE\\SOFTWARE\\PamGene\\Evolve:Unit");
//            unit_ = "";
//        }
//    } else {
//        logger->logInfo("Cannot find registry key for unit name : HKEY_LOCAL_MACHINE\\SOFTWARE\\PamGene\\Evolve");
//        unit_ = "";
//    }
//    delete Reg;
}

Protocol::~Protocol() {
    for (unsigned i = 0; i < steps_->size(); i++) {
        AMStep *aStep = (*steps_)[i];
        delete aStep;
    }
    delete steps_;
    delete msgQueue_;
}

std::string Protocol::getUnit() {
    return unit_;
}

AMStep *Protocol::createStep(tinyxml2::XMLElement *aStepElement) {
    AMStep *aStep = 0;
    std::string aStepName = aStepElement->Attribute("name");
    if (aStepName.compare("loadStep") == 0) aStep = new AMLoadStep(this);
    if (aStepName.compare("unLoadStep") == 0) aStep = new AMUnLoadStep(this);
    if (aStepName.compare("exceptionStep") == 0) aStep = new AMExceptionStep(this);
    if (aStepName.compare("readExStep") == 0) aStep = new AMReadExStep(this);
    if (aStepName.compare("readByCycleExStep") == 0) aStep = new AMReadByCycleExStep(this);
    if (aStepName.compare("pumpingStep") == 0) aStep = new AMPumpingStep(this);
    if (aStepName.compare("dispenseStep") == 0) aStep = new AMDispenseStep(this);
    if (aStepName.compare("aspirateStep") == 0) aStep = new AMAspirateStep(this);
    if (aStepName.compare("aspirateQuickStep") == 0) aStep = new AMAspirateQuickStep(this);
    if (aStepName.compare("primeStep") == 0) aStep = new AMPrimeStep(this);
    if (aStepName.compare("waitStep") == 0) aStep = new AMWaitStep(this);
    if (aStepName.compare("temperatureStep") == 0) aStep = new AMTemperatureStep(this);
    if (aStepName.compare("washStep") == 0) aStep = new AMWashStep(this);
    if (aStepName.compare("washQuickStep") == 0) aStep = new AMQuickWashStep(this);
    if (aStepName.compare("manualDispenseStep") == 0) aStep = new AMManualDispenseStep(this);
    if (aStepName.compare("manualAspirateStep") == 0) aStep = new AMManualAspirateStep(this);


    if (aStep == NULL) return 0;
    if (!aStep->initializeFromXMLElement(aStepElement)) {
        delete aStep;
        return 0;
    }
    return aStep;
}


std::string Protocol::getStateStr() {
    std::stringstream st;
    switch (state_) {
        case pInit: {
            st << "initialized";
        }
            break;
        case pRunning: {
            st << "running";
        }
            break;
        case pAborting: {
            st << "aborting";
        }
            break;
        case pAborted: {
            st << "aborted";
        }
            break;
        case pCompleted: {
            st << "completed";
        }
            break;
    }
    return st.str();
}

void Protocol::appendXML(std::stringstream *st) {

    *st << "<protocol";

    *st << " id=\"";
    *st << id_;
    *st << "\"";

    *st << " path=\"";
    *st << path_;
    *st << "\"";

    *st << " state=\"";
    *st << getStateStr();
    *st << "\"";

    *st << " barcode1=\"";
    *st << barcode1_;
    *st << "\"";

    *st << " barcode2=\"";
    *st << barcode2_;
    *st << "\"";

    *st << " barcode3=\"";
    *st << barcode3_;
    *st << "\"";

    *st << " wellBitSet=\"";
    *st << wellBitSet_;
    *st << "\"";

    *st << " currentStepIndex=\"";
    *st << currentStepIndex;
    *st << "\"";

    *st << " cycle=\"";
    *st << cycle_;
    *st << "\"";

    *st << ">";

    *st << "<steps>";

    for (unsigned i = 0; i < steps_->size(); i++) {
        (*steps_)[i]->appendXML(st);
    }

    *st << "</steps>";
    *st << "</protocol>";
}

std::string Protocol::getXML() {
    std::stringstream st;
    appendXML(&st);
    return st.str();
}

std::vector<AMStep *> *Protocol::steps() {
    return steps_;
}

bool Protocol::initializeFromXML(const char *xml) {
    tinyxml2::XMLDocument doc;
    if (doc.Parse(xml) > 0) return false;
    tinyxml2::XMLElement *protocolElement = doc.FirstChildElement("protocol");
    if (protocolElement == NULL) return false;

    const char *attrV = protocolElement->Attribute("barcode1");
    if (attrV) barcode1_.append(attrV);
    attrV = protocolElement->Attribute("barcode2");
    if (attrV) barcode2_.append(attrV);
    attrV = protocolElement->Attribute("barcode3");
    if (attrV) barcode3_.append(attrV);

    attrV = protocolElement->Attribute("id");
    if (attrV) id_.append(attrV);
    attrV = protocolElement->Attribute("path");
    if (attrV) path_.append(attrV);

    protocolElement->QueryUnsignedAttribute("wellBitSet", &wellBitSet_);

    tinyxml2::XMLElement *stepsElement = protocolElement->FirstChildElement("steps");
    if (stepsElement == NULL) return true;
    tinyxml2::XMLElement *stepElement = stepsElement->FirstChildElement("step");
    while (stepElement) {
        AMStep *aStep = createStep(stepElement);
        if (aStep) steps_->push_back(aStep);
        else return false;
        stepElement = stepElement->NextSiblingElement("step");
    }
    return true;
}


/*
typedef enum
{
      pInit     = 0,
      pRunning  = 1,
      pAborting  = 2,
      pAborted  = 3,
      pCompleted = 4
} PState;
*/
void Protocol::setState(PState aState) {
    std::lock_guard<std::recursive_mutex> guard(mutex);


    switch (state_) {
        case pInit: {
            switch (aState) {
                case pInit: {
                    throw AMStateException();
                }
                case pRunning: {
                    state_ = aState;
                }
                    break;
                case pAborting: {
                    throw AMStateException();
                }
                case pAborted: {
                    throw AMStateException();
                }

                case pCompleted: {
                    throw AMStateException();
                }
            }
        }
            break;
        case pRunning: {
            switch (aState) {
                case pInit: {
                    throw AMStateException();
                }

                case pRunning: {
                    throw AMStateException();
                }
                case pAborting: {
                    state_ = aState;
                }
                    break;
                case pAborted: {
                    state_ = aState;
                }
                    break;
                case pCompleted: {
                    state_ = aState;
                }
                    break;
            }
        }
            break;
        case pAborting: {
            switch (aState) {
                case pInit: {
                    throw AMStateException();
                }
                case pRunning: {
                    throw AMStateException();
                }

                case pAborting: {
                    throw AMStateException();
                }
                case pAborted: {
                    state_ = aState;
                }
                    break;
                case pCompleted: {
                    throw AMStateException();
                }
            }
        }
            break;
        case pAborted: {
            switch (aState) {
                case pInit: {
                    throw AMStateException();
                }
                case pRunning: {
                    state_ = aState;
                }
                    break;
                case pAborting: {
                    throw AMStateException();
                }
                case pAborted: {
                    throw AMStateException();
                }
                case pCompleted: {
                    throw AMStateException();
                }
            }
        }
            break;
        case pCompleted: {
            throw AMStateException();
        }
    }

    addStateMsg();
}

void Protocol::addStateMsg() {
    std::stringstream st;
    st << "<protocolState value=\"";
    st << getStateStr();
    st << "\"/>";
    std::string aMsg = st.str();

    AMProtocolMsg *pm = new AMProtocolMsg(1, aMsg);
    addMsg(pm);
}

void Protocol::run() {

    PamStation *pPamStation = PamStationFactory::getCurrentPamStation();
    PropLeds *pLeds = pPamStation->getLeds();

    try {
        pLeds->SetRedLed(true);
        pLeds->SetGreenLed(false);
        basicRun();
        pLeds->SetRedLed(false);
        pLeds->SetGreenLed(true);
    }
    catch (...) {
        pLeds->SetRedLed(false);
        pLeds->SetGreenLed(true);
        throw;
    }
}


void Protocol::basicRun() {
    try {
        setState(pRunning);

        // make sure the camera is initialized
        PamStation *pPam = PamStationFactory::getCurrentPamStation();
        auto *ccd = dynamic_cast<PropCCD *>(pPam->getCCD());
        ccd->setEnabled(true);

        while ((currentStepIndex < steps_->size()) && (!isAborting())) {
            std::stringstream st;
            st << "<run currentStepIndex=\"";
            st << currentStepIndex;
            st << "\"/>";
            std::string aMsg = st.str();

            AMProtocolMsg *pm = new AMProtocolMsg(1, aMsg);
            addMsg(pm);

            (*steps_)[currentStepIndex]->run();

            if (!isAborting()) currentStepIndex++;
        }
        setState(pCompleted);    //if aborting this call will generate an AMStateException
    }
    catch (AMStateException &e) {
        setState(pAborted);
    }
    catch (PamStationException &e) {
        setState(pAborted);

        std::stringstream st;
        st << "<error description=\"";
        st << e.getDescription().c_str();
        st << "\" id=\"";
        st << e.getId();
        st << "\"/>";
        std::string aMsg = st.str();

        AMProtocolMsg *pm = new AMProtocolMsg(1, aMsg);
        addMsg(pm);
    }
//    catch (Exception &e) {
//        setState(pAborted);
//
//        std::stringstream st;
//        st << "<error description=\"";
//        st << e.Message.c_str();
//        st << "\" id=\"";
//        st << -1;
//        st << "\"/>";
//        std::string aMsg = st.str();
//
//        AMProtocolMsg *pm = new AMProtocolMsg(1, aMsg);
//        addMsg(pm);
//    }
    catch (...) {
        setState(pAborted);

        std::stringstream st;
        st << "<error description=\"";
        st << "unknown error -- ";
        st << boost::current_exception_diagnostic_information();
        st << "\" id=\"";
        st << -2;
        st << "\"/>";
        std::string aMsg = st.str();

        AMProtocolMsg *pm = new AMProtocolMsg(1, aMsg);
        addMsg(pm);
    }

    try {
        PamStation *pPam = PamStationFactory::getCurrentPamStation();
        auto *ccd = dynamic_cast<PropCCD *>(pPam->getCCD());
        ccd->setEnabled(false);
    }
    catch (PamStationException &e) {
        std::stringstream st;
        st << "<error description=\"";
        st << e.getDescription().c_str();
        st << "\" id=\"";
        st << e.getId();
        st << "\"/>";
        std::string aMsg = st.str();

        AMProtocolMsg *pm = new AMProtocolMsg(1, aMsg);
        addMsg(pm);
    }
}

void Protocol::abort() {
    setState(pAborting);
    PamStationFactory::getCurrentPamStation()->abort();
}

void Protocol::addMsg(AMProtocolMsg *msg) {
    logger->logInfo(msg->msg);
    msgQueue_->add(msg);
}

void Protocol::getProtocolMsgs(std::string *out) {
    msgQueue_->getMsgs(out);
}

void Protocol::getProtocolMsgs(int i, std::string *out) {
    msgQueue_->getMsgs(i, out);
}

std::string Protocol::id() {
    return id_;
}

std::string Protocol::path() {
    return path_;
}

int Protocol::imageCount() {
    return imageCount_;
}

std::string Protocol::barcode1() {
    return barcode1_;
}

std::string Protocol::barcode2() {
    return barcode2_;
}

std::string Protocol::barcode3() {
    return barcode3_;
}

unsigned Protocol::wellBitSet() {
    return wellBitSet_;
}

unsigned Protocol::cycle() {
    return cycle_;
}

bool Protocol::isRunning() {
    return state_ == pRunning;
}

bool Protocol::isAbort() {
    return state_ == pAborted;
}

bool Protocol::isAborting() {
    return state_ == pAborting;
}

bool Protocol::isCompleted() {
    return state_ == pCompleted;
}

void Protocol::incrementImageCount() {
    imageCount_++;
}

void Protocol::incrementCycle() {
    cycle_++;
    std::stringstream st;
    st << "<incrementCycle cycle=\"";
    st << cycle_;
    st << "\"/>";
    std::string aMsg = st.str();
    AMProtocolMsg *pm = new AMProtocolMsg(1, aMsg);
    addMsg(pm);
}

void Protocol::checkBrokenMembranes() {
    checkBrokenMembranes(wellBitSet_);
}

void Protocol::generateBrokenMembranesTest() {
    wellBitSet_ = wellBitSet_ & (~3);
    std::stringstream st;
    st << "<brokenMembrane wellBitSet=\"";
    st << 3;
    st << "\"/>";
    std::string aMsg = st.str();
    AMProtocolMsg *pm = new AMProtocolMsg(1, aMsg);
    addMsg(pm);

    wellBitSet_ = wellBitSet_ & (~4);
    std::stringstream st2;
    st2 << "<brokenMembrane wellBitSet=\"";
    st2 << 4;
    st2 << "\"/>";
    std::string aMsg2 = st2.str();
    AMProtocolMsg *pm2 = new AMProtocolMsg(1, aMsg2);
    addMsg(pm2);

    wellBitSet_ = wellBitSet_ & (~16);
    std::stringstream st3;
    st3 << "<brokenMembrane wellBitSet=\"";
    st3 << 16;
    st3 << "\"/>";
    std::string aMsg3 = st3.str();
    AMProtocolMsg *pm3 = new AMProtocolMsg(1, aMsg3);
    addMsg(pm3);

    wellBitSet_ = wellBitSet_ & (~256);
    std::stringstream st4;
    st4 << "<brokenMembrane wellBitSet=\"";
    st4 << 256;
    st4 << "\"/>";
    std::string aMsg4 = st4.str();
    AMProtocolMsg *pm4 = new AMProtocolMsg(1, aMsg4);
    addMsg(pm4);
}


unsigned Protocol::checkBrokenMembranes(unsigned aWellBitSet) {
    PamStation *pPamStation = PamStationFactory::getCurrentPamStation();
    Disposable *pDisposable = pPamStation->getDisposable();
    unsigned brokenWellBitSet = pDisposable->getBrokenMembranes(aWellBitSet);
    if (brokenWellBitSet) {
        wellBitSet_ = wellBitSet_ & (~brokenWellBitSet);
        std::stringstream st;
        st << "<brokenMembrane wellBitSet=\"";
        st << brokenWellBitSet;
        st << "\"/>";
        std::string aMsg = st.str();
        AMProtocolMsg *pm = new AMProtocolMsg(1, aMsg);
        addMsg(pm);
    }
    return brokenWellBitSet;
}








