
#ifndef AMPrimeStepH
#define AMPrimeStepH

#include "Protocol.h"
#include "AMStep.h"

#include <string>
#include "tinyxml2.h"


class Protocol;

class AMPrimeStep : virtual public AMStep
{
public:

    AMPrimeStep(Protocol * aP);
    ~AMPrimeStep();
    virtual std::string name();
    virtual bool initializeFromXMLElement(tinyxml2::XMLElement* aStepElement);
    virtual void appendXML(std::stringstream * st);
    virtual void run();


private:
    Protocol * protocol;
    std::string name_;
    unsigned headBitSet   ;
    unsigned currentHeadBitSet   ;
};

#endif //AMPrimeStepH



