//---------------------------------------------------------------------------
#pragma hdrstop

#include "AMManualDispenseStep.h"                      //for header-file

#include "modules/pimcore/PamStationFactory.h"   //for obtaining & using PamStation
#include "modules/pimcore/PamStation.h"          //for obtaining & using PamStation
#include "modules/pimcore/Disposable.h"          //for obtaining & using object
#include "modules/pimcore/WagonXYPositioner.h"   //for obtaining & using object

//#include "protocols/EjectSubStep.h"              //for using ProtocolStep
#include "modules/pimcore/WagonHeater.h"  

#include "HomeTransporterSubStep.h"    //for use of this sub-step
#include "tinyxml2.h"

#pragma package(smart_init)


//---------------------------------------------------------------------------

AMManualDispenseStep::AMManualDispenseStep(Protocol * aP)
{
    protocol = aP;
    name_ = "manualDispenseStep";
}
AMManualDispenseStep::~AMManualDispenseStep() {}

std::string AMManualDispenseStep::name()
{
    return name_;
}

void AMManualDispenseStep::appendXML(std::stringstream * st)
{
    *st << "<step";

    *st << " name=\"";
    *st <<  name_;
    *st << "\"";

    *st << "/>";
}

bool AMManualDispenseStep::initializeFromXMLElement(tinyxml2::XMLElement* aStepElement)
{

    return true;
}

void AMManualDispenseStep::run()
{
    PamStation       * pPamStation        = PamStationFactory::getCurrentPamStation();
    Disposable       * pDisposable        = pPamStation->getDisposable();
    WagonXYPositioner* pWagonXYPositioner = pPamStation->getWagonXYPositioner();

    //EjectSubStep aEjectSubStep = EjectSubStep();

    try
    {
        //move incubation chamber to incubate position
       // pWagonXYPositioner->goIncubationPosition();
       // pWagonXYPositioner->waitReady();

        //eject.run() =
        //1 on button-press: goFrontPanelPosition, then goLoad()
        //2 allow user to manual dispense
        //2 on button press: goFrontPanelPosition() again
        //aEjectSubStep.run();
        runEject();
        pDisposable->checkDisposablesLoaded(protocol->wellBitSet()); //throws exception "DISPOSABLE_NOT_LOADED"

        pWagonXYPositioner->goIncubationPosition();
        pWagonXYPositioner->waitReady();
        //pump manually dispense fluid down in the selected wells
        pDisposable->pumpDown(protocol->wellBitSet()); //extra waitTime = 0 ms;
//        pDisposable->waitPumpReady();
//        pDisposable->pumpDownAsync(protocol->wellBitSet()).get();

        protocol->checkBrokenMembranes(  );

        pWagonXYPositioner->setEnabled(false);
    }

    catch (...)
    {
        pWagonXYPositioner->setEnabled(false);
        throw ;
    }

}


void AMManualDispenseStep::runEject()
{
    PamStation            * pPamStation             = PamStationFactory::getCurrentPamStation();
    WagonHeater           * pWagonHeater            = pPamStation->getWagonHeater();
    WagonXYPositioner     * pWagonXYPositioner      = pPamStation->getWagonXYPositioner();

    HomeTransporterSubStep  aHomeTransporterSubStep = HomeTransporterSubStep();

    //home the wagonPositioner first, use sub step "HomeTransporterSubStep"
    //button will be enabled, and is blocking untill pressed
    aHomeTransporterSubStep.run();

    //WagonPositioner is now homed & in FrontPanelPosition

    //check if unloadtemperature safe
    pWagonHeater->checkUnloadTemperatureSafe();

    //goto (un)load position
    pWagonXYPositioner->goLoadPosition();
    pWagonXYPositioner->waitReady();

    //wagon is now in (un)load position

   /* if (coverNotClosedException)
    {
        coverNotClosedException = false;
        SysExceptionGenerator * excptionGenerator = dynamic_cast<SysExceptionGenerator*>( pPamStation->getExceptionGenerator());
        excptionGenerator->onException(this, WAGON_COVER_NOT_CLOSED, eErrorSafety, "WAGON_COVER_NOT_CLOSED");
    }
    */
    //button will be enabled, and is blocking untill pressed
    //in other words: the wagon will go to home (inside the instrument) on pressing the button
    aHomeTransporterSubStep.run();  //run again, now from load position

    //the wagon is back in FrontPanelPosition
}

