# 

```shell

PSSERVER_IP=10.110.253.57
ssh root@10.110.253.57

apt-get update
```

## Install Pylon5

https://www.baslerweb.com/en/sales-support/downloads/software-downloads/#type=pylonsoftware;language=all;version=5.2.0;os=linuxarm
pylon 5.2.0 Camera Software Suite Linux ARM 64 bit hardfloat

```bash

scp ./docker/pylon-5.2.0.13457-arm64.tar.gz root@10.110.253.57:~/pylon-5.2.0.13457-arm64.tar.gz

ssh root@10.110.253.57

tar xvzf pylon-5.2.0.13457-arm64.tar.gz
cd pylon-5.2.0.13457-arm64
mkdir /opt
tar -C /opt -xzf pylonSDK*.tar.gz  
cd ..  
rm pylon-5.2.0.13457-arm64.tar.gz
cd pylon-5.2.0.13457-arm64
chmod +x setup-usb.sh
./setup-usb.sh
cd ..
#rm -r pylon-5.2.0.13457-arm64
exit

# see : /home/alex/dev/pamgene/pamservice/lib/src/pamservice/commands/setup.dart
    

scp -r ./setup root@10.110.253.57:~/
ssh root@10.110.253.57
chmod +x /home/root/setup/install.sh

cd ./setup
cp config/pamstation_error.json ~/pamstation_error.json
mkdir /var/lib/psserver

# scp _build/aarch64-unknown-linux/psserver root@10.110.253.57:/var/lib/psserver/psserver

set +e
systemctl stop psserver
set -e

cp ./psserver/psserver /var/lib/psserver/psserver
cp -ar ./psserver/web /var/lib/psserver
# need this folder for camera config
mkdir -p /var/lib/psserver/config

chmod +x /var/lib/psserver/psserver

cd /var/lib/psserver
#export PYLON_CAMEMU=1
./psserver

#cp systemd/psserver.service /etc/systemd/system/psserver.service
#
#systemctl daemon-reload
#
#systemctl enable psserver
#systemctl start psserver


```

## Install boost

```bash
opkg update
opkg install boost

```


# old doc

# Toradex apalis-tk1

## Install Pylon5

https://www.baslerweb.com/en/sales-support/downloads/software-downloads/#type=pylonsoftware;language=all;version=5.2.0;os=linuxarm
pylon 5.2.0 Camera Software Suite Linux ARM 32 bit hardfloat
  
```bash
cd psserver/docker
scp ./docker/pylon-5.2.0.13457-armhf.tar.gz root@10.110.253.47:~/pylon-5.2.0.13457-armhf.tar.gz

ssh root@10.110.253.49

tar xvzf pylon-5.2.0.13457-armhf.tar.gz 
cd pylon-5.2.0.13457-armhf  
mkdir /opt
tar -C /opt -xzf pylonSDK*.tar.gz  
cd ..  
rm pylon-5.2.0.13457-armhf.tar.gz  
cd pylon-5.2.0.13457-armhf
chmod +x setup-usb.sh
./setup-usb.sh
cd ..
rm -r pylon-5.2.0.13457-armhf
    
```

## Install boost

```bash
opkg update
opkg install boost

```
## Configure socketCAN
 
```bash 
ip link show
ip link set can1 type can bitrate 1000000
ip link set up can1
ip addr ls dev can1
ifconfig can1

```
 
# Compile

see docker/README.md

# Deploy

see docker/README.md

```bash

# Unit035 - dev board
IP=10.110.253.49
# Unit021
IP=10.110.253.47

ssh root@${IP} systemctl stop psserver
scp _build/arm/psserver root@${IP}:/var/lib/psserver/psserver
scp config/pamstation_error.json root@${IP}:/home/root/pamstation_error.json
scp -r web root@10.110.253.49:/var/lib/psserver/web 

ssh root@${IP} systemctl start psserver

```

```bash
# Unit021
IP=10.110.253.114
# Unit034
IP=10.110.253.102

scp _build/arm/psserver pamstation@${IP}:C:/Users/pamstation/Desktop/psserver

scp _build/arm/psserver pamstation@${IP}:C:/Users/pamstation/Desktop/psserver

```