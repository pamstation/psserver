//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include <vector>
#include <string>

#include "AMStep.h"


#include "ProtocolTest.h"
#include "Protocol.h"
#include "ProtocolThread.h"

#pragma package(smart_init)


//---------------------------------------------------------------------------
ProtocolTest::ProtocolTest()
{
    failedTestName = "none";
}

ProtocolTest::~ProtocolTest()
{
}

bool ProtocolTest::run()
{
    if (!test1())
    {
        failedTestName = "test1";
        return false;
    }
    if (!test2())
    {
        failedTestName = "test2";
        return false;
    }
    return true;
}

bool ProtocolTest::test1()
{
    const char* xml = "<?xml version=\"1.0\"?>"
                      "<protocol barcode1=\"aaa\" wellBitSet=\"2\">"
                      "</protocol>";
    Protocol * p = new Protocol();
    if (!p->initializeFromXML(xml))
    {
        return false;
    }
    std::string bTest = "aaa";
    if (   bTest.compare(p->barcode1()) != 0 )
    {
        return false;
    }

    if (    p->wellBitSet()  != 2 )
    {
        return false;
    }

    return true;
}

bool ProtocolTest::test2()
{
    const char* xml = "<?xml version=\"1.0\"?>"
                      "<protocol barcode1=\"aaa\" wellBitSet=\"2\">"
                      "       <steps>"
                      "               <step name=\"loadStep\"/>"
                      "               <step name=\"unLoadStep\"/>"
                      "       </steps>"
                      "</protocol>";
    Protocol * p = new Protocol();
    if (!p->initializeFromXML(xml))
    {
        return false;
    }
    std::string bTest = "aaa";
    if (bTest.compare(p->barcode1()) != 0 ) return false;

    if (p->wellBitSet() != 2 ) return false;

    std::vector<AMStep*> * aSteps = p->steps();
    if (aSteps->size() != 2) return false;

    AMStep * aStep = (*aSteps)[0];
    if (aStep->name().compare("loadStep") != 0) return false;
    aStep = (*aSteps)[1];
    if (aStep->name().compare("unLoadStep") != 0) return false;


    ProtocolThread * pt = new ProtocolThread();
    pt->protocol = p;
    pt->Resume();

    //if (!p->isCompleted()) return false;

    return true;
}
