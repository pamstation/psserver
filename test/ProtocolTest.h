#ifndef ProtocolTestH
#define ProtocolTestH

#include <string>

using namespace std;

class ProtocolTest
{
public:

    ProtocolTest();
    ~ProtocolTest();
    virtual bool run();
    virtual bool test1();
    virtual bool test2();

    string failedTestName;



};



#endif //ProtocolTestH

